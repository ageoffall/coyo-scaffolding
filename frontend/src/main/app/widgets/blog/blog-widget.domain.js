(function (angular) {
  'use strict';

  angular.module('coyo.widgets.blog')
      .factory('BlogWidgetModel', BlogWidgetModel);

  /**
   * @ngdoc service
   * @name coyo.widgets.blog.BlogWidgetModel
   *
   * @description
   * Domain model representation for the latest blog articles shown in the blog widget
   *
   * @requires restResourceFactory
   * @requires CoyoEndpoints
   */
  function BlogWidgetModel(restResourceFactory) {
    var BlogWidget = restResourceFactory({
      url: '/web/widgets/blog',
      httpConfig: {
        autoHandleErrors: false
      }
    });

    angular.extend(BlogWidget, {

      /**
       * @ngdoc function
       * @name coyo.widgets.blog.BlogWidgetModel#getLatest
       * @methodOf coyo.widgets.blog.BlogWidgetModel
       *
       * @description
       * Returns the list of of latest blog articles.
       *
       * @param {string} sourceSelection Can be one of ALL, SUBSCRIBED or SELECTED
       * @param {int} count The (max) number of articles to retrieve
       * @param {boolean} showTeaserImage Show teaser image if true, else false
       * @param {string=} appIds ID's of the blog app to limit the articles to. Used if sourceSelection is set to SELECTED.
       *
       * @returns {promise} An $http promise resolving to the list of articles.
       */
      getLatest: function (sourceSelection, count, showTeaserImage, appIds) {
        return BlogWidget.$post(this.$url('latest'),
            {sourceSelection: sourceSelection, count: count, appIds: appIds});
      }
    });

    return BlogWidget;
  }

})(angular);
