(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.blog')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "WIDGETS.BLOG.DESCRIPTION": "Zeigt eine Liste der neuesten Blogbeiträge an, entweder global oder für eine ausgewählte Blog-App.",
          "WIDGETS.BLOG.NAME": "Neueste Blogbeiträge",
          "WIDGETS.BLOG.NO_ARTICLES": "Kein Beitrag gefunden",
          "WIDGETS.BLOG.NO_BLOG_APPS_FOUND": "Keine Blog-Apps gefunden.",
          "WIDGETS.BLOG.SETTINGS.ARTICLE_COUNT.HELP": "Die maximale Anzahl von Blogbeiträgen, die in diesem Widget angezeigt werden sollen.",
          "WIDGETS.BLOG.SETTINGS.ARTICLE_COUNT.LABEL": "Anzahl von Beiträgen",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.AT": "über",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.HELP": "Wähle die Blog-Apps aus, aus der die neuesten Beiträge angezeigt werden sollen.",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.IN": "in",
          "WIDGETS.BLOG.SETTINGS.BLOG_APP.LABEL": "Blog-Apps",
          "WIDGETS.BLOG.SETTINGS.BLOG_USE_ALL_BLOGS.LABEL": "Alle",
          "WIDGETS.BLOG.SETTINGS.BLOG_USE_SELECTED_BLOGS.LABEL": "Ausgewählte",
          "WIDGETS.BLOG.SETTINGS.BLOG_USE_SUBSCRIBED_BLOGS.LABEL": "Nur abonnierte",
          "WIDGETS.BLOG.SETTINGS.BLOG_SHOW_TEASER_IMAGE.HELP": "Zeigt die Blog Vorschaubilder im Widget an.",
          "WIDGETS.BLOG.SETTINGS.BLOG_SHOW_TEASER_IMAGE.LABEL": "Vorschaubilder anzeigen",
          "WIDGETS.BLOG.SETTINGS.BLOG_SOURCES.HELP": "Die Blogs der ausgewählten Einstellung werden im Widget angezeigt.",
          "WIDGETS.BLOG.SETTINGS.BLOG_SOURCES.LABEL": "Blogs"
        });
        /* eslint-enable quotes */
      });
})(angular);
