(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.birthday')
      .controller('BirthdayWidgetSettingsController', BirthdayWidgetSettingsController);

  function BirthdayWidgetSettingsController($scope, birthdayConfig) {
    var vm = this;
    vm.$onInit = onInit;

    function onInit() {
      vm.model = $scope.model;

      if (!vm.model.settings._daysBeforeBirthday && !vm.model.settings._displayAge && !vm.model.settings._birthdayNumber
          && !vm.model.settings._fetchBirthdays) {
        $scope.model.settings._daysBeforeBirthday = 10;
        $scope.model.settings._displayAge = true;
        $scope.model.settings._birthdayNumber = birthdayConfig.list.paging.pageSize;
        $scope.model.settings._fetchBirthdays = 'ALL';
      }
    }
  }

})(angular);
