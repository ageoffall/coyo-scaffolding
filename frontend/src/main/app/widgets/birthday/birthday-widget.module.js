(function (angular) {
  'use strict';

  angular
  /**
   * @ngdoc overview
   * @name coyo.widgets.birthday
   *
   * @description
   * # Birthday Widget #
   * Shows upcoming birthday within a widget.
   */
      .module('coyo.widgets.birthday', [
        'coyo.widgets.api',
        'commons.i18n'
      ])
      .config(registerBirthdayWidget)
      .constant('birthdayConfig', {
        list: {
          paging: {
            pageSize: 5
          }
        }
      });

  function registerBirthdayWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      key: 'birthday',
      name: 'WIDGET.BIRTHDAY.NAME',
      description: 'WIDGET.BIRTHDAY.DESCRIPTION',
      titles: ['WIDGET.BIRTHDAY.NAME'],
      icon: 'zmdi-cake',
      categories: 'dynamic',
      directive: 'coyo-birthday-widget',
      settings: {
        controller: 'BirthdayWidgetSettingsController',
        controllerAs: '$ctrl',
        templateUrl: 'app/widgets/birthday/birthday-widget-settings.html'
      },
      renderOptions: {
        skeleton: {
          name: 'skeleton-widget-birthday'
        }
      },
      whitelistExternal: false
    });
  }

})(angular);
