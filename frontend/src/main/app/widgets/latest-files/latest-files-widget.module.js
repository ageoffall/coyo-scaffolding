(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.latestfiles', [
        'coyo.widgets.api',
        'commons.i18n'
      ])
      .config(registerLatestFilesWidget);

  function registerLatestFilesWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      key: 'latestfiles',
      name: 'WIDGETS.LATESTFILES.NAME',
      description: 'WIDGETS.LATESTFILES.DESCRIPTION',
      icon: 'zmdi-coyo zmdi-coyo-image',
      categories: 'dynamic',
      directive: 'coyo-latest-files-widget',
      titles: ['WIDGETS.LATESTFILES.NAME'],
      settings: {
        controller: 'LatestFilesWidgetSettingsController',
        controllerAs: '$ctrl',
        templateUrl: 'app/widgets/latest-files/latest-files-widget-settings.html'
      },
      renderOptions: {
        skeleton: {
          name: 'skeleton-widget-latest-files'
        }
      },
      whitelistExternal: true
    });
  }

})(angular);
