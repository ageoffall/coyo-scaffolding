(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.latestfiles')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGETS.LATESTFILES.DESCRIPTION": "Displays the latest file changes of a documents app.",
          "WIDGETS.LATESTFILES.NAME": "Latest files",
          "WIDGETS.LATESTFILES.NO_FILES": "No files to display",
          "WIDGETS.LATESTFILES.UPDATED": "Updated",
          "WIDGETS.LATESTFILES.SETTINGS.DOCUMENTAPP.HELP": "The widget will show the latest file changes of the choosen documents app.",
          "WIDGETS.LATESTFILES.SETTINGS.DOCUMENTAPP.LABEL": "Documents app",
          "WIDGETS.LATESTFILES.SETTINGS.FILE_COUNT.LABEL": "Number of files",
          "WIDGETS.LATESTFILES.SETTINGS.FILE_COUNT.HELP": "The amount of files to show. Must be between 1 and 20.",
          "WIDGETS.LATESTFILES.SETTINGS.NO_DOCUMENT_APPS_FOUND": "No document apps found"
        });
        /* eslint-enable quotes */
      });
})(angular);
