(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.upcomingEvents')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGETS.UPCOMING.EVENTS.NAME": "Upcoming Events",
          "WIDGETS.UPCOMING.EVENTS.DESCRIPTION": "Displays upcoming events",
          "WIDGETS.UPCOMING.EVENTS.NO.UPCOMING.EVENTS": "There are no upcoming events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.DISPLAY.ONGOING": "Display ongoing events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENT_NUMBER.LABEL": "Number of displayed events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS.DISPLAY.LABEL": "Display events",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS_ALL": "All",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.HELP.EVENTS_ALL": "All events that the user can access.",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS_SELECTED": "Selected",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.HELP.EVENTS_SELECTED": "All events of a selected page or workspace.",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.HELP.EVENTS_SUBSCRIBED": "All events that the user is subscribed to.",
          "WIDGETS.UPCOMING.EVENTS.SETTINGS.UPCOMING.EVENTS_SUBSCRIBED": "All subscribed"
        });
        /* eslint-enable quotes */
      });
})(angular);
