(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.interestingcolleagues')
      .component('coyoInterestingColleaguesWidget', interestingColleaguesWidget())
      .controller('InterestingColleaguesWidgetController', InterestingColleaguesWidgetController);

  /**
   * @ngdoc directive
   * @name coyo.widgets.interesting.coyoInterestingColleaguesWidget:coyoInterestingColleaguesWidget
   * @element ANY
   * @restrict E
   * @scope
   *
   * @description
   * Renders a widget that shows colleagues you might know
   *
   * @param {object} widget
   * The widget configuration
   */
  function interestingColleaguesWidget() {
    return {
      templateUrl: 'app/widgets/interesting-colleagues/interesting-colleagues-widget.html',
      bindings: {
        widget: '=',
        showWidget: '=',
        editMode: '<'
      },
      controller: 'InterestingColleaguesWidgetController'
    };
  }

  function InterestingColleaguesWidgetController($scope, widgetStatusService, interestingColleaguesService) {
    var vm = this;

    vm.loadData = loadData;

    widgetStatusService.refreshOnSettingsChange($scope);

    function loadData() {
      return interestingColleaguesService.getColleagues().then(function (result) {
        vm.data = result.data.content;
        vm.showWidget.show = result.data.totalElements > 0;
        return result;
      });
    }
  }

})(angular);
