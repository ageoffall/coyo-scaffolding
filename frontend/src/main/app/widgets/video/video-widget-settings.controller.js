(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.video')
      .controller('VideoWidgetSettingsController', VideoWidgetSettingsController);

  function VideoWidgetSettingsController($scope, $http, $q, $timeout, coyoEndpoints, VideoInfoModel) {
    var vm = this;

    vm.model = $scope.model;
    vm.safeUrl = true;

    vm.checkUrl = checkUrl;

    function checkUrl() {
      // use s.substring() because s.startsWith() is not supported by older IE versions
      vm.safeUrl = !vm.model.settings._url || vm.model.settings._url.substring(0, 8) === 'https://';
    }

    function onBeforeSave() {
      vm.model._loading = true;
      return VideoInfoModel.generateVideoInfo(vm.model.settings._url).then(function (response) {
        var data = response.shift();
        var videoUrl = data.videoUrl;
        var ratio = (data.width && data.height) ? (data.height / data.width * 100) : 56.25;
        if (videoUrl && ratio) {
          vm.model.settings._backendData = {
            videoUrl: videoUrl,
            ratio: ratio
          };
        } else {
          // do not cache invalid data
          delete vm.model.settings._backendData;
        }
      }).catch(function () {
        // do not cache invalid data
        delete vm.model.settings._backendData;
        return $q.resolve();
      }).finally(function () {
        $timeout(function () {
          vm.model._loading = false;
        });
      });
    }

    (function _init() {
      $scope.saveCallbacks.onBeforeSave = onBeforeSave;
      checkUrl();
    })();
  }

})(angular);
