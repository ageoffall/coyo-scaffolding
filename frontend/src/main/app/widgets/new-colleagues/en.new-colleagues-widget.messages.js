(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.newcolleagues')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "WIDGETS.NEWCOLLEAGUES.DESCRIPTION": "Displays a list of your newest colleagues.",
          "WIDGETS.NEWCOLLEAGUES.NAME": "New colleagues"
        });
        /* eslint-enable quotes */
      });
})(angular);
