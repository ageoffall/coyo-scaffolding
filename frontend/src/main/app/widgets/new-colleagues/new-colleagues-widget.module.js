(function (angular) {
  'use strict';

  angular
      .module('coyo.widgets.newcolleagues', [
        'coyo.widgets.api',
        'commons.resource',
        'commons.target',
        'commons.i18n'
      ])
      .config(registerWidget);

  function registerWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      name: 'WIDGETS.NEWCOLLEAGUES.NAME',
      key: 'newcolleagues',
      icon: 'zmdi-accounts-add',
      categories: 'dynamic',
      directive: 'coyo-new-colleagues-widget',
      description: 'WIDGETS.NEWCOLLEAGUES.DESCRIPTION',
      titles: ['WIDGETS.NEWCOLLEAGUES.NAME'],
      whitelistExternal: false
    });
  }

})(angular);
