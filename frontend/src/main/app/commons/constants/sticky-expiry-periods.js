(function (angular) {
  'use strict';

  angular
      .module('commons.constants', ['coyo.base'])
      .constant('stickyExpiryPeriods', {
        none: {expiry: null, label: 'MODULE.TIMELINE.STICKY.EXPIRY.NONE', icon: 'zmdi-timer-off'},
        oneDay: {expiry: 1000 * 60 * 60 * 24, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_DAY', icon: 'zmdi-timer'},
        oneWeek: {expiry: 1000 * 60 * 60 * 24 * 7, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_WEEK', icon: 'zmdi-timer'},
        oneMonth: {expiry: 1000 * 60 * 60 * 24 * 30, label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_MONTH', icon: 'zmdi-timer'}
      });
})(angular);
