(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .component('oyocLicenseWarningBar', licenseWarningBar())
      .controller('LicenseWarningBarController', LicenseWarningBarController);

  /**
   * @ngdoc directive
   * @name commons.ui.oyocLicenseWarningBar:oyocLicenseWarningBar
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Renders the warning bar when license is invalid.
   *
   * @requires $rootScope
   * @requires $scope
   * @requires $localStorage
   * @requires coyo.domain.LicenseModel
   */
  function licenseWarningBar() {
    return {
      templateUrl: 'app/commons/ui/components/license-warning/license-warning.html',
      bindings: {
        area: '<'
      },
      controller: 'LicenseWarningBarController'
    };
  }

  function LicenseWarningBarController($rootScope, $scope, $localStorage, LicenseModel, authService) {
    var vm = this;

    vm.$onInit = _init;
    vm.invalid = false;
    vm.classes = {};

    function _updateLicense(license) {
      vm.invalid = !!license.status;
      vm.target = 'admin.license.' + (license.environment === 'enterprise' ? 'license' : 'subscription');
    }

    function _checkLicense() {
      $scope.$watch(function () {
        return _.get($localStorage, 'messagingSidebar.compact', true);
      }, function (newVal) {
        vm.classes.compact = newVal;
      });

      LicenseModel.checkLicense().then(function (valid) {
        vm.status = 'LICENSE.INVALID.WARNING';
        vm.invalid = !valid;
      });
    }

    function _init() {
      if (vm.area === 'admin') {
        authService.getUser().then(function (user) {
          if (user.hasGlobalPermissions('MANAGE_LICENSE')) {
            vm.classes.admin = true;
            vm.status = 'ADMIN.LICENSE.INVALID.WARNING';
            LicenseModel.load().then(_updateLicense);
            var unsubscribe = $rootScope.$on('license:loaded', function (event, license) {
              _updateLicense(license);
            });
            $scope.$on('$destroy', unsubscribe);
          } else {
            _checkLicense();
          }
        });
      } else {
        _checkLicense();
      }
    }
  }

})(angular);
