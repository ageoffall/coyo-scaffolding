(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .filter('stripHtml', stripHtml);

  /**
   * @ngdoc filter
   * @name commons.ui:stripHtml
   * @function
   *
   * @description
   * stripHtml strips html
   */
  function stripHtml($document) {
    return function (text) {
      if (!angular.isString(text)) {
        return text;
      }
      var elem = $document[0].createElement('div');
      var newText = text.replace(/<p>/g, ' ');
      newText = newText.replace(/<\/p>/g, '');
      elem.innerHTML = newText.trim();
      var strippedText = elem.textContent;
      elem.remove();
      return strippedText;
    };
  }
})(angular);
