(function () {
  'use strict';

  angular
      .module('commons.ui')
      .component('coyoBtnAttachments', attachmentButton())
      .controller('attachmentButtonController', attachmentButtonController);

  /**
   * @ngdoc directive
   * @name commons.ui.coyoBtnAttachments:coyoBtnAttachments
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Renders an attachment button with an context menu. The context menu has two options:
   * Upload File and Select from file Library.
   *
   * @param {expression} onSelectAttachments
   * Callback when files are selected to upload
   *
   * @param {expression} onSelectFileLibrary
   * Callback when file library files are selected
   *
   * @param {boolean} active
   * Flag that indicates if the button should have active class
   *
   * @param {object} author
   * The current author in which context the file library should be opened
   *
   * @param {string} contextId
   * The context in which the button is created (e.g. WS or Page id)
   *
   * @param {string} timelineType
   * The type of the timeline the attachment are supposed to be added. If not 'personal' the fileLibrary is opened with
   * the contextId as starting point. Default is 'personal'.
   *
   */
  function attachmentButton() {
    return {
      templateUrl: 'app/commons/ui/components/btn-attachments/btn-attachments.html',
      bindings: {
        onSelectAttachments: '&',
        onSelectFileLibrary: '&',
        active: '<',
        author: '<',
        contextId: '@',
        timelineType: '<',
        focusVar: '=?'
      },
      controller: 'attachmentButtonController'
    };
  }

  function attachmentButtonController($element, $timeout, fileLibraryModalService, utilService, $injector) {
    var vm = this;
    vm.fileLibraryActivated = false;
    vm.googleApiActivated = false;
    vm.o365ApiActivated = false;
    vm.$onInit = onInit;
    vm.openCoyoFileLibrary = openCoyoFileLibrary;
    vm.openGSuitePicker = openGSuitePicker;
    vm.openSharePointOnlinePicker = openSharePointOnlinePicker;

    function openCoyoFileLibrary() {
      _resetFocusStates();
      fileLibraryModalService.open(vm.timelineType === 'personal' ? vm.author : {id: vm.contextId},
          {selectMode: 'multiple'})
          .then(function (selectedFiles) {
            var attachments = _.map(selectedFiles, function (elem) {
              return {
                fileId: elem.id,
                senderId: elem.senderId,
                displayName: elem.displayName
              };
            });
            vm.onSelectFileLibrary({files: attachments});
          })
          .finally(function () {
            _setFocusAfterFileSelection();
          });
    }

    function openGSuitePicker() {
      _resetFocusStates();
      vm.GDrivePickerService.open()
          .then(function (selectedFiles) {
            var attachments = _.map(selectedFiles, function (file) {
              return {
                uid: file.id,
                name: file.displayName,
                contentType: file.mimeType,
                storage: 'G_SUITE'
              };
            });
            vm.onSelectAttachments({files: attachments});
          })
          .finally(function () {
            _setFocusAfterFileSelection();
          });
    }

    function openSharePointOnlinePicker() {
      vm.SharePointFilepicker.openFilepicker();
    }

    function _resetFocusStates() {
      if (angular.isDefined(vm.focusVar)) {
        vm.focusVar = false;
      }
    }

    function _setFocusAfterFileSelection() {
      if (angular.isDefined(vm.focusVar)) {
        vm.focusVar = true;
      } else {
        _focusAttachmentLink();
      }
    }

    function _focusAttachmentLink() {
      $timeout(function () {
        $element.find('.attachment-link').focus();
      });
    }

    function onInit() {
      vm.GDrivePickerService = $injector.get('ngxGDrivePickerService');
      vm.GoogleApiService = $injector.get('ngxGoogleApiService');
      vm.O365ApiService = $injector.get('ngxO365ApiService');
      vm.SharePointFilepicker = $injector.get('ngxSharePointFilepickerService');
      vm.uuid = utilService.uuid();
      if (!vm.timelineType) {
        vm.timelineType = 'personal';
      }
      vm.GoogleApiService.isGoogleApiActive().subscribe(function (isActivated) {
        vm.googleApiActivated = isActivated;
      });

      vm.O365ApiService.isApiActive().subscribe(function (isActivated) {
        vm.o365ApiActivated = isActivated;
      });

      vm.fileLibraryActivated = vm.author.globalPermissions.includes('ACCESS_FILES');
    }
  }
})();
