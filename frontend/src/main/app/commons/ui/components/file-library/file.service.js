(function () {
  'use strict';

  angular.module('commons.ui')
      .factory('fileService', FileService);

  function FileService(FileModel, socketService) {

    return {
      subscribeToLock: subscribeToLock
    };

    function subscribeToLock(elem, callback) {
      var lock = '/topic/item.' + elem.id + '.lock';
      return socketService.subscribe(lock, function () {
        FileModel.get({
          id: elem.id,
          senderId: elem.senderId
        }).then(callback);
      });
    }
  }
})();
