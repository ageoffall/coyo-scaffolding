(function (angular) {
  'use strict';

  angular
      .module('commons.i18n.custom')
      .factory('coyoTranslationLoader', coyoTranslationLoader);

  /**
   * @ngdoc service
   * @name commons.i18n.custom.coyoTranslationLoader
   *
   * @description
   * This factory provides a custom loader for angular-translate.
   * The loader fetches all translation tables provided in the translation registry.
   * These tables are extended with the custom translations the administrator provided for the current user's language.
   * The final translation tables are the returned to angular-translate under the key of one of the default languages
   * (en or de) chosen (as fallback).
   *
   * @requires $q
   * @requires $localStorage
   * @requires $injector
   * @requires coyo.domain.LanguagesModel
   * @requires commons.i18n.custom.translationRegistry
   */
  function coyoTranslationLoader($q, $localStorage, $injector, LanguagesModel, translationRegistry) {
    var translationTables = translationRegistry.getTranslationTables();
    var cachedTranslations = {};

    function handleOverrides(overrides) {
      return _.chain(overrides).keyBy('key').mapValues('translation').value();
    }

    function isFullySupportedLanguage(lang) {
      return ['de', 'en'].indexOf(lang.toLowerCase()) >= 0;
    }

    function getDefaultLanguageOverrides(lang) {
      return LanguagesModel.getDefaultLanguage().then(function (defaultLanguageObject) {
        var defaultLanguage = defaultLanguageObject ? defaultLanguageObject.language.toLowerCase() : 'en';
        if (lang !== defaultLanguage) {
          return LanguagesModel.getTranslations(defaultLanguage).then(function (translations) {
            var defaultOverrides = handleOverrides(translations);
            var translationsFinished = angular.extend({}, translationTables[defaultLanguage], defaultOverrides);
            return translationsFinished;
          });
        } else {
          return translationTables[defaultLanguage];
        }
      });
    }

    return function (options) {
      var lang = options.key;

      if (_.has(cachedTranslations, lang)) {
        return cachedTranslations[lang];
      }

      var promises = [];

      promises.push(LanguagesModel.getTranslations(lang).then(function (overridesObject) {
        return $q.resolve(handleOverrides(overridesObject));
      }));

      if (!isFullySupportedLanguage(lang)) {
        promises.push(getDefaultLanguageOverrides(lang));
      }

      cachedTranslations[lang] = $q.all(promises).then(function (overrides) {
        var defaultOverrides = overrides.length === 2 ? overrides[1] : translationTables[lang];
        return $q.resolve(angular.extend({}, defaultOverrides, overrides[0]));
      }).catch(function (errorResponse) {
        var errorService = $injector.get('errorService');
        if (errorService) {
          errorService.suppressNotification(errorResponse);
        }
        cachedTranslations = _.omit(cachedTranslations, lang);
        return $q.resolve(translationTables.en);
      });

      return cachedTranslations[lang];
    };
  }

})(angular);
