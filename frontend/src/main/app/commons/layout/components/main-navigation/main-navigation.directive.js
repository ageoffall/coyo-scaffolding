(function (angular) {
  'use strict';

  angular
      .module('commons.layout')
      .directive('oyocMainNavigation', mainNavigation)
      .controller('MainNavigationController', MainNavigationController);

  /**
   * @ngdoc directive
   * @name commons.layout.oyocMainNavigation:oyocMainNavigation
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Displays the main navigation menu including the search bar.
   *
   * @requires $scope
   * @requires $rootScope
   * @requires $state
   * @requires commons.ui.sidebarService
   * @requires commons.auth.authService
   * @requires coyo.admin.adminStates
   * @requires commons.resource.backendUrlService
   * @requires commons.ui.userGuideService
   * @requires commons.tour.tourService
   * @requires commons.tour.tourSelectionModal
   * @requires commons.terms.termsService
   * @requires commons.ui.aboutCoyoService
   * @requires domain.SettingsModel
   */
  function mainNavigation() {
    return {
      restrict: 'E',
      scope: {},
      bindToController: {},
      templateUrl: 'app/commons/layout/components/main-navigation/main-navigation.html',
      replace: true,
      controller: 'MainNavigationController',
      controllerAs: '$ctrl'
    };
  }

  function MainNavigationController($scope, $rootScope, $injector, $state, sidebarService, authService,
                                    adminStates, backendUrlService, userGuideService, tourService, tourSelectionModal,
                                    termsService, aboutCoyoService, SettingsModel) {
    var vm = this;

    vm.searchVisible = false;
    vm.accessLandingPages = false;
    vm.fileLibraryActivated = false;
    vm.allAdminPermissions = _.map(adminStates, 'globalPermission').join(',');
    vm.backendUrl = backendUrlService.getUrl();

    vm.openMenu = openMenu;
    vm.openLaunchpad = openLaunchpad;
    vm.help = help;
    vm.about = about;
    vm.restartTour = restartTour;
    vm.logout = authService.logout;
    vm.showTerms = termsService.showModal;

    // ----------------------------------------------------------------

    function openMenu() {
      sidebarService.open('menu');
    }

    function openLaunchpad() {
      $injector.get('coyoLaunchpad').open();
    }

    function help() {
      if ($state.current && $state.current.data && $state.current.data.guide) {
        userGuideService.open($state.current.data.guide);
      } else {
        userGuideService.notFound();
      }
    }

    function about() {
      aboutCoyoService.open();
    }

    function restartTour() {
      tourSelectionModal.open(tourService.getTopics());
    }

    // ----------------------------------------------------------------

    function _checkTermsActive() {
      termsService.termsActive().then(function (active) {
        vm.termsActive = active;
      });
    }

    function _checkTourSteps() {
      vm.hasTourSteps = tourService.getTopics().length > 0;
    }

    (function _init() {
      authService.onGlobalPermissions('ACCESS_LANDING_PAGES', function (permission, user) {
        vm.user = user;
        vm.accessLandingPages = permission;
        _checkTermsActive();
      });

      authService.onGlobalPermissions('ACCESS_FILES', function (hasPermission) {
        vm.fileLibraryActivated = hasPermission;
      });

      SettingsModel.retrieve().then(function (settings) {
        vm.launchpadActive = settings.launchpadActive === 'true';
      });
      var unsubscribe = $rootScope.$on('tourService:stepsChanged', _checkTourSteps);
      $scope.$on('$destroy', unsubscribe);
      _checkTourSteps();
    })();
  }

})(angular);
