(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.list')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('de', {
          "APP.LIST.SEARCH": "Suche",
          "APP.LIST.ADD_ENTRY": "Neuer Eintrag",
          "APP.LIST.ADD_ENTRY_BUTTON": "Neuen Eintrag hinzufügen",
          "APP.LIST.ADD_ENTRY.MODAL.TITLE": "Eintrag hinzufügen",
          "APP.LIST.ADD_ENTRY.SUCCESS": "Neuer Eintrag erfolgreich erstellt",
          "APP.LIST.ADD_ENTRY.ERROR": "Es ist ein Fehler beim Erstellen des Eintrags aufgetreten",
          "APP.LIST.CONTEXTMENU.OPEN": "Detailansicht",
          "APP.LIST.DESCRIPTION": "Erstelle und pflege dynamische Listen mit selbstdefinierten Einträgen.",
          "APP.LIST.DETAIL.MODAL.TITLE.VIEW": "Eintrag ansehen",
          "APP.LIST.DETAIL.MODAL.TITLE.EDIT": "Eintrag bearbeiten",
          "APP.LIST.DETAIL.MODAL.ENTRY": "Details",
          "APP.LIST.DETAIL.MODAL.COMMENTS": "Kommentare",
          "APP.LIST.DETAIL.MODAL.HISTORY": "Historie",
          "APP.LIST.DETAIL.MODAL.HISTORY.AUTHOR": "Erstellt von",
          "APP.LIST.DETAIL.MODAL.HISTORY.EDITOR": "Bearbeitet von",
          "APP.LIST.DETAIL.MODAL.EDIT": "Bearbeiten",
          "APP.LIST.ELEMENT": "Elementname",
          "APP.LIST.ELEMENT.HELP": "Der Name der Elemente, die in dieser Liste angezeigt werden. Beispielsweise 'Telefonnummern', 'Kunden' etc.",
          "APP.LIST.FIELDS.CONFIGURE.NO_FIELDS": "Es wurden noch keine Felder konfiguriert. Du benötigst Felder um den Inhalt deiner Liste zu definieren. Lege jetzt die ersten Felder an!",
          "APP.LIST.NAME": "Liste",
          "APP.LIST.MODAL.DELETE.TITLE": "Eintrag löschen",
          "APP.LIST.MODAL.DELETE.TEXT": "Möchtest du den Eintrag wirklich löschen? Der Eintrag kann nicht wiederhergestellt werden",
          "APP.LIST.NO_ENTRIES": "Diese Liste hat keine Einträge",
          "APP.LIST.NO_READ_PERMISSIONS": "Du hast keine ausreichenden Berechtigungen, um diese Einträge zu sehen",
          "APP.LIST.NO_SEARCH_RESULTS": "Keine Einträge gefunden",
          "APP.LIST.NOTIFICATIONS.LABEL": "Benachrichtigung bei neuen Einträgen",
          "APP.LIST.NOTIFICATIONS.ALL": "Alle",
          "APP.LIST.NOTIFICATIONS.ADMIN": "Administratoren",
          "APP.LIST.NOTIFICATIONS.NONE": "Keiner",
          "APP.LIST.PERMISSIONS": "Berechtigungen",
          "APP.LIST.PERMISSIONS.ALL": "Ja, alle",
          "APP.LIST.PERMISSIONS.OWN": "Ja, eigene",
          "APP.LIST.PERMISSIONS.NONE": "Nein, keine",
          "APP.LIST.PERMISSIONS.HEADLINE": "Normale Benutzer dürfen Einträge…",
          "APP.LIST.PERMISSION_CREATE": "… erstellen",
          "APP.LIST.PERMISSION_READ": "… einsehen",
          "APP.LIST.PERMISSION_EDIT": "… bearbeiten",
          "APP.LIST.PERMISSION_DELETE": "… löschen",
          "ENTITY_TYPE.LIST_ENTRY": "Listeneintrag",
          "ENTITY_TYPE.LIST_ENTRIES": "Listeneinträge",
          "NOTIFICATIONS.LIST.ENTRY.NEW": "*{s1}* hat einen neuen Listeneintrag erstellt in *{s2}* > *{s3}*",
          "NOTIFICATIONS.LIST.ENTRY.UPDATED": "*{s1}* hat einen Listeneintrag bearbeitet in *{s2}* > *{s3}*"
        });
        /* eslint-enable quotes */
      });
})(angular);
