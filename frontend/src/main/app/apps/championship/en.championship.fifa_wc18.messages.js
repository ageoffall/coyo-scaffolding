(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.championship')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.AR": "Argentina",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.AU": "Australia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.BE": "Belgium",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.BR": "Brazil",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.CH": "Switzerland",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.CO": "Colombia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.CR": "Costa Rica",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.DE": "Germany",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.DK": "Denmark",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.EG": "Egypt",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.ES": "Spain",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.FR": "France",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.GB": "England",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.HR": "Croatia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.IR": "IR Iran",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.IS": "Iceland",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.JP": "Japan",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.KR": "Republic of Korea",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.MA": "Morocco",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.MX": "Mexico",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.NG": "Nigeria",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.PA": "Panama",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.PE": "Peru",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.PL": "Poland",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.PT": "Portugal",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.RS": "Serbia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.RU": "Russia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SA": "Saudi Arabia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SE": "Sweden",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.AR": "ARG",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.AU": "AUS",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.BE": "BEL",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.BR": "BRA",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.CH": "SUI",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.CO": "COL",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.CR": "CRC",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.DE": "GER",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.DK": "DEN",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.EG": "EGY",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.ES": "ESP",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.FR": "FRA",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.GB": "ENG",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.HR": "CRO",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.IR": "IRN",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.IS": "ISL",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.JP": "JPN",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.KR": "KOR",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.MA": "MAR",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.MX": "MEX",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.NG": "NGA",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.PA": "PAN",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.PE": "PER",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.PL": "POL",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.PT": "POR",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.RS": "SRB",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.RU": "RUS",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.SA": "KSA",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.SE": "SWE",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.SN": "SEN",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.TN": "TUN",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SHORT.UY": "URU",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.SN": "Senegal",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.TN": "Tunisia",
          "APP.CHAMPIONSHIP.FIFA_WC18.COUNTRIES.UY": "Uruguay",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.A": "Group A",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.B": "Group B",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.C": "Group C",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.D": "Group D",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.E": "Group E",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.F": "Group F",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.FINAL": "Final",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.G": "Group G",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.H": "Group H",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.QUARTERFINALS": "Quarter-finals",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.RO16": "Round of 16",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.SEMIFINALS": "Semi-finals",
          "APP.CHAMPIONSHIP.FIFA_WC18.GROUP.THIRD": "Third place play-off",
          "APP.CHAMPIONSHIP.FIFA_WC18.TITLE": "FIFA World Cup 2018"
        });
        /* eslint-enable quotes */
      });

})(angular);
