(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.championship')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "APP.CHAMPIONSHIP.DATE": "Date",
          "APP.CHAMPIONSHIP.DESCRIPTION": "Compete with your colleagues! Who will be able to guess the most sports results correctly?",
          "APP.CHAMPIONSHIP.FINAL_SCORE": "Final score",
          "APP.CHAMPIONSHIP.GAME_SAVED": "Guess saved",
          "APP.CHAMPIONSHIP.GAME_NOT_SAVED": "Guess not saved",
          "APP.CHAMPIONSHIP.GAMES": "Guess results",
          "APP.CHAMPIONSHIP.GAMES_PAST": "Past games",
          "APP.CHAMPIONSHIP.MATCHUP": "Match up",
          "APP.CHAMPIONSHIP.NAME": "Championship",
          "APP.CHAMPIONSHIP.NO_GAMES": "Currently there are no games available here.",
          "APP.CHAMPIONSHIP.NO_RANKING": "No points have been awarded yet.",
          "APP.CHAMPIONSHIP.RANKING": "Ranking",
          "APP.CHAMPIONSHIP.RANKING.MATCHES": "Matches & Schedule",
          "APP.CHAMPIONSHIP.RANKING.NAME": "Name",
          "APP.CHAMPIONSHIP.RANKING.POINTS": "Points",
          "APP.CHAMPIONSHIP.RANKING.POINTS_MOBILE": "P",
          "APP.CHAMPIONSHIP.RANKING.RANK": "Rank",
          "APP.CHAMPIONSHIP.RANKING.RANK_MOBILE": "#",
          "APP.CHAMPIONSHIP.SELECT_TEAM": "Select team"
        });
        /* eslint-enable quotes */
      });

})(angular);
