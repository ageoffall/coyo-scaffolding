(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.file-library')
      .config(function (translationRegistryProvider) {
        /* eslint-disable quotes */
        translationRegistryProvider.registerTranslations('en', {
          "APP.FILE_LIBRARY.DESCRIPTION": "Upload files to the documents app, sort them into folders and share them with your colleagues.",
          "APP.FILE_LIBRARY.NAME": "Documents",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.LABEL": "Editors",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.EVERYONE.LABEL": "Everyone",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.EVERYONE.DESCRIPTION": "All users can upload and modify files.",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.ADMINS.LABEL": "Admins",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.ADMINS.DESCRIPTION": "Only admins can upload and modify files.",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.AUTHORS.LABEL": "Show author",
          "APP.FILE_LIBRARY.SETTINGS.EDITORS.AUTHORS.DESCRIPTION": "Activate to show the creators and editors of files in your app.",
          "APP.FILE_LIBRARY.SETTINGS.NOTIFICATIONS.LABEL": "New document notification",
          "APP.FILE_LIBRARY.SETTINGS.NOTIFICATIONS.ALL": "All",
          "APP.FILE_LIBRARY.SETTINGS.NOTIFICATIONS.ADMIN": "Administrators",
          "APP.FILE_LIBRARY.SETTINGS.NOTIFICATIONS.NONE": "No one"
        });
        /* eslint-enable quotes */
      });
})(angular);
