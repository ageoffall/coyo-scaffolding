(function (angular) {
  'use strict';

  angular
      .module('coyo.workspaces')
      .controller('WorkspaceMemberInvitedController', WorkspaceMemberInvitedController);

  function WorkspaceMemberInvitedController($q, $stateParams, Pageable, ExternalUserModel, currentUser, workspace,
                                            workspacesConfig) {
    var vm = this;

    vm.loading = false;
    vm.currentUser = currentUser;
    vm.showDirectlyAddedHint = $stateParams.showDirectlyAddedHint;
    vm.showDirectlyAddedExternalsHint = $stateParams.showDirectlyAddedExternalsHint;

    vm.decline = decline;
    vm.declineExternal = declineExternal;

    function decline(userId, event) {
      event.stopPropagation();
      workspace.removeMember(userId).then(function () {
        _.remove(vm.currentPage.content, function (invitation) {
          return invitation.user && invitation.user.id === userId;
        });

        var pageDiff = !vm.currentPage.first && !vm.currentPage.content.length ? 1 : 0;
        var pageable = new Pageable(vm.currentPage.number - pageDiff, workspacesConfig.members.paging.pageSize);
        _loadMembers(pageable);
      });
    }

    function declineExternal(email, event) {
      event.stopPropagation();
      ExternalUserModel.declineExternalMember(workspace.id, email).then(function () {
        _.remove(vm.currentPage.content, function (invitation) {
          return invitation.email === email;
        });

        var pageDiff = !vm.currentPage.first && !vm.currentPage.content.length ? 1 : 0;
        var pageable = new Pageable(vm.currentPage.number - pageDiff, workspacesConfig.members.paging.pageSize);
        _loadMembers(pageable);
      });
    }

    function _loadMembers(pageable) {
      if (!vm.loading) {
        vm.loading = true;
        var _pageable = pageable || new Pageable(0, workspacesConfig.members.paging.pageSize);
        return workspace.getInvitedExternalIncluded(_pageable).then(function (page) {
          vm.currentPage = page;
        }).finally(function () {
          vm.loading = false;
        });
      }

      return $q.reject();
    }

    (function _init() {
      _loadMembers();
    })();
  }

})(angular);
