(function (angular) {
  'use strict';

  angular
      .module('coyo.search')
      .controller('SearchController', SearchController);

  /**
   * Controller for the full search
   */
  function SearchController($rootScope, $scope, $http, $httpParamSerializer, $q, $state, $stateParams, $translate,
                            coyoConfig, coyoEndpoints, targetService, Page, currentUser, errorService, $document,
                            selectionFilterService, userGuideService, externalSearchRequestLimit, $timeout,
                            SettingsModel, $log, ngxGoogleApiService) {

    var vm = this;

    vm.currentUser = currentUser;
    vm.subscriptionLoading = true;

    vm.showHelp = showHelp;
    vm.isEmpty = isEmpty;
    vm.getType = getType;
    vm.selectFilter = selectFilter;
    vm.clearAllFilters = clearAllFilters;
    vm.setNewTerm = setNewTerm;
    vm.focusSearch = focusSearch;
    vm.search = search;
    vm.sortFilter = sortFilter;
    vm.clickHandler = clickHandler;
    vm.$onInit = init;

    vm.links = {
      results: {},
      senders: {}
    };

    vm.hasAnySearchResult = false;
    vm.hasExternalSearchResults = false;
    vm.hasCoyoSearchResults = false;
    vm.hasSuggestions = false;
    vm.isFiltered = false;
    vm.showEmptySearchAndSuggestionsPanel = false;
    vm.showSearchResultsPanel = false;

    function showHelp() {
      userGuideService.open('search_operators');
    }

    function isEmpty(object) {
      return _.size(object) === 0;
    }

    function getType(typeName) {
      return coyoConfig.entityTypes[typeName] || coyoConfig.entityTypes.default;
    }

    function clickHandler(result) {
      if (_isMessage(result) && result.canLinkToResult) {
        _openMessage(result);
      } else if (_isFile(result) && result.canLinkToResult) {
        _openFile(result);
      }
    }

    function selectFilter(key, selected) {
      var filter = $rootScope.search.filter;
      if (selected.length > 0) {
        filter[key] = selected;
      } else {
        delete filter[key];
      }
      return search(0, true);
    }

    function clearAllFilters() {
      $rootScope.search.filter = {};
      return search(0, true);
    }

    function setNewTerm(newTerm) {
      $rootScope.search.term = newTerm;
      search(0, true);
    }

    function focusSearch() {
      $rootScope.search.visible = true;
      var searchField = angular.element('#global-search-input-field');
      if (searchField) {
        searchField.focus();
      }
    }

    function search(page, writeToUrl) {
      if (vm.loading || !$rootScope.search.term) {
        _updateViewVariables();
        return $q.reject();
      }
      vm.isFiltered = !_.isEmpty($rootScope.search.filter);
      vm.loading = true;

      // write params to URL
      if (writeToUrl) {
        return $state.transitionTo('main.search', angular.extend({
          term: $rootScope.search.term
        }, _.mapKeys($rootScope.search.filter, function (value, key) {
          return key + '[]';
        })), {notify: false}).finally(function () {
          vm.loading = false;
          vm.showSearchResults = true;
          _updateViewVariables();
        });
      } else {
        // execute search
        _externalSearchRequest();
        return $http({
          method: 'GET',
          url: coyoEndpoints.search.query.replace('{page}', page).replace('{pageSize}', 20),
          params: {
            term: $rootScope.search.term,
            filters: $httpParamSerializer($rootScope.search.filter),
            aggregations: $httpParamSerializer({
              type: _.keys(coyoConfig.entityTypes).length,
              modified: '',
              sender: 100,
              author: 100
            }),
            newSearch: $rootScope.search.new
          }
        }).then(function (result) {
          vm.currentPage = new Page(result.data, {
            _page: page,
            _pageSize: 20
          });

          // permission checks
          _.forEach(result.data.content, function (result) {
            targetService.onCanLinkTo(result.target, function (canLink) {
              result.canLinkToResult = canLink;
              if (!_isMessage(result) && !_isFile(result)) {
                vm.links.results[result.id] = _getResultLink(result);
              }
            });
            if (result.sender) {
              targetService.onCanLinkTo(result.sender.target, function (canLink) {
                result.canLinkToSender = canLink;
                vm.links.senders[result.sender.id] = _getSenderLink(result);
              });
            }
          });

          vm.typeAggregationTotal = _.sumBy(vm.currentPage.aggregations.type, 'count');

          vm.currentUserAuthorCount =
            _.get(_.find(vm.currentPage.aggregations.author, ['key', vm.currentUser.id]), 'count', 0);
          vm.currentUserSenderCount =
            _.get(_.find(vm.currentPage.aggregations.sender, ['key', vm.currentUser.id]), 'count', 0);

          _initFilters();

          vm.error = false;
          $rootScope.search.new = false;
        }).catch(function (errorResponse) {
          if (_.get(errorResponse, 'data.errorStatus') === 'INVALID_SEARCH_REQUEST') {
            errorService.suppressNotification(errorResponse);
            vm.error = true;
          }
        }).finally(function () {
          vm.loading = false;
          if (!vm.externalSearchLoading) {
            vm.showSearchResults = true;
            _updateViewVariables();
          } else {
            _startSearchResultTimer();
          }
        });
      }
    }

    /**
     * This method waits a short while for the external search request to finish. If the external search request
     * doesn't finish in that time the search results, which are already loaded, are forced to be displayed.
     * The purpose of this is to prevent the search result blocks from flickering and moving each other around.
     */
    function _startSearchResultTimer() {
      $timeout(function () {
        vm.showSearchResults = true;
        _updateViewVariables();
      }, 2000);
    }

    function sortFilter(item) {
      return item.key === vm.currentUser.id ? 0 : 1;
    }

    function init() {
      // extract search params from URL
      $rootScope.search = angular.extend($rootScope.search || {}, {
        visible: true,
        term: $stateParams.term,
        filter: _.chain($stateParams).pickBy(function (value, key) {
          return _.endsWith(key, '[]') && !!value;
        }).mapKeys(function (value, key) {
          return key.slice(0, -2);
        }).value()
      });

      // execute first search request
      search(0, false);
    }

    // ================================

    function _externalSearchRequest() {
      ngxGoogleApiService.isGoogleApiActive().subscribe(function (integrationActive) {
        if (!integrationActive) {
          return;
        }

        vm.externalSearchLoading = true;
        $http({
          method: 'GET',
          url: coyoEndpoints.search.external,
          params: {
            term: $rootScope.search.term,
            limit: externalSearchRequestLimit
          }
        }).then(function (result) {
          vm.externalSearchResults = result.data;
          vm.externalSearchResultsCount = _.sumBy(result.data, function (searchResultList) {
            return _.get(searchResultList, 'searchResults.length', 0);
          });
        }).catch(function (error) {
          $log.error('[external search error]', error);
        }).finally(function () {
          vm.externalSearchLoading = false;
          if (!vm.loading) {
            vm.showSearchResults = true;
            _updateViewVariables();
          } else {
            _startSearchResultTimer();
          }
        });
      });
    }

    function _openMessage(message) {
      if (message.canLinkToResult) {
        targetService.go(message.target);
      }
    }

    function _openFile(file) {
      targetService.go(file.target);
    }

    function _isMessage(result) {
      return result.typeName === 'message';
    }

    function _isFile(result) {
      return result.typeName === 'file';
    }

    function _initFilters() {
      _initTypeFilter();
      _initModifiedFilter();
      _initSenderFilter();
      _initAuthorFilter();
    }

    function _initTypeFilter() {
      vm.typeFilter = _initFilter('type', function (aggregation) {
        return {
          text: $translate.instant(getType(aggregation.key).label),
          icon: 'zmdi-' + vm.getType(aggregation.key).icon,
          iconColor: vm.getType(aggregation.key).color
        };
      }, vm.typeAggregationTotal);
    }

    function _initModifiedFilter() {
      vm.modifiedFilter = _initFilter('modified', function (aggregation) {
        return {order: aggregation.data.total};
      }, vm.currentPage.totalElements);
      vm.modifiedFilter.allItem.textKey = 'MODULE.SEARCH.FILTER.MODIFIED.EVER';
      vm.modifiedFilter.icon = 'zmdi-time';
    }

    function _initSenderFilter() {
      vm.senderFilter = _initFilter('sender', function (aggregation) {
        return {
          text: _getSenderNameOrOwnTimelineText(aggregation),
          icon: 'zmdi-' + vm.getType(aggregation.data.typeName).icon,
          iconColor: vm.getType(aggregation.data.typeName).color
        };
      });
      vm.senderFilter.allItem.textKey = 'MODULE.SEARCH.FILTER.LOCATION.ANY';
    }

    function _initAuthorFilter() {
      vm.authorFilter = _initFilter('author', function (aggregation) {
        return {
          text: _getSenderNameOrSelfText(aggregation),
          icon: 'zmdi-' + vm.getType(aggregation.data.typeName).icon,
          iconColor: vm.getType(aggregation.data.typeName).color
        };
      });

      vm.authorFilter.allItem.textKey = 'MODULE.SEARCH.FILTER.AUTHOR.ANY';
      vm.authorFilter.allIcon = 'zmdi-accounts-alt';
    }

    function _getSenderNameOrSelfText(aggregation) {
      return _getSenderNameOrAlternativeText(aggregation, 'MODULE.SEARCH.FILTER.AUTHOR.MYSELF');
    }

    function _getSenderNameOrOwnTimelineText(aggregation) {
      return _getSenderNameOrAlternativeText(aggregation, 'MODULE.SEARCH.FILTER.SENDER.MYSELF');
    }

    function _getSenderNameOrAlternativeText(aggregation, alternativeTextMessageKey) {
      if (aggregation.key === vm.currentUser.id) {
        return $translate.instant(alternativeTextMessageKey);
      } else {
        return aggregation.data.displayName;
      }
    }

    function _initFilter(aggregationName, optionsFunction, count) {
      var itemConfigs = _createFilterConfig(aggregationName, optionsFunction);
      return selectionFilterService.builder().count(count).items(itemConfigs).build();
    }

    function _createFilterConfig(aggregationName, optionsFunction) {
      if (!angular.isArray(vm.currentPage.aggregations[aggregationName])) {
        return [];
      }
      var filter = $rootScope.search.filter[aggregationName];
      return vm.currentPage.aggregations[aggregationName].map(function (aggregation) {
        return angular.extend({
          key: aggregation.key,
          active: filter && filter.indexOf(aggregation.key) >= 0,
          count: aggregation.count
        }, optionsFunction(aggregation));
      });
    }

    function _getResultLink(result) {
      if (result.canLinkToResult) {
        return targetService.getLink(result.target);
      } else {
        return '';
      }
    }

    function _getSenderLink(result) {
      if (result.sender && result.canLinkToSender) {
        return targetService.getLink(result.sender.target);
      } else {
        return '';
      }
    }

    function _hasExternalSearchResults() {
      return _.some(vm.externalSearchResults, function (searchResultProvider) {
        return _.get(searchResultProvider, 'searchResults.length') > 0;
      });
    }

    function _hasCoyoSearchResults() {
      return _.get(vm, 'currentPage.totalElements') > 0;
    }

    function _hasSuggestions() {
      return _.get(vm, 'currentPage.suggestions.length') > 0;
    }

    function _setHasAnySearchResult() {
      // Ignore external search results when filters are set
      if (vm.isFiltered) {
        vm.hasAnySearchResult = _hasCoyoSearchResults();
      } else {
        vm.hasAnySearchResult = _hasCoyoSearchResults() || _hasExternalSearchResults();
      }
    }

    function _setHasExternalSearchResults() {
      vm.hasExternalSearchResults = _hasExternalSearchResults();
    }

    function _setHasCoyoSearchResults() {
      vm.hasCoyoSearchResults = _hasCoyoSearchResults();
    }

    function _setHasSuggestions() {
      vm.hasSuggestions = _hasSuggestions();
    }

    function _setIsFiltered() {
      vm.isFiltered = !_.isEmpty($rootScope.search.filter);
    }

    function _setShowEmptySearchAndSuggestionsPanel() {
      vm.showEmptySearchAndSuggestionsPanel = vm.showSearchResults && !vm.hasAnySearchResult || vm.hasSuggestions;
    }

    function _setShowSearchResultsPanel() {
      vm.showSearchResultsPanel = vm.showSearchResults && vm.hasAnySearchResult;
    }

    function _setShowSearchTermPanel() {
      vm.showSearchTermPanel = vm.hasAnySearchResult;
    }

    function _updateViewVariables() {
      _setHasCoyoSearchResults();
      _setHasExternalSearchResults();
      _setHasAnySearchResult();
      _setHasSuggestions();
      _setIsFiltered();
      $timeout(function () {
        _setShowEmptySearchAndSuggestionsPanel();
        _setShowSearchResultsPanel();
        _setShowSearchTermPanel();
      });
    }
  }

})(angular);
