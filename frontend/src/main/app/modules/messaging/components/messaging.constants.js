(function () {
  'use strict';

  angular
      .module('coyo.messaging')
      .constant('optimisticTypeMessage', 'OPTIMISTIC_TYPE_MESSAGE')
      .constant('tempUploadsTTLSeconds', 86400);
})();
