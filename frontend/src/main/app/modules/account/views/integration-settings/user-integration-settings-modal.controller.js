(function (angular) {
  'use strict';

  angular
      .module('coyo.account')
      .controller('UserIntegrationSettingsModalController', UserIntegrationSettingsModalController);

  function UserIntegrationSettingsModalController($injector, $uibModalInstance) {
    var vm = this;
    vm.$onInit = onInit;

    vm.isCalendarSyncEnabled = false;
    vm.save = save;

    function save() {
      if (vm.isCalendarSyncEnabled) {
        enableCalendarSyncAndCloseModal();
      } else {
        disableCalendarSyncAndCloseModal();
      }
    }

    function enableCalendarSyncAndCloseModal() {
      vm.userIntegrationSettingsService.enableCalendarSync().toPromise().then(function () {
        $uibModalInstance.close();
      });
    }

    function disableCalendarSyncAndCloseModal() {
      vm.userIntegrationSettingsService.disableCalendarSync().toPromise().then(function () {
        $uibModalInstance.close();
      });
    }

    function onInit() {
      vm.userIntegrationSettingsService = $injector.get('ngxUserIntegrationSettingsService');
      vm.userIntegrationSettingsService.isCalendarSyncEnabled().toPromise().then(function (isCalendarSyncEnabled) {
        vm.isCalendarSyncEnabled = isCalendarSyncEnabled;
      });
    }

  }
})(angular);
