(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .component('coyoEventList', coyoEventList())
      .controller('EventListComponentController', EventListComponentController);

  /**
   * @ngdoc directive
   * @name coyo.events.coyoEventList:coyoEventList
   * @scope
   * @restrict 'E'
   * @element ANY
   *
   * @description
   * Renders a list of events.
   *
   * @param {object} page
   * The current page of events.
   *
   * @param {boolean} loading
   * The loading state of the list.
   *
   * @param {boolean} showTour
   * Whether or not to include the coyo tour for this event list.
   *
   * @param {boolean} filtersResettable
   * Whether or not filter settings are resettable.
   *
   * @param {expression} resetFilters
   * Function provided to reset filter settings.
   */
  function coyoEventList() {
    return {
      templateUrl: 'app/modules/events/components/event-list/event-list.html',
      bindings: {
        page: '=',
        loading: '<',
        showTour: '<',
        filtersResettable: '<?',
        resetFilters: '&?'
      },
      controller: 'EventListComponentController',
      controllerAs: '$ctrl'
    };
  }

  function EventListComponentController(moment) {
    var vm = this;

    vm.hasStarted = hasStarted;
    vm.isOngoing = isOngoing;
    vm.isToday = isToday;
    vm.showDivider = showDivider;
    vm.onParticipationStatusChanged = onParticipationStatusChanged;

    function hasStarted(start) {
      return moment().isSameOrAfter(start);
    }

    function isOngoing(start, end) {
      var now = moment();
      return now.isSameOrAfter(start) && now.isSameOrBefore(end);
    }

    function isToday(date) {
      return moment().isSame(date, 'day');
    }

    function showDivider($index) {
      var currentEvent = _.get(vm.page, 'content[' + $index + ']');
      var previousEvent = _.get(vm.page, 'content[' + ($index - 1) + ']');
      return angular.isUndefined(previousEvent)
          || !moment(previousEvent.startDate).isSame(currentEvent.startDate, 'day');
    }

    function onParticipationStatusChanged(event, newStatus, oldStatus) {
      var newIsAttending = newStatus === 'ATTENDING';
      var oldIsAttending = oldStatus === 'ATTENDING';
      if (newIsAttending !== oldIsAttending) {
        if (newIsAttending) {
          event.attendingCount++;
        } else {
          event.attendingCount--;
        }
      }
    }
  }

})(angular);
