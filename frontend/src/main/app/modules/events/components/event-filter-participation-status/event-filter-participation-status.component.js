(function (angular) {
  'use strict';

  angular
      .module('coyo.events')
      .component('coyoEventFilterParticipationStatus', eventFilterParticipationStatus())
      .controller('EventFilterParticipationStatusController', EventFilterParticipationStatusController);

  /**
   * @ngdoc directive
   * @name coyo.events.coyoEventFilterParticipationStatus:coyoEventFilterParticipationStatus
   *
   * @description
   * Filter event participation status
   *
   * @param {object} filterModel
   * The filter model containing selectable items.
   *
   * @param {expression} onSelectionChange
   * Callback that will be invoked when selection changed. A named parameter 'selected' will receive an array of
   * the selected keys.
   */
  function eventFilterParticipationStatus() {
    return {
      templateUrl: 'app/modules/events/components/event-filter-participation-status/event-filter-participation-status.html',
      bindings: {
        filterModel: '<',
        onSelectionChange: '&'
      },
      controller: 'EventFilterParticipationStatusController'
    };
  }

  function EventFilterParticipationStatusController() {
    var vm = this;

    vm.callOnSelectionChange = callOnSelectionChange;

    function callOnSelectionChange(selected) {
      vm.onSelectionChange({selected: selected});
    }
  }

})(angular);
