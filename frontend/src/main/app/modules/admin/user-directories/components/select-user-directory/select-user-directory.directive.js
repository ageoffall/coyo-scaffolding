(function (angular) {
  'use strict';

  angular
      .module('coyo.admin.userDirectories')
      .directive('coyoSelectUserDirectory', selectUserDirectory);
  /**
   * @ngdoc directive
   * @name coyo.admin.userDirectories.coyoSelectUserDirectory:coyoSelectUserDirectory
   * @element ANY
   * @restrict E
   * @scope
   *
   * @description
   * Renders a UI select field for a user directory.
   *
   * @param {object} parameters
   * if the parameters object contains a property 'withInternal' that evaluates to 'true', then the internal user
   * directory is added to the list of directories at the beginning of the list.
   */
  function selectUserDirectory(selectFactoryModel, UserDirectoryModel, userDirectoryTypeRegistry, $translate, $filter) {

    var userDirectories = null;
    var internalUserDirectoryName = $translate.instant('ADMIN.USER_DIRECTORIES.INTERNAL.INSTANCE.NAME');
    var internalUserDirectoryTranslatedType = {
      id: 'INTERNAL',
      displayName: internalUserDirectoryName,
      type: $translate.instant('ADMIN.USER_DIRECTORIES.INTERNAL.TYPE')
    };
    var internalUserDirectory = {
      id: 'INTERNAL',
      displayName: internalUserDirectoryName,
      type: 'internal'
    };

    return selectFactoryModel({
      refresh: _refresh,
      transformSelection: _transformSelection,
      sublines: ['type'],
      emptyText: 'ADMIN.USER_DIRECTORIES.NO_USER_DIRECTORIES_FOUND'
    });

    function _transformSelection(model) {
      return (model && model.id === 'INTERNAL') ? internalUserDirectory : model;
    }

    function _refresh(pageable, search, parameters) {
      var withInternal = _.get(parameters, 'withInternal', false);

      if (userDirectories === null) {
        userDirectories = UserDirectoryModel.query().then(function (response) {
          return _.map(response, function (userDirectory) {
            var type = userDirectoryTypeRegistry.get(userDirectory.type);
            return {
              id: userDirectory.id,
              displayName: userDirectory.name,
              type: $translate.instant(type.name)
            };
          });
        });
      }

      return userDirectories.then(function (data) {
        var dataCopy = data.slice();
        if (withInternal) {
          dataCopy.unshift(internalUserDirectoryTranslatedType);
        }
        var filtered = search ? $filter('filter')(dataCopy, function (item) {
          return item.displayName.toLowerCase().includes(search.toLowerCase()) ||
              item.type.toLowerCase().includes(search.toLowerCase());
        }) : dataCopy;
        return angular.extend(filtered, {
          meta: {
            last: true,
            number: 0
          }
        });
      });
    }
  }

})(angular);
