(function (angular) {
  'use strict';

  angular
      .module('coyo.admin.jobs')
      .component('oyocJobOptions', jobOptions())
      .controller('JobOptionsController', JobOptionsController);

  /**
   * @ngdoc directive
   * @name coyo.admin.jobs.oyocJobOptions:oyocJobOptions
   * @restrict 'E'
   *
   * @description
   * Context menu options for job list.
   *
   * @param {object} actions the available user actions
   * @param {object} job the job
   */
  function jobOptions() {
    return {
      scope: {},
      controller: 'JobOptionsController',
      controllerAs: '$ctrl',
      templateUrl: 'app/modules/admin/jobs/components/job-options/job-options.html',
      bindings: {
        job: '<',
        refresh: '&',
        jobStatus: '<?'
      }
    };
  }

  function JobOptionsController() {
    var vm = this;

    vm.startJob = startJob;
    vm.stopJob = stopJob;

    function startJob() {
      vm.job.start().finally(vm.refresh);
    }

    function stopJob() {
      vm.job.stop().finally(vm.refresh);
    }
  }

})(angular);
