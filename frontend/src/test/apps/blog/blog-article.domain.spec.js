(function () {
  'use strict';

  describe('domain: BlogArticleModel', function () {

    beforeEach(module('commons.target'));
    beforeEach(module('coyo.apps.blog'));

    var $httpBackend, BlogArticleModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _BlogArticleModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      BlogArticleModel = _BlogArticleModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {

      it('should count blog articles', function () {
        // given
        var app = {senderId: 'senderId', id: 'appId'};
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/blog/articles/count' +
                '?includeDrafts=true&includePublished=true&includeScheduled=false')
            .respond(200, {'2015-01': 3, '2015-07': 7});

        // when
        var response = null;
        BlogArticleModel.count(app, true, false, true).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response['2015-01']).toEqual(3);
        expect(response['2015-07']).toEqual(7);
      });

    });

    describe('instance members', function () {

      it('should load all articles', function () {
        // given
        var response = null;
        var articles = [
          {
            title: 'title1',
            text: 'text1',
            teaserText: 'teaserText1',
            publishDate: '0'
          },
          {
            title: 'title2',
            text: 'text2',
            teaserText: 'teaserText2',
            publishDate: '0'
          }
        ];
        var model = new BlogArticleModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/blog/articles')
            .respond(200, articles);

        // when
        model.get().then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(2);
      });

      it('should load a specific blog article', function () {
        // given
        var article = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          publishDate: '0'
        };
        var response = null;
        var model = new BlogArticleModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/blog/articles/blogArticleId')
            .respond(200, article);

        // when
        model.get('blogArticleId').then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(article.title);
        expect(response.text).toEqual(article.text);
        expect(response.teaserText).toEqual(article.teaserText);
        expect(response.publishDate).toEqual(article.publishDate);
      });

      it('should delete an existing blog article', function () {
        // given
        var model = new BlogArticleModel({senderId: 'senderId', appId: 'appId', id: 'blogArticleId'});
        $httpBackend
            .expectDELETE(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/blog/articles/blogArticleId')
            .respond(200);

        // when
        model.delete();
        $httpBackend.flush();

        // then
        // nothing to do here...
      });

      it('should build the layout name of a blog article', function () {
        // given
        var model = new BlogArticleModel({id: 'blogArticleId'}),
            appId = 'appId',
            key = 'NONE';

        // when
        var name = model.buildLayoutName(appId, key);

        // then
        expect(name).toEqual('app-blog-' + appId + '-' + model.id);
      });

      it('should build the layout name of a translated blog article', function () {
        // given
        var model = new BlogArticleModel({id: 'blogArticleId', defaultLanguage: 'lang'}),
            appId = 'appId',
            key = 'key';

        // when
        var name = model.buildLayoutName(appId, key);

        // then
        expect(name).toEqual('app-blog-' + appId + '-' + model.id + '-' + key);
      });

    });
  });
}());
