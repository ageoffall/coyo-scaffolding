(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';

  describe('module: ' + moduleName, function () {

    // the service's name
    var listSortService,
        fields;

    beforeEach(function () {

      module(moduleName);

      inject(function (_listSortService_) {
        listSortService = _listSortService_;
      });

      fields = [{
        id: 'field-test-1',
        key: 'text'
      }, {
        id: 'field-test-2',
        key: 'text'
      }];
    });

    describe('Service: listSortService', function () {

      it('should update sort config', function () {
        // given
        expect(listSortService.getSortConfig().field).toBeUndefined();

        // when
        var sortConfig = listSortService.updateSortConfig(fields[0]);

        // then
        expect(sortConfig.field).toBe(fields[0]);
        expect(sortConfig.dir).toBe('ASC');
        expect(sortConfig.query).toBe('values.field-test-1.raw');
      });

      it('should initialize sort config with first field', function () {
        // given
        expect(listSortService.getSortConfig().field).toBeUndefined();

        // when
        listSortService.initSortConfig(fields, 'test-app-id');
        var sortConfig = listSortService.getSortConfig();

        // then
        expect(sortConfig.field).toBe(fields[0]);
        expect(sortConfig.dir).toBe('ASC');
        expect(sortConfig.query).toBe('values.field-test-1.raw');
      });

      it('should initialize sort config from local storage', function () {
        // given
        var appId = 'test-app-id';
        expect(listSortService.getSortConfig().field).toBeUndefined();
        listSortService.updateSortConfig(fields[1]);
        listSortService.storeSortConfig(appId);

        // when
        listSortService.initSortConfig(fields, appId);
        var sortConfig = listSortService.getSortConfig();

        // then
        expect(sortConfig.field).toBe(fields[1]);
      });

      it('should create sort query', function () {
        // given

        // when
        var sortQuery = listSortService.createSortQuery();

        // then
        expect(sortQuery).toBe('created,DESC');
      });

    });

  });
})();
