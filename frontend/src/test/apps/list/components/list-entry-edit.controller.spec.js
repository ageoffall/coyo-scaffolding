(function () {
  'use strict';

  var moduleName = 'coyo.apps.list';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, ListEntryModel, listEntryDetailsService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
      $controller = _$controller_;
      $q = _$q_;
      $scope = _$rootScope_;
      ListEntryModel = jasmine.createSpyObj('ListEntryModel', ['get']);
      listEntryDetailsService = jasmine.createSpyObj('listEntryDetailsService', ['getCurrentContext', 'editComplete']);
    }));

    var controllerName = 'ListEntryEditController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          ListEntryModel: ListEntryModel,
          listEntryDetailsService: listEntryDetailsService
        });
      }

      it('should return promise from save', function () {
        // given
        var ctrl = buildController();
        var deferred = $q.defer();
        ctrl.context = {};
        ctrl.entry = jasmine.createSpyObj('entry', ['save']);
        ctrl.entry.save.and.returnValue(deferred.promise);

        // when
        var returnValue = ctrl.save();

        var called = false;
        returnValue.then(function () {
          called = true;
        });

        deferred.resolve('testDone');
        $scope.$apply();

        // then
        expect(called).toBeTruthy();
      });

      it('should get list entry model in edit mode', function () {
        // given
        var ctrl = buildController();
        ListEntryModel.get.and.returnValue($q.resolve({}));
        listEntryDetailsService.getCurrentContext.and.returnValue({});

        // when
        ctrl.$onInit();

        // then
        expect(ListEntryModel.get).toHaveBeenCalledWith({}, {mode: 'edit'});
      });

    });
  });

})();
