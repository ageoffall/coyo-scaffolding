(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';
  var controllerName = 'WikiListController';

  describe('module: ' + moduleName, function () {

    var $httpBackend, backendUrlService, $controller, $q, $rootScope, $log, $state,
        wikiArticleService, WikiArticleModel, app, modalService, sender, currentUser;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$httpBackend_, _WikiArticleModel_, _$q_, _$rootScope_, _$controller_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      backendUrlService = _backendUrlService_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      $controller = _$controller_;

      WikiArticleModel = _WikiArticleModel_;
      app = jasmine.createSpyObj('app', ['']);
      sender = jasmine.createSpyObj('sender', ['']);
      wikiArticleService = jasmine.createSpyObj('wikiArticleService',
          ['deleteArticle', 'exportPreview', 'exportPreviewWithSubArticles', 'confirmWikiExport']);
      wikiArticleService.deleteArticle.and.returnValue($q.resolve());
      $state = jasmine.createSpyObj('$state', ['reload', 'go']);
      $log = jasmine.createSpyObj('$log', ['error']);
      _.set($state, 'current.name', 'CURRENT_STATE');

      currentUser = {
        id: 1,
        name: 'A man has no name',
        language: 'EN',
        getBestSuitableLanguage: function () {
          return $q.resolve('EN');
        }
      };

      WikiArticleModel.getSubArticlesWithPermissions = function () {
        return $q.resolve([]);
      };
      WikiArticleModel.count = function () {
        return $q.resolve(0);
      };

      modalService = jasmine.createSpyObj('modalService', ['confirm']);
      modalService.confirm.and.returnValue({result: $q.resolve()});

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    function buildController() {
      return $controller(controllerName, {
        $log: $log,
        $state: $state,
        wikiArticleService: wikiArticleService,
        WikiArticleModel: WikiArticleModel,
        app: app,
        sender: sender,
        modalService: modalService,
        currentUser: currentUser
      });
    }

    describe('controller init: ' + controllerName, function () {

      it('should initialize the controller correctly', function () {
        // given
        // nothing to see here...

        // when
        var ctrl = buildController();
        $rootScope.$apply();

        // then
        expect(ctrl.articleCount).toBe(0);
        expect(ctrl.wikiArticles.length).toBe(0);
        expect(ctrl.loading).toBe(false);
      });

    });

    describe('controller active: ' + controllerName, function () {

      var ctrl;

      beforeEach(function () {
        ctrl = buildController();
        $rootScope.$apply();
      });

      it('should toggle an article (collapsed)', function () {
        // given
        var article = {collapsed: true};

        // when
        ctrl.toggle(article);
        $rootScope.$apply();

        // then
        expect(article.collapsed).toBeFalse();
      });

      it('should toggle an article (not collapsed)', function () {
        // given
        var article = {collapsed: false};

        // when
        ctrl.toggle(article);
        $rootScope.$apply();

        // then
        expect(article.collapsed).toBeTrue();
      });

      it('should load sub articles and initialize their nodes (article undefined)', function () {
        // given
        ctrl.loading = false;
        WikiArticleModel.getSubArticlesWithPermissions = function () {
          return $q.resolve([{id: 1}, {id: 2}]);
        };

        // when
        ctrl.loadSubArticles();
        $rootScope.$apply();

        // then
        expect(ctrl.wikiArticles.length).toBe(2);
        expect(ctrl.wikiArticles[0].id).toBe(1);
        expect(ctrl.wikiArticles[1].id).toBe(2);
        expect(ctrl.wikiArticles[0].nodes).toBeDefined();
        expect(ctrl.wikiArticles[1].nodes).toBeDefined();
      });

      it('should load sub articles and initialize their nodes (article defined)', function () {
        // given
        ctrl.loading = false;
        ctrl.wikiArticles = [{id: 0, nodes: []}];
        WikiArticleModel.getSubArticlesWithPermissions = function () {
          return $q.resolve([{id: 1}, {id: 2}]);
        };

        // when
        ctrl.loadSubArticles(ctrl.wikiArticles[0]);
        $rootScope.$apply();

        // then
        expect(ctrl.wikiArticles[0].nodes.length).toBe(2);
        expect(ctrl.wikiArticles[0].nodesLoaded).toBeTrue();
        expect(ctrl.wikiArticles[0].collapsed).toBeFalse();
        expect(ctrl.wikiArticles[0].nodes[0].id).toBe(1);
        expect(ctrl.wikiArticles[0].nodes[1].id).toBe(2);
        expect(ctrl.wikiArticles[0].nodes[0].nodes).toBeDefined();
        expect(ctrl.wikiArticles[0].nodes[1].nodes).toBeDefined();
      });

      it('should delete an article', function () {
        // given
        var article = {id: 'ARTICLE-ID'};

        // when
        ctrl.deleteArticle(article);
        $rootScope.$apply();

        // then
        expect($state.reload).toHaveBeenCalledWith($state.current.name);
        expect(wikiArticleService.deleteArticle).toHaveBeenCalledWith(app, article);
      });

      it('should open the view of an article', function () {
        // given
        var article = {id: 0};
        ctrl.loading = false;

        // when
        ctrl.openView(article);
        $rootScope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('.view', {id: article.id, currentLanguage: undefined});
      });

      it('should open the view of an article with current language', function () {
        // given
        var article = {id: 0};
        ctrl.loading = false;
        ctrl.currentLanguage = 'EN';

        // when
        ctrl.openView(article);
        $rootScope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('.view', {id: article.id, currentLanguage: ctrl.currentLanguage});
      });

      it('should handle dropped articles', function () {
        // given
        var article0 = new WikiArticleModel({
          senderId: 'senderId',
          appId: 'appId',
          id: 'wikiArticleId-0',
          sortOrder: 0
        });
        var article1 = new WikiArticleModel({
          senderId: 'senderId',
          appId: 'appId',
          id: 'wikiArticleId-1',
          sortOrder: 1
        });
        var event = {
          source: {
            nodeScope: {
              $modelValue: article1
            }
          },
          dest: {
            index: 0,
            nodesScope: {
              $modelValue: [article0, article1],
              $nodeScope: {
                $modelValue: article0
              }
            }
          }
        };

        ctrl.loading = false;

        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/wikiArticleId-1')
            .respond(200, article1);

        // when
        ctrl.treeOptions.dropped(event);
        $rootScope.$apply();

        // then
        // expect(event.source.nodeScope.$modelValue.update).toHaveBeenCalled();
      });

      it('should open export preview for single article', function () {
        // given
        var article = {
          id: 'ARTICLE-ID',
          title: 'Article Title',
          usedLanguage: 'EN'
        };
        var exportContent = {appId: app.id, articles: [article], modalTitle: article.title, selectedLanguage: article.usedLanguage};

        // when
        ctrl.exportPreview(article);

        // then
        expect(wikiArticleService.exportPreview).toHaveBeenCalledWith(exportContent);
      });

      it('should open export preview for article with sub-articles', function () {
        // given
        var article = {
          id: 'ARTICLE-ID',
          title: 'Article Title',
          usedLanguage: 'EN',
          deepCountChildren: function () {
            return $q.resolve(2);
          }
        };
        wikiArticleService.confirmWikiExport.and.returnValue($q.resolve());

        // when
        ctrl.exportSubArticlePreview(article);
        $rootScope.$apply();

        // then
        expect(wikiArticleService.confirmWikiExport).toHaveBeenCalledWith(3);
        expect(wikiArticleService.exportPreviewWithSubArticles).toHaveBeenCalledWith(app, article);
      });

      it('should not open export preview for article with many sub-articles when confirmation is cancelled', function () {
        // given
        var article = {
          id: 'ARTICLE-ID',
          title: 'Article Title',
          usedLanguage: 'EN',
          deepCountChildren: function () {
            return $q.resolve(81);
          }
        };
        wikiArticleService.confirmWikiExport.and.returnValue($q.reject());

        // when
        ctrl.exportSubArticlePreview(article);
        $rootScope.$apply();

        // then
        expect(wikiArticleService.confirmWikiExport).toHaveBeenCalledWith(82);
        expect(wikiArticleService.exportPreviewWithSubArticles).not.toHaveBeenCalled();
      });

    });
  });

})();
