(function () {
  'use strict';

  var moduleName = 'coyo.apps.form';

  describe('module: ' + moduleName, function () {

    var $controller, app, fields, Pageable, formService, FormEntryModel, $q, $scope, fieldTypeRegistry, modalService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _Pageable_, _$q_, $rootScope) {
      $controller = _$controller_;
      Pageable = _Pageable_;
      $q = _$q_;
      $scope = $rootScope.$new();

      formService = jasmine.createSpyObj('formService', ['initEntries', 'getFieldValue']);

      FormEntryModel = jasmine.createSpyObj('FormEntryModel', ['pagedQuery']);

      fieldTypeRegistry = jasmine.createSpyObj('fieldTypeRegistry', ['getRenderProperty', 'get']);

      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);

      app = {
        id: 'app-id',
        senderId: 'sender-id'
      };

      fields = [];
    }));

    var controllerName = 'FormEntriesController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {app: app, fields: fields, Pageable: Pageable,
          formService: formService, FormEntryModel: FormEntryModel, fieldTypeRegistry: fieldTypeRegistry,
          modalService: modalService});
      }

      it('should initialize form entry list', function () {
        // given
        var ctrl = buildController();
        var result = {content: []};

        FormEntryModel.pagedQuery.and.returnValue($q.resolve(result));

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(formService.initEntries).toHaveBeenCalledTimes(1);
        expect(ctrl.currentPage).toBe(result);
        expect(ctrl.loading).toBe(false);
      });

      it('should get the value of a field', function () {
        // given
        var ctrl = buildController();

        formService.getFieldValue.and.returnValue({value: 'test'});
        // when
        var result = ctrl.getFieldValue({}, {id: 'test-id'});

        // then
        expect(result).toBe('test');
        expect(formService.getFieldValue).toHaveBeenCalledTimes(1);
      });

      it('should get render properties', function () {
        // given
        var ctrl = buildController();
        var renderConfig = {id: 'renderConfig'};

        fieldTypeRegistry.getRenderProperty.and.returnValue(renderConfig);

        // when
        var result = ctrl.getConfig({key: 'field-key'});

        // then
        expect(result).toBe(renderConfig);
      });

      it('should sort form entries by created', function () {
        // given
        var ctrl = buildController();
        ctrl.sortConfig = {};
        FormEntryModel.pagedQuery.and.returnValue($q.resolve({}));

        // when
        ctrl.sortOnCreated();

        // then
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(ctrl.sortConfig.id).toBe('created');
        expect(ctrl.sortConfig.sortOn).toBe('created');
        expect(ctrl.sortConfig.dir).toBe('ASC');
      });

      it('should sort desc when sort is called two times', function () {
        // given
        var ctrl = buildController();
        ctrl.sortConfig = {};
        FormEntryModel.pagedQuery.and.returnValue($q.resolve({}));
        ctrl.sortOnCreated();
        $scope.$apply();

        // when
        ctrl.sortOnCreated();

        // then
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(2);
        expect(ctrl.sortConfig.id).toBe('created');
        expect(ctrl.sortConfig.sortOn).toBe('created');
        expect(ctrl.sortConfig.dir).toBe('DESC');
      });

      it('should undefine sort when sort is called three times', function () {
        // given
        var ctrl = buildController();
        ctrl.sortConfig = {};
        FormEntryModel.pagedQuery.and.returnValue($q.resolve({}));
        ctrl.sortOnCreated();
        $scope.$apply();
        ctrl.sortOnCreated();
        $scope.$apply();

        // when
        ctrl.sortOnCreated();

        // then
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(3);
        expect(ctrl.sortConfig.id).toBeUndefined();
        expect(ctrl.sortConfig.sortOn).toBeUndefined();
        expect(ctrl.sortConfig.dir).toBeUndefined();
      });

      it('should sort form entries by author', function () {
        // given
        var ctrl = buildController();
        ctrl.sortConfig = {};
        FormEntryModel.pagedQuery.and.returnValue($q.resolve({}));

        // when
        ctrl.sortOnAuthor();

        // then
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(ctrl.sortConfig.id).toBe('author');
        expect(ctrl.sortConfig.sortOn).toBe('author.displayName');
        expect(ctrl.sortConfig.dir).toBe('ASC');
      });

      it('should sort form entries by dynamic field', function () {
        // given
        var ctrl = buildController();
        ctrl.sortConfig = {};
        FormEntryModel.pagedQuery.and.returnValue($q.resolve({}));
        fieldTypeRegistry.get.and.returnValue({sortOn: 'sortOnProp'});

        // when
        ctrl.sortOnField({id: 'field-id'});

        // then
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(ctrl.sortConfig.id).toBe('field-id');
        expect(ctrl.sortConfig.sortOn).toBe('values.field-id.sortOnProp');
        expect(ctrl.sortConfig.dir).toBe('ASC');
      });

      it('should delete entry', function () {
        // given
        var ctrl = buildController();
        ctrl.app = app;
        ctrl.sortConfig = {};
        var entry = jasmine.createSpyObj('entry', ['delete']);

        entry.delete.and.returnValue($q.resolve({}));

        modalService.confirmDelete.and.returnValue({result: $q.resolve()});

        // when
        ctrl.deleteEntry(entry);
        $scope.$apply();

        // then
        expect(modalService.confirmDelete).toHaveBeenCalledTimes(1);
        expect(entry.delete).toHaveBeenCalledTimes(1);
        expect(FormEntryModel.pagedQuery).toHaveBeenCalledTimes(1);
      });

      it('should only show the latest queried data', function () {
        // given
        var ctrl = buildController();
        ctrl.sortConfig = {};
        var deferred = $q.defer();

        FormEntryModel.pagedQuery.and.callFake(function (pageable, queryParams) {
          if (queryParams.query === 'tes') {
            return deferred.promise.then(function () {
              $q.resolve({content: ['wrong']});
            });
          } else {
            deferred.resolve({content: ['correct']});
            return deferred.promise;
          }
        });

        // when
        ctrl.search = 'tes';
        ctrl.searchKeyPressed();
        ctrl.search = 'test';
        ctrl.searchKeyPressed();
        $scope.$apply();

        // then
        expect(formService.initEntries).toHaveBeenCalledTimes(1);
        expect(formService.initEntries).toHaveBeenCalledWith(['correct'], undefined);
      });
    });
  });

})();
