(function () {
  'use strict';

  var moduleName = 'coyo.apps.forum';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $rootScope, $scope, $state, $timeout, $element, $compile, app, thread;
    var backendUrlService, forumThreadService, forumAppConfig;

    beforeEach(module(moduleName));

    beforeEach(function () {
      inject(function (_$controller_, _$q_, _$rootScope_, _$timeout_, _$compile_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;
        $timeout = _$timeout_;
        $compile = _$compile_;

        $element = jasmine.createSpyObj('$element', ['find']);
        $state = jasmine.createSpyObj('$state', ['go']);

        app = jasmine.createSpyObj('app', ['']);
        app.id = 'app-id';
        app.senderId = 'app-sender-id';

        thread = jasmine.createSpyObj('thread', ['update', 'delete']);
        thread.id = 'thread-id';
        thread.senderId = app.senderId;

        backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);
        backendUrlService.getUrl.and.returnValue('http://localhost:8080');

        forumThreadService = jasmine.createSpyObj('forumThreadService', ['pin', 'unpin', 'close', 'reopen', 'delete']);

        forumAppConfig = {
          paging: {
            threads: {
              pageSize: 10
            },
            threadAnswers: {
              pageSize: 10
            }
          },
          endpoints: {
            thread: {
              preview: '/attachments/{{id}}'
            },
            threadAnswer: {
              preview: '/attachments/{{id}}'
            }
          },
          templates: {
            contextMenuList: 'app/apps/forum/templates/forum-thread-list-context-menu.html',
            contextMenuView: 'app/apps/forum/templates/forum-thread-view-context-menu.html',
            contextMenuAnswer: 'app/apps/forum/templates/forum-thread-answer-context-menu.html'
          }
        };
      });
    });

    var controllerName = 'ForumThreadViewController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope,
          $state: $state,
          $timeout: $timeout,
          $element: $element,
          $compile: $compile,
          backendUrlService: backendUrlService,
          forumThreadService: forumThreadService,
          app: app,
          thread: thread,
          forumAppConfig: forumAppConfig
        });
      }

      it('should set the app and the thread correctly', function () {
        // given
        var app = {
          id: 'app-id'
        };

        var thread = {
          id: 'thread-id'
        };

        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.backendUrl).toEqual('http://localhost:8080');
        expect(backendUrlService.getUrl).toHaveBeenCalled();
        expect(ctrl.app.id).toEqual(app.id);
        expect(ctrl.thread.id).toEqual(thread.id);
      });

      it('should pin a forum thread', function () {
        // given
        forumThreadService.pin.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.pin();
        $scope.$apply();

        // then
        expect(forumThreadService.pin).toHaveBeenCalledWith(thread);
      });

      it('should unpin a forum thread', function () {
        // given
        forumThreadService.unpin.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.unpin();
        $scope.$apply();

        // then
        expect(forumThreadService.unpin).toHaveBeenCalledWith(thread);
      });

      it('should close a forum thread', function () {
        // given
        forumThreadService.close.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.close();
        $scope.$apply();

        // then
        expect(forumThreadService.close).toHaveBeenCalledWith(thread);
      });

      it('should reopen a forum thread', function () {
        // given
        forumThreadService.reopen.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.reopen();
        $scope.$apply();

        // then
        expect(forumThreadService.reopen).toHaveBeenCalledWith(thread);
      });

      it('should delete a forum thread', function () {
        // given
        forumThreadService.delete.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.delete();
        $scope.$apply();

        // then
        expect(forumThreadService.delete).toHaveBeenCalledWith(thread);
        expect($state.go).toHaveBeenCalledWith('^');
      });

    });
  });

})();
