(function () {
  'use strict';

  var moduleName = 'coyo.apps.forum';
  var controllerName = 'ForumThreadListController';

  describe('module: ' + moduleName, function () {

    var $httpBackend, backendUrlService, $controller, $q, $rootScope, $scope, $state;
    var ForumThreadModel, app, forumAppConfig, forumThreadService;
    var thread;

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$httpBackend_, _$q_, _ForumThreadModel_, _$controller_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      backendUrlService = _backendUrlService_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      $rootScope.screenSize = {
        isXs: false,
        isSm: false,
        isMd: true,
        isLg: false,
        isRetina: true
      };
      $scope = $rootScope.$new();
      $controller = _$controller_;

      ForumThreadModel = _ForumThreadModel_;
      app = jasmine.createSpyObj('app', ['']);

      ForumThreadModel = jasmine.createSpyObj('ForumThreadModel', ['pagedQueryWithPermissions', 'count']);
      ForumThreadModel.pagedQueryWithPermissions.and.returnValue($q.resolve({totalElements: 0, content: []}));
      ForumThreadModel.count.and.returnValue($q.resolve(0));

      forumAppConfig = {
        paging: {
          threads: {
            pageSize: 10
          },
          threadAnswers: {
            pageSize: 10
          }
        },
        endpoints: {
          thread: {
            preview: '/attachments/{{id}}'
          },
          threadAnswer: {
            preview: '/attachments/{{id}}'
          }
        },
        templates: {
          contextMenu: 'app/apps/forum/forum-thread-list-context-menu.html'
        }
      };

      thread = jasmine.createSpyObj('thread', ['']);
      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

      forumThreadService = jasmine.createSpyObj('forumThreadService', ['pin', 'unpin', 'close', 'reopen', 'delete']);
    }));

    function buildController() {
      return $controller(controllerName, {
        $rootScope: $rootScope,
        $scope: $scope,
        $state: $state,
        app: app,
        ForumThreadModel: ForumThreadModel,
        forumAppConfig: forumAppConfig,
        forumThreadService: forumThreadService
      });
    }

    describe('controller init: ' + controllerName, function () {

      it('should initialize the controller correctly', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: true,
          includeClosed: true,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.threadCount).toBe(0);
        expect(ctrl.forumThreads.content.length).toBe(0);
        expect(ctrl.loading).toBe(false);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should pin a forum thread', function () {
        // given
        forumThreadService.pin.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.pin(thread);
        $scope.$apply();

        // then
        expect(forumThreadService.pin).toHaveBeenCalledWith(thread);
      });

      it('should unpin a forum thread', function () {
        // given
        forumThreadService.unpin.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.unpin(thread);
        $scope.$apply();

        // then
        expect(forumThreadService.unpin).toHaveBeenCalledWith(thread);
      });

      it('should close a forum thread', function () {
        // given
        forumThreadService.close.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.close(thread);
        $scope.$apply();

        // then
        expect(forumThreadService.close).toHaveBeenCalledWith(thread);
      });

      it('should reopen a forum thread', function () {
        // given
        forumThreadService.reopen.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.reopen(thread);
        $scope.$apply();

        // then
        expect(forumThreadService.reopen).toHaveBeenCalledWith(thread);
      });

      it('should delete a forum thread', function () {
        // given
        forumThreadService.delete.and.returnValue($q.resolve());
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        ctrl.delete(thread);
        $scope.$apply();

        // then
        expect(forumThreadService.delete).toHaveBeenCalledWith(thread);
      });

      it('should return whether a status filter is active, no filter active', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = true;
        ctrl.forumThreads._queryParams.includeClosed = true;

        // when
        var statusFilterActive = ctrl.statusFilterActive();

        // then
        expect(statusFilterActive).toBe(false);
      });

      it('should return whether a filter is active, only open filter active', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = true;
        ctrl.forumThreads._queryParams.includeClosed = false;

        // when
        var statusFilterActive = ctrl.statusFilterActive();

        // then
        expect(statusFilterActive).toBe(true);
      });

      it('should return whether a filter is active, only closed filter active', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = false;
        ctrl.forumThreads._queryParams.includeClosed = true;

        // when
        var statusFilterActive = ctrl.statusFilterActive();

        // then
        expect(statusFilterActive).toBe(true);
      });

      it('should return whether a search term filter is active, no filter active', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.searchTerm = '';

        // when
        var searchTermFilterActive = ctrl.searchTermFilterActive();

        // then
        expect(searchTermFilterActive).toBe(false);
      });

      it('should return whether a search term filter is active, filter active', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.searchTerm = 'The search term';

        // when
        var searchTermFilterActive = ctrl.searchTermFilterActive();

        // then
        expect(searchTermFilterActive).toBe(true);
      });

      it('should toggle include open filter, include true', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: false,
          includeClosed: true,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = true;
        ctrl.forumThreads._queryParams.includeClosed = true;

        // when
        ctrl.toggleIncludeOpen();

        // then
        expect(ctrl.forumThreads._queryParams.includeOpen).toBe(false);
        expect(ctrl.forumThreads._queryParams.includeClosed).toBe(true);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should toggle include open filter, include false', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: true,
          includeClosed: true,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = false;
        ctrl.forumThreads._queryParams.includeClosed = true;

        // when
        ctrl.toggleIncludeOpen();

        // then
        expect(ctrl.forumThreads._queryParams.includeOpen).toBe(true);
        expect(ctrl.forumThreads._queryParams.includeClosed).toBe(true);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should toggle include open filter, include true and include closed false', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: false,
          includeClosed: true,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = true;
        ctrl.forumThreads._queryParams.includeClosed = false;

        // when
        ctrl.toggleIncludeOpen();

        // then
        expect(ctrl.forumThreads._queryParams.includeOpen).toBe(false);
        expect(ctrl.forumThreads._queryParams.includeClosed).toBe(true);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should toggle include closed filter, include true', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: true,
          includeClosed: false,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = true;
        ctrl.forumThreads._queryParams.includeClosed = true;

        // when
        ctrl.toggleIncludeClosed();

        // then
        expect(ctrl.forumThreads._queryParams.includeOpen).toBe(true);
        expect(ctrl.forumThreads._queryParams.includeClosed).toBe(false);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should toggle include closed filter, include false', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: true,
          includeClosed: true,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = true;
        ctrl.forumThreads._queryParams.includeClosed = false;

        // when
        ctrl.toggleIncludeClosed();

        // then
        expect(ctrl.forumThreads._queryParams.includeOpen).toBe(true);
        expect(ctrl.forumThreads._queryParams.includeClosed).toBe(true);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should toggle include closed filter, include true and include open false', function () {
        // given
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: true,
          includeClosed: false,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: ''
        };
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.forumThreads._queryParams.includeOpen = false;
        ctrl.forumThreads._queryParams.includeClosed = true;

        // when
        ctrl.toggleIncludeClosed();

        // then
        expect(ctrl.forumThreads._queryParams.includeOpen).toBe(true);
        expect(ctrl.forumThreads._queryParams.includeClosed).toBe(false);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

      it('should search for a given term', function () {
        // given
        var searchTerm = 'The search term';
        var _queryParams = {
          _page: 0,
          _pageSize: forumAppConfig.paging.threads.pageSize,
          includeOpen: true,
          includeClosed: true,
          _orderBy: 'lastAnswerDate,desc',
          searchTerm: searchTerm
        };
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        ctrl.search(searchTerm);

        // then
        expect(ctrl.forumThreads._queryParams.searchTerm).toBe(searchTerm);
        expect(ForumThreadModel.pagedQueryWithPermissions).toHaveBeenCalledWith(
            undefined, _queryParams, {senderId: app.senderId, appId: app.id}, ['close', 'pin', 'delete']
        );
      });

    });
  });

})();
