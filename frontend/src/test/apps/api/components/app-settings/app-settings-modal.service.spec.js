(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var $scope, appSettingsModalService, appService, modalService, app, $q, SenderModel;

    beforeEach(function () {

      appService = jasmine.createSpyObj('appService', ['reloadApp']);
      SenderModel = jasmine.createSpyObj('SenderModel', ['get']);

      app = {
        id: 'TEST-ID'
      };

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: function (callback) {
            return callback(app);
          }
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('modalService', modalService);
        $provide.value('appService', appService);
        $provide.value('SenderModel', SenderModel);
      });

      inject(function (_appSettingsModalService_, $rootScope, _$q_) {
        $scope = $rootScope.$new();
        appSettingsModalService = _appSettingsModalService_;
        $q = _$q_;
      });

    });

    describe('service: appSettingsModalService', function () {

      it('should open settings and load and retrieve untranslated app and retrieve sender as result', function () {
        // given
        var loadedApp = {id: 'app-identifier'};
        var sender = jasmine.createSpyObj('sender', ['getApp']);
        sender.getApp.and.returnValue(loadedApp);
        SenderModel.get.and.returnValue($q.resolve(sender));

        // when
        appSettingsModalService.open(app, sender);
        $scope.$apply();

        // then
        expect(modalService.open).toHaveBeenCalled();

        var args = modalService.open.calls.mostRecent().args;
        expect(args[0].resolve.app()).toBe(loadedApp);
        expect(args[0].resolve.sender()).toBe(sender);
        expect(sender.getApp).toHaveBeenCalledWith('TEST-ID', {origin: true});
      });
    });
  });

})();
