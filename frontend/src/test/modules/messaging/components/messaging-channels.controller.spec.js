(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingChannelsController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, $timeout, $rootScope, $q, authService, socketService, MessageChannelModel;
    var userMock = {id: 123};

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _MessageChannelModel_, _$timeout_, _$q_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $timeout = _$timeout_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        authService = jasmine.createSpyObj('authService', ['onGlobalPermissions']);
        socketService = jasmine.createSpyObj('socketService', ['subscribe']);
        MessageChannelModel = _MessageChannelModel_;

        authService.onGlobalPermissions.and.callFake(function (permissionName, callback) {
          callback(permissionName === 'USE_MESSAGING');
        });
        spyOn(MessageChannelModel, 'pagedQuery').and.callThrough();
        spyOn($rootScope, '$emit').and.callThrough();
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {
          $scope: $scope,
          authService: authService,
          socketService: socketService
        }, {
          currentUser: userMock
        });
      }

      function channel(data) {
        return angular.extend(jasmine.createSpyObj('MessageChannelDomain', ['getUnreadCount']), data);
      }

      it('should init controller', function () {
        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.currentUser).toBe(userMock);
        expect(ctrl.channels).toEqual([]);
        expect(MessageChannelModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(socketService.subscribe).toHaveBeenCalledWith('/user/topic/messaging', jasmine.any(Function), 'channelUpdated');
        expect(socketService.subscribe).toHaveBeenCalledWith('/user/topic/messaging', jasmine.any(Function), 'channelLeave');
      });

      it('should load more', function () {
        // given
        var ctrl = buildCtrl();
        var page = {
          content: [{
            id: 'channel-1',
            lastCreatedMessage: {
              attachments: [],
              data: {
                message: 'message'
              }
            },
            getUnreadCount: function () {
              return 0;
            }
          }],
          totalElements: 1
        };

        MessageChannelModel.pagedQuery.and.returnValue({
          then: function (callback) {
            callback(page);
            return {
              'finally': function (callback) {
                callback();
              }
            };
          }
        });

        // when
        ctrl.$onInit();

        // then
        expect(MessageChannelModel.pagedQuery).toHaveBeenCalledTimes(1);
        expect(ctrl.channels).toEqual(page.content);
        expect(ctrl.currentPage).toBe(page);
        expect(ctrl.loading).toBeFalsy();

        // when loading again
        ctrl.loadMore();

        // then
        expect(MessageChannelModel.pagedQuery).toHaveBeenCalledTimes(1); // no more calls than before
      });

      it('should add new and updated channels on reconnect', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.channels = [
          channel({id: 'old1', updated: 1}),
          channel({id: 'old2', updated: 1})
        ];
        MessageChannelModel.pagedQuery.and.returnValue($q.resolve({
          content: [
            channel({id: 'old1', updated: 1}),
            channel({id: 'old2', updated: 2}),
            channel({id: 'new1', updated: 1}),
            channel({id: 'new2', updated: 1})
          ]
        }));

        // when
        $rootScope.$emit('socketService:reconnected');
        $scope.$apply();
        $timeout.flush();

        // then
        expect(ctrl.channels.length).toBe(4);
        expect(ctrl.channels[0].id).toBe('new2');
        expect(ctrl.channels[1].id).toBe('new1');
        expect(ctrl.channels[2].id).toBe('old2');
        expect(ctrl.channels[2].updated).toBe(2);
        expect(ctrl.channels[3].id).toBe('old1');
        expect($rootScope.$emit).toHaveBeenCalledWith('messaging-channel:old2:updated');
        expect($rootScope.$emit).toHaveBeenCalledWith('messaging-channel:new1:updated');
        expect($rootScope.$emit).toHaveBeenCalledWith('messaging-channel:new2:updated');
        expect($rootScope.$emit).not.toHaveBeenCalledWith('messaging-channel:old1:updated');
      });

      it('should trigger refresh if all channels are new or updated', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        ctrl.channels = [
          channel({id: 'old1', updated: 1}),
          channel({id: 'old2', updated: 1})
        ];
        var newPage = {content: []};
        for (var i = 0; i < 25; i++) {
          newPage.content.push(channel({id: 'new' + i, updated: 1}));
        }
        MessageChannelModel.pagedQuery.and.returnValue($q.resolve(newPage));

        // when
        $rootScope.$emit('socketService:reconnected');
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect($rootScope.$emit).toHaveBeenCalledWith('messaging-channels:refresh');
      });
    });
  });
})();
