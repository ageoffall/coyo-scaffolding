(function () {
  'use strict';

  var moduleName = 'coyo.admin.userManagement';
  var controllerName = 'AdminRoleListController';

  describe('module: ' + moduleName, function () {

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    describe('controller: ' + controllerName, function () {
      var $controller, $rootScope, $scope, ctrl, role, RoleModel, emptyResolve, $log, $sessionStorage, modalService;

      beforeEach(inject(function (_$controller_, _$rootScope_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;

        emptyResolve = {
          then: function (thenCallback) {
            thenCallback();
            return {
              finally: function (finallyCallback) {
                finallyCallback();
              }
            };
          }
        };

        // mock screensize information
        $rootScope.screenSize = {
          isXs: false,
          isSm: false,
          isMd: true,
          isLg: false,
          isRetina: true
        };

        RoleModel = jasmine.createSpyObj('RoleModel', ['pagedQueryWithPermissions']);
        mockRoleSearch({});

        modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
        $log = jasmine.createSpyObj('$log', ['debug']);
        $sessionStorage = {};
        role = jasmine.createSpyObj('role', ['delete']);
      }));

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          RoleModel: RoleModel,
          $log: $log,
          $sessionStorage: $sessionStorage,
          modalService: modalService
        });
      }

      function mockRoleSearch(result) {
        RoleModel.pagedQueryWithPermissions.and.returnValue({
          then: function (callback) {
            callback(result);
            return {
              finally: function (callback) {
                callback();
              }
            };
          }
        });
      }

      describe('controller init', function () {
        beforeEach(function () {
          mockRoleSearch({content: 'testdata', _queryParams: {_page: 0}, number: 0});
        });

        it('should perform initial search', function () {
          // given

          // when
          ctrl = buildController();

          // then
          expect(ctrl.page.content).toBe('testdata');
          expect(ctrl.page._queryParams._page).toBe(0);
          expect(ctrl.page.loading).toBe(false);
          expect(ctrl.firstLoad).toBe(false);
          expect(RoleModel.pagedQueryWithPermissions).toHaveBeenCalled();
        });

        it('should initialize default query parameters', function () {
          // when
          ctrl = buildController();

          // then
          expect(RoleModel.pagedQueryWithPermissions).toHaveBeenCalled();
          expect(RoleModel.pagedQueryWithPermissions.calls.mostRecent().args[1]).toEqual({
            _page: 0,
            _pageSize: 10,
            _orderBy: 'displayName'
          });
          expect(RoleModel.pagedQueryWithPermissions.calls.mostRecent().args[3]).toEqual(['manage', 'delete']);
        });

        it('should initialize deviating page size on mobile', function () {
          // given
          $rootScope.screenSize.isXs = true;
          $rootScope.screenSize.isMd = false;

          // when
          ctrl = buildController();

          // then
          expect(RoleModel.pagedQueryWithPermissions).toHaveBeenCalled();
          expect(RoleModel.pagedQueryWithPermissions.calls.mostRecent().args[1]._pageSize).toBe(30);
        });

        it('should use query parameter from session storage', function () {
          // given
          $sessionStorage.roleList = {orderBy: 'xyz'};

          // when
          ctrl = buildController();

          // then
          expect(RoleModel.pagedQueryWithPermissions).toHaveBeenCalled();
          expect(RoleModel.pagedQueryWithPermissions.calls.mostRecent().args[1]).toEqual({
            orderBy: 'xyz',
            _pageSize: 10
          });
        });
      });

      describe('controller reinit', function () {
        it('should sync query params with current page number', function () {
          // given
          mockRoleSearch({content: 'testdata', _queryParams: {_page: 0}, number: 1});

          // when
          ctrl = buildController();

          // then
          expect(ctrl.page._queryParams._page).toBe(1);

        });
      });

      describe('controller active', function () {
        beforeEach(function () {
          mockRoleSearch({content: 'testdata', _queryParams: {_page: 0}, number: 0});
          ctrl = buildController();
        });

        describe('next page', function () {
          beforeEach(function () {
            ctrl.page = jasmine.createSpyObj('Page', ['nextAppended']);
          });

          it('should not call page when already at last page', function () {
            // given
            ctrl.page.content = ['test'];
            ctrl.page.last = true;

            // when
            ctrl.nextPage();

            // then
            expect(ctrl.page.nextAppended).not.toHaveBeenCalled();
          });

          it('should not call page when list is empty', function () {
            // given
            ctrl.page.content = [];

            // when
            ctrl.nextPage();

            // then
            expect(ctrl.page.nextAppended).not.toHaveBeenCalled();
          });

          it('should call page append', function () {
            // given
            ctrl.page.content = ['test'];

            // when
            ctrl.nextPage();

            // then
            expect(ctrl.page.nextAppended).toHaveBeenCalled();
          });
        });

        describe('actions', function () {

          it('should delete role', function () {
            //given
            role.delete.and.returnValue(emptyResolve);
            role.userCount = 10;
            modalService.confirmDelete.and.returnValue({result: emptyResolve});
            ctrl.page._queryParams = {};

            // when
            ctrl.actions.deleteRole(role);

            // then
            expect(role.delete).toHaveBeenCalled();
            expect(modalService.confirmDelete).toHaveBeenCalled();
            var args = modalService.confirmDelete.calls.mostRecent().args;
            expect(args[0].title).toBe('ADMIN.USER_MGMT.ROLES.OPTIONS.DELETE.MODAL.TITLE');
            expect(args[0].text).toBe('ADMIN.USER_MGMT.ROLES.OPTIONS.DELETE.MODAL.TEXT');
            expect(args[0].translationContext.userCount).toBe(10);
            expect(RoleModel.pagedQueryWithPermissions).toHaveBeenCalled();
          });

        });
      });
    });
  });
})();
