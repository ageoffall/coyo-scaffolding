(function () {
  'use strict';

  var moduleName = 'coyo.admin.authenticationProviders.api';

  describe('module: ' + moduleName, function () {

    var authenticationProviderTypeRegistryProvider, authenticationProviderTypeRegistry;

    beforeEach(function () {
      // initialize the service provider by injecting it to a fake module's config block
      angular.module('coyo.admin.authenticationProviders.test', ['coyo.admin.authenticationProviders.api']);
      angular.module('coyo.admin.authenticationProviders.test').config(function (_authenticationProviderTypeRegistryProvider_) {
        authenticationProviderTypeRegistryProvider = _authenticationProviderTypeRegistryProvider_;

        authenticationProviderTypeRegistryProvider.register({
          key: 'saml',
          name: 'ADMIN.AUTHENTICATION.SAML.NAME',
          description: 'ADMIN.AUTHENTICATION.SAML.DESCRIPTION',
          directive: 'oyoc-saml-settings'
        });

        authenticationProviderTypeRegistryProvider.register({
          key: 'saml2',
          name: 'ADMIN.AUTHENTICATION.SAML_TWO.NAME',
          description: 'ADMIN.AUTHENTICATION.SAML_TWO.DESCRIPTION',
          directive: 'oyoc-saml-two-settings'
        });
      });

      // initialize test.app injector
      module('coyo.admin.authenticationProviders.test');
    });

    beforeEach(inject(function (_authenticationProviderTypeRegistry_) {
      authenticationProviderTypeRegistry = _authenticationProviderTypeRegistry_;
    }));

    describe('service: authenticationProviderTypeRegistry', function () {

      it('should get all registered authentication provider', function () {
        // when
        var provider = authenticationProviderTypeRegistry.getAll();

        // then
        expect(provider.length).toBe(2);
      });

      it('should get a registered authentication provider', function () {
        // when
        var provider = authenticationProviderTypeRegistry.get('saml');

        // then
        expect(provider.name).toBe('ADMIN.AUTHENTICATION.SAML.NAME');
      });

      it('should return null if requested authentication provider is not available', function () {
        // when
        var provider = authenticationProviderTypeRegistry.get('saml3');

        // then
        expect(provider).toBeNull();
      });
    });
  });

  describe('module: ' + moduleName, function () {

    describe('provider: authenticationProviderTypeRegistryProvider', function () {

      // Create a fake module with the provider. You can use this method in any test
      function createModule(authenticationProviderConfig) {
        angular.module('coyo.admin.authenticationProviders.test', ['coyo.admin.authenticationProviders.api']);
        angular.module('coyo.admin.authenticationProviders.test').config(function (_authenticationProviderTypeRegistryProvider_) {
          // Call methods on the provider in config phase here
          _authenticationProviderTypeRegistryProvider_.register(authenticationProviderConfig);
        });

        module('coyo.admin.authenticationProviders.test');
        inject(function () {
        });
      }

      it('should register a valid authentication provider type', function () {
        // given
        var config = {
          key: 'test-authentication-provider-type',
          name: 'Test Authentication Provider Type',
          description: 'A Authentication Provider Type',
          directive: 'example-authentication-provider-type-settings'
        };

        // when
        createModule(config);

        // then -> perform without error
      });

      it('should fail if key is missing', function () {
        // given
        var config = {
          name: 'Test Authentication Provider Type',
          description: 'A Authentication Provider Type',
          directive: 'example-authentication-provider-type-settings'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "key" is required/);
      });

      it('should fail if name is missing', function () {
        // given
        var config = {
          key: 'test-authentication-provider-type',
          description: 'A Authentication Provider Type',
          directive: 'example-authentication-provider-type-settings'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "name" is required/);
      });

      it('should fail if description is missing', function () {
        // given
        var config = {
          key: 'test-authentication-provider-type',
          name: 'Test Authentication Provider Type',
          directive: 'example-authentication-provider-type-settings'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "description" is required/);
      });

      it('should fail if directive is missing', function () {
        // given
        var config = {
          key: 'test-authentication-provider-type',
          name: 'Test Authentication Provider Type',
          description: 'A Authentication Provider Type'
        };

        // when
        function instantiate() {
          createModule(config);
        }

        // then
        expect(instantiate).toThrowError(/Config property "directive" is required/);
      });
    });
  });

})();
