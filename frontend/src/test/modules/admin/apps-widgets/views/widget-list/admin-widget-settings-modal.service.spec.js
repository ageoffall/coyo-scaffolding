(function () {
  'use strict';

  var moduleName = 'coyo.admin.apps-widgets';

  describe('module: ' + moduleName, function () {

    var adminWidgetSettingsModal, modalService;

    beforeEach(function () {

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: angular.noop
        }
      });

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('modalService', modalService);
      });

      inject(function (_adminWidgetSettingsModal_) {
        // inject dependencies for test
        adminWidgetSettingsModal = _adminWidgetSettingsModal_;
      });

    });

    describe('service: adminWidgetSettingsModal', function () {

      it('should open modal', function () {
        // given
        var widget = {id: 'widgetId'};

        // when
        adminWidgetSettingsModal.open(widget);

        // then
        expect(modalService.open).toHaveBeenCalled();
        var args = modalService.open.calls.mostRecent().args;
        expect(args[0].resolve.widget()).toEqual(widget);
      });

    });

    describe('controller: AdminWidgetSettingsModalController', function () {

      var $controller, $uibModalInstance, widget;

      beforeEach(function () {

        widget = {
          id: 'widgetId'
        };

        inject(function (_$controller_) {
          $controller = _$controller_;
          $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
        });
      });

      function buildController() {
        return $controller('AdminWidgetSettingsModalController', {
          $uibModalInstance: $uibModalInstance,
          widget: widget
        });
      }

      it('should init controller', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.widget.id).toBe(widget.id);
      });

      it('should check whether the widget is enabled, enabled', function () {
        // given
        widget.enabled = true;

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var isWidgetEnabled = ctrl.isWidgetEnabled();

        // then
        expect(isWidgetEnabled).toBe(true);
      });

      it('should check whether the widget is enabled, not enabled', function () {
        // given
        widget.enabled = false;

        // when
        var ctrl = buildController();
        ctrl.$onInit();
        var isWidgetEnabled = ctrl.isWidgetEnabled();

        // then
        expect(isWidgetEnabled).toBe(false);
      });

      it('should save', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.save();

        // then
        expect($uibModalInstance.close).toHaveBeenCalledWith(widget);
      });

    });
  });

})();
