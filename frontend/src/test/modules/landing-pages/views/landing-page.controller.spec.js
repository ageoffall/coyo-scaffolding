(function () {
  'use strict';

  var moduleName = 'coyo.landing-pages';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $q;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
      $controller = _$controller_;
      $q = _$q_;
      $rootScope = _$rootScope_;
    }));

    var controllerName = 'LandingPageController';

    describe('controller: ' + controllerName, function () {

      var ctrl, widgetLayoutService, $scope;
      var landingPages = [{id: 'PAGE1'}, {id: 'PAGE2'}];
      var landingPage = landingPages[0];

      beforeEach(function () {
        widgetLayoutService = jasmine.createSpyObj('widgetLayoutService', ['edit', 'save', 'cancel']);
        $scope = $rootScope.$new();
        ctrl = $controller(controllerName, {
          $scope: $scope,
          widgetLayoutService: widgetLayoutService,
          landingPages: landingPages,
          landingPage: landingPage
        });
      });

      it('should init landing page', function () {
        expect(ctrl.landingPages).toBe(landingPages);
        expect(ctrl.landingPage).toBe(landingPage);
      });

      it('should edit', function () {
        // when
        ctrl.edit();

        // then
        expect(ctrl.editMode).toBe(true);
        expect(widgetLayoutService.edit).toHaveBeenCalledWith($scope);
      });

      it('should save', function () {
        // given
        ctrl.editMode = true;
        widgetLayoutService.save.and.returnValue($q.resolve());

        // when
        ctrl.save();
        $rootScope.$apply();

        // then
        expect(ctrl.editMode).toBe(false);
        expect(widgetLayoutService.save).toHaveBeenCalledWith($scope);
      });

      it('should cancel', function () {
        // given
        ctrl.editMode = true;

        // when
        ctrl.cancel();

        // then
        expect(ctrl.editMode).toBe(false);
        expect(widgetLayoutService.cancel).toHaveBeenCalledWith($scope);
      });

    });
  });

})();
