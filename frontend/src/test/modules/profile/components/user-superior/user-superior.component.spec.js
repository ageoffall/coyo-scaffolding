(function () {
  'use strict';

  var moduleName = 'coyo.profile';
  var directiveName = 'oyocUserSuperior';

  describe('module: ' + moduleName, function () {

    var $controller, $q, UserModel, errorService, $scope;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $controller = _$controller_;
      }));

      var controllerName = 'UserSuperiorController';

      describe('controller: ' + controllerName, function () {
        function buildController(user) {
          UserModel = jasmine.createSpyObj('UserModel', ['get']);
          errorService = jasmine.createSpyObj('errorService', ['suppressNotification']);
          var controller = $controller(controllerName, {
            $scope: $scope,
            UserModel: UserModel,
            errorService: errorService
          });
          controller.user = user;
          return controller;
        }

        it('should set superior on init', function () {
          // given
          var user = {manager: '1-2-3-4'};
          var superior = {};
          var ctrl = buildController(user);

          UserModel.get.and.returnValue($q.resolve(superior));

          // when
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(ctrl.superior).toBe(superior);
          expect(errorService.suppressNotification).not.toHaveBeenCalled();
        });

        it('should supress error message and delete manager if failing to load superior', function () {
          // given
          var user = {manager: '1-2-3-4'};
          var ctrl = buildController(user);

          UserModel.get.and.returnValue($q.reject());

          // when
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(ctrl.user.manager).toBeUndefined();
          expect(errorService.suppressNotification).toHaveBeenCalled();
        });
      });

    });

  });
})();
