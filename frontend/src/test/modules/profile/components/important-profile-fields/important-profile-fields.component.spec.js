(function () {
  'use strict';

  var moduleName = 'coyo.profile';
  var directiveName = 'oyocImportantProfileFields';

  describe('module: ' + moduleName, function () {

    var $controller;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_) {
        $controller = _$controller_;
      }));

      var controllerName = 'ImportantProfileFieldsController';

      describe('controller: ' + controllerName, function () {

        function buildController(user, profileGroups) {
          return $controller(controllerName, {}, {
            user: user,
            profileGroups: profileGroups
          });
        }

        it('should fetch important fields on init', function () {
          // given
          var user = {
            properties: {
              phone: '123456789',
              department: 'Marketing',
              location: 'Hamburg',
              twitter: '@test'
            }
          };
          var profileGroups = [
            {
              name: 'contact',
              fields: [
                {name: 'phone', type: 'PHONE', order: 0, important: true},
                {name: 'mobile', type: 'PHONE', order: 1},
                {name: 'twitter', type: 'LINK', order: 2}]
            }, {
              name: 'work',
              fields: [
                {name: 'jobTitle', type: 'TEXT', order: 0},
                {name: 'company', type: 'TEXT', order: 1},
                {name: 'department', type: 'TEXT', order: 2, important: true}]
            }
          ];
          var ctrl = buildController(user, profileGroups);

          // when
          ctrl.$onInit();

          // then
          expect(ctrl.importantFields).toBeArrayOfSize(2);
          expect(ctrl.importantFields[0].name).toBe('phone');
          expect(ctrl.importantFields[0].important).toBe(true);
          expect(ctrl.importantFields[1].name).toBe('department');
          expect(ctrl.importantFields[1].important).toBe(true);
        });

      });

    });

  });
})();
