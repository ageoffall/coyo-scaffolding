(function () {
  'use strict';

  var moduleName = 'coyo.login';

  describe('module: ' + moduleName, function () {
    var $controller, $window, authenticationProviderConfigs, $rootScope, $scope, $interval, $timeout, backendUrlService,
        setupService, $state, $stateParams, $q, authService, errorService, $translate, themeService,
        mobileEventsService, deeplinkService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$interval_, _$timeout_, _$q_) {
      $controller = _$controller_;
      authenticationProviderConfigs = [];
      $window = {};
      $rootScope = _$rootScope_;
      $q = _$q_;
      $interval = _$interval_;
      $timeout = _$timeout_;
      $scope = $rootScope.$new();
      backendUrlService = jasmine.createSpyObj('backendUrlService', ['isSet', 'getUrl', 'isConfigurable']);
      setupService = jasmine.createSpyObj('setupService', ['check']);
      $state = jasmine.createSpyObj('$state', ['go', 'href']);
      $stateParams = {errorCode: null};
      authService = jasmine.createSpyObj('authService', ['isAuthenticated', 'login', 'getLastLogin']);
      errorService = jasmine.createSpyObj('errorService', ['suppressNotification']);
      $translate = jasmine.createSpy();
      themeService = jasmine.createSpyObj('themeService', ['applyTheme']);
      mobileEventsService = jasmine.createSpyObj('mobileEventsService', ['propagate']);
      deeplinkService = jasmine.createSpyObj('deeplinkService', ['getReturnToState', 'getReturnToStateParams', 'clearReturnToState']);
    }));

    var controllerName = 'LoginMainController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          authenticationProviderConfigs: authenticationProviderConfigs,
          $window: $window,
          $scope: $scope,
          $state: $state,
          $stateParams: $stateParams,
          backendUrlService: backendUrlService,
          setupService: setupService,
          authService: authService,
          errorService: errorService,
          $translate: $translate,
          themeService: themeService,
          mobileEventsService: mobileEventsService
        });
      }

      describe('init', function () {

        it('should redirect to main if already logged in', function () {
          // given
          var ctrl = buildController();
          backendUrlService.isSet.and.returnValue(true);
          authService.isAuthenticated.and.returnValue(true);
          themeService.applyTheme.and.returnValue($q.resolve());

          // when
          ctrl.$onInit();

          // then
          expect($state.go).toHaveBeenCalledWith('main');
        });

        it('should redirect to setup if check fails', function () {
          // given
          var ctrl = buildController();
          backendUrlService.isSet.and.returnValue(true);
          setupService.check.and.returnValue($q.resolve(false));

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect($state.go).toHaveBeenCalledWith('setup');
        });

        it('should trigger auto login on init', function () {
          // given
          var ctrl = buildController();
          authenticationProviderConfigs.push({slug: 'slug1', autoLogin: false});
          authenticationProviderConfigs.push({slug: 'slug2', autoLogin: true});
          backendUrlService.isSet.and.returnValue(true);
          setupService.check.and.returnValue($q.resolve(true));
          backendUrlService.isConfigurable.and.returnValue(true);
          backendUrlService.getUrl.and.returnValue('http://foo');

          // when
          ctrl.$onInit();
          expect($window.location).toBeUndefined();
          $interval.flush(2000);

          // then
          expect(mobileEventsService.propagate).toHaveBeenCalledWith('authService:provider', {
            provider: [
              {loginUrl: 'http://foo/web/sso/login/slug1'},
              {loginUrl: 'http://foo/web/sso/login/slug2'}
            ]
          });
          expect($window.location).toBe('http://foo/web/sso/login/slug2');
          expect(ctrl.autoLogin).toBe(true);
        });

        it('should show initial error and suppress auto login', function () {
          // given
          $stateParams = {errorCode: 'SSO_LOGIN_DENIED'};
          var ctrl = buildController();
          backendUrlService.isSet.and.returnValue(true);
          setupService.check.and.returnValue($q.resolve(true));
          $translate.and.returnValue($q.resolve('Authentication failed'));

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.autoLogin).toBe(false);
          expect(ctrl.status.nrOfErrors).toBe(1);
          expect(ctrl.status.error).toBeTruthy();
          expect($translate).toHaveBeenCalledWith('MODULE.LOGIN.AUTHENTICATION_FAILED');
        });

        it('should show already registered error and suppress auto login', function () {
          // given
          $stateParams = {errorCode: 'ALREADY_REGISTERED'};
          var ctrl = buildController();
          backendUrlService.isSet.and.returnValue(true);
          setupService.check.and.returnValue($q.resolve(true));
          $translate.and.returnValue($q.resolve('Already registered.'));

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.autoLogin).toBe(false);
          expect(ctrl.status.nrOfErrors).toBe(1);
          expect(ctrl.status.error).toBeTruthy();
          expect($translate).toHaveBeenCalledWith('MODULE.LOGIN.ALREADY_REGISTERED');
        });

        it('should log in locally immediately if a password is given', function () {
          $stateParams = {username: 'not.your@business.com', password: 'secret'};
          var ctrl = buildController();
          setupService.check.and.returnValue($q.resolve(true));
          backendUrlService.isSet.and.returnValue(true);
          backendUrlService.isConfigurable.and.returnValue(true);
          backendUrlService.getUrl.and.returnValue('http://foo');
          authService.login.and.returnValue($q.resolve());

          // when
          ctrl.$onInit();
          $rootScope.$apply();
          $timeout.flush();

          // then
          expect(ctrl.autoLogin).toBe(false);
          expect(authService.login).toHaveBeenCalledWith('not.your@business.com', 'secret');
        });

        it('should show login page', function () {
          var ctrl = buildController();
          authenticationProviderConfigs.push({slug: 'slug1', autoLogin: false});
          setupService.check.and.returnValue($q.resolve(true));
          backendUrlService.isSet.and.returnValue(true);
          backendUrlService.isConfigurable.and.returnValue(true);
          backendUrlService.getUrl.and.returnValue('http://foo');

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(mobileEventsService.propagate).toHaveBeenCalledWith('authService:provider', {
            provider: [{loginUrl: 'http://foo/web/sso/login/slug1'}]
          });
          expect($state.go).not.toHaveBeenCalled();
          expect(ctrl.autoLogin).toBe(false);
          expect(ctrl.status.error).toBeFalsy();
          expect(ctrl.status.nrOfErrors).toBe(0);
        });
      });

      describe('login', function () {

        it('should show blocked error if user is blocked', function () {
          // given
          var ctrl = buildController();
          var deferred = $q.defer();
          authService.login.and.returnValue(deferred.promise);
          ctrl.status = {nrOfErrors: 0};
          ctrl.user = {};

          // when
          ctrl.login();
          deferred.reject({status: 403, data: {errorCode: 'USER_BLOCKED'}});
          $rootScope.$apply();

          // then
          expect(ctrl.status.nrOfErrors).toBe(1);
          expect($translate).toHaveBeenCalledWith('MODULE.LOGIN.AUTHENTICATION_BLOCK.ERROR');
        });

        it('should redirect to page root if no target state is defined', function () {
          // given
          $window = {
            location: {
              href: ''
            }
          };
          var ctrl = buildController();
          var loginResult = $q.resolve();
          authService.login.and.returnValue(loginResult);
          ctrl.user = {};
          ctrl.status = {nrOfErrors: 0};
          deeplinkService.getReturnToState.and.returnValue(undefined);
          deeplinkService.getReturnToStateParams.and.returnValue(undefined);
          $state.href.and.returnValue('');

          // when
          ctrl.login();
          $rootScope.$apply();

          // then
          expect($window.location.href).toBe('/');
        });
      });
    });
  });
})();
