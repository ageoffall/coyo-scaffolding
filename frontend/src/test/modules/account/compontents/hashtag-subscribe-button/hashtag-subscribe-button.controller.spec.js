(function () {
  'use strict';

  var moduleName = 'coyo.account';
  var targetName = 'HashtagSubscriptionButtonController';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, $injector;
    var hashtagServiceMock, hashtagServiceSubscriptionMock, authServiceMock;

    beforeEach(function () {
      module(moduleName, function () {
      });

      inject(function (_$injector_, _$controller_, _$rootScope_, _$httpBackend_, _backendUrlService_, _$q_) {
        $controller = _$controller_;
        $q = _$q_;
        $injector = _$injector_;
        $scope = _$rootScope_.$new();

        hashtagServiceMock = jasmine.createSpyObj('hashtagService', ['isConcludedHashtag']);
        hashtagServiceSubscriptionMock =
            jasmine.createSpyObj('hashtagSubscriptionsService', ['subscribe', 'unsubscribe', 'getSubscriptions']);
        spyOn($injector, 'get').and.callFake(function (arg) {
          if (arg === 'ngxHashtagService') {
            return hashtagServiceMock;
          } else {
            return undefined;
          }
        });

        authServiceMock = jasmine.createSpyObj('authService', ['canUseHashtags', 'getCurrentUserId']);
        authServiceMock.canUseHashtags.and.returnValue(true);
        authServiceMock.getCurrentUserId.and.returnValue('userId');
      });
    });

    describe('controller: ' + targetName, function () {

      function buildController() {
        return $controller(targetName, {
          hashtagSubscriptionsService: hashtagServiceSubscriptionMock,
          authService: authServiceMock
        });
      }

      it('should populate subscribed hashtags if search is a hashtag search', function () {
        // given
        var ctrl = buildController();
        var subscribedHashtags = ['#coyo', '#yolo'];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(true);
        hashtagServiceSubscriptionMock.getSubscriptions.and.returnValue($q.resolve(subscribedHashtags));
        ctrl.term = '#coyo';

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.subscribedHashtags).toEqual(subscribedHashtags);
      });

      it('should not populate subscribed hashtags if search is no hashtag search', function () {
        // given
        var ctrl = buildController();
        var subscribedHashtags = ['#coyo', '#yolo'];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(false);
        hashtagServiceSubscriptionMock.getSubscriptions.and.returnValue($q.resolve(subscribedHashtags));
        ctrl.term = 'coyo';

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.subscribedHashtags).toBeUndefined();
      });

      it('should call hashtagService', function () {

        // given
        var ctrl = buildController();
        ctrl.term = '#coyo';

        // when
        ctrl.isSearchTermAHashtag();

        // then
        expect(hashtagServiceMock.isConcludedHashtag).toHaveBeenCalled();

      });

      it('should return true if hashtag is subscribed hashtag', function () {

        // given
        var ctrl = buildController();
        var hashtag = '#coyo';
        ctrl.subscribedHashtags = [hashtag];
        ctrl.term = hashtag;

        // when
        var result = ctrl.isSubscribedHashtag();

        // then
        expect(result).toBeTruthy();
      });

      it('should return false if hashtag is not subscribed hashtag', function () {

        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = ['#yolo'];
        ctrl.term = '#coyo';

        // when
        var result = ctrl.isSubscribedHashtag();

        // then
        expect(result).toBeFalsy();
      });

      it('should return false if there are no subscribed hashtags', function () {

        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = undefined;
        ctrl.term = '#coyo';

        // when
        var result = ctrl.isSubscribedHashtag();

        // then
        expect(result).toBeFalsy();
      });

      it('should return true and ignore capitalization', function () {

        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = ['#coyo'];
        ctrl.term = '#Coyo';

        // when
        var result = ctrl.isSubscribedHashtag();

        // then
        expect(result).toBeTruthy();
      });

      it('should subscribe to hashtag when clicking it', function () {
        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = [];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(true);
        authServiceMock.getCurrentUserId.and.returnValue('user-id');
        spyOn(ctrl, 'isSubscribedHashtag').and.returnValue(false);
        hashtagServiceSubscriptionMock.subscribe.and.returnValue($q.resolve());
        ctrl.term = '#coyo';

        // when
        ctrl.onHashtagSubscribeButtonClick().then(function () {
          // then
          expect(ctrl.subscribedHashtags[0]).toBe('#coyo');
          expect(hashtagServiceSubscriptionMock.subscribe).toHaveBeenCalledWith('user-id', '#coyo');
        });
      });

      it('should unsubscribe from hashtag when clicking it', function () {
        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = ['#coyo'];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(true);
        authServiceMock.getCurrentUserId.and.returnValue('user-id');
        spyOn(ctrl, 'isSubscribedHashtag').and.returnValue(true);
        hashtagServiceSubscriptionMock.unsubscribe.and.returnValue($q.resolve());
        ctrl.term = '#coyo';

        // when
        ctrl.onHashtagSubscribeButtonClick().then(function () {
          // then
          expect(ctrl.subscribedHashtags.length).toBe(0);
          expect(hashtagServiceSubscriptionMock.unsubscribe).toHaveBeenCalledWith('user-id', '#coyo');
        });

      });

      it('should not add subscribed hashtag if subscribe request fails', function () {
        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = [];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(true);
        spyOn(ctrl, 'isSubscribedHashtag').and.returnValue(false);
        hashtagServiceSubscriptionMock.subscribe.and.returnValue($q.resolve());
        ctrl.term = '#coyo';

        // when
        ctrl.onHashtagSubscribeButtonClick().then(function () {
          // then
          expect(ctrl.subscribedHashtags.length).toBe(0);
          expect(hashtagServiceSubscriptionMock.subscribe).toHaveBeenCalledWith('user-id', '#coyo');
        });
      });

      it('should keep subscribed hashtag if unsubscribe request fails', function () {
        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = ['#coyo'];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(true);
        spyOn(ctrl, 'isSubscribedHashtag').and.returnValue(true);
        hashtagServiceSubscriptionMock.unsubscribe.and.returnValue($q.reject());
        ctrl.term = '#coyo';

        // when
        ctrl.onHashtagSubscribeButtonClick().then(function () {
          // then
          expect(ctrl.subscribedHashtags[0]).toBe('#coyo');
          expect(hashtagServiceSubscriptionMock.unsubscribe).toHaveBeenCalledWith('user-id', '#coyo');
        });
      });

      it('should add hashtag in uppercase', function () {
        // given
        var ctrl = buildController();
        ctrl.subscribedHashtags = [];
        hashtagServiceMock.isConcludedHashtag.and.returnValue(true);
        spyOn(ctrl, 'isSubscribedHashtag').and.returnValue(false);
        hashtagServiceSubscriptionMock.subscribe.and.returnValue($q.resolve());
        ctrl.term = '#Coyo';

        // when
        ctrl.onHashtagSubscribeButtonClick().then(function () {
          // then
          expect(ctrl.subscribedHashtags[0]).toBe('#COYO');
        });
      });
    });
  });
})();
