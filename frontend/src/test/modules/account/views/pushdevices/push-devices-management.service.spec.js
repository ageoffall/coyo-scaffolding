(function () {
  'use strict';

  var moduleName = 'coyo.account';
  var targetName = 'pushDevicesManagementService';

  describe('module: ' + moduleName, function () {
    var $rootScope, $q, authService, pushDevicesService, pushDevicesManagementService;

    var currentUser = jasmine.createSpyObj('user', ['hasGlobalPermissions']);
    currentUser.id = 'user-identifer';

    pushDevicesService =
        jasmine.createSpyObj('pushDevicesService', ['getPushDevices', 'deletePushDevice', 'togglePushDevice']);

    var vm, pushAppInstallations, appInstallation1, appInstallation2, appInstallation3;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('pushDevicesService', pushDevicesService);
      });

      inject(function (_$rootScope_, _$q_, _authService_, _pushDevicesManagementService_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        authService = _authService_;
        pushDevicesManagementService = _pushDevicesManagementService_;
      });

      spyOn(authService, 'getUser').and.returnValue($q.resolve(currentUser));

      // setup vm and push app installations
      vm = {};

      appInstallation1 = {
        userId: 'user-identifier',
        name: 'Google Pixel',
        active: true,
        appType: 'COYO',
        token: 'fcm-token-1',
        id: 'c5a11e5c-f8c3-45da-817d-013cce14a2ee',
        created: 1531224598571
      };

      appInstallation2 = {
        userId: 'user-identifier',
        name: 'Google Pixel 2',
        active: true,
        appType: 'COYO',
        token: 'fcm-token-2',
        id: 'dd392fe4-aa9c-4e15-96b8-9fb7b00d06ce',
        created: 1531224598572
      };

      appInstallation3 = {
        userId: 'user-identifier',
        name: 'Google Pixel 2',
        active: false,
        appType: 'MESSENGER',
        token: 'fcm-token-3',
        id: 'e05b295d-6b46-4d94-9dde-5f0c3fdb1d71',
        created: 1531224598573
      };

      pushAppInstallations = [appInstallation1, appInstallation2, appInstallation3];

      vm.pushDevices = {
        'Google Pixel': [appInstallation1],
        'Google Pixel 2': [appInstallation2, appInstallation3]
      };

      vm.pushDevicesAmount = 2;
      vm.pushDevicesDetails = {
        'Google Pixel': {
          created: 1531224598571,
          active: true
        },
        'Google Pixel 2': {
          created: 1531224598572,
          active: true
        }
      };
    });

    describe('service: ' + targetName, function () {

      it('should delete push app installation and remove device', function () {
        // given
        authService.getUser.and.returnValue($q.resolve(currentUser));
        currentUser.hasGlobalPermissions.and.returnValue(true);
        pushDevicesService.deletePushDevice.and.returnValue($q.resolve());

        // when
        pushDevicesManagementService.deletePushAppInstallation(vm, appInstallation1);
        $rootScope.$apply();

        // then
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('MANAGE_USER');

        expect(vm.pushDevicesAmount).toBe(1);
      });

      it('should delete push app installation and update device created and active status', function () {
        // given
        authService.getUser.and.returnValue($q.resolve(currentUser));
        currentUser.hasGlobalPermissions.and.returnValue(true);
        pushDevicesService.deletePushDevice.and.returnValue($q.resolve());

        // when
        pushDevicesManagementService.deletePushAppInstallation(vm, appInstallation2);
        $rootScope.$apply();

        // then
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('MANAGE_USER');

        expect(vm.pushDevicesAmount).toBe(2);
        expect(vm.pushDevicesDetails['Google Pixel 2'].created).toBe(1531224598573);
        expect(vm.pushDevicesDetails['Google Pixel 2'].active).toBe(false);
      });

      it('should toggle push app installation active state', function () {
        // given
        authService.getUser.and.returnValue($q.resolve(currentUser));
        currentUser.hasGlobalPermissions.and.returnValue(true);
        pushDevicesService.togglePushDevice.and.returnValue($q.resolve());

        // when
        pushDevicesManagementService.togglePushAppInstallationStatus(vm, appInstallation2);
        $rootScope.$apply();

        // then
        expect(currentUser.hasGlobalPermissions).toHaveBeenCalledWith('MANAGE_USER');

        expect(appInstallation2.active).toBe(false);
        expect(vm.pushDevicesDetails['Google Pixel 2'].active).toBe(false);
      });

      it('should prepare vm for provided app installations', function () {
        // given
        vm = {};
        authService.getUser.and.returnValue($q.resolve(currentUser));
        pushDevicesService.getPushDevices.and.returnValue($q.resolve(pushAppInstallations));

        // when
        pushDevicesManagementService.preparePushDevices(vm);
        $rootScope.$apply();

        // then
        expect(pushDevicesService.getPushDevices).toHaveBeenCalledWith(currentUser);

        expect(vm.pushDevices['Google Pixel']).toContain(appInstallation1);
        expect(vm.pushDevices['Google Pixel 2']).toContain(appInstallation2);
        expect(vm.pushDevices['Google Pixel 2']).toContain(appInstallation3);
        expect(vm.pushDevicesAmount).toBe(2);
        expect(vm.pushDevicesDetails['Google Pixel'].created).toBe(1531224598571);
        expect(vm.pushDevicesDetails['Google Pixel'].active).toBe(true);
        expect(vm.pushDevicesDetails['Google Pixel'].appsCollapsed).toBe(false);
        expect(vm.pushDevicesDetails['Google Pixel 2'].created).toBe(1531224598572);
        expect(vm.pushDevicesDetails['Google Pixel 2'].active).toBe(true);
        expect(vm.pushDevicesDetails['Google Pixel 2'].appsCollapsed).toBe(false);
      });
    });
  });
})();
