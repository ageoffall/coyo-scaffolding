(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var serviceName = 'eventDateSyncService';

  describe('module: ' + moduleName, function () {
    var $log, eventDateSyncService;

    beforeEach(function () {
      module(moduleName, function () {});

      inject(function (_$log_, _eventDateSyncService_) {
        $log = _$log_;
        eventDateSyncService = _eventDateSyncService_;
      });
    });

    describe('service: ' + serviceName, function () {

      it('should log an error message for invalid date', function () {
        // given
        var event = {
          startDate: 'invalid stuff',
          startTime: new Date('2013-01-01T12:00:00'),
          endDate: new Date('2013-01-02T08:00:00'),
          endTime: new Date('2013-01-02T08:00:00')
        };
        spyOn($log, 'error');

        // when
        eventDateSyncService.updateStartDate(event);

        // then
        expect($log.error).toHaveBeenCalled();
      });

      describe('Changing the date', function () {

        it('should update the linked end date if start date is changed', function () {
          // given
          var event = {
            startDate: new Date('2013-02-06T12:00:00'),
            startTime: new Date('2013-02-06T12:00:00'),
            endDate: new Date('2013-02-06T13:00:00'),
            endTime: new Date('2013-02-06T13:00:00')
          };

          // when
          event.startDate = new Date('2013-02-08T12:00:00');
          eventDateSyncService.updateStartDate(event, true);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-02-08T12:00:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-02-08T12:00:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-02-08T13:00:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-02-08T13:00:00').toISOString());
        });

        it('should not update the unlinked end date if start date is changed', function () {
          // given
          var event = {
            startDate: new Date('2013-01-01T12:00:00'),
            startTime: new Date('2013-01-01T12:00:00'),
            endDate: new Date('2013-01-08T12:00:00'),
            endTime: new Date('2013-01-08T12:00:00')
          };

          // when
          event.startDate = new Date('2013-01-02T12:00:00');
          eventDateSyncService.updateStartDate(event, false);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-08T12:00:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-08T12:00:00').toISOString());
        });

        it('should update the end date if start date is after the end date', function () {
          // given
          var event = {
            startDate: new Date('2013-01-01T12:00:00'),
            startTime: new Date('2013-01-01T12:00:00'),
            endDate: new Date('2013-01-02T12:00:00'),
            endTime: new Date('2013-01-02T12:00:00')
          };

          // when
          event.startDate = new Date('2013-01-03T12:00:00');
          eventDateSyncService.updateStartDate(event, false);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-03T12:00:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-03T12:00:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-03T13:00:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-03T13:00:00').toISOString());
        });

        it('should update the start date if end date is before the start date', function () {
          // given
          var event = {
            startDate: new Date('2013-01-02T12:00:00'),
            startTime: new Date('2013-01-02T12:00:00'),
            endDate: new Date('2013-01-02T13:00:00'),
            endTime: new Date('2013-01-02T13:00:00')
          };

          // when
          event.endDate = new Date('2013-01-01T13:00:00');
          eventDateSyncService.updateEndDate(event);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T12:00:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T12:00:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T13:00:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T13:00:00').toISOString());
        });

      });

      describe('Changing the date time', function () {

        it('should update the linked end date time if start date time is changed', function () {
          // given
          var event = {
            startDate: new Date('2013-01-02T12:00:00'),
            startTime: new Date('2013-01-02T12:00:00'),
            endDate: new Date('2013-01-02T13:00:00'),
            endTime: new Date('2013-01-02T13:00:00')
          };

          // when
          event.startTime = new Date('2013-01-02T14:30:00');
          eventDateSyncService.updateStartTime(event);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-02T14:30:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-02T14:30:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T15:30:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T15:30:00').toISOString());
        });

        it('should not update the unlinked end date time if start date time is changed', function () {
          // given
          var event = {
            startDate: new Date('2013-01-01T12:00:00'),
            startTime: new Date('2013-01-01T12:00:00'),
            endDate: new Date('2013-01-02T12:00:00'),
            endTime: new Date('2013-01-02T12:00:00')
          };

          // when
          event.startTime = new Date('2013-01-01T14:00:00');
          eventDateSyncService.updateStartTime(event, false);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T14:00:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T14:00:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T12:00:00').toISOString());
        });

        it('should update the end date time if start date time is after the end date time', function () {
          // given
          var event = {
            startDate: new Date('2013-01-02T12:00:00'),
            startTime: new Date('2013-01-02T12:00:00'),
            endDate: new Date('2013-01-02T13:00:00'),
            endTime: new Date('2013-01-02T13:00:00')
          };

          // when
          event.startTime = new Date('2013-01-02T13:37:00');
          eventDateSyncService.updateStartTime(event, false);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-02T13:37:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-02T13:37:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-02T14:37:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-02T14:37:00').toISOString());
        });

        it('should update the start date time if end date time is before the start date time', function () {
          // given
          var event = {
            startDate: new Date('2013-01-01T12:00:00'),
            startTime: new Date('2013-01-01T12:00:00'),
            endDate: new Date('2013-01-01T13:00:00'),
            endTime: new Date('2013-01-01T13:00:00')
          };

          // when
          event.endTime = new Date('2013-01-01T08:45:00');
          eventDateSyncService.updateEndTime(event);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T07:45:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T07:45:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T08:45:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T08:45:00').toISOString());
        });

        it('should update the start time if end date changes and times becomes invalid', function () {
          // given
          var event = {
            startDate: new Date('2013-01-01T12:00:00'),
            startTime: new Date('2013-01-01T12:00:00'),
            endDate: new Date('2013-01-02T08:00:00'),
            endTime: new Date('2013-01-02T08:00:00')
          };

          // when
          event.endDate = new Date('2013-01-01T08:00:00');
          eventDateSyncService.updateEndDate(event);

          // then
          expect(event.startDate.toISOString()).toBe(new Date('2013-01-01T07:00:00').toISOString());
          expect(event.startTime.toISOString()).toBe(new Date('2013-01-01T07:00:00').toISOString());
          expect(event.endDate.toISOString()).toBe(new Date('2013-01-01T08:00:00').toISOString());
          expect(event.endTime.toISOString()).toBe(new Date('2013-01-01T08:00:00').toISOString());
        });

      });

    });
  });

})();
