(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var controllerName = 'EventParticipationController';

  describe('module: ' + moduleName, function () {
    var $controller;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_) {
        $controller = _$controller_;
      });
    });

    function buildController(event) {
      return $controller(controllerName, {}, {
        event: event
      });
    }

    describe('controller: ' + controllerName, function () {
      it('should enable request definite answers', function () {
        // given
        var event = {
          requestDefiniteAnswer: true
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.allowedChoices[0]).toBe('ATTENDING');
        expect(ctrl.allowedChoices[1]).toBe('NOT_ATTENDING');
        expect(ctrl.allowedChoices[2]).toBeUndefined();
      });

      it('should disable request definite answers', function () {
        // given
        var event = {
          requestDefiniteAnswer: false
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.allowedChoices[0]).toBe('ATTENDING');
        expect(ctrl.allowedChoices[1]).toBe('MAYBE_ATTENDING');
        expect(ctrl.allowedChoices[2]).toBe('NOT_ATTENDING');
      });

      it('should initialize event status with PARTICIPATE if not set', function () {
        // given
        var event = {
          status: null
        };

        // when
        var ctrl = buildController(event);
        ctrl.$onInit();

        // then
        expect(ctrl.event.status).toBe('PARTICIPATE');
      });

    });
  });
})();
