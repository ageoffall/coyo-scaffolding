(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';
  var directiveName = 'oyocWorkspaceActions';

  describe('module: ' + moduleName, function () {
    var $controller, $q, $scope, $state;
    var workspace;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$rootScope_, _$q_, _$controller_) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = _$rootScope_.$new();
        $state = jasmine.createSpyObj('$state', ['go']);
        workspace = jasmine.createSpyObj('workspace', ['join', 'leave']);
      }));

      var controllerName = 'WorkspaceActionsController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          var ctrlFn = $controller(controllerName, {$state: $state}, true);
          ctrlFn.instance.workspace = workspace;
          ctrlFn.instance.workspace.slug = 'my-workspace';
          ctrlFn.instance.workspace.membershipStatus = 'NONE';
          return ctrlFn();
        }

        it('should join a workspace', function () {
          // given
          var ctrl = buildController();
          workspace.join.and.returnValue($q.resolve({status: 'APPROVED'}));

          // when
          ctrl.join();
          $scope.$apply();

          // then
          expect(workspace.join).toHaveBeenCalled();
          expect($state.go).toHaveBeenCalledWith('main.workspace.show', {idOrSlug: workspace.slug});
          expect(workspace.membershipStatus).toBe('APPROVED');
        });

        it('should leave a workspace', function () {
          // given
          var ctrl = buildController();
          workspace.membershipStatus = 'APPROVED';
          workspace.leave.and.returnValue($q.resolve());

          // when
          ctrl.leave();
          $scope.$apply();

          // then
          expect(workspace.leave).toHaveBeenCalled();
          expect(workspace.membershipStatus).toBe('NONE');
        });
      });
    });
  });

})();
