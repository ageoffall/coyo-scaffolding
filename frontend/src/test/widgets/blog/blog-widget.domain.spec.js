(function () {
  'use strict';

  describe('domain: BlogWidgetModel', function () {

    beforeEach(module('coyo.widgets.blog'));

    var $httpBackend, BlogWidgetModel, backendUrlService;
    var $rootScope;

    beforeEach(inject(function (_$rootScope_, _$httpBackend_, _BlogWidgetModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      BlogWidgetModel = _BlogWidgetModel_;
      backendUrlService = _backendUrlService_;

      $rootScope = _$rootScope_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('should find latest articles', function () {
        // given
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/widgets/blog/latest', {sourceSelection: 'ALL', count: 2, appIds: ['AppId']})
            .respond(200, ['article']);

        // when
        var result = null;

        BlogWidgetModel.getLatest('ALL', 2, false, ['AppId']).then(function (response) {
          result = response;
        });
        $rootScope.$apply();
        $httpBackend.flush();

        // then
        expect(result).toEqual(['article']);
      });
    });
  });
}());
