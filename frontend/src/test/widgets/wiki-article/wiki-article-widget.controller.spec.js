(function () {
  'use strict';

  var moduleName = 'coyo.widgets.wikiarticle';
  var directiveName = 'coyoWikiArticleWidget';

  describe('module: ' + moduleName, function () {

    var $controller, WikiArticleWidgetModel, targetService, $q, widget, $rootScope, $scope, widgetStatusService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        $scope = $rootScope.$new();
        WikiArticleWidgetModel = jasmine.createSpyObj('WikiArticleWidgetModel', ['getArticle']);
        WikiArticleWidgetModel.getArticle.and.returnValue($q.resolve());
        targetService = jasmine.createSpyObj('targetService', ['getLink']);
        targetService.getLink.and.returnValue('/some-link');
        widgetStatusService = jasmine.createSpyObj('widgetStatusService', ['refreshOnSettingsChange']);
        widget = {settings: {}};
      }));

      var controllerName = 'WikiArticleWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            WikiArticleWidgetModel: WikiArticleWidgetModel,
            targetService: targetService,
            $scope: $scope,
            widgetStatusService: widgetStatusService
          }, {
            widget: widget
          });
        }

        it('should load article', function () {
          // given
          var article = ['article'];
          widget.settings._articleId = 'articleId';
          WikiArticleWidgetModel.getArticle.and.returnValue($q.resolve(article));
          var ctrl = buildController();

          // when
          ctrl.loadArticle();
          $rootScope.$apply();

          // then
          expect(ctrl.widget.article).toEqual(article);
          expect(WikiArticleWidgetModel.getArticle).toHaveBeenCalledWith('articleId');
        });

        it('should register reload on settings change', function () {
          // when
          buildController();

          // then
          expect(widgetStatusService.refreshOnSettingsChange).toHaveBeenCalledWith($scope);
        });

        it('should call targetService.getLink', function () {
          // given
          var articles = [{
            articleTarget: {params: {id: 'articleId'}},
            senderTarget: {params: {id: 'senderId'}},
            appTarget: {params: {id: 'appId'}}
          }];
          WikiArticleWidgetModel.getArticle.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticle();
          $rootScope.$apply();

          // then
          expect(targetService.getLink).toHaveBeenCalled();
        });

        it('should build link object', function () {
          // given
          var articleId = 'a9asd-123sf';
          var senderId = '98jsf-33agz';
          var articles = [{
            articleTarget: {params: {id: articleId}},
            senderTarget: {params: {id: senderId}}
          }];
          WikiArticleWidgetModel.getArticle.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticle();
          $rootScope.$apply();

          // then
          expect(ctrl.links).not.toBeUndefined();
          expect(ctrl.links.article).toEqual('/some-link');
          expect(ctrl.links.sender).toEqual('/some-link');
        });

      });
    });
  });
})();
