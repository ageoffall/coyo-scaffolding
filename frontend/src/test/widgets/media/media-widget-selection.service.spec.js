(function () {
  'use strict';

  var moduleName = 'coyo.widgets.media';

  describe('module: ' + moduleName, function () {

    var $rootScope, $q, mediaWidgetSelectionService, fileLibraryModalService, SenderModel, appService;

    beforeEach(function () {
      fileLibraryModalService = jasmine.createSpyObj('fileLibraryModalService', ['open']);
      SenderModel = jasmine.createSpyObj('SenderModel', ['getCurrentIdOrSlug', 'getWithPermissions']);
      appService = jasmine.createSpyObj('appService', ['getCurrentAppIdOrSlug']);

      module(moduleName, function ($provide) {
        $provide.value('fileLibraryModalService', fileLibraryModalService);
        $provide.value('SenderModel', SenderModel);
        $provide.value('appService', appService);
      });

      inject(function (_$rootScope_, _$q_, _mediaWidgetSelectionService_) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        mediaWidgetSelectionService = _mediaWidgetSelectionService_;
      });
    });

    it('should open the file library with correct parameters', function () {
      // given
      var app = {id: 'app-id', rootFolderId: 'rootFolder-id'};
      var initialFolder = {id: app.rootFolderId};
      var selection = {id: 'file-id', senderId: 'sender-id'};

      var sender = jasmine.createSpyObj('SenderInstance', ['getApp']);
      sender.getApp.and.returnValue($q.resolve(app));
      sender.id = 'sender-id';

      SenderModel.getCurrentIdOrSlug.and.returnValue(sender.id);
      appService.getCurrentAppIdOrSlug.and.returnValue(app.id);
      SenderModel.getWithPermissions.and.returnValue($q.resolve(sender));
      fileLibraryModalService.open.and.returnValue($q.resolve(selection));

      // when
      mediaWidgetSelectionService.selectMedia();
      $rootScope.$apply();

      // then
      expect(fileLibraryModalService.open).toHaveBeenCalled();
      var args = fileLibraryModalService.open.calls.mostRecent().args;
      expect(args[0]).toBe(sender);
      expect(args[1].uploadMultiple).toBe(true);
      expect(args[1].selectMode).toBe('multiple');
      expect(args[1].filterContentType[0]).toBe('image');
      expect(args[1].filterContentType[1]).toBe('video');
      expect(args[1].initialFolder).toEqual(initialFolder);
    });
  });

})();
