(function () {
  'use strict';

  var moduleName = 'coyo.widgets.singlefile';

  describe('module: ' + moduleName, function () {

    var $controller, $rootScope, $scope, DocumentModel, FileModel, SenderModel, backendUrlService, $filter, $q;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$filter_, _$q_) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $filter = _$filter_;
      $q = _$q_;

      DocumentModel = jasmine.createSpyObj('DocumentModel', ['$url']);
      FileModel = jasmine.createSpyObj('FileModel', ['get']);
      SenderModel = jasmine.createSpyObj('SenderModel', ['get']);
      backendUrlService = jasmine.createSpyObj('backendUrlService', ['getUrl']);

      // Mocks
      SenderModel.get.and.returnValue($q.resolve({displayName: 'Max Mustermann'}));

      FileModel.get.and.returnValue($q.resolve({
        created: 'created_at',
        name: 'filename',
        contentType: 'image'
      }));
    }));

    var controllerName = 'SingleFileWidgetController';

    // Controller mit übergebener SenderId & FileId
    function buildController(senderId, fileId) {
      return $controller(controllerName, {
        $scope: $scope,
        $filter: $filter,
        DocumentModel: DocumentModel,
        FileModel: FileModel,
        SenderModel: SenderModel,
        backendUrlService: backendUrlService
      }, {
        widget: {
          settings: {
            _senderId: senderId,
            _fileId: fileId
          }
        }
      });
    }

    it('should load sender on init', function () {
      // when
      var ctrl = buildController('7bf7939d-5829-4f2e-9942-6e567f4c0616', '68d9f426-ee5f-40bc-ab8f-0117d0e69574');
      ctrl.loadFile();
      $rootScope.$apply();

      // then
      expect(SenderModel.get).toHaveBeenCalledWith('7bf7939d-5829-4f2e-9942-6e567f4c0616');
      expect(FileModel.get).toHaveBeenCalledWith({senderId: '7bf7939d-5829-4f2e-9942-6e567f4c0616', id: '68d9f426-ee5f-40bc-ab8f-0117d0e69574'});
    });
  });
})();
