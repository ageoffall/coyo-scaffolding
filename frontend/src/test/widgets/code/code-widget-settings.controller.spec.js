(function () {
  'use strict';

  var moduleName = 'coyo.widgets.code';

  describe('module: ' + moduleName, function () {

    var $controller, $scope;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope) {
      $controller = _$controller_;
      $scope = $rootScope.$new();
    }));

    var controllerName = 'CodeWidgetSettingsController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $scope: $scope
        });
      }

      it('should initialize placeholders', function () {
        // given
        $scope.model = {settings: {}};

        // when
        var ctrl = buildController();
        ctrl.$onInit();

        // then
        expect(ctrl.placeholderHtml).not.toBeUndefined();
        expect(ctrl.placeholderJs).not.toBeUndefined();
        expect(ctrl.placeholderCss).not.toBeUndefined();
      });

    });
  });

})();
