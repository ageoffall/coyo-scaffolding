(function () {
  'use strict';

  var moduleName = 'coyo.widgets.wiki';
  var directiveName = 'coyoWikiWidget';

  describe('module: ' + moduleName, function () {

    var $controller, WikiWidgetModel, targetService, $q, widget, $rootScope, $scope, widgetStatusService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        $scope = $rootScope.$new();
        WikiWidgetModel = jasmine.createSpyObj('WikiWidgetModel', ['getLatest']);
        WikiWidgetModel.getLatest.and.returnValue($q.resolve());
        targetService = jasmine.createSpyObj('targetService', ['getLink']);
        targetService.getLink.and.returnValue('/some-link');
        widgetStatusService = jasmine.createSpyObj('widgetStatusService', ['refreshOnSettingsChange']);
        widget = {settings: {}};
      }));

      var controllerName = 'WikiWidgetController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            WikiWidgetModel: WikiWidgetModel,
            targetService: targetService,
            $scope: $scope,
            widgetStatusService: widgetStatusService
          }, {
            widget: widget
          });
        }

        it('should load articles', function () {
          // given
          var articles = ['article'];
          widget.settings._articleCount = 10;
          widget.settings._appId = 'AppId';
          WikiWidgetModel.getLatest.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticles();
          $rootScope.$apply();

          // then
          expect(ctrl.articles).toEqual(articles);
          expect(WikiWidgetModel.getLatest).toHaveBeenCalledWith(10, 'AppId');
        });

        it('should register reload on settings change', function () {
          // when
          buildController();

          // then
          expect(widgetStatusService.refreshOnSettingsChange).toHaveBeenCalledWith($scope);
        });

        it('should call targetService.getLink', function () {
          // given
          var articles = [{
            articleTarget: {params: {id: 'articleId'}},
            senderTarget: {params: {id: 'senderId'}},
            appTarget: {params: {id: 'appId'}}
          }];
          WikiWidgetModel.getLatest.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticles();
          $rootScope.$apply();

          // then
          expect(targetService.getLink).toHaveBeenCalled();
        });

        it('should build link object', function () {
          // given
          var articleId = 'a9asd-123sf';
          var senderId = '98jsf-33agz';
          var appId = 'xczss-18hjk';
          var articles = [{
            articleTarget: {params: {id: articleId}},
            senderTarget: {params: {id: senderId}},
            appTarget: {params: {id: appId}}
          }];
          WikiWidgetModel.getLatest.and.returnValue($q.resolve(articles));
          var ctrl = buildController();

          // when
          ctrl.loadArticles();
          $rootScope.$apply();

          // then
          expect(ctrl.links).not.toBeUndefined();
          expect(ctrl.links.articles[articleId]).toEqual('/some-link');
          expect(ctrl.links.senders[senderId]).toEqual('/some-link');
        });
      });
    });
  });
})();
