(function () {
  'use strict';

  describe('domain: LaunchpadLinkModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, LaunchpadLinkModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _LaunchpadLinkModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      LaunchpadLinkModel = _LaunchpadLinkModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('should save', function () {
        // given
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/launchpad/categories/123/links')
            .respond(200, {});

        // when
        var model = new LaunchpadLinkModel();
        model.saveOrUpdate({id: 123});
        $httpBackend.flush();
      });

      it('should update', function () {
        // given
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/launchpad/categories/123/links/linkId')
            .respond(200, {});

        // when
        var model = new LaunchpadLinkModel();
        model.id = 'linkId';
        model.saveOrUpdate({id: 123});
        $httpBackend.flush();
      });

      it('should delete', function () {
        // given
        $httpBackend.expectDELETE(backendUrlService.getUrl() + '/web/launchpad/categories/123/links/linkId')
            .respond(200, {});

        // when
        var model = new LaunchpadLinkModel();
        model.id = 'linkId';
        model.delete({id: 123});
        $httpBackend.flush();
      });
    });
  });
}());
