(function () {
  'use strict';

  describe('domain: ApiClientModel', function () {
    beforeEach(module('coyo.domain'));

    var $httpBackend, ApiClientModel, backendUrlService, Pageable;

    beforeEach(inject(function (_$httpBackend_, _ApiClientModel_, _backendUrlService_, _Pageable_) {
      $httpBackend = _$httpBackend_;
      ApiClientModel = _ApiClientModel_;
      Pageable = _Pageable_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('get paged API clients', function () {
        // given
        var apiClient1 = {clientId: 'CLIENT-ID-1', clientSecret: 'CLIENT-SECRET-1'};
        var apiClient2 = {clientId: 'CLIENT-ID-2', clientSecret: 'CLIENT-SECRET-2'};
        var apiClients = [apiClient1, apiClient2];
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/api-clients?_page=1&_pageSize=2')
            .respond(200, {content: apiClients});

        // when
        var result = null;
        ApiClientModel.pagedQuery(new Pageable(1, 2))
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual([new ApiClientModel(apiClient1), new ApiClientModel(apiClient2)]);
      });

      it('should create API client', function () {
        // given
        var clientId = 'CLIENT-ID';
        var model = new ApiClientModel({clientId: clientId});
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/api-clients', function (data) {
          var payload = angular.fromJson(data);
          expect(payload.clientId).toBe(clientId);
          return true;
        }).respond(200);

        // when
        model.save();
        $httpBackend.flush();
      });

      it('should delete API client', function () {
        // given
        var id = 'UniqueId-1';
        var model = new ApiClientModel({id: id, clientId: 'CLIENT-ID', clientSecret: 'CLIENT-SECRET'});
        $httpBackend.expectDELETE(backendUrlService.getUrl() + '/web/api-clients/' + id).respond(200);

        // when
        model.delete();
        $httpBackend.flush();
      });

    });

  });
}());
