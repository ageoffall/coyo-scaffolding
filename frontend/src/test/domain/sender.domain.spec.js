(function () {
  'use strict';

  describe('domain: SenderModel', function () {
    beforeEach(module('coyo.domain'));

    var $httpBackend, SenderModel, senderModel, AppModel, $state, $stateParams, backendUrlService, Pageable, sender1, sender2;

    beforeEach(inject(function (_$httpBackend_, _SenderModel_, _AppModel_, _$state_, _$stateParams_, _backendUrlService_, _Pageable_) {
      $httpBackend = _$httpBackend_;
      SenderModel = _SenderModel_;
      AppModel = _AppModel_;
      $state = _$state_;
      $stateParams = _$stateParams_;
      backendUrlService = _backendUrlService_;
      Pageable = _Pageable_;
      senderModel = new SenderModel({id: 'sender-id'});
      sender1 = {
        'id': 'ab0c5367-c447-402d-9fd1-ff838e691e9a',
        'slug': 'company-news',
        'typeName': 'page',
        'displayName': 'Company News'
      };
      sender2 = {
        'id': '73922901-5544-4463-b8f3-3ddfa5d7efa0',
        'slug': 'company-tasks',
        'typeName': 'workspace',
        'displayName': 'Company Tasks'
      };

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {
      it('should get current sender id', function () {
        // given
        _.set($state, 'current.data.senderParam', 'idOrSlug');
        $stateParams.idOrSlug = 'id-or-slug';

        // when
        var currentIdOrSlug = SenderModel.getCurrentIdOrSlug();

        // then
        expect(currentIdOrSlug).toBe('id-or-slug');
      });

      it('should return undefined when no sender param configured', function () {
        // when
        var currentIdOrSlug = SenderModel.getCurrentIdOrSlug();

        // then
        expect(currentIdOrSlug).not.toBeDefined();
      });

      it('should get paged senders for search term', function () {
        // given
        var response = {
          'content': [sender1, sender2],
          'aggregations': {},
          'totalPages': 1,
          'totalElements': 2,
          'size': 10,
          'number': 0,
          'sort': [{'direction': 'ASC', 'property': 'displayName.sort'}],
          'first': true,
          'last': true,
          'numberOfElements': 2
        };

        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/senders/search?_orderBy=displayName.sort&_page=0&_pageSize=10'
            + '&aggregations=&filters=type%3Duser%26type%3Dworkspace%26type%3Dpage&searchFields=displayName&term=company')
            .respond(200, response);

        // when
        var result = null;
        SenderModel.searchSendersWithFilter('company', new Pageable(0, 10, 'displayName.sort'), {type: ['user', 'workspace', 'page']}, ['displayName'], {})
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual([new SenderModel(sender1), new SenderModel(sender2)]);
      });

      it('should get only admin senders for search term', function () {
        // given
        var response = {
          'content': [sender1],
          'aggregations': {},
          'totalPages': 1,
          'totalElements': 2,
          'size': 10,
          'number': 0,
          'sort': [{'direction': 'ASC', 'property': 'displayName.sort'}],
          'first': true,
          'last': true,
          'numberOfElements': 2
        };

        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/senders/search/managed?_orderBy=displayName.sort&_page=0&_pageSize=10'
            + '&aggregations=&filters=type%3Duser%26type%3Dworkspace%26type%3Dpage&searchFields=displayName&term=company')
            .respond(200, response);

        // when
        var result = null;
        SenderModel.searchManagedSendersWithFilter('company', new Pageable(0, 10, 'displayName.sort'), {type: ['user', 'workspace', 'page']}, ['displayName'], {})
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual([new SenderModel(sender1)]);
      });

      it('should get only senders where user can post to for search term', function () {
        // given
        var response = {
          'content': [sender1],
          'aggregations': {},
          'totalPages': 1,
          'totalElements': 2,
          'size': 10,
          'number': 0,
          'sort': [{'direction': 'ASC', 'property': 'displayName.sort'}],
          'first': true,
          'last': true,
          'numberOfElements': 2
        };

        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/senders/search/sharing-recipients?_orderBy=displayName.sort&_page=0&_pageSize=10'
            + '&aggregations=&filters=type%3Duser%26type%3Dworkspace%26type%3Dpage&searchFields=displayName&term=company')
            .respond(200, response);

        // when
        var result = null;
        SenderModel.searchSharingRecipientsWithFilter('company', new Pageable(0, 10, 'displayName.sort'), {type: ['user', 'workspace', 'page']}, ['displayName'], {})
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual([new SenderModel(sender1)]);
      });

    });

    describe('instance members', function () {
      it('should get apps', function () {
        // given
        var app1 = {id: 'app1-id', name: 'app1-name'};
        var app2 = {id: 'app2-id', name: 'app2-name'};
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/senders/sender-id/apps?_permissions=manage').respond(200, [app1, app2]);

        // when
        var result = null;
        senderModel.getApps().then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual([
          new AppModel(app1),
          new AppModel(app2)
        ]);
      });

      it('should add app', function () {
        // given
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/senders/sender-id/apps').respond(200, {key: 'app-key'});

        // when
        var result = null;
        senderModel.addApp('app-key', 'app-name').then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result.senderId).toBe('sender-id');
        expect(result.key).toBe('app-key');
        expect(result.name).toBe('app-name');
      });

      it('should remove app', function () {
        // given
        $httpBackend.expectDELETE(backendUrlService.getUrl() + '/web/senders/sender-id/apps/app-key').respond(200);

        // when
        senderModel.removeApp('app-key');
        $httpBackend.flush();
      });

      it('should get app', function () {
        // given
        var app1 = {id: 'app1-id', name: 'app1-name'};
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/senders/sender-id/apps/app1-id?_permissions=*').respond(200, app1);

        // when
        var result = null;
        senderModel.getApp('app1-id').then(function (response) {
          result = response;
        });
        $httpBackend.flush();

        // then
        expect(result).toEqual(new AppModel(app1));
      });

      it('should init translations', function () {
        // given
        var viewModel = {};
        senderModel.defaultLanguage = 'DE';
        senderModel.translations = {'EN': {}};

        // when
        senderModel.initTranslations(viewModel);

        // then
        expect(viewModel.isSenderTranslated).toBe(true);
        expect(viewModel.defaultLanguage).toBe('DE');
        expect(viewModel.languages).toEqual(jasmine.objectContaining({
          EN: {active: true, translations: {}}
        }));
        expect(viewModel.languages).toEqual(jasmine.objectContaining({
          DE: {active: true, translations: {}}
        }));
      });

      it('should resolve that no translation is required when the default language is not set', function () {
        // given
        senderModel.defaultLanguage = 'NONE';
        senderModel.translations = {DE: {}, EN: {}};

        // when
        var actualResult = senderModel.isTranslationRequired({}, null, null);

        // then
        expect(actualResult).toBe(false);
      });

      it('should resolve that translation is required when the current language is equal to the given', function () {
        // given
        senderModel.defaultLanguage = 'DE';
        senderModel.translations = {DE: {}, EN: {}};

        // when
        var actualResult = senderModel.isTranslationRequired({}, 'DE', 'DE');

        // then
        expect(actualResult).toBe(true);
      });

      it('should resolve that translation is required when translations exist for the the given language', function () {
        // given
        senderModel.defaultLanguage = 'DE';
        senderModel.translations = {DE: {}, EN: {}};

        var availableTranslations = {
          EN: {
            translations: {
              name: 'der-name'
            }
          }
        };

        // when
        var actualResult = senderModel.isTranslationRequired(availableTranslations, 'DE', 'EN');

        // then
        expect(actualResult).toBe(true);
      });
    });
  });
}());
