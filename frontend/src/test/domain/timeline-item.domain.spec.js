(function () {
  'use strict';

  describe('domain: TimelineItemModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, TimelineItemModel, backendUrlService, authService, $q, $rootScope;

    beforeEach(
        inject(function (_$httpBackend_, _TimelineItemModel_, _backendUrlService_, _authService_, _$q_, _$rootScope_) {
          $httpBackend = _$httpBackend_;
          TimelineItemModel = _TimelineItemModel_;
          authService = _authService_;
          backendUrlService = _backendUrlService_;

          $q = _$q_;
          $rootScope = _$rootScope_;
          $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
        }));

    describe('instance members', function () {

      it('should mark item as read', function () {
        // given
        var timelineItem = new TimelineItemModel({id: 'item-id'});
        spyOn(authService, 'getUser').and.returnValue($q.resolve({id: 'user-id'}));
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/timeline-items/item-id/read'
          + '?_permissions=edit,delete,accessoriginalauthor,like,comment,share,sticky,actAsSender')
            .respond(200, {id: 'item-id'});

        // when
        var result = null;
        timelineItem.markAsRead().then(function (response) {
          result = response;
        });
        $rootScope.$apply();
        $httpBackend.flush();

        // then
        expect(result.id).toBe('item-id');
      });

      it('should make item unsticky', function () {
        // given
        var result = null;
        $httpBackend.expectPOST(backendUrlService.getUrl() + '/web/timeline-items/item-id/unsticky'
          + '?_permissions=edit,delete,accessoriginalauthor,like,comment,share,sticky,actAsSender')
            .respond(200, {id: 'new-item-id'});

        // when
        new TimelineItemModel({id: 'item-id'}).makeUnsticky().then(function (response) {
          result = response;
        });
        $rootScope.$apply();
        $httpBackend.flush();

        // then
        expect(result.id).toBe('new-item-id');
      });
    });
  });
}());
