(function () {
  'use strict';

  var moduleName = 'commons.i18n';

  describe('module: ' + moduleName, function () {
    var $translate, $document;
    var updateLanguageService;

    beforeEach(function () {
      module(moduleName, function () {
      });
      inject(function (_updateLanguageService_, _$translate_, _$document_) {
        updateLanguageService = _updateLanguageService_;
        $translate = _$translate_;
        $document = _$document_;
      });
    });

    describe('service: updateLanguageService', function () {
      it('should add elements to the list of synced html nodes', function () {
        // given
        var initialCount = updateLanguageService.syncedElements.length;
        var element = $document.find('body');

        // when
        updateLanguageService.addElement(element);

        // then
        expect(updateLanguageService.syncedElements.length).toBe(++initialCount);

      });

      it('should change a passed in element\'s lang attribute properly', function () {
        // given
        var element = $document.find('body');
        element.attr('lang', 'fr');
        updateLanguageService.addElement(element);

        // when
        $translate.use('de').then(function () {

          // then
          expect(element).toBe('de');
        });
      });

      it('should change default lang attribute properly (on html root element)', function () {
        // given
        var element = $document.find('html');
        element.attr('lang', 'fr');

        // when
        $translate.use('de').then(function () {

          // then
          expect(element).toBe('de');
        });
      });
    });
  });
})();
