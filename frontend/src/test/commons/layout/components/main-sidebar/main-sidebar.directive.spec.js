(function () {
  'use strict';

  var moduleName = 'commons.layout';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $q, $injector, $controller, sidebarService, targetService, authService;
    var ngxSubscriptionService, ngxPaginationService;

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
          $provide.value('tourService', jasmine.createSpyObj('tourService', ['getTopics']));
          $provide.value('tourSelectionModal', jasmine.createSpyObj('tourSelectionModal', ['open']));
        })
    );

    beforeEach(inject(function (_$rootScope_, _$controller_, _$q_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $controller = _$controller_;
      $q = _$q_;
      $injector = jasmine.createSpyObj('$injector', ['get']);
      sidebarService = jasmine.createSpyObj('sidebarService', ['register']);
      targetService = jasmine.createSpyObj('targetService', ['getLink']);
      authService = jasmine.createSpyObj('authService', ['getUser', 'onGlobalPermissions']);
      authService.getUser.and.returnValue($q.resolve({id: 'user'}));
      authService.onGlobalPermissions.and.callFake(function (permissionNames, callback) {
        callback(true);
      });
      targetService.getLink.and.returnValue('/some-link');

      ngxPaginationService = jasmine.createSpyObj('ngxPaginationService', ['search', 'pageable']);
      ngxSubscriptionService = jasmine.createSpyObj('ngxSubscriptionService', ['getSubscribedSenders']);
      $injector.get.and.callFake(function () {
        return arguments[0] === 'ngxPaginationService'
          ? ngxPaginationService
          : arguments[0] === 'ngxSubscriptionService'
            ? ngxSubscriptionService
            : undefined;
      });
    }));

    describe('directive: oyoc-main-sidebar', function () {

      var controllerName = 'MainSidebarController';

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $q: $q,
          $injector: $injector,
          sidebarService: sidebarService,
          targetService: targetService,
          authService: authService,
          adminStates: [
            {globalPermission: 'P1'}, {globalPermission: 'P2'}
          ]
        });
      }

      describe('controller: ' + controllerName, function () {

        it('should register at service and be hidden on init', function () {
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.showSidebar).toBeFalsy();
          expect(sidebarService.register).toHaveBeenCalled();

          var api = sidebarService.register.calls.mostRecent().args[0];
          expect(api.name).toBe('menu');
        });

        it('should check if file library is active', function () {
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(authService.onGlobalPermissions).toHaveBeenCalled();
          expect(ctrl.fileLibraryActivated).toBeTruthy();
        });

        it('should init admin permissions', function () {
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.allAdminPermissions).toBe('P1,P2');
        });

        it('should open the sidebar and handle backdrop', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          // when
          var api = sidebarService.register.calls.mostRecent().args[0];
          api.open();

          // then
          expect(ctrl.showSidebar).toBeTruthy();
          expect($rootScope.showBackdrop).toBeTruthy();
        });

        it('should close the sidebar and handle backdrop', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          // when
          var api = sidebarService.register.calls.mostRecent().args[0];
          api.close();

          // then
          expect(ctrl.showSidebar).toBeFalsy();
          expect($rootScope.showBackdrop).toBeFalsy();
        });

        it('should get links for each sender on init', function () {
          // given
          var sender1 = {id: 'id1', target: 'target'};
          var sender2 = {id: 'id2', target: 'target'};
          var page = {
            content: [sender1, sender2]
          };
          ngxSubscriptionService.getSubscribedSenders.and.returnValue({
            toPromise: function () {
              return $q.resolve({
                page: page,
                data: {favorites: []}
              });
            }
          });
          var ctrl = buildController();
          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(_.size(ctrl.links.workspace)).toEqual(2);
          expect(_.size(ctrl.links.page)).toEqual(2);
        });

        it('should load links for workspaces', function () {
          // given
          var workspace = {id: 'workspaceId', target: 'target'};
          var page = {
            content: [workspace]
          };
          ngxSubscriptionService.getSubscribedSenders.and.returnValue({
            toPromise: function () {
              return $q.resolve({
                page: page,
                data: {favorites: []}
              });
            }
          });
          var ctrl = buildController();
          ctrl.user = {id: 'userId'};

          // when
          ctrl.loadMoreSubscriptions('workspace');
          $rootScope.$apply();

          // then
          expect(_.size(ctrl.links.workspace)).toEqual(1);
          expect(_.size(ctrl.links.page)).toEqual(0);
        });

        it('should load links for pages', function () {
          // given
          var senderPage = {id: 'pageId', target: 'target'};
          var page = {
            content: [senderPage]
          };
          ngxSubscriptionService.getSubscribedSenders.and.returnValue({
            toPromise: function () {
              return $q.resolve({
                page: page,
                data: {favorites: []}
              });
            }
          });
          var ctrl = buildController();
          ctrl.user = {id: 'userId'};

          // when
          ctrl.loadMoreSubscriptions('page');
          $rootScope.$apply();

          // then
          expect(_.size(ctrl.links.page)).toEqual(1);
          expect(_.size(ctrl.links.workspace)).toEqual(0);
        });
      });
    });
  });
})();
