(function () {
  'use strict';

  var moduleName = 'commons.layout';
  var targetName = 'oyoc-suppress-scroll';
  var controllerName = 'SuppressScrollController';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $controller, $window, elementSpy;

    beforeEach(module(moduleName));

    describe('directive: ' + targetName, function () {

      beforeEach(inject(function (_$rootScope_, _$controller_, _$window_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $window = _$window_;

        elementSpy = jasmine.createSpyObj('elementSpy', ['css', 'addClass', 'removeClass', 'scrollTop']);
        elementSpy.css.and.returnValue(angular.noop);
        elementSpy.addClass.and.returnValue(angular.noop);
        elementSpy.removeClass.and.returnValue(angular.noop);
        elementSpy.scrollTop.and.returnValue(240);

        spyOn(angular, 'element').and.returnValue(elementSpy);
        spyOn($window, 'scrollTo');
      }));

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            $rootScope: $rootScope,
            $scope: $scope,
            $window: $window
          });
        }

        it('should save scroll position if backdrop is visible', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();
          $rootScope.showBackdrop = false;
          $scope.$digest();

          // when
          $rootScope.showBackdrop = true;
          $scope.$digest();

          // then
          expect(elementSpy.scrollTop).toHaveBeenCalled();
          expect(elementSpy.addClass).toHaveBeenCalledWith('fixed');
          expect(elementSpy.css).toHaveBeenCalledWith({'position': 'fixed', 'top': '-240px'});
        });

        it('should restore scroll position if backdrop disappears', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();
          $rootScope.showBackdrop = false;
          $scope.$digest();
          $rootScope.showBackdrop = true;
          $scope.$digest();

          // when
          $rootScope.showBackdrop = false;
          $scope.$digest();

          // then
          expect(elementSpy.removeClass).toHaveBeenCalledWith('fixed');
          expect(elementSpy.css).toHaveBeenCalledWith({'position': '', 'top': ''});
          expect($window.scrollTo).toHaveBeenCalledWith(0, 240);
        });

        it('should unpin background if scope is destroyed', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();

          // when
          $scope.$destroy();

          // then
          expect(elementSpy.removeClass).toHaveBeenCalledWith('fixed');
          expect(elementSpy.css).toHaveBeenCalledWith({'position': '', 'top': ''});
        });
      });
    });
  });
})();
