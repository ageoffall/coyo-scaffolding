(function () {
  'use strict';

  var moduleName = 'commons.browsernotifications';

  describe('module: ' + moduleName, function () {

    // the service's name
    var $rootScope, anTinyconMock, $timeout;

    beforeEach(function () {

      anTinyconMock = jasmine.createSpyObj('anTinycon', ['reset']);

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('anTinycon', anTinyconMock);
      });

      inject(function (_tabNotificationsService_, _$rootScope_, _$timeout_) {
        // inject dependencies for test
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
      });

    });

    describe('Service: tabNotificationsService', function () {

      it('should reset notification bubble after logout', function () {
        // when
        $rootScope.$emit('authService:logout:success', {});
        $rootScope.$apply();
        $timeout.flush();

        // then
        expect(anTinyconMock.reset).toHaveBeenCalled();
      });

    });

  });
})();
