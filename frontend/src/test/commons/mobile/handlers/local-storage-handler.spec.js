(function () {
  'use strict';

  var moduleName = 'commons.mobile';
  var targetName = 'mobileStorageHandler';

  describe('module: ' + moduleName, function () {

    var mobileStorageHandler, mobileStorageService, mobileEventsService;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        mobileStorageService = jasmine.createSpyObj('mobileStorageService', ['get', 'set']);
        $provide.value('mobileStorageService', mobileStorageService);

        mobileEventsService = jasmine.createSpyObj('mobileEventsService', ['propagate']);
        $provide.value('mobileEventsService', mobileEventsService);
      });

      inject(function (_mobileStorageHandler_) {
        mobileStorageHandler = _mobileStorageHandler_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should persist value to local storage', function () {
        // given
        var data = {key: 'data-key', value: 'data-value'};

        // when
        mobileStorageHandler.setStorageValue(data);

        // then
        expect(mobileStorageService.set).toHaveBeenCalledWith(data.key, data.value);
      });

      it('should read value from local storage', function () {
        // given
        var data = {key: 'data-key', default: 'fallback-value'};

        // when
        mobileStorageHandler.getStorageValue(data);

        // then
        expect(mobileStorageService.get).toHaveBeenCalledWith(data.key, data.default);
        expect(mobileEventsService.propagate).toHaveBeenCalled();
      });

    });
  });
})();
