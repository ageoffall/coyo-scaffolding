(function () {
  'use strict';

  var moduleName = 'commons.resource';

  describe('module: ' + moduleName, function () {
    var $httpBackend, Page, $scope;

    beforeEach(function () {
      module(moduleName, function () {
      });

      inject(function (_$httpBackend_, _Page_, _$rootScope_) {
        Page = _Page_;
        $httpBackend = _$httpBackend_;
        $scope = _$rootScope_.$new();
      });
    });

    describe('page factory', function () {

      it('should cancel previous page request', function () {

        // given
        var page = new Page({}, null, {url: 'test'});
        $httpBackend.expectGET('test?_page=0');
        $httpBackend.expectGET('test?_page=1');

        // when
        var rejectSpy = jasmine.createSpy();
        page.page(0).catch(rejectSpy);

        page.page(1);
        $scope.$apply();

        // then
        expect(rejectSpy).toHaveBeenCalled();

      });
    });
  });
}());
