(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $uibModalInstance, UserModel, GroupModel;

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('coyo.domain'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope, $q) {
      $scope = $rootScope.$new();
      $controller = _$controller_;
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);

      UserModel = jasmine.createSpyObj('UserModel', ['searchWithFilter', 'query']);
      UserModel.searchWithFilter.and.callFake(function () {
        return $q.resolve({
          aggregations: {
            department: [{key: 'IT', count: 1}, {key: 'Management', count: 3}],
            location: [{key: 'Hamburg', count: 1}, {key: 'Munich', count: 1}, {key: 'Berlin', count: 1}]
          },
          content: 'Test'
        });
      });
      UserModel.query.and.callFake(function () {
        return $q.resolve({
          userA: 'user-a-data',
          userB: 'user-b-data'
        });
      });

      GroupModel = jasmine.createSpyObj('GroupModel', ['search']);
      GroupModel.search.and.callFake(function () {
        return $q.resolve({
          content: 'Test'
        });
      });
    }));

    describe('directive: userChooser', function () {

      describe('controller: UserChooserModalController', function () {
        var profileGroups = [
          {
            name: 'contact',
            fields: [
              {name: 'phone', type: 'PHONE', order: 0, userChooser: true},
              {name: 'mobile', type: 'PHONE', order: 1},
              {name: 'twitter', type: 'LINK', order: 2}]
          }, {
            name: 'work',
            fields: [
              {name: 'jobTitle', type: 'TEXT', order: 0},
              {name: 'company', type: 'TEXT', order: 1},
              {name: 'department', type: 'TEXT', order: 2, userChooser: true}]
          }
        ];

        function buildController(internalOnly, userIds) {
          return $controller('UserChooserModalController', {
            $scope: $scope,
            $uibModalInstance: $uibModalInstance,
            UserModel: UserModel,
            GroupModel: GroupModel,
            users: userIds ? userIds : [],
            groups: [],
            profileFieldGroups: profileGroups,
            settings: {
              useObjects: false,
              usersOnly: false,
              groupsOnly: false,
              internalOnly: internalOnly,
              usersField: 'userIds',
              groupsField: 'groupIds'
            }
          });
        }

        describe('controller: init', function () {

          it('should init with default values', function () {
            // when
            var ctrl = buildController();
            ctrl.$onInit();

            // then
            expect(ctrl.title).toBe('USER_CHOOSER.TITLE');
            expect(ctrl.tab).toBe(1);
            expect(ctrl.users.list).toBeEmptyArray();
            expect(ctrl.selection.users.ids).toBeEmptyArray();
            expect(ctrl.users.filters).toBeEmptyArray();
            expect(ctrl.users.aggregations).toBeEmptyArray();
            expect(ctrl.groups.list).toBeEmptyArray();
            expect(ctrl.selection.groups.ids).toBeEmptyArray();
            expect(ctrl.profileFields.length).toBe(2);
            expect(ctrl.profileFields[0].name).toBe('phone');
            expect(ctrl.profileFields[0].userChooser).toBe(true);
            expect(ctrl.profileFields[1].name).toBe('department');
            expect(ctrl.profileFields[1].userChooser).toBe(true);
          });

          it('should init with empty counts', function () {
            // when
            var ctrl = buildController();
            ctrl.$onInit();

            // then
            expect(ctrl.countUsers()).toBe(0);
            expect(ctrl.countGroups()).toBe(0);
          });

          it('should get users with initial ids', function () {
            // given
            var initialUserIds = ['id-1', 'id-2'];

            // when
            var ctrl = buildController(false, initialUserIds);
            ctrl.$onInit();

            // then
            expect(UserModel.query).toHaveBeenCalledWith({userIds: initialUserIds.toString(), includeDeleted: true});
          });

        });

        describe('controller: selection and filters', function () {

          it('should toggle selection for an item', function () {
            // given
            var item = {id: 'test'};

            // when
            var ctrl = buildController();
            ctrl.$onInit();

            // then - user
            expect(ctrl.selection.users.ids).toBeEmptyArray();
            ctrl.toggleSelectionUser(item);
            expect(ctrl.userSelectionType(item)).toBe('NEW');
            ctrl.toggleSelectionUser(item);
            expect(ctrl.userSelectionType(item)).toBe('NONE');

            // then - group
            expect(ctrl.selection.groups.ids).toBeEmptyArray();
            ctrl.toggleSelectionGroup(item);
            expect(ctrl.groupSelectionType(item)).toBe('NEW');
            ctrl.toggleSelectionGroup(item);
            expect(ctrl.groupSelectionType(item)).toBe('NONE');
          });

          it('should set changed flag when an user was selected', function () {
            // given
            var item = {id: 'test'};
            var ctrl = buildController();
            ctrl.$onInit();

            // when
            ctrl.toggleSelectionUser(item);
            ctrl.save();

            // then
            expect($uibModalInstance.close).toHaveBeenCalledWith({
              hasChanges: true,
              users: [item.id],
              groups: []
            });
          });

          it('should set changed flag when a group was selected', function () {
            // given
            var item = {id: 'test'};
            var ctrl = buildController();
            ctrl.$onInit();

            // when
            ctrl.toggleSelectionGroup(item);
            ctrl.save();

            // then
            expect($uibModalInstance.close).toHaveBeenCalledWith({
              hasChanges: true,
              users: [],
              groups: [item.id]
            });
          });

          it('should set changed flag when nothing was selected', function () {
            // given
            var ctrl = buildController();
            ctrl.$onInit();

            // when
            ctrl.save();

            // then
            expect($uibModalInstance.close).toHaveBeenCalledWith({
              hasChanges: false,
              users: [],
              groups: []
            });
          });

          it('should toggle user filters', function () {
            // given
            var field = 'department';
            var filter = {key: 'IT'};

            // when
            var ctrl = buildController();
            ctrl.$onInit();

            // then
            expect(ctrl.users.filters).toBeEmptyArray();
            ctrl.toggleFilterUser(field, filter);
            expect(ctrl.isFilteredUser(field, filter)).toBeTrue();
            ctrl.toggleFilterUser(field, filter);
            expect(ctrl.isFilteredUser(field, filter)).toBeFalse();
          });

        });

        describe('controller: search', function () {

          it('should search users by name', function () {
            // given
            var term = 'b';
            var aggregations = {department: 100, location: 100};
            var searchFields = ['displayName', 'properties.department', 'properties.location'];

            // when
            var ctrl = buildController();
            ctrl.$onInit();
            ctrl.searchUsers(term);
            $scope.$apply();

            // then
            expect(ctrl.users.term).toBe(term);
            expect(UserModel.searchWithFilter).toHaveBeenCalled();

            // check that all params where passed correctly
            expect(UserModel.searchWithFilter.calls.argsFor(0).length).toBe(6);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[0]).toBe(term);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[2]).toBeEmptyObject();
            expect(UserModel.searchWithFilter.calls.argsFor(0)[3]).toEqual(searchFields);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[4]).toEqual(aggregations);

            // check whether the current page and the user have been assigned
            expect(ctrl.users.page).toBeNonEmptyObject();
            expect(ctrl.users.list).toEqual(['Test']);
          });

          it('should search users by name and filter', function () {
            // given
            var term = '';
            var aggregations = {department: 100, location: 100};
            var searchFields = ['displayName', 'properties.department', 'properties.location'];
            var field = 'department';
            var filter = {key: 'IT'};

            // when
            var ctrl = buildController();
            ctrl.$onInit();
            ctrl.toggleFilterUser(field, filter);
            ctrl.searchUsers(term);
            $scope.$apply();

            // then
            expect(ctrl.users.term).toBe(term);
            expect(UserModel.searchWithFilter).toHaveBeenCalled();

            // check that all params where passed and that the filters where transformed correctly
            expect(UserModel.searchWithFilter.calls.argsFor(0).length).toBe(6);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[0]).toBe(term);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[2]).toEqual({department: ['IT']});
            expect(UserModel.searchWithFilter.calls.argsFor(0)[3]).toEqual(searchFields);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[4]).toEqual(aggregations);
          });

          it('should return sorted aggregations', function () {
            // given
            var term = '';
            var aggregations = {department: 100, location: 100};

            // when
            var ctrl = buildController();
            ctrl.$onInit();
            ctrl.searchUsers(term);
            $scope.$apply();

            // then
            expect(ctrl.users.term).toBe(term);
            expect(UserModel.searchWithFilter).toHaveBeenCalled();
            expect(UserModel.searchWithFilter.calls.argsFor(0)[4]).toEqual(aggregations);

            // check for aggregations in the correct order
            expect(ctrl.users.aggregations.department[0].key).toEqual('IT');
            expect(ctrl.users.aggregations.department[1].key).toEqual('Management');
            expect(ctrl.users.aggregations.location[0].key).toEqual('Berlin');
            expect(ctrl.users.aggregations.location[1].key).toEqual('Hamburg');
            expect(ctrl.users.aggregations.location[2].key).toEqual('Munich');
          });

          it('should search groups by name', function () {
            // given
            var term = 'b';

            // when
            var ctrl = buildController();
            ctrl.$onInit();
            ctrl.searchGroups(term);
            $scope.$apply();

            // then
            expect(ctrl.groups.term).toBe(term);
            expect(GroupModel.search).toHaveBeenCalled();

            // check that all params where passed correctly
            expect(GroupModel.search.calls.argsFor(0).length).toBe(3);
            expect(GroupModel.search.calls.argsFor(0)[0]).toBe(term);

            // check whether the current page and the group have been assigned
            expect(ctrl.groups.page).toBeNonEmptyObject();
            expect(ctrl.groups.list).toEqual(['Test']);
          });

          it('should search users by name internals only', function () {
            // given
            var term = 'b';

            // when
            var ctrl = buildController(true);
            ctrl.$onInit();
            ctrl.searchUsers(term);
            $scope.$apply();

            // then
            expect(ctrl.users.term).toBe(term);
            expect(UserModel.searchWithFilter).toHaveBeenCalled();

            // check that all params where passed correctly
            expect(UserModel.searchWithFilter.calls.argsFor(0).length).toBe(6);
            expect(UserModel.searchWithFilter.calls.argsFor(0)[5]).toEqual(true);
          });

        });
      });
    });
  });
})();
