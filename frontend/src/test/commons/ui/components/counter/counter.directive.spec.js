(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
    }));

    describe('directive: counter', function () {

      var controllerName = 'CounterController';

      describe('controller: ' + controllerName, function () {

        it('should return key for zero', function () {
          // given
          var ctrl = buildController(0);

          // when
          var key = ctrl.translationKey();

          // then
          expect(key).toBe('KEY_NONE');
        });

        it('should return key for one', function () {
          // given
          var ctrl = buildController(1);

          // when
          var key = ctrl.translationKey();

          // then
          expect(key).toBe('KEY_SINGULAR');
        });

        it('should return key for plural', function () {
          // given
          var ctrl = buildController(2);

          // when
          var key = ctrl.translationKey();

          // then
          expect(key).toBe('KEY_PLURAL');
        });

        function buildController(value) {
          return $controller(controllerName, undefined, {
            value: value,
            keyNone: 'KEY_NONE',
            keySingular: 'KEY_SINGULAR',
            keyPlural: 'KEY_PLURAL'
          });
        }
      });
    });
  });
})();
