(function () {
  'use strict';

  var moduleName = 'commons.ui',
      componentName = 'coyoSelectBirthday',
      controllerName = 'SelectBirthdayController';

  describe('module: ' + moduleName, function () {

    var $controller, moment, momentInstance, birthdayService, dateString, dateObject, daysInMonth, ngModelCtrl,
        validationKey;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
    }));

    beforeEach(function () {
      dateString = '2016-05-20';
      dateObject = {
        year: 2016,
        month: 4,
        day: 20
      };

      daysInMonth = 30;

      validationKey = 'birthdayFormat';

      birthdayService = jasmine.createSpyObj('birthdayService',
          ['parseDate', 'getReferenceLeapYear', 'formatDate', 'createDays', 'createMonths', 'createYears', 'isDateEmpty', 'isFieldValue']);
      birthdayService.getReferenceLeapYear.and.returnValue('2016');
      birthdayService.parseDate.and.returnValue(dateObject);
      birthdayService.createDays.and.returnValue('days');
      birthdayService.createMonths.and.returnValue('months');
      birthdayService.createYears.and.returnValue('years');
      birthdayService.isDateEmpty.and.returnValue(false);
      birthdayService.isFieldValue.and.callFake(function isFieldValue(fieldValue) {
        return fieldValue !== -1;
      });

      momentInstance = jasmine.createSpyObj('momentInstance', ['isValid', 'daysInMonth']);
      momentInstance.daysInMonth.and.returnValue(daysInMonth);
      momentInstance.isValid.and.returnValue(true);

      moment = jasmine.createSpy('moment');
      moment.and.returnValue(momentInstance);

      ngModelCtrl = jasmine.createSpyObj('ngModelCtrl', ['$setValidity', '$setViewValue']);
    });

    describe('component: ' + componentName, function () {

      describe('controller: ' + controllerName, function () {

        function buildController() {
          var ctrl = $controller(controllerName, {
            moment: moment,
            birthdayService: birthdayService
          }, {
            formattedDate: dateString,
            ngModelCtrl: ngModelCtrl
          });
          ctrl.$onInit();
          return ctrl;
        }

        function updateDateObject(year, month, day) {
          dateObject.year = year;
          dateObject.month = month;
          dateObject.day = day;
        }

        describe('init', function () {

          it('should init values', function () {
            // given

            // when
            var ctrl = buildController();

            // then
            expect(ctrl.formattedDate).toBe(dateString);
            expect(ctrl.dayOptions).toBe('days');
            expect(ctrl.monthOptions).toBe('months');
            expect(ctrl.yearOptions).toBe('years');
            expect(ctrl.year).toBe(dateObject.year);
            expect(ctrl.month).toBe(dateObject.month);
            expect(ctrl.day).toBe(dateObject.day);
          });
        });

        describe('active', function () {

          function shouldValidateFieldsWithEmptyDate(controllerFunction) {
            // given
            updateDateObject(-1, -1, -1);
            birthdayService.isDateEmpty.and.returnValue(true);
            var ctrl = buildController();

            // when
            controllerFunction(ctrl);

            // then
            expect(ctrl.dayFieldValid).toBe(true);
            expect(ctrl.monthFieldValid).toBe(true);
          }

          function shouldValidateFieldsWithDayMissing(controllerFunction) {
            // given
            var ctrl = buildController();
            ctrl.day = -1;

            // when
            controllerFunction(ctrl);

            // then
            expect(ctrl.dayFieldValid).toBe(false);
            expect(ctrl.monthFieldValid).toBe(true);
          }

          function shouldValidateFieldsWithMonthMissing(controllerFunction) {
            // given
            var ctrl = buildController();
            ctrl.month = -1;

            // when
            controllerFunction(ctrl);

            // then
            expect(ctrl.dayFieldValid).toBe(true);
            expect(ctrl.monthFieldValid).toBe(false);
          }

          function shouldValidateFieldsWithYearMissing(controllerFunction) {
            // given
            var ctrl = buildController();
            ctrl.year = -1;

            // when
            controllerFunction(ctrl);

            // then
            expect(ctrl.dayFieldValid).toBe(true);
            expect(ctrl.monthFieldValid).toBe(true);
          }

          function shouldSetValidityToModelWithEmptyDate(controllerFunction) {
            // given
            updateDateObject(-1, -1, -1);
            birthdayService.isDateEmpty.and.returnValue(true);
            var ctrl = buildController();

            // when
            controllerFunction(ctrl);

            // then
            expect(momentInstance.isValid.calls.any()).toBe(false);
            expect(ctrl.ngModelCtrl.$setValidity).toHaveBeenCalledWith(validationKey, true);
          }

          function shouldSetValidityToModelWithValidDate(controllerFunction) {
            // given
            var ctrl = buildController();

            // when
            controllerFunction(ctrl);

            // then
            expect(momentInstance.isValid).toHaveBeenCalled();
            expect(ctrl.ngModelCtrl.$setValidity).toHaveBeenCalledWith(validationKey, true);
          }

          function shouldSetValidityToModelWithInvalidDate(controllerFunction) {
            // given
            var ctrl = buildController();
            ctrl.day = -1;

            // when
            controllerFunction(ctrl);

            // then
            expect(momentInstance.isValid).toHaveBeenCalled();
            expect(ctrl.ngModelCtrl.$setValidity).toHaveBeenCalledWith(validationKey, false);
          }

          function shouldSetFormattedDateToModel(controllerFunction) {
            // given
            var formattedDate = '2014-12-02';
            updateDateObject(2014, 11, 2);
            birthdayService.formatDate.and.returnValue(formattedDate);
            var ctrl = buildController();

            // when
            controllerFunction(ctrl);

            // then
            expect(momentInstance.isValid).toHaveBeenCalled();
            expect(birthdayService.formatDate)
                .toHaveBeenCalledWith(dateObject.year, dateObject.month, dateObject.day);
            expect(ctrl.formattedDate).toBe(formattedDate);
            expect(ctrl.ngModelCtrl.$setViewValue).toHaveBeenCalledWith(formattedDate);
          }

          function executeValidateOnChangeDay(ctrl) {
            ctrl.validateOnChangeDay();
          }

          function executeValidateOnChangeMonthOrYear(ctrl) {
            ctrl.validateOnChangeMonthOrYear();
          }

          describe('validate on change day', function () {

            it('should validate fields with empty date', function () {
              shouldValidateFieldsWithEmptyDate(executeValidateOnChangeDay);
            });

            it('should validate fields with day missing', function () {
              shouldValidateFieldsWithDayMissing(executeValidateOnChangeDay);
            });

            it('should validate fields with month missing', function () {
              shouldValidateFieldsWithMonthMissing(executeValidateOnChangeDay);
            });

            it('should validate fields with year missing', function () {
              shouldValidateFieldsWithYearMissing(executeValidateOnChangeDay);
            });

            it('should set validity to model with empty date', function () {
              shouldSetValidityToModelWithEmptyDate(executeValidateOnChangeDay);
            });

            it('should set validity to model with valid date', function () {
              shouldSetValidityToModelWithValidDate(executeValidateOnChangeDay);
            });

            it('should set validity to model with invalid date', function () {
              shouldSetValidityToModelWithInvalidDate(executeValidateOnChangeDay);
            });

            it('should set formatted date to model', function () {
              shouldSetFormattedDateToModel(executeValidateOnChangeDay);
            });
          });

          describe('validate on change month or year', function () {

            it('should validate fields with empty date', function () {
              shouldValidateFieldsWithEmptyDate(executeValidateOnChangeMonthOrYear);
            });

            it('should validate fields with day missing', function () {
              shouldValidateFieldsWithDayMissing(executeValidateOnChangeMonthOrYear);
            });

            it('should validate fields with month missing', function () {
              shouldValidateFieldsWithMonthMissing(executeValidateOnChangeMonthOrYear);
            });

            it('should validate fields with year missing', function () {
              shouldValidateFieldsWithYearMissing(executeValidateOnChangeMonthOrYear);
            });

            it('should set validity to model with empty date', function () {
              shouldSetValidityToModelWithEmptyDate(executeValidateOnChangeMonthOrYear);
            });

            it('should set validity to model with valid date', function () {
              shouldSetValidityToModelWithValidDate(executeValidateOnChangeMonthOrYear);
            });

            it('should set validity to model with invalid date', function () {
              shouldSetValidityToModelWithInvalidDate(executeValidateOnChangeMonthOrYear);
            });

            it('should set formatted date to model', function () {
              shouldSetFormattedDateToModel(executeValidateOnChangeMonthOrYear);
            });

            it('should validate day options with empty month', function () {
              // given
              var ctrl = buildController();
              ctrl.month = -1;

              // when
              ctrl.validateOnChangeMonthOrYear();

              // then
              expect(birthdayService.createDays).toHaveBeenCalledWith(31);
            });

            it('should validate day options with valid date', function () {
              // given
              var ctrl = buildController();
              momentInstance.daysInMonth.and.returnValue(29);
              birthdayService.createDays.and.returnValue('newDays');
              birthdayService.createDays.calls.reset();

              // when
              ctrl.validateOnChangeMonthOrYear();

              // then
              expect(momentInstance.isValid).toHaveBeenCalled();
              expect(momentInstance.daysInMonth).toHaveBeenCalled();
              expect(birthdayService.createDays).toHaveBeenCalledWith(29);
              expect(ctrl.dayOptions).toBe('newDays');
            });
          });
        });
      });
    });
  });
})();
