(function () {
  'use strict';

  var moduleName = 'commons.ui',
      serviceName = 'birthdayService';

  describe('module: ' + moduleName, function () {

    var birthdayService, moment, $translate;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_birthdayService_, _moment_, _$translate_) {
      $translate = _$translate_;
      birthdayService = _birthdayService_;
      moment = _moment_;
      spyOn(moment, 'monthsShort');
      moment.monthsShort.and.returnValue(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
    }));

    describe('service: ' + serviceName, function () {
      it('should parse date', function () {
        // given
        var dateString = '2012-10-05';

        // when
        var result = birthdayService.parseDate(dateString);

        // then
        expect(result.year).toBe(2012);
        expect(result.month).toBe(9);
        expect(result.day).toBe(5);
      });

      it('should parse an invalid date string and provide object with null values', function () {
        // given
        var dateString = '2012-1-05';

        // when
        var result = birthdayService.parseDate(dateString);

        // then
        expect(result.year).toBe(-1);
        expect(result.month).toBe(-1);
        expect(result.day).toBe(-1);
      });

      it('should format date string from valid date values', function () {
        // given
        var expectedDateString = '2012-08-04';

        // when
        var result = birthdayService.formatDate(2012, 7, 4);

        // then
        expect(result).toBe(expectedDateString);
      });

      it('should format empty date string for empty date values', function () {
        // given
        var expectedDateString = '';

        // when
        var result = birthdayService.formatDate(-1, -1, -1);

        // then
        expect(result).toBe(expectedDateString);
      });

      it('should format date string from date values with missing year', function () {
        // given
        var expectedDateString = '10-05';

        // when
        var result = birthdayService.formatDate(-1, 9, 5);

        // then
        expect(result).toBe(expectedDateString);
      });

      it('should create days', function () {
        // given

        // when
        var result = birthdayService.createDays(5);

        // then
        expect(result.length).toBe(6);
        expect(result[0].value).toBe(-1);
        expect(result[0].label).toBe('--');
        expect(result[1].value).toBe(1);
        expect(result[1].label).toBe(1);
        expect(result[5].value).toBe(5);
        expect(result[5].label).toBe(5);
      });

      it('should create months', function () {
        // given

        // when
        var result = birthdayService.createMonths();

        // then
        expect(result.length).toBe(13);
        expect(result[0].value).toBe(-1);
        expect(result[0].label).toBe('--');
        expect(result[1].value).toBe(0);
        expect(result[1].label).toBe('Jan');
        expect(result[12].value).toBe(11);
        expect(result[12].label).toBe('Dec');
      });

      it('should create years', function () {
        // given
        var currentYear = new Date().getFullYear();

        // when
        var result = birthdayService.createYears(5);

        // then
        expect(result.length).toBe(102);
        expect(result[0].value).toBe(-1);
        expect(result[0].label).toBe('--');
        expect(result[1].value).toBe(currentYear);
        expect(result[1].label).toBe(currentYear);
        expect(result[101].value).toBe(currentYear - 100);
        expect(result[101].label).toBe(currentYear - 100);
      });

      it('should state that date is empty', function () {
        // given

        // when
        var result = birthdayService.isDateEmpty(-1, -1, -1);

        // then
        expect(result).toBe(true);
      });

      it('should state that date is not empty', function () {
        // given

        // when
        var result = birthdayService.isDateEmpty(2014, 5, 10);

        // then
        expect(result).toBe(false);
      });

      it('should state that date contains year', function () {
        // given

        // when
        var result = birthdayService.hasYear('2014-06-10');

        // then
        expect(result).toBe(true);
      });

      it('should state that date does not contain year', function () {
        // given

        // when
        var result = birthdayService.hasYear('06-10');

        // then
        expect(result).toBe(false);
      });

      it('should provide reference leap year', function () {
        // given

        // when
        var result = birthdayService.getReferenceLeapYear();

        // then
        expect(result).toBe(2016);
      });

      it('should provide default value', function () {
        // given

        // when
        var result = birthdayService.getDefaultValue();

        // then
        expect(result).toBe(-1);
      });

      it('should state that field has no value', function () {
        // given

        // when
        var result = birthdayService.isFieldValue(-1);

        // then
        expect(result).toBe(false);
      });

      it('should state that field has a value', function () {
        // given

        // when
        var result = birthdayService.isFieldValue(5);

        // then
        expect(result).toBe(true);
      });

      it('should convert the birthday string to a human readable date', function () {
        // given
        var dateWithYear = '2001-01-02';
        var dateWithoutYear = '01-02';
        $translate.instant = jasmine.createSpy('$translate.instant');
        $translate.instant.and.callFake(function (arg) {
          if (arg === 'DATE_FORMAT_MEDIUM') {
            return 'DD.MM.YYYY';
          } else if (arg === 'DATE_FORMAT_MEDIUM_WITHOUT_YEAR') {
            return 'DD.MM';
          } else {
            return arg;
          }
        });

        // when
        var resultWithYear = birthdayService.birthdayStringToDateString(dateWithYear);
        var resultWithoutYear = birthdayService.birthdayStringToDateString(dateWithoutYear);

        // then
        expect(resultWithYear).toBe('02.01.2001');
        expect(resultWithoutYear).toBe('02.01');
      });
    });
  });
})();
