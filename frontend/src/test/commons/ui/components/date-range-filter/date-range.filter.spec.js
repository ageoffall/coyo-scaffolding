(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'dateRange';

  describe('module: ' + moduleName, function () {
    var dateRange, moment;

    beforeEach(function () {
      module(moduleName);

      inject(function (_dateRangeFilter_, _moment_) {
        dateRange = _dateRangeFilter_;
        moment = _moment_;
      });
    });

    describe('filter: ' + targetName, function () {

      it('should be registered', function () {
        expect(dateRange).not.toBeUndefined();
      });

      it('should show date range from two dates', function () {
        // given
        var from = moment('20170112', 'YYYYMMDD');
        var to = moment('20170112', 'YYYYMMDD').add(2, 'days');

        // then
        expect(dateRange([from, to])).toEqual('1/12/2017 - 1/14/2017');
      });

      it('should show only one day if from and to match', function () {
        // given
        var from = moment('20170112', 'YYYYMMDD');
        var to = moment('20170112', 'YYYYMMDD');

        // then
        expect(dateRange([from, to])).toEqual('1/12/2017');
      });

      it('should display "today" as an alias for the current day', function () {
        // given
        var from = moment().subtract(2, 'days');
        var to = moment();

        // then
        expect(dateRange([from, to])).toEndWith('TODAY');
      });

      it('should return an empty string if input is invalid', function () {
        // given
        var from = moment();

        // then
        expect(dateRange([from])).toBeEmptyString();
        expect(dateRange()).toBeEmptyString();
        expect(dateRange('TEST')).toBeEmptyString();
        expect(dateRange(1234)).toBeEmptyString();
        expect(dateRange(true)).toBeEmptyString();
      });

    });
  });
})();
