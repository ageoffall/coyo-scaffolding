(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var controllerName = 'ManageTranslationsModalController';

  describe('module: ' + moduleName, function () {
    var $controller, $q, $scope, $translate, $filter, $uibModalInstance;
    var modalService, SettingsModel;
    var modalPromise;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _$q_, _$filter_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
        $filter = _$filter_;
      });

      $translate = jasmine.createSpyObj('$translate', ['instant']);
      $translate.instant.and.returnValue('TRANSLATION');

      modalService = jasmine.createSpyObj('modalService', ['confirmDelete']);
      modalPromise = $q.defer();
      modalService.confirmDelete.and.returnValue({result: modalPromise.promise});

      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
      SettingsModel.retrieveByKey.and.returnValue($q.resolve('EN'));
    });

    function buildController(defaultLanguage, languages) {
      return $controller(controllerName, {
        $translate: $translate,
        $filter: $filter,
        $uibModalInstance: $uibModalInstance,
        defaultLanguage: defaultLanguage,
        modalService: modalService,
        SettingsModel: SettingsModel,
        languages: languages,
        availableLanguages: ['DE', 'EN']
      });
    }

    describe('controller: ' + controllerName, function () {
      it('should init the controller with undefined default languages', function () {
        // given
        var ctrl = buildController(null, {});

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.data.default).toBe('EN');
        expect(ctrl.data.languages.EN.active).toBe(true);
        expect(ctrl.availableLanguages).toEqual([
          {language: 'de', name: 'TRANSLATION'},
          {language: 'en', name: 'TRANSLATION'}
        ]);
      });

      it('should init the controller with defined default languages', function () {
        // given
        var languages = {
          'DE': {
            'active': true
          },
          'EN': {
            'active': true
          }
        };

        var ctrl = buildController('DE', languages);

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.data.default).toBe('DE');
        expect(ctrl.data.languages.DE.active).toBe(true);
        expect(ctrl.availableLanguages).toEqual([
          {language: 'de', name: 'TRANSLATION'},
          {language: 'en', name: 'TRANSLATION'}
        ]);
      });

      it('should open a confirm modal and apply change when a user removes an activated language', function () {
        // given
        var languages = {
          DE: {
            active: true,
            translations: {
              'key': 'value'
            }
          },
          EN: {
            active: false, // should be negated which will usually done due to the user interaction (checkbox)
            translations: {
              'key': 'value'
            }
          }
        };
        var ctrl = buildController('DE', languages);

        // when
        ctrl.$onInit();
        ctrl.handleLanguageChange('EN');
        modalPromise.resolve();
        $scope.$apply();

        // then
        expect(modalService.confirmDelete).toHaveBeenCalled();
        expect(ctrl.data.languages.EN.active).toBe(false);
      });

      it('should not open a confirm modal but apply changes when a user adds and directly removes an activated language (without translations)',
          function () {
            // given
            var languages = {
              DE: {
                active: true,
                translations: {
                  'key': 'value'
                }
              },
              EN: {
                active: false // should be negated which will usually done due to the user interaction (checkbox)
              }
            };
            var ctrl = buildController('DE', languages);

            // when
            ctrl.$onInit();
            ctrl.handleLanguageChange('EN');
            $scope.$apply();

            // then
            expect(modalService.confirmDelete).not.toHaveBeenCalled();
            expect(ctrl.data.languages.EN.active).toBe(false);
          });

      it('should open a confirm modal and discard changes when an user dismiss the removal of an activated language',
          function () {
            // given
            var languages = {
              DE: {
                active: true,
                translations: {
                  'key': 'value'
                }
              },
              EN: {
                active: false, // should be negated which will usually done due to the user interaction (checkbox)
                translations: {
                  'key': 'value'
                }
              }
            };

            var ctrl = buildController('DE', languages);

            // when
            ctrl.$onInit();
            ctrl.handleLanguageChange('EN');
            modalPromise.reject();
            $scope.$apply();

            // then
            expect(modalService.confirmDelete).toHaveBeenCalled();
            expect(ctrl.data.languages.EN.active).toBe(true);
          });
    });
  });
})();
