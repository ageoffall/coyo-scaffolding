(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoRefresh';

  describe('module: ' + moduleName, function () {

    var $scope, $rootScope, $compile, template, element, beforeValue, afterValue;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$rootScope_, _$compile_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $compile = _$compile_;
        template = '<coyo-refresh watch="watcher" on-scope-event="refresh" on-root-scope-event="rootRefresh">' +
            '<span>{{::value}}</span>' +
            '</coyo-refresh>';
        element = $compile(angular.element(template))($scope);
        beforeValue = 'before';
        afterValue = 'after';

        $scope.value = beforeValue;
        $scope.$digest();
        expect(getValue()).toBe(beforeValue);
      }));

      it('should not refresh one-time binding on scope change', function () {
        // when
        $scope.value = afterValue;
        $scope.$digest();

        // then
        expect(getValue()).toBe(beforeValue);
      });

      it('should refresh one-time binding on watch', function () {
        // when
        $scope.value = afterValue;
        $scope.watcher = 1;
        $scope.$digest();

        // then
        expect(getValue()).toBe('after');
      });

      it('should refresh one-time binding on scope event', function () {
        // when
        $scope.value = afterValue;
        $scope.$broadcast('refresh');
        $scope.$digest();

        // then
        expect(getValue()).toBe('after');
      });

      it('should refresh one-time binding on root scope event', function () {
        // when
        $scope.value = afterValue;
        $rootScope.$emit('rootRefresh');
        $scope.$digest();

        // then
        expect(getValue()).toBe('after');
      });

      function getValue() {
        return element.find('span').text();
      }
    });
  });
})();
