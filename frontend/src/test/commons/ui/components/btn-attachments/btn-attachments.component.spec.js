(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {
    var $scope, $componentController;
    var authService, $timeout, fileLibraryModalService, utilService, $injector, ngxGDrivePickerService,
        ngxGoogleApiService, ngxO365ApiService, ngxSharePointFilepickerService;

    beforeEach(module(moduleName));

    beforeEach(module(function ($provide) {
      ngxGDrivePickerService = jasmine.createSpyObj('ngxGDrivePickerService', ['open']);
      ngxGoogleApiService = jasmine.createSpyObj('ngxGoogleApiService', ['isGoogleApiActive']);
      ngxO365ApiService = jasmine.createSpyObj('ngxO365ApiService', ['isApiActive']);
      ngxSharePointFilepickerService = jasmine.createSpyObj('ngxSharePointFilepickerService', ['openFilepicker']);

      $provide.value('ngxGDrivePickerService', ngxGDrivePickerService);
      $provide.value('ngxGoogleApiService', ngxGoogleApiService);
      $provide.value('ngxO365ApiService', ngxO365ApiService);
      $provide.value('ngxSharePointFilepickerService', ngxSharePointFilepickerService);
    }
    ));

    beforeEach(inject(
        function (_$rootScope_, _$componentController_, _authService_, _$timeout_, _fileLibraryModalService_,
                  _utilService_, _$injector_) {
          $scope = _$rootScope_.$new();
          $componentController = _$componentController_;
          authService = _authService_;
          $timeout = _$timeout_;
          fileLibraryModalService = _fileLibraryModalService_;
          utilService = _utilService_;
          $injector = _$injector_;
        }));

    describe('component: coyoBtnAttachments', function () {
      describe('controller: attachmentButtonController', function () {

        function buildController(author) {
          return $componentController('coyoBtnAttachments', {
            $element: null,
            authService: authService,
            $timeout: $timeout,
            fileLibraryModalService: fileLibraryModalService,
            utilService: utilService,
            $injector: $injector
          }, {
            author: author
          });
        }

        function createUserWithGlobalPermissions(globalPermissions) {
          return {
            id: 1,
            globalPermissions: globalPermissions
          };
        }

        it('should enable file library, when the author has the ACCESS_FILES permission', function () {
          // given
          ngxGoogleApiService.isGoogleApiActive.and.callFake(function () {
            return {
              subscribe: function (callBack) {
                return callBack(true);
              }
            };
          });
          ngxO365ApiService.isApiActive.and.callFake(function () {
            return {
              subscribe: function (callBack) {
                return callBack(true);
              }
            };
          });
          var author = createUserWithGlobalPermissions(['ACCESS_FILES']);

          // when
          var ctrl = buildController(author);
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(ctrl.fileLibraryActivated).toBe(true);
        });

        it('should disable file library, when the author does not have the ACCESS_FILES permission', function () {
          // given
          ngxGoogleApiService.isGoogleApiActive.and.callFake(function () {
            return {
              subscribe: function (callBack) {
                return callBack(false);
              }
            };
          });
          ngxO365ApiService.isApiActive.and.callFake(function () {
            return {
              subscribe: function (callBack) {
                return callBack(false);
              }
            };
          });
          var author = createUserWithGlobalPermissions([]);

          // when
          var ctrl = buildController(author);
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(ctrl.fileLibraryActivated).toBe(false);
        });
      });
    });
  });
})();
