import {InjectionToken} from '@angular/core';

export const CSS_VARS = new InjectionToken<any>('cssVars');
export const WINDOW = new InjectionToken<any>('Window');
export const JQUERY = new InjectionToken<any>('jQuery');
export const FROALA_EDITOR = new InjectionToken<any>('froalaEditor');
