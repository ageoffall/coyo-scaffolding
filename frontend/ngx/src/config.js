window.Config = {
  applicationName: 'Coyo',
  debug: true,
  likeReloadIntervalMinutes: 1,
  maxReconnectDelaySeconds: 30,
  minReconnectDelaySeconds: 10
};
