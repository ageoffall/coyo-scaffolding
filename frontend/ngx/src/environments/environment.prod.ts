export const environment = {
  production: true,
  enableAnimations: false,
  disabledServices: ([] as string[])
};
