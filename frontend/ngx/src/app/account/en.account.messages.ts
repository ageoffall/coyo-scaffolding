import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.HEADLINE': 'Integration settings',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.G_SUITE.HEADLINE': 'Google Calendar',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.OFFICE_365.HEADLINE': 'Office Calendar',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.CHECKBOX': 'Synchronize events',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.G_SUITE.HINT': 'Events you are attending will automatically appear in your Google Calendar.',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.OFFICE_365.HINT': 'Events you are attending will automatically appear in your Office Calendar.',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.SUCCESS': 'Event synchronization has successfully been ' +
      '{activated, select, no {deactivated} yes {activated} other {updated}}.',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.ERROR': 'Synchronization setting could not be saved. Please try again later.'
  }
};
