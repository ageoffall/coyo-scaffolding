import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserIntegrationSettingsService} from '@domain/integration-settings/user-integration-settings.service';
import {TranslateService} from '@ngx-translate/core';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {of, throwError} from 'rxjs';
import {IntegrationSettingsComponent} from './integration-settings.component';

describe('IntegrationSettingsComponent', () => {
  let component: IntegrationSettingsComponent;
  let fixture: ComponentFixture<IntegrationSettingsComponent>;
  let userIntegrationSettingsService: jasmine.SpyObj<UserIntegrationSettingsService>;
  let translateService: jasmine.SpyObj<TranslateService>;
  let notificationService: jasmine.SpyObj<Ng1CoyoNotification>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntegrationSettingsComponent],
      providers: [{
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['get'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success', 'error'])
      }, {
        provide: UserIntegrationSettingsService,
        useValue: jasmine.createSpyObj('userIntegrationSettingsService',
          ['isCalendarSyncEnabled', 'enableCalendarSync', 'disableCalendarSync']
        )
      }]
    }).overrideTemplate(IntegrationSettingsComponent, '')
      .compileComponents();

    userIntegrationSettingsService = TestBed.get(UserIntegrationSettingsService);
    translateService = TestBed.get(TranslateService);
    notificationService = TestBed.get(NG1_COYO_NOTIFICATION_SERVICE);
  }));

  beforeEach(() => {
    userIntegrationSettingsService.isCalendarSyncEnabled.and.returnValue(of(true));
    translateService.get.and.returnValue(of('message'));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch calendar sync status on init', () => {
    // when
    component.ngOnInit();

    // then
    expect(component.syncEvents).toBeTruthy();
  });

  it('should enable syncEvents on update', () => {
    // given
    component.syncEvents.setValue(true, {emitEvent: false});
    userIntegrationSettingsService.enableCalendarSync.and.returnValue(of(null));

    // when
    component.update();

    // then
    expect(userIntegrationSettingsService.enableCalendarSync).toHaveBeenCalled();
  });

  it('should disable syncEvents on update', () => {
    // given
    component.syncEvents.setValue(false, {emitEvent: false});
    userIntegrationSettingsService.disableCalendarSync.and.returnValue(of(null));

    // when
    component.update();

    // then
    expect(userIntegrationSettingsService.disableCalendarSync).toHaveBeenCalled();
  });

  it('should update on value change', () => {
    // given
    spyOn(component, 'update');
    component.ngOnInit();

    // when
    component.syncEvents.setValue(false);

    // then
    expect(component.update).toHaveBeenCalled();

  });

  it('should show success message on successful update', () => {
    // given
    component.syncEvents.setValue(false, {emitEvent: false});
    userIntegrationSettingsService.disableCalendarSync.and.returnValue(of(null));

    // when
    component.update();

    // then
    expect(notificationService.success).toHaveBeenCalled();
  });

  it('should show error message on non successful update an reset syncEvents value', () => {
    // given
    component.syncEvents.setValue(false, {emitEvent: false});
    userIntegrationSettingsService.disableCalendarSync.and.returnValue(throwError({status: 404}));

    // when
    component.update();

    // then
    expect(notificationService.error).toHaveBeenCalled();
    expect(component.syncEvents.value).toBeTruthy();
  });

  it('should unsubscribe in ngOnDestroy', () => {
    // given
    spyOn(component.syncEventsSubscription, 'unsubscribe');

    // when
    component.ngOnDestroy();

    // then
    expect(component.syncEventsSubscription.unsubscribe).toHaveBeenCalled();
  });
});
