import {HttpHeaders} from '@angular/common/http';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {UserIntegrationSettingsService} from '@domain/integration-settings/user-integration-settings.service';
import {TranslateService} from '@ngx-translate/core';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {Subscription} from 'rxjs';

/**
 * User integration settings. Users can configure integration related settings like syncing events
 * with a Google Calendar.
 */
@Component({
  selector: 'coyo-integration-settings',
  templateUrl: './integration-settings.component.html',
  styleUrls: ['./integration-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IntegrationSettingsComponent implements OnInit, OnDestroy {

  /**
   * Type of the integration API like G_SUITE or OFFICE_365
   */
  @Input() type: string;

  syncEvents: FormControl;
  syncEventsSubscription: Subscription;
  integrationType: string;
  integrationIcon: string;

  constructor(
      @Inject(NG1_COYO_NOTIFICATION_SERVICE) protected coyoNotification: Ng1CoyoNotification,
      private translate: TranslateService,
      private userIntegrationSettingsService: UserIntegrationSettingsService,
      private cd: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    if ('G_SUITE' === this.type) {
      this.integrationType = 'G_SUITE';
      this.integrationIcon = 'zmdi zmdi-google';
    }
    if ('OFFICE_365' === this.type) {
      this.integrationType = 'OFFICE_365';
      this.integrationIcon = 'zmdi zmdi-coyo zmdi-coyo-office365';
    }
    this.syncEvents = new FormControl();
    this.userIntegrationSettingsService.isCalendarSyncEnabled().subscribe(response => {
      this.syncEvents.setValue(response);
      this.syncEventsSubscription = this.syncEvents.valueChanges.subscribe(() => this.update());
      this.cd.detectChanges();
    });
  }

  /**
   * Saves the current settings at the server.
   */
  update(): void {
    const httpOptions = {
      headers: new HttpHeaders({
        handleErrors: 'false'
      })
    };
    if (this.syncEvents.value) {
      this.userIntegrationSettingsService.enableCalendarSync(httpOptions)
          .subscribe(() => this.onUpdateSuccess(), () => this.onUpdateError());
    } else {
      this.userIntegrationSettingsService.disableCalendarSync(httpOptions)
          .subscribe(() => this.onUpdateSuccess(), () => this.onUpdateError());
    }
  }

  private onUpdateSuccess(): void {
    this.translate.get('MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.SUCCESS', {activated: this.syncEvents.value ? 'yes' : 'no'})
        .subscribe(message => this.coyoNotification.success(message, false));
  }

  private onUpdateError(): void {
    this.coyoNotification.error('MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.ERROR');
    this.syncEvents.setValue(!this.syncEvents.value, {emitEvent: false});
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    this.syncEventsSubscription.unsubscribe();
  }
}
