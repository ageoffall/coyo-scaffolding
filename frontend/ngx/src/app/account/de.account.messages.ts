import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.HEADLINE': 'Integrationen',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.G_SUITE.HEADLINE': 'Google Kalender',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.OFFICE_365.HEADLINE': 'Office Kalender',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.CHECKBOX': 'Events synchronisieren',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.G_SUITE.HINT': 'Events an denen du teilnimmst, erscheinen automatisch in deinem Google Kalender.',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.OFFICE_365.HINT': 'Events an denen du teilnimmst, erscheinen automatisch in deinem Office Kalender.',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.SUCCESS': 'Die Synchronisation wurde erfolgreich ' +
      '{activated, select, no {deaktiviert} yes {aktiviert} other {aktualisiert}}.',
    'MODULE.ACCOUNT.SETTINGS.INTEGRATION.EVENTS.UPDATE.ERROR': 'Die Einstellung konnt enicht gespeichert werden. Bitte versuche es später nochmal.'
  }
};
