import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'EMOJI.CATEGORIES.ACTIVITY': 'Aktivität',
    'EMOJI.CATEGORIES.CUSTOM': 'Benutzerdefiniert',
    'EMOJI.CATEGORIES.FLAGS': 'Flaggen',
    'EMOJI.CATEGORIES.FOODS': 'Essen & Trinken',
    'EMOJI.CATEGORIES.NATURE': 'Natur',
    'EMOJI.CATEGORIES.PEOPLE': 'Leute',
    'EMOJI.CATEGORIES.PLACES': 'Reisen & Orte',
    'EMOJI.CATEGORIES.OBJECTS': 'Objekte',
    'EMOJI.CATEGORIES.RECENT': 'Häufig verwendet',
    'EMOJI.CATEGORIES.SEARCH': 'Suchergebnisse',
    'EMOJI.CATEGORIES.SYMBOLS': 'Symbole',
    'EMOJI.LABEL': 'Emoji auswählen',
    'EMOJI.NOT_FOUND': 'Kein Emoji gefunden',
    'EMOJI.SEARCH': 'Suche'
  }
};
