import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'EMOJI.CATEGORIES.ACTIVITY': 'Activity',
    'EMOJI.CATEGORIES.CUSTOM': 'Custom',
    'EMOJI.CATEGORIES.FLAGS': 'Flags',
    'EMOJI.CATEGORIES.FOODS': 'Food & Drink',
    'EMOJI.CATEGORIES.NATURE': 'Animals & Nature',
    'EMOJI.CATEGORIES.OBJECTS': 'Objects',
    'EMOJI.CATEGORIES.PEOPLE': 'Smileys & People',
    'EMOJI.CATEGORIES.PLACES': 'Travel & Places',
    'EMOJI.CATEGORIES.RECENT': 'Frequently Used',
    'EMOJI.CATEGORIES.SEARCH': 'Search Results',
    'EMOJI.CATEGORIES.SYMBOLS': 'Symbols',
    'EMOJI.LABEL': 'Select an emoji',
    'EMOJI.NOT_FOUND': 'No emoji found',
    'EMOJI.SEARCH': 'Search'
  }
};
