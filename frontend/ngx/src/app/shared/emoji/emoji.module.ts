import {NgModule} from '@angular/core';
import {PickerModule} from '@coyo/ngx-emoji-mart';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {NgxPopperModule} from 'ngx-popper';
import {messagesDe} from './de.emoji.messages';
import {EmojiPickerComponent} from './emoji-picker/emoji-picker.component';
import {messagesEn} from './en.emoji.messages';

/**
 * Module to provide the emoji picker.
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    TooltipModule,
    NgxPopperModule,
    PickerModule,
    TooltipModule
  ],
  declarations: [
    EmojiPickerComponent
  ],
  exports: [
    EmojiPickerComponent
  ],
  entryComponents: [
    EmojiPickerComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ],
})
export class EmojiModule {}
