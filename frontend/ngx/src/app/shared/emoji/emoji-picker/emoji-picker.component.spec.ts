import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {EmojiData, EmojiEvent} from '@coyo/ngx-emoji-mart/ngx-emoji';
import {TranslateService} from '@ngx-translate/core';
import {PopperController} from 'ngx-popper';
import {of} from 'rxjs';
import {EmojiPickerComponent} from './emoji-picker.component';

describe('EmojiPickerComponent', () => {
  let component: EmojiPickerComponent;
  let fixture: ComponentFixture<EmojiPickerComponent>;
  let translateService: jasmine.SpyObj<TranslateService>;
  let input: jasmine.SpyObj<HTMLTextAreaElement>;
  let popperController: jasmine.SpyObj<PopperController>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmojiPickerComponent],
      imports: [],
      providers: [{
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['get'])
      }]
    }).overrideTemplate(EmojiPickerComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmojiPickerComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);

    input = jasmine.createSpyObj('input', ['focus', 'setSelectionRange', 'dispatchEvent', 'addEventListener']);
    component.input = input;

    popperController = jasmine.createSpyObj('PopperController', ['hide']);
    component.popperController = popperController;
  });

  it('should create', () => {
    // given
    translateService.get.and.returnValue(of());

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init i18n keys', fakeAsync(() => {
    // given
    translateService.get.and.returnValue(of({
      'EMOJI.CATEGORIES.ACTIVITY': 'emoji.categories.activity',
      'EMOJI.CATEGORIES.CUSTOM': 'emoji.categories.custom',
      'EMOJI.CATEGORIES.FLAGS': 'emoji.categories.flags',
      'EMOJI.CATEGORIES.FOODS': 'emoji.categories.foods',
      'EMOJI.CATEGORIES.NATURE': 'emoji.categories.nature',
      'EMOJI.CATEGORIES.OBJECTS': 'emoji.categories.objects',
      'EMOJI.CATEGORIES.PEOPLE': 'emoji.categories.people',
      'EMOJI.CATEGORIES.PLACES': 'emoji.categories.places',
      'EMOJI.CATEGORIES.RECENT': 'emoji.categories.recent',
      'EMOJI.CATEGORIES.SEARCH': 'emoji.categories.search',
      'EMOJI.CATEGORIES.SYMBOLS': 'emoji.categories.symbols',
      'EMOJI.NOT_FOUND': 'emoji.not_found',
      'EMOJI.SEARCH': 'emoji.search'
    }));

    // when
    fixture.detectChanges();

    // then
    component.i18n$.subscribe(i18n => expect(i18n).toEqual({
      search: 'emoji.search',
      notfound: 'emoji.not_found',
      categories: {
        search: 'emoji.categories.search',
        recent: 'emoji.categories.recent',
        people: 'emoji.categories.people',
        nature: 'emoji.categories.nature',
        foods: 'emoji.categories.foods',
        activity: 'emoji.categories.activity',
        places: 'emoji.categories.places',
        objects: 'emoji.categories.objects',
        symbols: 'emoji.categories.symbols',
        flags: 'emoji.categories.flags',
        custom: 'emoji.categories.custom'
      }
    }));

    // finally
    tick();
  }));

  it('should select an emoji', () => {
    // given
    (input as any).value = 'Hello world!';
    (input as any).selectionStart = 6;
    (input as any).selectionEnd = 11;

    // when
    component.emojiSelect({
      $event: jasmine.createSpyObj('$event', ['preventDefault']),
      emoji: {native: '😀'} as EmojiData
    } as EmojiEvent);

    // then
    expect((input as any).value).toBe('Hello 😀!');
    expect(input.setSelectionRange).toHaveBeenCalledWith(8, 8);
    expect(input.focus).toHaveBeenCalled();
    expect(input.dispatchEvent).toHaveBeenCalled();
  });

  it('should close emoji picker', () => {
    // given
    translateService.get.and.returnValue(of());

    // when
    component.closePicker();

    // then
    expect(popperController.hide).toHaveBeenCalled();
  });
});
