import {TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {VideoDropdownPlugin} from '@shared/rte/video-plugin/video-dropdown-plugin';
import {VideoFromFileLibraryPlugin} from '@shared/rte/video-plugin/video-from-file-library-plugin';
import {VideoFromGSuitePlugin} from '@shared/rte/video-plugin/video-from-g-suite-plugin';
import {VideoFromUrlPlugin} from '@shared/rte/video-plugin/video-from-url-plugin';

describe('VideoDropdownPlugin', () => {
  let videoDropdownPlugin: VideoDropdownPlugin;
  let froala: jasmine.SpyObj<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VideoDropdownPlugin, {
        provide: FROALA_EDITOR,
        useValue: jasmine.createSpyObj('froala', ['RegisterCommand'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }]
    });

    videoDropdownPlugin = TestBed.get(VideoDropdownPlugin);
    froala = TestBed.get(FROALA_EDITOR);
    froala.COMMANDS = {};
  });

  it('should be created', () => {
    expect(videoDropdownPlugin).toBeTruthy();
  });

  it('should initialize image button command', () => {
    // given
    froala.COMMANDS = {};
    froala.COMMANDS[VideoFromUrlPlugin.KEY] = {title: 'URL'};

    // when
    videoDropdownPlugin.initialize({} as RteSettings);

    // then
    expect(froala.RegisterCommand).not.toHaveBeenCalledWith();
    expect(froala.COMMANDS['coyoInsertVideo']).toEqual(jasmine.objectContaining({
      title: 'URL'
    }));
  });

  it('should initialize image dropdown command', () => {
    // given
    froala.COMMANDS = {};
    froala.COMMANDS[VideoFromUrlPlugin.KEY] = {title: 'URL'};
    froala.COMMANDS[VideoFromFileLibraryPlugin.KEY] = {title: 'COYO'};
    froala.COMMANDS[VideoFromGSuitePlugin.KEY] = {title: 'Google'};

    // when
    videoDropdownPlugin.initialize({} as RteSettings);

    // then
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertVideo', jasmine.objectContaining({
      type: 'dropdown',
      options: {
        coyoInsertVideoFromURL: 'URL',
        coyoInsertVideoFromFileLibrary: 'COYO',
        coyoInsertVideoFromGSuite: 'Google'
      }
    }));
  });
});
