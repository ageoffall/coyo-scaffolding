import {Inject, Injectable} from '@angular/core';
import {Document} from '@domain/file/document';
import {DocumentService} from '@domain/file/document/document.service';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR} from '@root/injection-tokens';
import {Ng1FileLibraryModalService} from '@root/typings';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import {NG1_FILE_LIBRARY_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {from} from 'rxjs';
import {RtePlugin} from '../rte-plugin';

/**
 * A custom RTE plugin that connects the Froala editor to the COYO file library for image selection.
 */
@Injectable()
export class ImageFromFileLibraryPlugin extends RtePlugin {

  static readonly KEY: string = 'coyoInsertImageFromFileLibrary';

  constructor(@Inject(FROALA_EDITOR) private froala: any,
              @Inject(NG1_FILE_LIBRARY_MODAL_SERVICE) private fileLibraryModalService: Ng1FileLibraryModalService,
              private translateService: TranslateService,
              private documentService: DocumentService) {
    super();
  }

  protected doInitialize(settings: RteSettings): void {
    if (!settings.canAccessFiles) {
      return; // user is not allowed to access the file library
    }

    const plugin = this;
    plugin.froala.RegisterCommand(ImageFromFileLibraryPlugin.KEY, {
      title: plugin.translateService.instant('RTE.INSERT_IMAGE'),
      plugin: 'coyoImagePlugin',
      icon: 'insertImage',
      undo: true,
      focus: false,
      showOnMobile: true,
      refreshAfterCallback: false,
      callback: function(): void {
        plugin.openCoyoFileLibrary(this);
      }
    });
  }

  private openCoyoFileLibrary(editor: any): void {
    const plugin = this;
    const wrapper = editor.$oel.find('.fr-wrapper');
    const scrollPosition = wrapper.scrollTop();

    editor.selection.save();
    const prevImage = editor.image.get();
    from(plugin.fileLibraryModalService.open(editor.getSender(), {
      selectMode: 'single',
      filterContentType: ['image/gif', 'image/jpeg', 'image/png', 'image/bmp', 'image/svg+xml'],
      initialFolder: {
        id: editor.getApp() ? editor.getApp().rootFolderId : undefined
      }
    })).subscribe((document: Document) => {
      const url = plugin.documentService.getDownloadUrl(document);
      editor.selection.restore();
      editor.image.insert(url, true, {}, prevImage);
      wrapper.scrollTop(scrollPosition);
    }, () => {
      editor.selection.restore();
      wrapper.scrollTop(scrollPosition);
    });
  }
}
