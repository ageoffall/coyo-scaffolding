import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {AuthService} from '@core/auth/auth.service';
import {User} from '@domain/user/user';
import {of} from 'rxjs';
import {RteSettingsService} from './rte-settings.service';

describe('RteSettingsService', () => {
  let service: RteSettingsService;
  let authService: jasmine.SpyObj<AuthService>;
  let googleApiService: jasmine.SpyObj<GoogleApiService>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RteSettingsService, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser'])
      }, {
        provide: GoogleApiService,
        useValue: jasmine.createSpyObj('googleApiService', ['isGoogleApiActive'])
      }]
    });

    service = TestBed.get(RteSettingsService);
    authService = TestBed.get(AuthService);
    googleApiService = TestBed.get(GoogleApiService);
    httpClient = TestBed.get(HttpClient);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should GET settings', () => {
    // given
    authService.getUser.and.returnValue(of({
      globalPermissions: ['ACCESS_FILES']
    } as User));
    googleApiService.isGoogleApiActive.and.returnValue(of(true));

    // when
    const response = {license: '123'};
    const result$ = service.getSettings();

    // then
    result$.subscribe(data => expect(data).toEqual({
      license: response.license,
      canAccessFiles: true,
      canAccessGoogle: true
    }));
    const req = httpTestingController.expectOne(request => request.url === '/web/rte-settings');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  });
});
