import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FROALA_EDITOR, JQUERY} from '@root/injection-tokens';
import {RteSettings} from '@shared/rte/rte/rte-settings/rte-settings';
import * as _ from 'lodash';
import {AnchorPluginService} from './anchor-plugin.service';

describe('AnchorPluginService', () => {
  let froala: jasmine.SpyObj<any>;
  let jQuery: jasmine.SpyObj<any>;

  beforeEach(() => {
    const jQueryMock = jasmine.createSpy('jQuery') as any;
    jQueryMock.extend = jasmine.createSpy('jQueryExtend');
    const froalaMock = jasmine.createSpyObj('froala', ['RegisterCommand', 'DefineIcon']);
    froalaMock.PLUGINS = {};

    TestBed.configureTestingModule({
      providers: [AnchorPluginService, {
        provide: FROALA_EDITOR,
        useValue: froalaMock
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: JQUERY,
        useValue: jQueryMock
      }]
    });
    froala = TestBed.get(FROALA_EDITOR);
    jQuery = TestBed.get(JQUERY);
  });

  it('should be created', inject([AnchorPluginService], (service: AnchorPluginService) => {
    expect(service).toBeTruthy();
  }));

  it('should register icons', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // given
    const icons = [
      'coyoAnchorIcon',
      'coyoAnchorRemoveIcon'];

    // when
    service.initialize({} as RteSettings);

    // then
    icons.forEach(icon => expect(froala.DefineIcon).toHaveBeenCalledWith(icon, jasmine.any(Object)));
  }));

  it('should register commands', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // when
    service.initialize({} as RteSettings);

    // then
    expect(froala.PLUGINS.coyoAnchorPlugin()).toBeTruthy();
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertAnchor', jasmine.any(Object));
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoInsertAnchorImage', jasmine.any(Object));
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoEditAnchor', jasmine.any(Object));
    expect(froala.RegisterCommand).toHaveBeenCalledWith('coyoRemoveAnchor', jasmine.any(Object));
  }));

  it('should insert anchor', inject([AnchorPluginService], (service: AnchorPluginService): void => {
    // given
    const editor = {
      image: jasmine.createSpyObj('image', ['get']),
      popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
      selection: jasmine.createSpyObj('froalaSelection', ['get', 'save', 'restore']),
      html: jasmine.createSpyObj('html', ['get', 'getSelected', 'insert']),
      clean: jasmine.createSpyObj('clean', ['html'])
    };
    const anchorNode = {};
    editor['selection'].get.and.returnValue({
      anchorNode: anchorNode
    });
    const popup = jasmine.createSpyObj('popup', ['find']);
    const textInputs = jasmine.createSpyObj('textInputs', ['filter']);
    const textValue = jasmine.createSpyObj('value', ['val']);
    editor['popups'].get.and.returnValue(popup);
    popup['find'].and.returnValue(textInputs);
    textInputs.filter.and.returnValue(textValue);
    textValue.val.and.returnValue('an anchor');
    editor['html'].getSelected.and.returnValue(undefined);
    editor['html'].get.and.returnValue('<p></p>');
    editor['image'].get.and.returnValue(undefined);
    editor['clean'].html.and.callFake((html: string) => html);

    jQuery.and.callFake(function(param: object): jasmine.SpyObj<JQuery> {
      if (param === anchorNode) {
        const anchorNodeJQueryObj = jasmine.createSpyObj('anchorNodeJquery', ['find']);
        anchorNodeJQueryObj.find.and.returnValue(undefined);
        return anchorNodeJQueryObj;
      } else {
        const jQueryMock = jasmine.createSpyObj('jQueryMock', ['prop', 'html', 'find']);
        jQueryMock.prop.and.returnValue('');
        jQueryMock.html.and.returnValue('');
        jQueryMock.find.and.returnValue(jQueryMock);
        return jQueryMock;
      }
    });

    // when
    service.initialize({} as RteSettings);
    callCallbackOfCommand('coyoInsertAnchor', ['coyoInsertAnchorSubmit'], editor);

    // then
    expect(editor['popups'].hide).toHaveBeenCalled();
    expect(editor['html'].insert).toHaveBeenCalledTimes(1);
  }));

  it('should insert anchor link', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // given
    const editor = {
      popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
      selection: jasmine.createSpyObj('froalaSelection', ['get', 'save', 'restore']),
      html: jasmine.createSpyObj('html', ['getSelected', 'insert']),
      clean: jasmine.createSpyObj('clean', ['html'])
    };
    const anchorNode = {};
    editor['selection'].get.and.returnValue({
      anchorNode: anchorNode
    });
    const popup = jasmine.createSpyObj('popup', ['find']);
    const textValue = jasmine.createSpyObj('value', ['val']);
    editor['popups'].get.and.returnValue(popup);
    popup.find.and.returnValue(textValue);
    textValue.val.and.returnValue('an anchor link');
    editor['html'].getSelected.and.returnValue(undefined);
    editor['clean'].html.and.callFake((html: string) => html);

    service.initialize({} as RteSettings);
    callCallbackOfCommand('coyoInsertAnchor', ['coyoInsertAnchorLinkSubmit'], editor);

    // then
    expect(editor['popups'].hide).toHaveBeenCalled();
    expect(editor['html'].insert).toHaveBeenCalledTimes(1);
  }));

  it('should edit anchor', inject([AnchorPluginService], (service: AnchorPluginService) => {
    // given
    const editor = {
      html: jasmine.createSpyObj('html', ['get']),
      popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
      selection: jasmine.createSpyObj('selection', ['get', 'element'])
    };
    const anchor = jasmine.createSpyObj('anchor', ['id']);
    const popup = jasmine.createSpyObj('popup', ['find']);
    const textInputs = jasmine.createSpyObj('textInputs', ['filter']);
    const textValue = jasmine.createSpyObj('value', ['val']);
    editor['html'].get.and.returnValue('');
    editor['popups'].get.and.returnValue(popup);
    popup.find.and.returnValue(textInputs);
    textInputs.filter.and.returnValue(textValue);
    textValue.val.and.returnValue('an anchor');
    editor['selection'].get.and.returnValue({});
    editor['selection'].element.and.returnValue(anchor);
    anchor.id.and.returnValue('an-anchor2');
    anchor.classList = jasmine.createSpyObj('anchorClassMock', ['contains']);
    anchor.classList.contains.and.returnValue(true);
    const jQueryAnchor = jasmine.createSpyObj('jQueryMock', ['find', 'attr']);
    jQueryAnchor.find.and.returnValue([]);
    jQuery.and.callFake(() =>
      jQueryAnchor);

    // when
    service.initialize({} as RteSettings);
    callCallbackOfCommand('coyoEditAnchor', ['coyoEditAnchorSubmit'], editor);

    // then
    expect(editor['popups'].hide).toHaveBeenCalled();
    expect(jQueryAnchor.attr).toHaveBeenCalledWith('id', 'an-anchor');
  }));

  it('should remove anchor when editing with empty input field',
    inject([AnchorPluginService], (service: AnchorPluginService) => {
      // given
      const editor = {
        html: jasmine.createSpyObj('html', ['get']),
        link: jasmine.createSpyObj('link', ['remove']),
        popups: jasmine.createSpyObj('froalaPopups', ['get', 'hide']),
        selection: jasmine.createSpyObj('selection', ['get', 'element'])
      };
      const anchor = jasmine.createSpyObj('anchor', ['id']);
      const popup = jasmine.createSpyObj('popup', ['find']);
      const textInputs = jasmine.createSpyObj('textInputs', ['filter']);
      const textValue = jasmine.createSpyObj('value', ['val']);
      editor['html'].get.and.returnValue('');
      editor['popups'].get.and.returnValue(popup);
      popup.find.and.returnValue(textInputs);
      textInputs.filter.and.returnValue(textValue);
      textValue.val.and.returnValue('');
      editor['selection'].get.and.returnValue({});
      editor['selection'].element.and.returnValue(anchor);
      anchor.id.and.returnValue('an-anchor2');
      anchor.classList = jasmine.createSpyObj('anchorClassMock', ['contains']);
      anchor.classList.contains.and.returnValue(true);
      const jQueryAnchor = jasmine.createSpyObj('jQueryMock', ['find', 'removeAttr', 'removeClass']);
      jQueryAnchor.find.and.returnValue([]);
      jQuery.and.callFake(() => jQueryAnchor);

      // when
      service.initialize({} as RteSettings);
      callCallbackOfCommand('coyoEditAnchor', ['coyoEditAnchorSubmit'], editor);

      // then
      expect(editor['popups'].hide).toHaveBeenCalled();
      expect(editor['link'].remove).toHaveBeenCalled();
    }));

  function callCallbackOfCommand(commandKey: string, params: string[], editor: any): void {
    const calls = froala.RegisterCommand.calls.all();
    const command = _.find(calls, (call: jasmine.CallInfo): boolean => call.args[0] === commandKey);
    params.unshift(commandKey);
    command.args[1].callback.apply(editor, params);
  }
});
