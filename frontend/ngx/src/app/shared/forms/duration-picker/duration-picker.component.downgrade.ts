import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {DurationPickerComponent} from './duration-picker.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoDurationPicker', downgradeComponent({
    component: DurationPickerComponent
  }));
