import {AbstractControl, FormGroup} from '@angular/forms';
import {of} from 'rxjs';
import {CoyoValidators} from './validators';

describe('CoyoValidators', () => {

  describe('notBlank', () => {

    it('validate a valid input', () => {
      // given
      const control = {value: 'Valid text'} as AbstractControl;

      // when
      const result = CoyoValidators.notBlank(control);

      // then
      expect(result).toBeNull();
    });

    it('validate an invalid input (null)', () => {
      // given
      const control = {value: null} as AbstractControl;

      // when
      const result = CoyoValidators.notBlank(control);

      // then
      expect(result).toEqual({notBlank: true});
    });

    it('validate an invalid input (blank)', () => {
      // given
      const control = {value: '   '} as AbstractControl;

      // when
      const result = CoyoValidators.notBlank(control);

      // then
      expect(result).toEqual({notBlank: true});
    });
  });

  describe('options', () => {

    it('validate a valid input', () => {
      // given
      const control = {value: 'Apple'} as AbstractControl;

      // when
      const result = CoyoValidators.options('Apple', 'Banana', 'Orange')(control);

      // then
      expect(result).toBeNull();
    });

    it('validate an invalid input', () => {
      // given
      const control = {value: 'Peach'} as AbstractControl;

      // when
      const result = CoyoValidators.options('Apple', 'Banana', 'Orange')(control);

      // then
      expect(result).toEqual({options: {options: ['Apple', 'Banana', 'Orange'], actual: 'Peach'}});
    });
  });

  describe('requiredIfTrue', () => {

    it('validate an valid input', () => {
      // given
      const requiredTrueControlName = 'requiredNewControl';
      const requiredControlName = 'requiredControl';
      const form = createFormGroup(true, 'test');

      // when
      const validatorFn = CoyoValidators.requiredIfTrue(requiredTrueControlName, requiredControlName, control => control.value);
      const result = validatorFn(form);

      // then
      expect(result).toBeFalsy();
    });

    it('validate an invalid input', () => {
      // given
      const requiredTrueControlName = 'requiredNewControl';
      const requiredControlName = 'requiredControl';
      const form = createFormGroup(true, '');

      // when
      const validatorFn = CoyoValidators.requiredIfTrue(requiredTrueControlName, requiredControlName, control => control.value);
      const result = validatorFn(form);

      // then
      expect(result[requiredControlName].notBlank).toBeTruthy();
    });

    it('validate valid input when requiredControl is not necessary', () => {
      // given
      const requiredTrueControlName = 'requiredNewControl';
      const requiredControlName = 'requiredControl';
      const form = createFormGroup(false, '');

      // when
      const validatorFn = CoyoValidators.requiredIfTrue(requiredTrueControlName, requiredControlName, control => control.value);
      const result = validatorFn(form);

      // then
      expect(result).toBeFalsy();
    });

    it('validates api key positive', () => {
      // given
      const settingsService = jasmine.createSpyObj('settingsService', ['validateApiKey']);
      settingsService.validateApiKey.and.returnValue(of(true));
      const formControl = {
        parent: {
          get: jasmine.createSpy()
        }
      };
      const providerFormControl = {value: 'DEEP'};

      formControl.parent.get.and.returnValue(providerFormControl);

      // when
      const validatorFn = CoyoValidators.createApiKeyValidator(settingsService);
      validatorFn(formControl as any as AbstractControl).subscribe(value => {
        expect(value).toBeNull();
      });
    });

    it('validates api key negative', () => {
      // given
      const settingsService = jasmine.createSpyObj('settingsService', ['validateApiKey']);
      settingsService.validateApiKey.and.returnValue(of(false));
      const formControl = {
        parent: {
          get: jasmine.createSpy()
        }
      };
      const providerFormControl = {value: 'DEEP'};

      formControl.parent.get.and.returnValue(providerFormControl);

      // when
      const validatorFn = CoyoValidators.createApiKeyValidator(settingsService);
      validatorFn(formControl as any as AbstractControl).subscribe(value => {
        expect(value).toEqual({invalidKey: true});
      });
    });

    function createFormGroup(trueControlValue: boolean, requiredControlValue: string): FormGroup {
      const requiredTrueControl = {value: trueControlValue} as AbstractControl;
      const requiredControl = {value: requiredControlValue} as AbstractControl;
      const form = {
        get(name: string): AbstractControl {
          if (name === 'requiredNewControl') {
            return requiredTrueControl;
          } else {
            return requiredControl;
          }
        }
      } as FormGroup;
      return form;
    }
  });
});
