import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  Provider,
  ViewChild
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {TranslationService} from '@core/i18n/translation-service/translation.service';
import {BsLocaleService, defineLocale} from 'ngx-bootstrap';
import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import * as locales from 'ngx-bootstrap/locale';

const datePickerValueProvider: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DatePickerComponent), // tslint:disable-line:no-use-before-declare
  multi: true
};

/**
 * The date picker component.
 */
@Component({
  selector: 'coyo-date-picker',
  templateUrl: './date-picker.component.html',
  providers: [datePickerValueProvider],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerComponent implements OnInit, AfterViewChecked, ControlValueAccessor {

  /**
   * The initial date.
   */
  @Input() date?: Date = new Date();

  /**
   * The selected date.
   */
  @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();

  /**
   * The placeholder for the input form.
   */
  @Input() placeholder: string = '';

  @ViewChild('datePickerInput', {
    static: true
  }) datePickerInput: ElementRef;

  disabled: boolean = false;
  datePickerConfig: Partial<BsDatepickerConfig>;
  private onChangeFn: (date: Date) => void;
  private lastValidViewValue: string;

  constructor(private localeService: BsLocaleService,
              private translationService: TranslationService) { }

  ngOnInit(): void {
    this.datePickerConfig = {containerClass: 'theme-coyo'};
    this.defineLocales();
    this.localeService.use(this.translationService.getActiveLanguage() || 'en');
  }

  ngAfterViewChecked(): void {
    const inputElementValue = this.datePickerInput.nativeElement.value;
    if (inputElementValue && inputElementValue !== 'Invalid date') {
      this.lastValidViewValue = inputElementValue;
    }
  }

  /* tslint:disable-next-line:completed-docs */
  registerOnChange(fn: (date: Date) => void): void {
    this.onChangeFn = fn;
  }

  /* tslint:disable-next-line:completed-docs */
  registerOnTouched(fn: (date: Date) => void): void {
  }

  /* tslint:disable-next-line:completed-docs */
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  /* tslint:disable-next-line:completed-docs */
  writeValue(value: Date): void {
    this.date = value;
  }

  /**
   * Emits when bsValue changed and is valid.
   * @param date The new date
   */
  onBsValueChange(date: Date): void {
    if (!this.isValidDate(date)) {
      return;
    }
    if (typeof this.onChangeFn === 'function') {
      this.onChangeFn(date);
    }
    this.dateChange.emit(date);
    this.lastValidViewValue = this.datePickerInput.nativeElement.value;
  }

  /**
   * Resets the input value and update model value manually.
   */
  resetInputValue(): void {
    if (this.lastValidViewValue) {
      const inputElement = this.datePickerInput.nativeElement;
      if (this.lastValidViewValue !== inputElement.value) {
        inputElement.value = this.lastValidViewValue;
        inputElement.dispatchEvent(new Event('change'));
      }
    }
  }

  private defineLocales(): void {
    for (const locale of Object.keys(locales)) {
      defineLocale(locales[locale].abbr, locales[locale]);
    }
  }

  private isValidDate(date: Date): boolean {
    return date && !isNaN(date.getTime());
  }

}
