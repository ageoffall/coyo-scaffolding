import {NgModule} from '@angular/core';

import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {HashtagModule} from '@shared/hashtags/hashtag.module';
import {MarkdownModule} from '@shared/markdown/markdown.module';
import {MentionModule} from '@shared/mention/mention.module';
import {messagesDe} from './de.messages';
import {messagesEn} from './en.messages';
import {JitTranslationOutputComponent} from './jit-translation-output/jit-translation-output.component';
import './jit-translation-output/jit-translation-output.component.downgrade';
import {JitTranslationToggleComponent} from './jit-translation-toggle/jit-translation-toggle.component';
import './jit-translation-toggle/jit-translation-toggle.component.downgrade';

/**
 * Module for JIT translations.
 */
@NgModule({
  imports: [CoyoCommonsModule, MentionModule, HashtagModule, MarkdownModule],
  providers: [
    {provide: 'messages', multi: true, useValue: messagesDe},
    {provide: 'messages', multi: true, useValue: messagesEn}
  ],
  declarations: [JitTranslationToggleComponent, JitTranslationOutputComponent],
  entryComponents: [JitTranslationToggleComponent, JitTranslationOutputComponent],
  exports: [JitTranslationToggleComponent, JitTranslationOutputComponent]
})
export class JitTranslationModule {
}
