import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {TranslateService} from '@ngx-translate/core';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {forkJoin} from 'rxjs';
import {JitTranslationService} from './jit-translation.service';

describe('JitTranslationService', () => {
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [JitTranslationService, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('TranslateService', ['instant'])
      }, {
        provide: NG1_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('coyoNotificationService', ['info'])
      }]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should get translation', inject([JitTranslationService], (service: JitTranslationService) => {
    // given
    const id = 'some-id';
    const field = 'message';
    const language = 'de';
    const text = 'Hello world!';

    // when
    service.getTranslation(id, field, language, text).subscribe(translation => {
      expect(translation).toBe('Hallo Welt!');
    });

    // then
    const req = httpTestingController.expectOne(`/web/translation/translate/${id}/${field}`);

    expect(req.request.method).toEqual('POST');

    req.flush({
      translation: 'Hallo Welt!'
    });
  }));

  it('should get the language for an entity by field in a bulked request', fakeAsync(inject([JitTranslationService], (service: JitTranslationService) => {
    // given
    const id1 = 'id-1';
    const id2 = 'id-2';
    const field = 'message';
    const typeName = 'test-type';
    const msg1 = {language: 'de', disabledLanguages: [] as string[]};
    const msg2 = {language: 'en', disabledLanguages: [] as string[]};
    const response = {
      'id-1': {
        processed: true,
        languageMappings: {
          message: msg1
        }
      }, 'id-2': {
        processed: true,
        languageMappings: {
          message: msg2
        }
      },
    };

    // when
    const get1 = service.getLanguage(id1, field, typeName);
    const get2 = service.getLanguage(id2, field, typeName);
    forkJoin([get1, get2]).subscribe(([trans1, trans2]) => {
      expect(trans1).toBe(msg1);
      expect(trans2).toBe(msg2);
    });
    tick(300);

    // then
    const req = httpTestingController.expectOne(request =>
      request.url === `/web/translation/${typeName}/language` && request.params.get('ids') === [id1, id2].join(','));

    expect(req.request.method).toEqual('GET');

    req.flush(response);
    discardPeriodicTasks();
  })));
});
