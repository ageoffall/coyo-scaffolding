import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {JitTranslationToggleComponent} from './jit-translation-toggle.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoJitTranslationToggle', downgradeComponent({
    component: JitTranslationToggleComponent
  }));
