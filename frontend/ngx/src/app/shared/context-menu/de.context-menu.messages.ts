import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'CONTEXT_MENU.SHOW': 'Optionen anzeigen'
  }
};
