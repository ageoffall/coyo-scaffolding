import {HashtagService} from './hashtag.service';
import {HashtagsPipe} from './hashtags.pipe';

describe('HashtagsPipe', () => {
  let pipe: HashtagsPipe;
  let hashtagService: jasmine.SpyObj<HashtagService>;

  beforeEach(() => {
    hashtagService = jasmine.createSpyObj('HashtagService', ['hashtags']);
    pipe = new HashtagsPipe(hashtagService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('parse hashtags', () => {
    // given
    const text = 'This is a #hashtag!';

    hashtagService.hashtags.and.returnValue('This is a <a href="#hashtag">#hashtag<a>!');

    // when
    const result = pipe.transform(text);

    // then
    expect(result).toEqual('This is a <a href="#hashtag">#hashtag<a>!');
    expect(hashtagService.hashtags).toHaveBeenCalledWith(text);
  });
});
