import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {AttachmentBtnComponent} from '@shared/files/attachment-btn/attachment-btn.component';
import {AttachmentListComponent} from '@shared/files/attachment-list/attachment-list.component';
import {AttachmentComponent} from '@shared/files/attachment/attachment.component';
import '@shared/files/external-file-handler/external-file-handler.service.downgrade';
import {FileIconByMimeTypeComponent} from '@shared/files/file-icon/file-icon-by-mime-type.component';
import {FileIconComponent} from '@shared/files/file-icon/file-icon.component';
import {FileUploadModule} from 'ng2-file-upload';
import './file-icon/file-icon.component.downgrade';

/**
 * Module that holds services concerning different types of file operations.
 */
@NgModule({
  imports: [
    ContextMenuModule,
    CoyoCommonsModule,
    FileUploadModule
  ],
  declarations: [
    AttachmentBtnComponent,
    AttachmentComponent,
    AttachmentListComponent,
    FileIconComponent,
    FileIconByMimeTypeComponent
  ],
  exports: [
    AttachmentBtnComponent,
    AttachmentListComponent,
    FileIconComponent,
    FileIconByMimeTypeComponent
  ],
  entryComponents: [
    AttachmentBtnComponent,
    FileIconComponent
  ]
})
export class FileModule {
}
