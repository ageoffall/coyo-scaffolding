import {GoogleFileMetaData} from '@app/integration/gsuite/google-api/google-file-metadata';

/**
 * Abstraction of an external file. This interface should be used for all external files.
 */
export interface ExternalFileDetails extends GoogleFileMetaData {
  /**
   * Used to open the file provider in a new tab.
   */
  externalUrl: string;

  /**
   * Used to show a small thumbnail like preview.
   */
  previewUrl: string;

  /**
   * If available, used to display the file size in the file details modal.
   */
  fileSize: number | undefined;

  /**
   * Whether or not the currently logged in user has permissions to edit the file.
   */
  canEdit: boolean;
}
