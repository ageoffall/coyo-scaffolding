import {CoyoCommonsModule} from './commons.module';

describe('CoyoCommonsModule', () => {
  let coyoCommonsModule: CoyoCommonsModule;

  beforeEach(() => {
    coyoCommonsModule = new CoyoCommonsModule();
  });

  it('should create an instance', () => {
    expect(coyoCommonsModule).toBeTruthy();
  });
});
