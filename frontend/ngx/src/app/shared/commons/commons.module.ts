import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {UiModule} from '@coyo/ui';
import {TranslateModule} from '@ngx-translate/core';
import {AnchorModule} from '@shared/anchor/anchor.module';
import {IconModule} from '@shared/icon/icon.module';
import {MaterialModule} from '@shared/material/material.module';
import {SpinnerModule} from '@shared/spinner/spinner.module';
import {TrustHtmlModule} from '@shared/trust-html/trust-html.module';
import {TrustUrlModule} from '@shared/trust-url/trust-url.module';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {NgxPermissionsModule} from 'ngx-permissions';

/**
 * Module exporting a bundle of commonly used modules.
 *
 * Please only export other modules here!
 */
@NgModule({
  exports: [
    AnchorModule,
    CommonModule,
    HttpClientModule,
    IconModule,
    MaterialModule,
    UiModule,
    NgxPermissionsModule,
    SpinnerModule,
    TranslateModule,
    TrustHtmlModule,
    TrustUrlModule,
    UIRouterUpgradeModule
  ]
})
export class CoyoCommonsModule {
}
