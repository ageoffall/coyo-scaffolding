import {NgModule} from '@angular/core';
import {NgOptionHighlightModule} from '@ng-select/ng-option-highlight';
import {NgSelectModule} from '@ng-select/ng-select';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {ContextMenuModule} from '@shared/context-menu/context-menu.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {AvatarOverlayComponent} from '@shared/sender-ui/avatar-overlay/avatar-overlay.component';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {AppHrefComponent} from './app-href/app-href.component';
import {AvatarImageComponent} from './avatar-image/avatar-image.component';
import './avatar-image/avatar-image.component.downgrade';
import './avatar-overlay/avatar-overlay.component.downgrade';
import {ExternalIconComponent} from './external-icon/external-icon.component';
import './external-icon/external-icon.component.downgrade';
import {selectSenderMessagesDe} from './select-sender/de.select-sender.messages';
import {selectSenderMessagesEn} from './select-sender/en.select-sender.messages';
import {SelectSenderComponent} from './select-sender/select-sender.component';
import {SenderAvatarComponent} from './sender-avatar/sender-avatar.component';
import './sender-avatar/sender-avatar.component.downgrade';
import {SenderLinkComponent} from './sender-link/sender-link.component';
import './sender-link/sender-link.component.downgrade';
import {SenderListItemComponent} from './sender-list-item/sender-list-item.component';
import {SenderOptionsViewComponent} from './sender-options-view/sender-options-view.component';
import {UserAvatarImageComponent} from './user-avatar/user-avatar-image/user-avatar-image.component';
import {UserAvatarComponent} from './user-avatar/user-avatar.component';
import './user-avatar/user-avatar.component.downgrade';
import {UserFollowComponent} from './user-follow/user-follow.component';
import './user-follow/user-follow.component.downgrade';

/**
 * Shared module containing all components responsible for sender ui
 */
@NgModule({
  imports: [
    ContextMenuModule,
    CoyoCommonsModule,
    CoyoFormsModule,
    NgSelectModule,
    TooltipModule,
    NgOptionHighlightModule
  ],
  declarations: [
    AppHrefComponent,
    AvatarImageComponent,
    AvatarOverlayComponent,
    ExternalIconComponent,
    SelectSenderComponent,
    SenderAvatarComponent,
    SenderLinkComponent,
    SenderListItemComponent,
    SenderOptionsViewComponent,
    UserAvatarComponent,
    UserAvatarImageComponent,
    UserFollowComponent
  ],
  exports: [
    AppHrefComponent,
    AvatarImageComponent,
    ExternalIconComponent,
    SelectSenderComponent,
    SenderAvatarComponent,
    SenderLinkComponent,
    SenderListItemComponent,
    SenderOptionsViewComponent,
    UserAvatarComponent,
    UserAvatarImageComponent,
    UserFollowComponent,
  ],
  entryComponents: [
    AvatarImageComponent,
    AvatarOverlayComponent,
    ExternalIconComponent,
    SelectSenderComponent,
    SenderAvatarComponent,
    SenderLinkComponent,
    UserAvatarComponent,
    UserFollowComponent
  ],
  providers: [
    {provide: 'messages', multi: true, useValue: selectSenderMessagesDe},
    {provide: 'messages', multi: true, useValue: selectSenderMessagesEn}
  ]
})
export class SenderUIModule {}
