import {Messages} from '@core/i18n/messages';

export const selectSenderMessagesDe: Messages = {
  lang: 'de',
  messages: {
    'SELECT_SENDER.DATA_NOT_FOUND': 'Keine Einträge gefunden für "{searchTerm}".'
  }
};
