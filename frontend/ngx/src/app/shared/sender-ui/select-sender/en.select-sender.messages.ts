import {Messages} from '@core/i18n/messages';

export const selectSenderMessagesEn: Messages = {
  lang: 'en',
  messages: {
    'SELECT_SENDER.DATA_NOT_FOUND': 'No data found for "{searchTerm}".'
  }
};
