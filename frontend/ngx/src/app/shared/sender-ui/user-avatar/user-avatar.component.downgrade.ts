import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {UserAvatarComponent} from './user-avatar.component';

getAngularJSGlobal()
  .module('commons.sender')
  .directive('coyoUserAvatar', downgradeComponent({
    component: UserAvatarComponent,
    propagateDigest: false
  }));
