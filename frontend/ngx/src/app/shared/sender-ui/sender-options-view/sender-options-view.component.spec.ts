import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Sender} from '@domain/sender/sender';
import {SublineService} from '@shared/sender-ui/subline/subline.service';
import {SenderOptionsViewComponent} from './sender-options-view.component';

describe('SenderOptionsViewComponent', () => {
  let component: SenderOptionsViewComponent;
  let fixture: ComponentFixture<SenderOptionsViewComponent>;
  let sublineService: jasmine.SpyObj<SublineService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SenderOptionsViewComponent],
        providers: [{
          provide: SublineService,
          useValue: jasmine.createSpyObj('SublineService', ['getSublineForSender'])
        }]
      })
      .overrideTemplate(SenderOptionsViewComponent, '<div></div>')
      .compileComponents();

    sublineService = TestBed.get(SublineService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderOptionsViewComponent);
    component = fixture.componentInstance;
    component.sender = {} as Sender;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    expect(sublineService.getSublineForSender).toHaveBeenCalledWith({} as Sender);
  });

});
