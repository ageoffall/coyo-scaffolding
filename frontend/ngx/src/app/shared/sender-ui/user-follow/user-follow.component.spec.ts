import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {Target} from '@domain/sender/target';
import {Subscription} from '@domain/subscription/subscription';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {User} from '@domain/user/user';
import {of} from 'rxjs';
import {UserFollowComponent} from './user-follow.component';

describe('UserFollowComponent', () => {
  let component: UserFollowComponent;
  let fixture: ComponentFixture<UserFollowComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;

  const user = {
    id: 'user-id',
    globalPermissions: ['ACCESS_OWN_USER_PROFILE'],
    target: {
      name: 'user',
      params: {
        id: 'user-id',
        slug: 'bob-ross'
      }
    } as Target
  } as User;

  const subscription = {
    userId: 'user-id',
    senderId: 'sender-id',
    targetId: 'target-id',
    targetType: 'target-type',
    autoSubscribe: false
  } as Subscription;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFollowComponent ],
      providers: [
        {provide: AuthService, useValue: jasmine.createSpyObj('authService', ['getUser'])},
        {provide: SubscriptionService, useValue: jasmine.createSpyObj('subscriptionService', ['subscribe', 'unsubscribe', 'onSubscriptionChange$'])}
      ]
    }).overrideTemplate(UserFollowComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    authService.getUser.and.returnValue(of(user));
    subscriptionService = TestBed.get(SubscriptionService);
    subscriptionService.subscribe.and.returnValue(of(subscription));
    subscriptionService.unsubscribe.and.returnValue(of());
    subscriptionService.onSubscriptionChange$.and.returnValue(of(subscription));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFollowComponent);
    component = fixture.componentInstance;
    component.user = user;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not Toggle following(loading)', () => {
    // given
    const event = jasmine.createSpyObj('event', ['stopPropagation']);
    component.loading$.next(true);
    // when
    const result = component.toggle(event);
    // then
    expect(result).toBeUndefined();
  });

  it('should Toggle following', () => {
    // given
    const event = jasmine.createSpyObj('event', ['stopPropagation']);
    component.loading$.next(false);
    // when
    component.toggle(event);
    // then
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('should init', () => {
    // given
    const newUser = {id: '12345'} as User;
    authService.getUser.and.returnValue(of(newUser));
    // when
    component.ngOnInit();
    // then
    component.show$.subscribe(show => expect(show).toBe(true));
    component.following$.subscribe(following => expect(following).toBe(true));
  });
});
