import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {DownloadDirective} from './download.directive';

/**
 * Module responsible for downloads
 */
@NgModule({
  imports: [
    CommonModule,
    UpgradeModule
  ],
  declarations: [
    DownloadDirective
  ],
  exports: [
    DownloadDirective
  ]
})
export class DownloadModule {}
