import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TrustUrlPipe} from './trust-url.pipe';

/**
 * Module exporting the trust url pipe.
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TrustUrlPipe
  ],
  exports: [
    TrustUrlPipe
  ],
})
export class TrustUrlModule {}
