import {ViewportScroller} from '@angular/common';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {CollapsibleContentComponent} from './collapsible-content.component';

describe('CollapsibleContentComponent', () => {
  let component: CollapsibleContentComponent;
  let fixture: ComponentFixture<CollapsibleContentComponent>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let viewportScroller: jasmine.SpyObj<ViewportScroller>;
  let isMobile = false;
  const minCutoffDesktop = 4;
  const maxCharsDesktop = 7;
  const minCutoffMobile = 3;
  const maxCharsMobile = 5;

  const setContent = (str: string) => {
    component.content = str;
    component.ngOnChanges();
    fixture.detectChanges();
  };

  beforeEach(async() => {
    await TestBed.configureTestingModule({
      declarations: [CollapsibleContentComponent],
      providers: [{
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'isSm'])
      }, {
        provide: ViewportScroller,
        useValue: jasmine.createSpyObj('viewportScroller', ['scrollToPosition', 'scrollToAnchor', 'getScrollPosition'])
      }]
    }).overrideTemplate(CollapsibleContentComponent, '').compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    // tslint:disable-next-line:deprecation
    viewportScroller = TestBed.get(ViewportScroller);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsibleContentComponent);
    component = fixture.componentInstance;
    windowSizeService.isSm.and.callFake(() => isMobile);
    windowSizeService.isXs.and.callFake(() => isMobile);
    viewportScroller.getScrollPosition.and.returnValue([0, 0]);

    component.minCutoffDesktop = minCutoffDesktop;
    component.minCutoffMobile = minCutoffMobile;
    component.maxCharsDesktop = maxCharsDesktop;
    component.maxCharsMobile = maxCharsMobile;

    fixture.detectChanges();
  });

  it('should return the correct maxChars and minCutoff values for desktop', () => {
    // given
    isMobile = false;

    // when

    // then
    expect(component.maxChars).toEqual(maxCharsDesktop);
    expect(component.minCutoff).toEqual(minCutoffDesktop);
  });

  it('should return the correct maxChars and minCutoff values for mobile', () => {
    // given
    isMobile = true;

    // when

    // then
    expect(component.maxChars).toEqual(maxCharsMobile);
    expect(component.minCutoff).toEqual(minCutoffMobile);
  });

  it('should handle invalid input', () => {
    // given
    isMobile = false;
    component.collapse();
    // when
    setContent(null);
    const txt1 = component.text;

    setContent('');
    const txt2 = component.text;

    setContent(undefined);
    const txt3 = component.text;

    // then
    expect(txt1).toEqual(null);
    expect(txt2).toEqual('');
    expect(txt3).toEqual(undefined);
  });

  it('should shorten the given texts according to the desktop settings', () => {
    // given
    isMobile = false;
    component.collapse();

    // when
    setContent('aaaaaaaa');
    const txt1 = component.text;
    setContent('aaaaaaaa bbb');
    const txt2 = component.text;
    setContent('aaaaaaaa bbbb');
    const txt3 = component.text;
    setContent('aaaaaaaabbbb');
    const txt4 = component.text;
    setContent('aaaaaaaabb bb');
    const txt5 = component.text;
    setContent('aaaaaaaabb bbbb');
    const txt6 = component.text;

    // then
    expect(txt1).toEqual('aaaaaaaa');
    expect(txt2).toEqual('aaaaaaaa bbb');
    expect(txt3).toEqual('aaaaaaaa' + component.suffixString);
    expect(txt4).toEqual('aaaaaaaabbbb');
    expect(txt5).toEqual('aaaaaaaabb bb');
    expect(txt6).toEqual('aaaaaaaabb' + component.suffixString);
  });

  it('should shorten the given texts according to the mobile settings', () => {
    // given
    isMobile = true;
    component.collapse();

    // when
    setContent('aaaaa');
    const txt1 = component.text;
    setContent('aaaaa bb');
    const txt2 = component.text;
    setContent('aaaaa bbb');
    const txt3 = component.text;
    setContent('aaaaabbb');
    const txt4 = component.text;
    setContent('aaaaabb b');
    const txt5 = component.text;
    setContent('aaaaabb bbb');
    const txt6 = component.text;

    // then
    expect(txt1).toEqual('aaaaa');
    expect(txt2).toEqual('aaaaa bb');
    expect(txt3).toEqual('aaaaa' + component.suffixString);
    expect(txt4).toEqual('aaaaabbb');
    expect(txt5).toEqual('aaaaabb b');
    expect(txt6).toEqual('aaaaabb' + component.suffixString);
  });

  it('should collapse and expand correctly', () => {
    // given
    isMobile = false;

    // when
    setContent('aaaaaaa bbbb');
    component.collapse();
    const txt1 = component.text;
    component.expand();
    const txt2 = component.text;

    // then
    expect(txt1).toEqual('aaaaaaa' + component.suffixString);
    expect(txt2).toEqual('aaaaaaa bbbb');
  });

  it('should not destroy HTML tags', () => {
    // given
    isMobile = false;
    component.collapse();

    // when
    setContent('aabbcc<a href="test">boop </a> test test test test');
    const txt1 = component.text;

    setContent('<p>lol<ul><li>test1</li>\n<li>test2</li></ul></p>');
    const txt2 = component.text;

    setContent('aabbccdd<p><a href="test">boop </a></p> test test test test');
    const txt3 = component.text;

    // then
    expect(txt1).toEqual('aabbcc<a href="test">boop</a>' + component.suffixString);
    expect(txt2).toEqual('<p>lol<ul><li>test1</li></ul>' + component.suffixString + '</p>');
    expect(txt3).toEqual('aabbccdd<p><a href="test">boop</a>' + component.suffixString + '</p>');
  });

  it('should toggle correctly', () => {
    // given
    isMobile = false;

    // when
    setContent('aaaaaaa bbbb');
    component.expand();
    const txt1 = component.text;
    component.toggle();
    const txt2 = component.text;
    component.toggle();
    const txt3 = component.text;

    // then
    expect(txt1).toEqual('aaaaaaa bbbb');
    expect(txt2).toEqual('aaaaaaa' + component.suffixString);
    expect(txt3).toEqual('aaaaaaa bbbb');
  });
});
