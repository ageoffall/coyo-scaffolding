import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {TooltipDirective} from 'ngx-bootstrap/tooltip';
import {of} from 'rxjs';
import {HelpModalComponent} from '../help-modal/help-modal.component';
import {HelpComponent} from './help.component';

describe('HelpComponent', () => {
  let component: HelpComponent;
  let fixture: ComponentFixture<HelpComponent>;
  let modalService: jasmine.SpyObj<BsModalService>;
  let windowSizeService: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HelpComponent],
      providers: [{
        provide: BsModalService,
        useValue: jasmine.createSpyObj('modalService', ['show'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['observeScreenChange'])
      }]
    }).overrideTemplate(HelpComponent, '')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.LG));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpComponent);
    component = fixture.componentInstance;

    modalService = TestBed.get(BsModalService);
    component.tooltip = {} as TooltipDirective;
  });

  it('should initialize with default values', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component.placement).toBe('left');
    expect(component.aria).toBe('READ_INFO_TEXT');
    expect(component.triggers).toBe('hover');
    expect(component.disabled).toBe(false);
  });

  it('should open a modal on click when modal text is set', () => {
    // given
    component.modalText = 'TEST';
    const event = jasmine.createSpyObj('event', ['preventDefault']) as unknown as Event;

    // when
    component.openModal(event);

    // then
    expect(modalService.show).toHaveBeenCalledWith(HelpModalComponent, {
      animated: false,
      class: 'modal-lg',
      initialState: {
        text: component.modalText
      }
    });
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should activate tooltips on mobile when no modal text is given', () => {
    // given
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));

    // when
    fixture.detectChanges();

    // then
    expect(component.triggers).toBe('click');
    expect(component.disabled).toBe(false);
  });

  it('should not activate tooltips on mobile when modal text is given', () => {
    // given
    component.modalText = 'test';
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.XS));

    // when
    fixture.detectChanges();

    // then
    expect(component.triggers).toBe('click');
    expect(component.disabled).toBe(true);
  });
});
