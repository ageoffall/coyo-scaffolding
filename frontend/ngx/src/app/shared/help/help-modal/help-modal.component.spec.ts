import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UIRouter} from '@uirouter/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {EMPTY} from 'rxjs';
import {HelpModalComponent} from './help-modal.component';

describe('HelpModalComponent', () => {
  let component: HelpModalComponent;
  let fixture: ComponentFixture<HelpModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpModalComponent ],
      providers: [{
        provide: BsModalRef, useValue: null
      }, {
        provide: UIRouter, useValue: {globals: {start$: EMPTY}}
      }]
    }).overrideTemplate(HelpModalComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
