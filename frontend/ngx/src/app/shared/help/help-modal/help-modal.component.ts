import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Modal} from '@domain/modal/modal';
import {UIRouter} from '@uirouter/core';
import {BsModalRef} from 'ngx-bootstrap/modal';

/**
 * Modal for showing help information
 */
@Component({
  selector: 'coyo-help-modal',
  templateUrl: './help-modal.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelpModalComponent extends Modal<void> {

  text: string;

  constructor(modal: BsModalRef, uiRouter: UIRouter) {
    super(modal, uiRouter);
  }
}
