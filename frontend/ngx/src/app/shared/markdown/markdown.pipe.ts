import {Pipe, PipeTransform} from '@angular/core';
import {MarkdownService} from './markdown.service';

/**
 * Transforms text containing markdown to HTML.
 */
@Pipe({name: 'markdown'})
export class MarkdownPipe implements PipeTransform {

  constructor(private markdownService: MarkdownService) {
  }

  /**
   * Adds html enrichment for markdown syntax.
   *
   * @param text the markdown-flavoured source text
   * @param minimal a minimal flag to only use a small subset of markdown directives
   *
   * @return the html enriched text
   */
  transform(text: string, minimal: boolean = true): string {
    return this.markdownService.markdown(text, minimal);
  }
}
