import {inject, TestBed} from '@angular/core/testing';
import {WINDOW} from '@root/injection-tokens';
import {MarkdownService} from './markdown.service';

describe('MarkdownService', () => {
  const window: Window = {
    location: {
      host: 'mycoyo.com'
    }
  } as any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarkdownService, {
        provide: WINDOW,
        useFactory: () => window
      }]
    });
  });

  it('should be created', inject([MarkdownService], (service: MarkdownService) => {
    expect(service).toBeTruthy();
  }));

  // ===== minimal & classic

  function common(minimal: boolean): void {
    it('should handle empty input', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown(undefined, minimal)).toBeUndefined();
      expect(service.markdown(null, minimal)).toEqual(null);
      expect(service.markdown('', minimal)).toEqual('');
    }));

    it('should convert markdown with plain text to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Foo Bar', minimal)).toEqual('<p>Foo Bar</p>');
    }));

    it('should convert markdown with link to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Foo google.de Bar', minimal))
        .toEqual('<p>Foo google.de Bar</p>');
      expect(service.markdown('Foo www.google.de Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="http://www.google.de">www.google.de</a> Bar</p>');
      expect(service.markdown('Foo http://www.google.de Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="http://www.google.de">http://www.google.de</a> Bar</p>');
      expect(service.markdown('Foo https://www.google.de Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="https://www.google.de">https://www.google.de</a> Bar</p>');
      expect(service.markdown('Foo https://www.google.de/_test_ Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="https://www.google.de/_test_">https://www.google.de/_test_</a> Bar</p>');
      expect(service.markdown('Foo HTTP://www.google.de Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="HTTP://www.google.de">HTTP://www.google.de</a> Bar</p>');
      expect(service.markdown('Foo HTtpS://www.google.de Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="HTtpS://www.google.de">HTtpS://www.google.de</a> Bar</p>');
      expect(service.markdown('Foo wWw.google.de Bar', minimal))
        .toEqual('<p>Foo <a target="_blank" href="http://www.google.de">wWw.google.de</a> Bar</p>');
    }));

    it('should convert markdown with coyo-link to HTML with ng-href', inject([MarkdownService], (service: MarkdownService) => {
      const host = window.location.host;
      expect(service.markdown('Foo www.' + host + '/home/news Bar', minimal))
        .toEqual('<p>Foo <a href="/home/news">www.' + host + '/home/news</a> Bar</p>');
      expect(service.markdown('Foo http://' + host + '/home/news Bar', minimal))
        .toEqual('<p>Foo <a href="/home/news">http://' + host + '/home/news</a> Bar</p>');
      expect(service.markdown('Foo https://' + host + '/home/news Bar', minimal))
        .toEqual('<p>Foo <a href="/home/news">https://' + host + '/home/news</a> Bar</p>');
    }));

    it('should convert markdown with public file link to HTML with ng-href', inject([MarkdownService], (service: MarkdownService) => {
      const host = window.location.host;
      expect(service.markdown('Foo www.' + host + '/web/public-link/1/download', minimal))
        .toEqual('<p>Foo <a target="_blank" href="http://www.' + host + '/web/public-link/1/download">www.' + host + '/web/public-link/1/download</a></p>');
      expect(service.markdown('Foo http://' + host + '/web/public-link/1/download', minimal))
        .toEqual('<p>Foo <a target="_blank" href="http://' + host + '/web/public-link/1/download">http://' + host + '/web/public-link/1/download</a></p>');
      expect(service.markdown('Foo https://' + host + '/web/public-link/1/download', minimal))
        .toEqual('<p>Foo <a target="_blank" href="https://' + host + '/web/public-link/1/download">https://' + host + '/web/public-link/1/download</a></p>');
    }));

    it('should convert markdown with bold text to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('**This is bold text**', minimal)).toEqual('<p><strong>This is bold text</strong></p>');
      expect(service.markdown('__This is bold text__', minimal)).toEqual('<p><strong>This is bold text</strong></p>');
    }));

    it('should convert markdown with italic text to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('*This text is italicized*', minimal)).toEqual('<p><em>This text is italicized</em></p>');
      expect(service.markdown('_This text is italicized_', minimal)).toEqual('<p><em>This text is italicized</em></p>');
    }));

    it('should convert markdown with deleted text to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('~~This was mistaken text~~', minimal)).toEqual('<p><del>This was mistaken text</del></p>');
    }));

    it('should convert markdown with bold and italic text to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('**This text is _extremely_ important**', minimal)).toEqual('<p><strong>This text is <em>extremely</em> important</strong></p>');
      expect(service.markdown('*This text is __extremely__ important*', minimal)).toEqual('<p><em>This text is <strong>extremely</strong> important</em></p>');
    }));

    it('should convert markdown with quoted text to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('In the words of Abraham Lincoln:\n\n> Pardon my French', minimal))
        .toEqual('<p>In the words of Abraham Lincoln:</p>\n<blockquote>\n<p>Pardon my French</p>\n</blockquote>');
    }));

    it('should convert markdown with inline code to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Use `git status` to list all new or modified files that haven\'t yet been committed.', minimal))
        .toEqual('<p>Use <code>git status</code> to list all new or modified files that haven&#39;t yet been committed.</p>');
      expect(service.markdown('The `<title></title>` tag is required in all HTML documents.', minimal))
        .toEqual('<p>The <code>&lt;title&gt;&lt;/title&gt;</code> tag is required in all HTML documents.</p>');
    }));

    it('should convert markdown with block code to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Some basic Git commands are:\n\n```git status\ngit add\ngit commit\n```', minimal))
        .toEqual('<p>Some basic Git commands are:</p>\n<p><code>git status\ngit add\ngit commit</code></p>');
      expect(service.markdown('Define a title for your HTML document:\n\n```<title>Coyo</title>```', minimal))
        .toEqual('<p>Define a title for your HTML document:</p>\n<p><code>&lt;title&gt;Coyo&lt;/title&gt;</code></p>');
    }));

    it('should convert markdown with lists to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('- George Washington\n- John Adams\n- Thomas Jefferson', minimal))
        .toEqual('<ul>\n<li>George Washington</li>\n<li>John Adams</li>\n<li>Thomas Jefferson</li>\n</ul>');
    }));

    it('should not convert markdown with escaped markdown to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Let\'s rename \\*our-new-project\\* to \\*our-old-project\\*.', minimal))
        .toEqual('<p>Let&#39;s rename *our-new-project* to *our-old-project*.</p>');
    }));

    it('should convert markdown with block code to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('```\ntest\n```', minimal)).toEqual('<pre><code>test\n</code></pre>');
      expect(service.markdown('```java\ntest\n```', minimal)).toEqual('<pre><code class="lang-java">test\n</code></pre>');
      expect(service.markdown('```java script\ntest\n```', minimal)).toEqual('<p><code>java script\ntest</code></p>');
      expect(service.markdown('```test```', minimal)).toEqual('<p><code>test</code></p>');
    }));

    it('should keep hashtags', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('#goCoyo #hä #hallö #ölä #OLA', minimal)).toEqual('<p>#goCoyo #hä #hallö #ölä #OLA</p>');
    }));
  }

  // ===== minimal

  describe('(minimal)', () => {
    common(true);

    it('should convert markdown with line breaks to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Foo\nBar')).toEqual('<p>Foo<br>Bar</p>');
      expect(service.markdown('Foo\n\nBar')).toEqual('<p>Foo</p>\n<p>Bar</p>');
      expect(service.markdown('Foo\n\n\nBar')).toEqual('<p>Foo</p>\n<p>Bar</p>');
    }));

    it('should not convert markdown with header to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('# Foo\nBar')).toEqual('<p># Foo<br>Bar</p>');
      expect(service.markdown('## Foo\nBar')).toEqual('<p>## Foo<br>Bar</p>');
      expect(service.markdown('### Foo\nBar')).toEqual('<p>### Foo<br>Bar</p>');
      expect(service.markdown('#### Foo\nBar')).toEqual('<p>#### Foo<br>Bar</p>');
      expect(service.markdown('##### Foo\nBar')).toEqual('<p>##### Foo<br>Bar</p>');
      expect(service.markdown('###### Foo\nBar')).toEqual('<p>###### Foo<br>Bar</p>');
    }));

    it('should not convert markdown with markdown links to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Check out [Coyo](https://www.coyo.com/).'))
        .toEqual('<p>Check out [Coyo](<a target="_blank" href="https://www.coyo.com/">https://www.coyo.com/</a>).</p>');
    }));

    it('should not convert markdown with images to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Look: ![alt text](https://www.coyo.com/coyo.png "Coyo").'))
        .toEqual('<p>Look: ![alt text](<a target="_blank" href="https://www.coyo.com/coyo.png">https://www.coyo.com/coyo.png</a> &quot;Coyo&quot;).</p>');
    }));

    it('should convert markdown with block code to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Some basic Git commands are:\n\n    git status\n    git add\n    git commit'))
        .toEqual('<p>Some basic Git commands are:</p>\n<p>    git status<br>    git add<br>    git commit</p>');
    }));
  });

  // ===== classic

  describe('(classic)', () => {
    common(false);

    it('should convert markdown with line breaks to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Foo\nBar', false)).toEqual('<p>Foo\nBar</p>');
      expect(service.markdown('Foo\n\nBar', false)).toEqual('<p>Foo</p>\n<p>Bar</p>');
      expect(service.markdown('Foo\n\n\nBar', false)).toEqual('<p>Foo</p>\n<p>Bar</p>');
    }));

    it('should convert markdown header and linebreak to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('# Foo\nBar', false)).toEqual('<h1 id="foo">Foo</h1>\n<p>Bar</p>');
      expect(service.markdown('## Foo\nBar', false)).toEqual('<h2 id="foo">Foo</h2>\n<p>Bar</p>');
      expect(service.markdown('### Foo\nBar', false)).toEqual('<h3 id="foo">Foo</h3>\n<p>Bar</p>');
      expect(service.markdown('#### Foo\nBar', false)).toEqual('<h4 id="foo">Foo</h4>\n<p>Bar</p>');
      expect(service.markdown('##### Foo\nBar', false)).toEqual('<h5 id="foo">Foo</h5>\n<p>Bar</p>');
      expect(service.markdown('###### Foo\nBar', false)).toEqual('<h6 id="foo">Foo</h6>\n<p>Bar</p>');
    }));

    it('should convert markdown with markdown links to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Check out [Coyo](https://www.coyoapp.com/).', false))
        .toEqual('<p>Check out <a target="_blank" href="https://www.coyoapp.com/">Coyo</a>.</p>');
    }));

    it('should convert markdown with images to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Check out ![alt text](https://www.coyoapp.com/coyo.png "Coyo").', false))
        .toEqual('<p>Check out <img src="https://www.coyoapp.com/coyo.png" alt="alt text" title="Coyo">.</p>');
    }));

    it('should convert markdown with block code to HTML', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.markdown('Some basic Git commands are:\n\n    git status\n    git add\n    git commit', false))
        .toEqual('<p>Some basic Git commands are:</p>\n<pre><code>git status\ngit add\ngit commit\n</code></pre>');
    }));
  });

  // ===== strip

  describe('should strip', () => {
    it('"heading 1-6" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('# This is a heading')).toEqual('This is a heading');
      expect(service.strip('## This is a heading')).toEqual('This is a heading');
      expect(service.strip('### This is a heading')).toEqual('This is a heading');
      expect(service.strip('#### This is a heading')).toEqual('This is a heading');
      expect(service.strip('##### This is a heading')).toEqual('This is a heading');
      expect(service.strip('###### This is a heading')).toEqual('This is a heading');
    }));

    it('"bold" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('**Bold** content, more **bold** content.')).toEqual('Bold content, more bold content.');
      expect(service.strip('__Bold__ content, more __bold__ content.')).toEqual('Bold content, more bold content.');
    }));

    it('"italic" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('*Italic* content, more *italic* content.')).toEqual('Italic content, more italic content.');
      expect(service.strip('_Italic_ content, more _italic_ content.')).toEqual('Italic content, more italic content.');
    }));

    it('"strikethrough" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('~~Strikethrough~~ content, more ~~strikethrough~~ content.')).toEqual('Strikethrough content, more strikethrough content.');
    }));

    it('"quotation" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('> Quoted text')).toEqual('Quoted text');
    }));

    it('"singleline code" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('In `Getting Started` we set up `something` foo.')).toEqual('In Getting Started we set up something foo.');
    }));

    it('"multiline code" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('Some code: ```language\n(DEFUN HELLO ()\n  "HELLO WORLD"\n)')).toEqual('Some code: (DEFUN HELLO ()\n  "HELLO WORLD"\n)');
    }));

    it('"link" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('This is a paragraph with [a link](https://www.link-url.com).')).toEqual('This is a paragraph with a link.');
    }));

    it('"list" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('Some list\n\n* First point\n* Second point\n* Third point')).toEqual('Some list\n\nFirst point\nSecond point\nThird point');
      expect(service.strip('Some list\n\n- First point\n- Second point\n- Third point')).toEqual('Some list\n\nFirst point\nSecond point\nThird point');
      expect(service.strip('Some list\n\n+ First point\n+ Second point\n+ Third point')).toEqual('Some list\n\nFirst point\nSecond point\nThird point');
      expect(service.strip('Some list\n\n1. First point\n2. Second point\n3. Third point')).toEqual('Some list\n\nFirst point\nSecond point\nThird point');
    }));

    it('"divider" markdown', inject([MarkdownService], (service: MarkdownService) => {
      expect(service.strip('Single divider line: ___ or --- or ***.')).toEqual('Single divider line: _ or --- or *.');
    }));
  });
});
