import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {VideoPreview} from '@domain/preview/video-preview';
import {VideoPreviewComponent} from './video-preview.component';

describe('VideoPreviewComponent', () => {
  let component: VideoPreviewComponent;
  let fixture: ComponentFixture<VideoPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VideoPreviewComponent]
    }).overrideTemplate(VideoPreviewComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the high percentage for the padding', () => {
    // when
    const result = component.getHeightPercentage({height: 1, width: 2} as VideoPreview);

    // then
    expect(result).toBe('50%');
  });

  it('should get default high percentage when no height and width is given', () => {
    // given
    const result = component.getHeightPercentage({} as VideoPreview);

    // then
    expect(result).toBe('57%');
  });

  it('should replace youtube url to no-cookie', () => {
    // given
    const result = component.getVideoUrl({videoUrl: 'youtube.com/embed/12345'} as VideoPreview);

    // then
    expect(result).toBe('youtube-nocookie.com/embed/12345');
  });
});
