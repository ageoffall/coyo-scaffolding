import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {ExternalFileDetails} from '@shared/files/external-file-handler/external-file-details';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {File} from '@shared/files/external-file-handler/file';
import {BehaviorSubject, from, Observable, of, Subject} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'coyo-file-integration-link',
  templateUrl: './file-integration-link.component.html',
  styleUrls: ['./file-integration-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileIntegrationLinkComponent implements OnInit {

  /**
   * The COYO file that contains the fileId and storage type to retrieve the external file details.
   */
  @Input() file: File;

  isGoogleActive$: Subject<boolean> = new BehaviorSubject(false);
  externalFileDetails$: Observable<ExternalFileDetails>;

  constructor(private googleApiService: GoogleApiService, private fileService: ExternalFileHandlerService) {
  }

  ngOnInit(): void {
    this.googleApiService.isGoogleApiActive().subscribe(isActive => {
      this.externalFileDetails$ = from(this.fileService.getExternalFileDetails(this.file))
        .pipe(catchError(() => of({} as ExternalFileDetails)));
      this.isGoogleActive$.next(isActive);
    });
  }

}
