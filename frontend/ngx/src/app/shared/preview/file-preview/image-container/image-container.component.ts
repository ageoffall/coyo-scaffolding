import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {FilePreviewStatus} from '@shared/preview/file-preview/file-preview-status';
import {PinchEvent} from '@shared/preview/file-preview/image-container/pinch-event';
import {PinchHandler} from '@shared/preview/file-preview/image-container/pinch-handler';
import {SwipeEvent} from '@shared/preview/file-preview/image-container/swipe-event';
import {Vec2} from './vec2';

export enum ImageContainerStatus {
  INIT, LOADING, PROCESSING, SUCCESS, ERROR
}

/**
 * This Component handles the preview for pdf and video files.
 *
 * ChangeDetection.onPush has been removed in order to detect changes on the
 * Input "file" even if the reference has not changed.
 */
@Component({
  selector: 'coyo-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss']
})
export class ImageContainerComponent implements OnInit, OnDestroy, OnChanges {

  /**
   * The image file source url
   */
  @Input() src: string;
  /**
   * Defines how strongly images resist being pulled beyond the screen limits
   */
  @Input() borderSnappyness: number = 0.25;
  /**
   * Defines how quickly swipe motion continuation dissipates
   */
  @Input() swipeDrag: number = 0.06;
  /**
   * Defined how much an image can be magnified by zooming in (value * original resolution)
   */
  @Input() maxMagnification: number = 3;
  /**
   * Fires when the image loading status changes
   */
  @Output() statusUpdated: EventEmitter<FilePreviewStatus> = new EventEmitter<FilePreviewStatus>();
  /**
   * Fires when the user clicks or taps the image (for full screen usage)
   */
  @Output() released: EventEmitter<Vec2> = new EventEmitter<Vec2>();
  /**
   * Location of the center of the container on the image
   */
  location: Vec2 = new Vec2(0, 0);
  /**
   * Zoom factor (image on screen size = zoom * original image size)
   */
  zoom: number = 1;
  /**
   * Remaining velocity from the last drag/swipe interaction
   */
  velocity: Vec2 = new Vec2(0, 0);
  private lastStatus: ImageContainerStatus = ImageContainerStatus.INIT;
  private mousePressed: boolean = false;
  private clickPos: Vec2 = new Vec2(0, 0);
  private imgSize: Vec2 = new Vec2(0, 0);
  private pinchHandler: PinchHandler = new PinchHandler();
  private isInteracting: boolean = false;
  private isDestroyed: boolean = false;

  constructor(public element: ElementRef) {
  }

  /**
   * Returns the container HTML element containing the image
   */
  get containerElement(): HTMLDivElement {
    return this.element.nativeElement.children[0] as HTMLDivElement;
  }

  /**
   * Returns the image HTML element
   */
  get imageElement(): HTMLImageElement {
    return this.containerElement.children[0] as HTMLImageElement;
  }

  /**
   * Returns the original image dimensions
   */
  get imageSize(): Vec2 {
    return this.imgSize;
  }

  /**
   * Returns the minimum zoom factor which will allow the full image to be visible with the current resolution and aspect ratio
   */
  get minZoom(): number {
    return Math.min(this.containerElement.clientHeight / this.imgSize.y, this.containerElement.clientWidth / this.imgSize.x);
  }

  /**
   * Returns the size of the container element in pixels
   */
  get canvasSize(): Vec2 {
    return new Vec2(this.containerElement.clientWidth, this.containerElement.clientHeight);
  }

  /**
   * Returns the minimum image space location for panning (top-left)
   */
  get minLocation(): Vec2 {
    const halfImgSize = this.imgSize.scaleInverse(2);
    const halfCanvasSize = this.canvasSize.scaleInverse(2 * this.zoom);
    return Vec2.min(halfImgSize.subtract(halfCanvasSize), halfCanvasSize.subtract(halfImgSize));
  }

  /**
   * Returns the maximum image space location for panning (bottom-right)
   */
  get maxLocation(): Vec2 {
    const halfImgSize = this.imgSize.scaleInverse(2);
    const halfCanvasSize = this.canvasSize.scaleInverse(2 * this.zoom);
    return Vec2.max(halfImgSize.subtract(halfCanvasSize), halfCanvasSize.subtract(halfImgSize));
  }

  /**
   * Returns the current status of the image loading process
   */
  get status(): ImageContainerStatus {
    return this.lastStatus;
  }

  ngOnInit(): void {
    this.pinchHandler.onPinch(this.pinch.bind(this));
    this.pinchHandler.onSwipe(this.swipe.bind(this));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.src) {
      this.updateImg(true);
      if (changes.src.isFirstChange()) {
        requestAnimationFrame(this.frameLoop.bind(this));
      }
    }
  }

  ngOnDestroy(): void {
    this.pinchHandler = null;
    this.imageElement.src = null;
    this.isDestroyed = true;
  }

  /**
   * Loads the current image and stores the resolution.
   *
   * @param resetZoom If true, the zoom will be reset to the minimum zoom.
   */
  updateImg(resetZoom?: boolean): void {
    this.updateStatus(ImageContainerStatus.LOADING);
    const img = new Image();
    img.onload = () => {
      this.updateStatus(ImageContainerStatus.PROCESSING);
      img.onload = null;
      img.onerror = null;
      this.imgSize = new Vec2(img.width, img.height);
      this.imageElement.src = this.src;
      if (resetZoom) {
        this.zoom = this.minZoom;
        this.applyTransform();
      }
      this.updateStatus(ImageContainerStatus.SUCCESS);
    };
    img.onerror = () => {
      img.onload = null;
      img.onerror = null;
      this.updateStatus(ImageContainerStatus.ERROR);
    };
    img.src = this.src;
  }

  /**
   * Triggers on mousedown
   * @param e event parameters
   */
  @HostListener('mousedown', ['$event'])
  mouseDown(e: MouseEvent): void {
    if (e.button === 0) {
      this.isInteracting = true;
      e.preventDefault();
      e.stopPropagation();
      this.clickPos = new Vec2(e.x, e.y);
      this.mousePressed = true;
    }
  }

  /**
   * Triggers on mouseup (window-wide)
   * @param e event parameters
   */
  @HostListener('window:mouseup', ['$event'])
  mouseUp(e: MouseEvent): void {
    if (e.button === 0) {
      this.isInteracting = false;
      e.preventDefault();
      e.stopPropagation();
      const delta = new Vec2(e.x - this.clickPos.x, e.y - this.clickPos.y);
      if (delta.magnitudeSquared < 400) {
        this.released.emit(this.screen2ImageLocation(new Vec2(e.x, e.y)));
      }
      this.mousePressed = false;
    }
  }

  /**
   * Triggers on mousemove (window-wide)
   * @param e event parameters
   */
  @HostListener('window:mousemove', ['$event'])
  mouseMove(e: MouseEvent): void {
    if (this.mousePressed) {
      e.preventDefault();
      e.stopPropagation();
      this.setLocation(this.location.add(new Vec2(-e.movementX, -e.movementY).scaleInverse(this.zoom)));
    }
  }

  /**
   * Triggers on mousewheel events
   * @param e event parameters
   */
  @HostListener('wheel', ['$event'])
  scroll(e: WheelEvent): void {
    e.preventDefault();
    e.stopPropagation();
    const mouseLocation = new Vec2(e.clientX, e.clientY);
    const oldLocation = this.screen2ImageLocation(mouseLocation);
    if (e.deltaY < 0) {
      this.zoom *= 1.1;
    } else if (e.deltaY > 0) {
      this.zoom *= 0.9;
    }
    this.zoom = Math.min(this.maxMagnification, Math.max(this.minZoom, this.zoom));
    const newLocation = this.screen2ImageLocation(mouseLocation);
    this.location = this.location.subtract(newLocation.subtract(oldLocation));
  }

  /**
   * Triggers on touchstart (touch-displays)
   * @param e event parameters
   */
  @HostListener('touchstart', ['$event'])
  touchstart(e: any): void {
    this.isInteracting = true;
    this.pinchHandler.touchstart(e);
  }

  /**
   * Triggers on touchend (window-wide, touch-displays)
   * @param e event parameters
   */
  @HostListener('window:touchend', ['$event'])
  touchend(e: any): void {
    if (e.touches.length < 1) {
      this.isInteracting = false;
    }
    this.pinchHandler.touchend(e);
  }

  /**
   * Triggers on touchmove (window-wide)
   * @param e event parameters
   */
  @HostListener('touchmove', ['$event'])
  touchmove(e: any): void {
    e.preventDefault();
    e.stopPropagation();
    this.pinchHandler.touchmove(e);
  }

  /**
   * Transforms the given screen space coordinate into the image coordinate system
   *
   * @param p A screen space coordinate (relative to the container object)
   *
   * @returns An image space coordinate
   */
  screen2ImageLocation(p: Vec2): Vec2 {
    return p.subtract(this.canvasSize.scaleInverse(2)).scaleInverse(this.zoom).add(this.location);
  }

  /**
   * Transforms the given image coordinate into the screen space coordinate system (relative to the container object)
   *
   * @param p An image space coordinate
   *
   * @returns A screen space coordinate
   */
  image2ScreenLocation(p: Vec2): Vec2 {
    return p.subtract(this.location).scale(this.zoom).add(this.canvasSize.scaleInverse(2));
  }

  /**
   * Sets the current location. If used while there is no user interaction it will cause a swipe continuation effect.
   *
   * @param p A location in the image coordinate system
   */
  setLocation(p: Vec2): void {
    this.velocity = p.subtract(this.location);
    this.location = p;
  }

  /**
   * Resets the container
   */
  reset(): void {
    this.lastStatus = ImageContainerStatus.INIT;
    this.updateImg();
  }

  private updateStatus(event: ImageContainerStatus): void {
    this.lastStatus = event;
    this.statusUpdated.emit({
      loading: this.status === ImageContainerStatus.LOADING,
      isProcessing: this.status === ImageContainerStatus.PROCESSING,
      previewAvailable: this.status === ImageContainerStatus.SUCCESS,
      conversionError: this.status === ImageContainerStatus.ERROR
    });
  }

  private pinch(e: PinchEvent): void {
    const oldLocation = this.screen2ImageLocation(e.point);
    this.zoom *= e.factor;
    this.zoom = Math.min(this.maxMagnification, Math.max(this.minZoom, this.zoom));
    const newLocation = this.screen2ImageLocation(e.point);
    this.location = this.location.subtract(newLocation.subtract(oldLocation));
  }

  private swipe(e: SwipeEvent): void {
    this.setLocation(this.location.subtract(e.delta.scaleInverse(this.zoom)));
  }

  private applyTransform(): void {
    this.containerElement.scrollTo(0, 0);
    const transformedLocation = this.canvasSize.subtract(this.imgSize).scale(0.5).subtract(this.location.scale(this.zoom));
    this.imageElement.style.transform = `matrix(${this.zoom}, 0, 0, ${this.zoom}, ${transformedLocation.x}, ${transformedLocation.y})`;
  }

  private frameLoop(): void {
    if (!this.isDestroyed) {
      if (!this.isInteracting) {
        const min = this.minLocation;
        const max = this.maxLocation;
        const allowedLocation = Vec2.max(min, Vec2.min(max, this.location));
        this.location = this.location.add(this.velocity);
        this.velocity = this.velocity.scale(1 - this.swipeDrag);
        this.location = this.location.scale(1 - this.borderSnappyness).add(allowedLocation.scale(this.borderSnappyness));
      }
      this.applyTransform();
      requestAnimationFrame(this.frameLoop.bind(this));
    }
  }
}
