import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ImageContainerComponent} from '@shared/preview/file-preview/image-container/image-container.component';
import {Vec2} from './vec2';
import Spy = jasmine.Spy;

describe('FilePreviewComponent', () => {
  let component: ImageContainerComponent;
  let fixture: ComponentFixture<ImageContainerComponent>;
  let imageElementSpy: Spy;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ImageContainerComponent],
      providers: []
    }).overrideTemplate(ImageContainerComponent, '<div><img src="#"></div>')
      .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageContainerComponent);
    component = fixture.componentInstance;
    imageElementSpy = spyOnProperty(component, 'imageElement', 'get').and.returnValue({
      style: {transform: ''},
      src: null
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly initialize with an image', () => {
    const imgSrcSpy = spyOnProperty(HTMLImageElement.prototype, 'src', 'set');
    spyOnProperty(HTMLImageElement.prototype, 'width', 'get').and.returnValue(10);
    spyOnProperty(HTMLImageElement.prototype, 'height', 'get').and.returnValue(5);
    const imgOnLoadSpy = spyOnProperty(HTMLImageElement.prototype, 'onload', 'set').and.callFake((callback: any) => callback && callback());
    component.src = 'test';
    component.updateImg(false);
    expect(imgSrcSpy).toHaveBeenCalledWith('test');
    expect(imgOnLoadSpy).toHaveBeenCalled();
    expect(component.imageSize.x).toEqual(10);
    expect(component.imageSize.y).toEqual(5);
  });

  it('should correctly convert screen coordinates into image coordinates', () => {
    spyOnProperty(component, 'canvasSize', 'get').and.returnValue(new Vec2(100, 80));
    component.location = new Vec2(0, 0);

    component.zoom = 1;
    expect(component.screen2ImageLocation(new Vec2(50, 40))).toEqual(new Vec2(0, 0));
    expect(component.screen2ImageLocation(new Vec2(0, 0))).toEqual(new Vec2(-50, -40));
    expect(component.screen2ImageLocation(new Vec2(100, 80))).toEqual(new Vec2(50, 40));

    component.zoom = 0.5;
    expect(component.screen2ImageLocation(new Vec2(50, 40))).toEqual(new Vec2(0, 0));
    expect(component.screen2ImageLocation(new Vec2(0, 0))).toEqual(new Vec2(-100, -80));
    expect(component.screen2ImageLocation(new Vec2(100, 80))).toEqual(new Vec2(100, 80));

    component.zoom = 1;
    component.location = new Vec2(4, 6);
    expect(component.screen2ImageLocation(new Vec2(50, 40))).toEqual(new Vec2(4, 6));
    expect(component.screen2ImageLocation(new Vec2(0, 0))).toEqual(new Vec2(-46, -34));
    expect(component.screen2ImageLocation(new Vec2(100, 80))).toEqual(new Vec2(54, 46));

    component.zoom = 0.5;
    expect(component.screen2ImageLocation(new Vec2(50, 40))).toEqual(new Vec2(4, 6));
    expect(component.screen2ImageLocation(new Vec2(0, 0))).toEqual(new Vec2(-96, -74));
    expect(component.screen2ImageLocation(new Vec2(100, 80))).toEqual(new Vec2(104, 86));
  });

  it('should correctly convert image coordinates into screen coordinates', () => {
    spyOnProperty(component, 'canvasSize', 'get').and.returnValue(new Vec2(100, 80));
    component.location = new Vec2(0, 0);

    component.zoom = 1;
    expect(component.image2ScreenLocation(new Vec2(0, 0))).toEqual(new Vec2(50, 40));
    expect(component.image2ScreenLocation(new Vec2(-50, -40))).toEqual(new Vec2(0, 0));
    expect(component.image2ScreenLocation(new Vec2(50, 40))).toEqual(new Vec2(100, 80));

    component.zoom = 0.5;
    expect(component.image2ScreenLocation(new Vec2(0, 0))).toEqual(new Vec2(50, 40));
    expect(component.image2ScreenLocation(new Vec2(-100, -80))).toEqual(new Vec2(0, 0));
    expect(component.image2ScreenLocation(new Vec2(100, 80))).toEqual(new Vec2(100, 80));

    component.zoom = 1;
    component.location = new Vec2(4, 6);
    expect(component.image2ScreenLocation(new Vec2(4, 6))).toEqual(new Vec2(50, 40));
    expect(component.image2ScreenLocation(new Vec2(-46, -34))).toEqual(new Vec2(0, 0));
    expect(component.image2ScreenLocation(new Vec2(54, 46))).toEqual(new Vec2(100, 80));

    component.zoom = 0.5;
    expect(component.image2ScreenLocation(new Vec2(4, 6))).toEqual(new Vec2(50, 40));
    expect(component.image2ScreenLocation(new Vec2(-96, -74))).toEqual(new Vec2(0, 0));
    expect(component.image2ScreenLocation(new Vec2(104, 86))).toEqual(new Vec2(100, 80));
  });

  it('should correctly set the velocity', () => {
    component.location = new Vec2(10, 5);
    component.setLocation(new Vec2(30, 0));
    expect(component.velocity).toEqual(new Vec2(20, -5));
  });
});
