import {ComponentFactory, Injectable, Injector, NgModuleFactory} from '@angular/core';
import {FilePreviewAnimationLazyModule} from '@shared/preview/file-preview/animation/file-preview-animation-lazy.module';
import {FilePreviewAnimationComponent} from '@shared/preview/file-preview/animation/file-preview-animation/file-preview-animation.component';

@Injectable({
  providedIn: 'root'
})
export class FilePreviewAnimationLoader {
  constructor(private injector: Injector) {
  }

  load(): Promise<ComponentFactory<FilePreviewAnimationComponent>> {
    // @ts-ignore
    return import('../file-preview-animation-lazy.module.ngfactory')
      .then(module => {
        const moduleRef = (module.FilePreviewAnimationLazyModuleNgFactory as NgModuleFactory<FilePreviewAnimationLazyModule>).create(this.injector);
        const factory = moduleRef.componentFactoryResolver.resolveComponentFactory(FilePreviewAnimationComponent);
        return factory;
      });
  }
}
