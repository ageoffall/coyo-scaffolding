import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {GoogleFileMetaData} from '@app/integration/gsuite/google-api/google-file-metadata';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {GSUITE} from '@domain/attachment/storage';
import {FilePreview} from '@domain/preview/file-preview';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

/**
 * Component to indicate that a preview is not available.
 *
 * ChangeDetection.onPush has been removed in order to detect changes on the
 * Input 'file' even if the reference has not changed.
 */
@Component({
  selector: 'coyo-no-preview-available',
  templateUrl: './no-preview-available.component.html',
  styleUrls: ['./no-preview-available.component.scss']
})
export class NoPreviewAvailableComponent implements OnInit, OnChanges {

  private static WEB_CONTENT_LINK: string = 'webContentLink';

  /**
   * The file preview.
   */
  @Input() file: FilePreview;

  isDesktop$: Observable<boolean>;
  googleFileData: GoogleFileMetaData;
  isGSuiteFile: boolean;
  gSuiteActive: boolean;
  loading: boolean;

  constructor(private googleApiService: GoogleApiService, private windowSizeService: WindowSizeService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.isDesktop$ = this.windowSizeService.observeScreenChange()
      .pipe(map(screenSize => screenSize >= ScreenSize.MD));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.file && changes.file.currentValue !== changes.file.previousValue) {
      this.initFile();
    }
  }

  private initFile(): void {
    if (this.file.storage === GSUITE) {
      this.loading = true;
      this.isGSuiteFile = true;
      this.googleApiService.isGoogleApiActive()
        .subscribe({
          next: isApiActive => {
            this.gSuiteActive = isApiActive;
            this.getGoogleFileData().subscribe({
              next: data => this.googleFileData = data,
              complete: () => this.loading = false
            });
          },
          error: () => this.gSuiteActive = false
        });
    } else {
      this.gSuiteActive = false;
      this.isGSuiteFile = false;
    }
  }

  private getGoogleFileData(): Observable<GoogleFileMetaData> {
    return from(this.googleApiService.getFileMetadata(this.file.fileId, NoPreviewAvailableComponent.WEB_CONTENT_LINK)
      .catch(() => ({})));
  }
}
