/**
 * Representation of a google document type. Mainly used to have a list of supported document types and
 * to have a mapping from document types to a corresponding icon file.
 */
export interface GoogleDocumentType {
  documentType: string;
  previewIconName: string;
}
