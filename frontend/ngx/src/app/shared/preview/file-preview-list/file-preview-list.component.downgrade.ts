import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {FilePreviewListComponent} from './file-preview-list.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoFilePreviewList', downgradeComponent({
    component: FilePreviewListComponent,
    propagateDigest: false
  }));
