import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'FILE_DETAILS.EDIT_IN_G_SUITE': 'Edit in G Suite',
    'FILE_DETAILS.GENERATING_PREVIEW': 'A preview of this document is being created for you',
    'FILE_DETAILS.OPEN_IN_G_SUITE': 'Open in G Suite',
    'FILE_DETAILS.OPEN_IN_G_SUITE.ARIA': 'View or edit this file in Google G Suite'
  }
};
