import {ChangeDetectorRef, NgZone} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {TimeService} from '@shared/time/time/time.service';
import * as moment from 'moment-timezone';
import {messagesEn} from '../en.time.messages';
import {TimeAgoPipe} from './time-ago.pipe';

describe('TimeAgoPipe', () => {
  let pipe: TimeAgoPipe;
  let window: jasmine.SpyObj<Window>;
  let cdRef: jasmine.SpyObj<ChangeDetectorRef>;
  let ngZone: jasmine.SpyObj<NgZone>;
  let timeService: jasmine.SpyObj<TimeService>;
  let translateService: jasmine.SpyObj<TranslateService>;

  beforeEach(() => {
    window = jasmine.createSpyObj('window', ['setTimeout', 'clearTimeout']);
    cdRef = jasmine.createSpyObj('cdRef', ['markForCheck']);
    ngZone = jasmine.createSpyObj('ngZone', ['runOutsideAngular', 'run']);
    timeService = jasmine.createSpyObj('timeService', ['getTimezone']);
    translateService = jasmine.createSpyObj('translateService', ['instant']);
    pipe = new TimeAgoPipe(window, cdRef, ngZone, timeService, translateService);

    window.setTimeout.and.callFake((fn: Function) => fn());
    ngZone.runOutsideAngular.and.callFake((fn: Function) => fn());
    ngZone.run.and.callFake((fn: Function) => fn());
    timeService.getTimezone.and.returnValue('UTC');
    translateService.instant.and.callFake((key: string, params: object) =>
      `${messagesEn.messages[key]}${params ? ':[' + JSON.stringify(params) + ']' : ''}`);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform diffs < 2min to "Just now"', () => {
    // given
    const now = moment.unix(1532703600);
    const date = now.clone().subtract(10, 'second');

    // when
    const result = pipe.transform(date, false, now);

    // then
    expect(result).toBe('Just now:[{"minutes":0}]');
  });

  it('transform diffs < 1h to "X minutes ago"', () => {
    // given
    const now = moment.unix(1532703600);
    const date = now.clone().subtract(10, 'minute');

    // when
    const result = pipe.transform(date, false, now);

    // then
    expect(result).toBe('{minutes} minutes ago:[{"minutes":10}]');
  });

  it('transform diffs from today to "Today, HH:mm"', () => {
    // given
    const now = moment.unix(1532703600);
    const date = now.clone().subtract(10, 'hour');

    // when
    const result = pipe.transform(date, false, now);

    // then
    expect(result).toBe('Today, {time}:[{"date":"Jul 27, 2018:{\\"sameYear\\":true}","time":"05:00 AM"}]');
  });

  it('transform diffs from yesterday "Yesterday, HH:mm"', () => {
    // given
    const now = moment.unix(1532703600);
    const date = now.clone().subtract(24, 'hour');

    // when
    const result = pipe.transform(date, false, now);

    // then
    expect(result).toBe('Yesterday, {time}:[{"date":"Jul 26, 2018:{\\"sameYear\\":true}","time":"03:00 PM"}]');
  });

  it('transform diffs from an old date "D. MMM YYYY, HH:mm"', () => {
    // given
    const now = moment.unix(1532703600);
    const date = now.clone().subtract(10, 'day');

    // when
    const result = pipe.transform(date, false, now);

    // then
    expect(result).toBe('{date}, {time}:[{"date":"Jul 17, 2018:{\\"sameYear\\":true}","time":"03:00 PM"}]');
  });

  it('transform invalid dates to an empty string', () => {
    // given
    const now = moment.unix(1532703600);

    // when
    const result = pipe.transform(null, false, now);

    // then
    expect(result).toBe('');
  });

  it('should update the text when input values change', () => {
    // given
    const now = moment.unix(1532703600);
    const date1 = now.clone().subtract(10, 'second');
    const date2 = now.clone().subtract(10, 'day');

    // when
    const result1 = pipe.transform(date1, false, now);
    const result2 = pipe.transform(date2, false, now);

    // then
    expect(result1).toBe('Just now:[{"minutes":0}]');
    expect(result2).toBe('{date}, {time}:[{"date":"Jul 17, 2018:{\\"sameYear\\":true}","time":"03:00 PM"}]');
  });

  it('should update periodically', () => {
    // given
    const now = moment.unix(1532703600);
    const date = now.clone().subtract(10, 'second');

    // when
    pipe.transform(date, false, now);

    // then
    expect(ngZone.runOutsideAngular).toHaveBeenCalled();
    expect(ngZone.run).toHaveBeenCalled();
    expect(cdRef.markForCheck).toHaveBeenCalled();
    expect(window.setTimeout).toHaveBeenCalledWith(jasmine.any(Function), 110000);
  });
});
