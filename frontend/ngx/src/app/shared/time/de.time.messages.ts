import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'TIME_AGO.NOW': 'Gerade eben',
    'TIME_AGO.NOW.SHORT': 'Gerade eben',
    'TIME_AGO.MINUTES': 'Vor {minutes} Minuten',
    'TIME_AGO.MINUTES.SHORT': '{minutes} Min.',
    'TIME_AGO.TODAY': 'Heute, {time} Uhr',
    'TIME_AGO.TODAY.SHORT': 'Heute, {time}',
    'TIME_AGO.YESTERDAY': 'Gestern, {time} Uhr',
    'TIME_AGO.YESTERDAY.SHORT': 'Gestern, {time}',
    'TIME_AGO.EXACT': '{date}, {time} Uhr',
    'TIME_AGO.EXACT.SHORT': '{date}, {time}',
    'TIME_AGO.DATE': 'D. MMM YYYY',
    'TIME_AGO.DATE.SHORT': '{sameYear, select, true{DD.MM.} other{DD.MM.YY}}',
    'TIME_AGO.TIME': 'HH:mm',
    'TIME_AGO.TIME.SHORT': 'HH:mm'
  }
};
