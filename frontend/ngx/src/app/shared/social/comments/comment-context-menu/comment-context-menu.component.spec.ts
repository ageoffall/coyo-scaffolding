import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Comment} from '@domain/comment/comment';
import {CommentService} from '@domain/comment/comment.service';
import {NG1_AUTH_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {CommentContextMenuComponent} from './comment-context-menu.component';

describe('CommentContextMenuComponent', () => {
  let component: CommentContextMenuComponent;
  let fixture: ComponentFixture<CommentContextMenuComponent>;
  let commentService: jasmine.SpyObj<CommentService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentContextMenuComponent],
      providers: [{
        provide: NG1_AUTH_SERVICE, useValue: jasmine.createSpyObj('authService', ['getCurrentUserId'])
      }, {
        provide: CommentService, useValue: jasmine.createSpyObj('commentService', ['get'])
      }]
    }).overrideTemplate(CommentContextMenuComponent, '')
      .compileComponents();

    commentService = TestBed.get(CommentService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the show original author event', fakeAsync(() => {
    // when
    component.showAuthor.subscribe((showAuthor: boolean) => {
      expect(showAuthor).toBeTruthy();
    });

    component.onClickShowOriginalAuthors();

    // then --> observable
    tick();
  }));

  it('should show delete entry', () => {
    // given
    component.comment = {
      _permissions: {
        delete: true
      }
    } as Comment;

    // when
    const result = component.hasPermission();

    // then
    expect(result).toBeTruthy();
  });

  it('should show edit entry', () => {
    // given
    component.comment = {
      _permissions: {
        edit: true
      }
    } as Comment;

    // when
    const result = component.hasPermission();

    // then
    expect(result).toBeTruthy();
  });

  it('should show show original author entry', () => {
    // given
    component.comment = {
      author: {
        id: 'other-id'
      },
      _permissions: {
        accessoriginalauthor: true
      }
    } as Comment;

    // when
    const result = component.hasPermission();

    // then
    expect(result).toBeTruthy();
  });

  it('should show nothing when no permission is set', () => {
    // given
    component.comment = {
      _permissions: {
      }
    } as Comment;

    // when
    const result = component.hasPermission();

    // then
    expect(result).toBeFalsy();
  });

  it('should request the comment permissions when context menu is open and comment was send over websocket', () => {
    // given
    component.comment = {
      id: 'comment-id',
      partiallyLoaded: true,
      _permissions: {
      }
    } as Comment;

    const emitSpy = spyOn(component.changed, 'emit');

    const comment = {
      _permissions: {
        delete: true
      }
    } as Comment;

    commentService.get.and.returnValue(of(comment));

    // when
    const callback = component.onOpen();
    callback().subscribe();

    // then
    expect(commentService.get).toHaveBeenCalledWith('comment-id', {params: {_permissions: '*'}, handleErrors: false});
    expect(emitSpy).toHaveBeenCalledWith(comment);
    expect(comment.partiallyLoaded).toBeTruthy();
  });
});
