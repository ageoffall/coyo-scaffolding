import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {Comment} from '@domain/comment/comment';
import {CommentService} from '@domain/comment/comment.service';
import {Ng1AuthService} from '@root/typings';
import {NG1_AUTH_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

/**
 * Component display the context menu of comments
 */
@Component({
  selector: 'coyo-comment-context-menu',
  templateUrl: './comment-context-menu.component.html',
  styleUrls: ['./comment-context-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentContextMenuComponent implements OnInit {

  /**
   * The comment to show the context menu for
   */
  @Input() comment: Comment;

  /**
   * If the comment is editable based on non user permission criteria.
   * This could be true for example if the comment is the last one in the list.
   */
  @Input() editable: boolean;

  /**
   * Emits if show author is clicked
   */
  @Output() showAuthor: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Emits if edit is clicked
   */
  @Output() edit: EventEmitter<void> = new EventEmitter<void>();

  /**
   * Emits if delete is clicked
   */
  @Output() delete: EventEmitter<void> = new EventEmitter<void>();

  /**
   * Emits when the comment is changed
   */
  @Output() changed: EventEmitter<Comment> = new EventEmitter<Comment>();

  loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  currentUserId: string;

  private showOriginalAuthor: boolean;

  constructor(@Inject(NG1_AUTH_SERVICE) private authService: Ng1AuthService, private commentService: CommentService) {
  }

  ngOnInit(): void {
    this.currentUserId = this.authService.getCurrentUserId();
  }

  /**
   * Emits the show original author event and toggles the flag
   */
  onClickShowOriginalAuthors(): void {
    this.showOriginalAuthor = !this.showOriginalAuthor;
    this.showAuthor.emit(this.showOriginalAuthor);
  }

  /**
   * Checks if the user can access any actions of the context menu
   *
   * @returns true if the user has access to at least one action
   */
  hasPermission(): boolean {
    return (this.comment._permissions.accessoriginalauthor && this.comment.author.id !== this.currentUserId)
      || this.comment._permissions.edit || this.comment._permissions.delete;
  }

  /**
   * Creates a callback function called when the context menu is opened to load the comments permissions when it
   * was sent over websockets.
   *
   * @returns the callback function
   */
  onOpen(): () => Observable<Comment> {
    return () => {
      if (this.comment.partiallyLoaded && (!this.comment._permissions || !_.keys(this.comment._permissions).length)) {
        this.loading$.next(true);
        const options = {params: {_permissions: '*'}, handleErrors: false};
        return this.commentService.get(this.comment.id, options)
          .pipe(map(comment => {
            comment.partiallyLoaded = true;
            this.loading$.next(false);
            this.changed.emit(comment);
            return comment;
          }))
          .pipe(catchError(err => {
            this.loading$.next(false);
            return of(this.comment);
          }));
      } else {
        return of(this.comment);
      }
    };
  }
}
