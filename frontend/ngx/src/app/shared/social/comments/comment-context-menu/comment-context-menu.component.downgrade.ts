import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {CommentContextMenuComponent} from '@shared/social/comments/comment-context-menu/comment-context-menu.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoCommentContextMenu', downgradeComponent({
    component: CommentContextMenuComponent,
    propagateDigest: false
  }));
