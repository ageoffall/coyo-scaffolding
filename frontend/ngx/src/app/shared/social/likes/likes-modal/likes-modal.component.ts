import {AfterViewInit, ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {Like} from '@domain/like/like';
import {LikeService} from '@domain/like/like.service';
import {Likeable} from '@domain/like/likeable';
import {Modal} from '@domain/modal/modal';
import {Direction} from '@domain/pagination/direction.enum';
import {Order} from '@domain/pagination/order';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {UIRouter} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Observable, of, Subject} from 'rxjs';
import {catchError, map, scan, switchMap} from 'rxjs/operators';

/**
 * Modal showing a list of senders who liked the given target.
 */
@Component({
  selector: 'coyo-likes-modal',
  templateUrl: './likes-modal.component.html',
  styleUrls: ['./likes-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LikesModalComponent extends Modal<void> implements AfterViewInit {

  target: Likeable;

  likes$: Observable<Like[]>;

  currentPage: Page<Like>;

  loading: boolean = true;

  private readonly SORT: Order = new Order('created', Direction.Desc);
  private loadMoreSubject: Subject<void> = new Subject<void>();

  constructor(modal: BsModalRef, @Inject(LikeService) private likeService: LikeService, uiRouter: UIRouter) {
    super(modal, uiRouter);
    this.likes$ = this.loadMoreSubject
      .pipe(switchMap(() => this.loadNextPage()))
      .pipe(scan((acc, value) => acc.concat(value)));
  }

  ngAfterViewInit(): void {
    this.loadMoreSubject.next();
  }

  /**
   * Loads the next page of likes
   */
  loadMore(): void {
    if (this.loading || this.currentPage.last) {
      return;
    }
    this.loading = true;
    this.loadMoreSubject.next();
  }

  private loadNextPage(): Observable<Like[]> {
    return this.likeService.getPage(new Pageable(this.currentPage ? this.currentPage.number + 1 : 0, 20, null, this.SORT), {
      context: {
        targetId: this.target.id,
        targetType: this.target.typeName
      }
    }).pipe(map(page => {
      this.currentPage = page;
      this.loading = false;
      return this.currentPage.content;
    })).pipe(catchError(() => {
      this.loading = false;
      return of([]);
    }));
  }
}
