import {Injectable} from '@angular/core';
import {Likeable} from '@domain/like/likeable';
import {environment} from '@root/environments/environment';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {LikesModalComponent} from './likes-modal.component';

/**
 * Modal service for opening the likes modal
 */
@Injectable({
  providedIn: 'root'
})
export class LikesModalService {

  constructor(private modalService: BsModalService) {
  }

  /**
   * Opens a modal listing all the likes for the given target
   *
   * @param target The like target
   * @return The modal reference
   */
  open(target: Likeable): BsModalRef {
    return this.modalService.show(LikesModalComponent, {
      animated: environment.enableAnimations,
      initialState: {target}
    });
  }
}
