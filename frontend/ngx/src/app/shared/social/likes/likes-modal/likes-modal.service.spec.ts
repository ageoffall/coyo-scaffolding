import {TestBed} from '@angular/core/testing';
import {Likeable} from '@domain/like/likeable';
import {BsModalService} from 'ngx-bootstrap/modal';
import {LikesModalComponent} from './likes-modal.component';
import {LikesModalService} from './likes-modal.service';

describe('LikesModalService', () => {
  let service: LikesModalService;
  let modalService: jasmine.SpyObj<BsModalService>;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: BsModalService, useValue: jasmine.createSpyObj('modalService', ['show'])
    }]
  }));

  beforeEach(() => {
    service = TestBed.get(LikesModalService);
    modalService = TestBed.get(BsModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open the like modal', () => {
    // given
    const likable = {
      id: 'id',
      typeName: 'timeline-item'
    } as Likeable;

    // when
    service.open(likable);

    // then
    expect(modalService.show).toHaveBeenCalledWith(LikesModalComponent, {
      animated: false,
      initialState: {target: likable}
    });
  });
});
