import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LikeState} from '@domain/like/like-state';
import {LikeService} from '@domain/like/like.service';
import {Likeable} from '@domain/like/likeable';
import {Sender} from '@domain/sender/sender';
import {LikesModalService} from '@shared/social/likes/likes-modal/likes-modal.service';
import {LikeInfoComponent} from './like-info.component';

describe('LikeInfoComponent', () => {
  let component: LikeInfoComponent;
  let fixture: ComponentFixture<LikeInfoComponent>;
  let likesModalService: jasmine.SpyObj<LikesModalService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LikeInfoComponent],
      providers: [{
        provide: LikeService,
        useValue: jasmine.createSpyObj('likeService', ['getLikeTargetState$'])
      }, {
        provide: LikesModalService,
        useValue: jasmine.createSpyObj('likesModalService', ['open'])
      }]
    }).overrideTemplate(LikeInfoComponent, '')
      .compileComponents();

    likesModalService = TestBed.get(LikesModalService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the likes modal', () => {
    // when
    const target: Likeable = {
      id: 'test-id',
      typeName: 'timeline-item'
    };
    component.target = target;

    // given
    component.openLikes();

    // then
    expect(likesModalService.open).toHaveBeenCalledWith(target);
  });

  it('should update the like count info on changes', () => {
    // given
    component.state = {
      isLiked: true,
      totalCount: 100,
      othersCount: 99,
      others: [{displayName: 'John'} as Sender]
    } as LikeState;

    // when
    component.ngOnChanges({});

    // then
    expect(component.info.count).toBe(100);
    expect(component.info.isLiked).toBeTruthy();
    expect(component.info.latest).toBe('John');
    expect(component.info.othersCount).toBe(99);
  });
});
