import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {Shareable} from '@domain/share/shareable';
import {Ng1NotificationService} from '@root/typings';
import {ShareModalComponent} from '@shared/social/shares/share-modal/share-modal.component';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {NgxPermissionsService} from 'ngx-permissions';
import {of, Subject} from 'rxjs';
import {ShareButtonComponent} from './share-button.component';

describe('ShareButtonComponent', () => {
  let component: ShareButtonComponent;
  let fixture: ComponentFixture<ShareButtonComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let modalService: jasmine.SpyObj<BsModalService>;
  let shareService: jasmine.SpyObj<ShareService>;
  let notificationService: jasmine.SpyObj<Ng1NotificationService>;
  let permissionService: jasmine.SpyObj<NgxPermissionsService>;
  let onCloseSubject: Subject<object>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareButtonComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser'])
      }, {
        provide: BsModalService,
        useValue: jasmine.createSpyObj('BsModalService', ['show'])
      }, {
        provide: ShareService,
        useValue: jasmine.createSpyObj('shareService', ['getShares', 'getShareCount'])
      }, {
        provide: NG1_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success'])
      }, {
        provide: NgxPermissionsService,
        useValue: jasmine.createSpyObj('permissionService', ['hasPermission'])
      }]
    }).overrideTemplate(ShareButtonComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    modalService = TestBed.get(BsModalService);
    shareService = TestBed.get(ShareService);
    notificationService = TestBed.get(NG1_NOTIFICATION_SERVICE);
    permissionService = TestBed.get(NgxPermissionsService);
    onCloseSubject = new Subject<object>();
  }));

  beforeEach(() => {
    authService.getUser.and.returnValue(of({}));
    modalService.show.and.returnValue({
      content: {
        onClose: onCloseSubject.asObservable()
      },
      hide: () => {
      }
    });
    shareService.getShares.and.returnValue(of([{} as Share]));
    shareService.getShareCount.and.returnValue(of(1));
    permissionService.hasPermission.and.returnValue(of(true));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareButtonComponent);
    component = fixture.componentInstance;

    // given data
    component.target = {
      id: 'target-id',
      created: new Date().valueOf(),
      modified: new Date().valueOf(),
      typeName: 'timeline-item',
      itemType: 'item-type',
      parentPublic: true,
      shares: [{} as Share],
      _permissions: {
        sticky: true
      }
    } as Shareable;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // when
    fixture.detectChanges();

    // then
    expect(authService.getUser).toHaveBeenCalled();
  });

  describe('Share modal', () => {

    it('should open', () => {
      // when
      component.openShareModal();

      // then
      expect(modalService.show).toHaveBeenCalled();
    });

    it('should open with initial state', () => {
      // given
      fixture.detectChanges();
      const expectedInitialState = {
        initialSender: {},
        parentIsPublic: true,
        canCreateStickyShare: true,
        targetId: 'target-id',
        typeName: 'timeline-item'
      };

      // when
      component.openShareModal();

      // then
      const args = modalService.show.calls.mostRecent().args as any;
      expect(args[0]).toEqual(ShareModalComponent);
      expect(args[1].initialState).toBeDefined();
      expect(args[1].initialState.initialSender).toEqual(expectedInitialState.initialSender);
      expect(args[1].initialState.parentIsPublic).toEqual(expectedInitialState.parentIsPublic);
      expect(args[1].initialState.targetId).toEqual(expectedInitialState.targetId);
      expect(args[1].initialState.typeName).toEqual(expectedInitialState.typeName);
      args[1].initialState.canCreateStickyShare.subscribe((canCreateStickyShare: boolean) =>
        expect(canCreateStickyShare).toEqual(expectedInitialState.canCreateStickyShare));
      expect(permissionService.hasPermission).toHaveBeenCalledWith('CREATE_STICKY_TIMELINE_ITEM');
    });

    it('should open and process result on close', () => {
      // given
      spyOn(component.shareCreated, 'emit');

      // when
      component.openShareModal();
      onCloseSubject.next({} as Share);

      // then
      expect(component.shareCreated.emit).toHaveBeenCalled();
      expect(notificationService.success).toHaveBeenCalled();
    });
  });
});
