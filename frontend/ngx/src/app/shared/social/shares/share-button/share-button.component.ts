import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {Shareable} from '@domain/share/shareable';
import {User} from '@domain/user/user';
import {environment} from '@root/environments/environment';
import {Ng1NotificationService} from '@root/typings';
import {ShareModalComponent} from '@shared/social/shares/share-modal/share-modal.component';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {NgxPermissionsService} from 'ngx-permissions';
import {from} from 'rxjs';

/**
 * Renders a share button and a share count from the perspective of the current user.
 */
@Component({
  selector: 'coyo-share-button',
  templateUrl: './share-button.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareButtonComponent implements OnInit {

  /**
   * Model with data.
   */
  @Input() target: Shareable;

  /**
   * Only show the icon.
   */
  @Input() condensed: boolean;

  /**
   * Event if a share was created.
   */
  @Output() shareCreated: EventEmitter<Share> = new EventEmitter<Share>();

  private currentUser: User;
  private shareModalIsOpen: boolean = false;

  constructor(private authService: AuthService,
              private modalService: BsModalService,
              private permissionService: NgxPermissionsService,
              @Inject(NG1_NOTIFICATION_SERVICE) private notificationService: Ng1NotificationService) {
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(user => this.currentUser = user);
  }

  /**
   * Opens a modal with the share form.
   */
  openShareModal(): void {
    if (this.shareModalIsOpen) {
      return;
    }
    this.shareModalIsOpen = true;

    this.modalService.show(ShareModalComponent, {
      animated: environment.enableAnimations,
      backdrop: 'static',
      initialState: {
        initialSender: this.currentUser,
        parentIsPublic: this.target.parentPublic,
        targetId: this.target.id,
        typeName: this.target.typeName,
        canCreateStickyShare: from(this.permissionService.hasPermission('CREATE_STICKY_TIMELINE_ITEM'))
      }
    }).content.onClose.subscribe((createdShare: Share) => {
      this.shareModalIsOpen = false;
      if (createdShare) {
        this.shareCreated.emit(createdShare);
        this.notificationService.success('COMMONS.SHARES.SUCCESS');
      }
    });
  }
}
