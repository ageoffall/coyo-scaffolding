import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {StickyExpiryPeriods} from '@app/timeline/timeline-form/sticky-btn/sticky-expiry-periods';
import {UrlService} from '@core/http/url/url.service';
import {Sender} from '@domain/sender/sender';
import {ShareService} from '@domain/share/share.service';
import {UIRouterModule} from '@uirouter/angular';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {of} from 'rxjs';
import {ShareModalComponent} from './share-modal.component';

describe('ShareWithModalComponent', () => {
  let component: ShareModalComponent;
  let fixture: ComponentFixture<ShareModalComponent>;
  let shareService: jasmine.SpyObj<ShareService>;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule.forRoot({useHash: true})],
      declarations: [ShareModalComponent],
      providers: [{
        provide: ShareService,
        useValue: jasmine.createSpyObj('ShareService', ['post'])
      }, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['toUrlParamString'])
      }, {
        provide: NG1_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['error'])
      }, BsModalRef, FormBuilder]
    }).overrideTemplate(ShareModalComponent, '<div></div>')
      .compileComponents();

    shareService = TestBed.get(ShareService);
    urlService = TestBed.get(UrlService);
  }));

  beforeEach(() => {
    shareService.post.and.returnValue(of({key: 'value'}));
    urlService.toUrlParamString.and.callThrough();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareModalComponent);
    component = fixture.componentInstance;
    component.initialSender = {
      typeName: 'user',
      displayName: 'Robert Lang'
    } as Sender;
    component.parentIsPublic = true;
    component.canCreateStickyShare = of(true);
    component.targetId = 'target-id';
    component.typeName = 'type-name';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    expect(component.shareForm).toBeTruthy();
    expect(component.shareForm.contains('shareWith')).toBeTruthy();
    expect(component.shareForm.contains('message')).toBeTruthy();
    expect(component.shareForm.contains('shareAs')).toBeTruthy();
    expect(component.shareForm.contains('stickyExpiry')).toBeTruthy();
    expect(component.onClose).toBeDefined();

    expect(component.showExtended).toBeFalsy();
    expect(urlService.toUrlParamString).toHaveBeenCalledWith('type', ['user', 'page', 'workspace']);
    expect(component.shareWithConfig.findSharingRecipients).toBeTruthy();
    expect(urlService.toUrlParamString).toHaveBeenCalledWith('type', ['user', 'page', 'workspace', 'event']);
    expect(component.shareAsConfig.findOnlyManagedSenders).toBeTruthy();
  });

  it('should disable sticky expiry field if type name is user', () => {
    component.shareForm.controls.shareWith.setValue({typeName: 'user'} as Sender);
    expect(component.shareForm.controls.stickyExpiry.disabled).toBeTruthy();
  });

  it('should provide form data to parent after submit', () => {
    // given
    const shareWithSender = {
      id: 'share-with-id'
    };
    const shareAsSender = {
      id: 'share-as-id'
    };
    const message = 'Message';
    const stickyExpiry = StickyExpiryPeriods.oneDay;
    spyOn(component.onClose, 'next');
    spyOn(component.shareForm, 'disable');
    component.shareForm.controls['shareWith'].setValue(shareWithSender);
    component.shareForm.controls['shareAs'].setValue(shareAsSender);
    component.shareForm.controls['message'].setValue(message);
    component.shareForm.controls['stickyExpiry'].setValue(stickyExpiry);

    // when
    component.onSubmit();

    // then
    expect(component.shareForm.disable).toHaveBeenCalled();
    expect(shareService.post).toHaveBeenCalled();
    fixture.detectChanges();
    expect(component.onClose.next).toHaveBeenCalledWith({key: 'value'});
  });

  it('should call close on destroy', () => {
    // given
    spyOn(component, 'close');

    // when
    component.ngOnDestroy();

    // then
    expect(component.close).toHaveBeenCalledWith();
  });
});
