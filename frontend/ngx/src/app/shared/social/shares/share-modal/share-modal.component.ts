import {ChangeDetectionStrategy, Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StickyExpiryPeriod} from '@app/timeline/timeline-form/sticky-btn/sticky-expiry-period';
import {StickyExpiryPeriods} from '@app/timeline/timeline-form/sticky-btn/sticky-expiry-periods';
import {UrlService} from '@core/http/url/url.service';
import {Modal} from '@domain/modal/modal';
import {Sender} from '@domain/sender/sender';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {Ng1NotificationService} from '@root/typings';
import {SenderParameter} from '@shared/sender-ui/select-sender/sender-parameter';
import {UIRouter} from '@uirouter/angular';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Observable} from 'rxjs';
import {distinctUntilChanged} from 'rxjs/operators';

/**
 * Renders the share modal view.
 */
@Component({
  selector: 'coyo-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareModalComponent extends Modal<Share> implements OnInit, OnDestroy {

  /**
   * The initial sender.
   */
  @Input() initialSender: Sender;

  /**
   * Boolean to indicate if the parent is public.
   */
  @Input() parentIsPublic: boolean;

  /**
   * The id of the current target.
   */
  @Input() targetId: string;

  /**
   * The type name of the target. E.g.: "timeline-item".
   */
  @Input() typeName: string;

  /**
   * The user can create a sticky share.
   */
  @Input() canCreateStickyShare: Observable<boolean>;

  stickyExpiryOptions: StickyExpiryPeriod[] = StickyExpiryPeriods.all;

  showExtended: boolean = false;
  shareForm: FormGroup;
  shareWithConfig: SenderParameter = {
    filters: this.urlService.toUrlParamString('type', ['user', 'page', 'workspace']),
    findSharingRecipients: true
  };
  shareAsConfig: SenderParameter = {
    filters: this.urlService.toUrlParamString('type', ['user', 'page', 'workspace', 'event']),
    findOnlyManagedSenders: true
  };

  constructor(private shareService: ShareService,
              private urlService: UrlService,
              private formBuilder: FormBuilder,
              modal: BsModalRef,
              uiRouter: UIRouter,
              @Inject(NG1_NOTIFICATION_SERVICE) private notificationService: Ng1NotificationService) {
    super(modal, uiRouter);
  }

  ngOnInit(): void {
    this.shareForm = this.formBuilder.group({
      shareWith: [null, Validators.required],
      message: '',
      shareAs: [this.initialSender, Validators.required],
      stickyExpiry: StickyExpiryPeriods.none
    });
    this.initValueChangeValidation();
  }

  ngOnDestroy(): void {
    this.close();
    super.ngOnDestroy();
  }

  /**
   * Will be called on a form submit, save the share form data and pass the response to the parent component.
   */
  onSubmit(): void {
    this.shareForm.disable();
    const shareWith = this.shareForm.controls['shareWith'].value;
    const message = this.shareForm.controls['message'].value;
    const shareAs = this.shareForm.controls['shareAs'].value;
    const stickyExpiry = this.shareForm.controls['stickyExpiry'].value;
    const share: any = {
      itemId: this.targetId,
      recipientId: shareWith.id,
      data: {
        message: message
      },
      authorId: shareAs.id,
      stickyExpiry: this.getDurationDate(stickyExpiry.expiry)
    };

    this.shareService.post(share, {path: '/' + this.typeName})
      .subscribe((createdShare: Share) => {
        this.close(createdShare);
      }, () => {
        this.notificationService.error('ERRORS.STATUSCODE.NOT_FOUND.DEFAULT');
        this.shareForm.enable();
      });
  }

  private initValueChangeValidation(): void {
    this.shareForm.controls.shareWith.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((shareWithData: Sender) => {
        if (shareWithData) {
          const stickyExpiryField = this.shareForm.get('stickyExpiry');
          if (shareWithData.typeName === 'user') {
            stickyExpiryField.disable();
          } else {
            stickyExpiryField.enable();
          }
        }
      });
  }

  private getDurationDate(duration: number): number {
    return duration ? new Date().getTime() + duration : null;
  }
}
