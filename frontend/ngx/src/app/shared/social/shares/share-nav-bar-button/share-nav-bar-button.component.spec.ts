import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {Ng1NotificationService} from '@root/typings';
import {ShareModalComponent} from '@shared/social/shares/share-modal/share-modal.component';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {NgxPermissionsService} from 'ngx-permissions';
import {of, Subject} from 'rxjs';
import {ShareNavBarButtonComponent} from './share-nav-bar-button.component';

describe('ShareLinkComponent', () => {
  let component: ShareNavBarButtonComponent;
  let fixture: ComponentFixture<ShareNavBarButtonComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let modalService: jasmine.SpyObj<BsModalService>;
  let notificationService: jasmine.SpyObj<Ng1NotificationService>;
  let permissionService: jasmine.SpyObj<NgxPermissionsService>;
  let onCloseSubject: Subject<object>;
  const shareTarget: any = {
    id: 'sender-id',
    typeName: 'type-name',
    public: true
  };

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ShareNavBarButtonComponent],
        providers: [{
          provide: AuthService,
          useValue: jasmine.createSpyObj('AuthService', ['getUser'])
        }, {
          provide: BsModalService,
          useValue: jasmine.createSpyObj('BsModalService', ['show'])
        }, {
          provide: NG1_NOTIFICATION_SERVICE,
          useValue: jasmine.createSpyObj('notificationService', ['success'])
        }, {
          provide: NgxPermissionsService,
          useValue: jasmine.createSpyObj('permissionService', ['hasPermission'])
        }]
      })
      .overrideTemplate(ShareNavBarButtonComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    modalService = TestBed.get(BsModalService);
    notificationService = TestBed.get(NG1_NOTIFICATION_SERVICE);
    permissionService = TestBed.get(NgxPermissionsService);
    onCloseSubject = new Subject<object>();
  }));

  beforeEach(() => {
    authService.getUser.and.returnValue(of({}));
    modalService.show.and.returnValue({
      content: {
        onClose: onCloseSubject.asObservable()
      },
      hide: () => {
      }
    });
    permissionService.hasPermission.and.returnValue(of(true));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareNavBarButtonComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(authService.getUser).toHaveBeenCalled();
  });

  it('should open share modal', () => {
    // given
    const expectedInitialState = {
      initialSender: {},
      parentIsPublic: shareTarget.public,
      targetId: shareTarget.id,
      typeName: shareTarget.typeName,
      canCreateStickyShare: true
    };
    component.shareTarget = shareTarget;

    // when
    fixture.detectChanges();
    component.openShareModal();

    // then
    const args = modalService.show.calls.mostRecent().args as any;
    expect(args[0]).toEqual(ShareModalComponent);
    expect(args[1].initialState).toBeDefined();
    expect(args[1].initialState.initialSender).toEqual(expectedInitialState.initialSender);
    expect(args[1].initialState.parentIsPublic).toEqual(expectedInitialState.parentIsPublic);
    expect(args[1].initialState.targetId).toEqual(expectedInitialState.targetId);
    expect(args[1].initialState.typeName).toEqual(expectedInitialState.typeName);
    args[1].initialState.canCreateStickyShare.subscribe((canCreateStickyShare: boolean) => {
      expect(canCreateStickyShare).toEqual(expectedInitialState.canCreateStickyShare);
    });
    expect(permissionService.hasPermission).toHaveBeenCalledWith('CREATE_STICKY_TIMELINE_ITEM');
  });

  it('should open modal and process result on close', () => {
    // given
    component.shareTarget = shareTarget;

    // when
    component.openShareModal();
    onCloseSubject.next({} as Share);

    // then
    expect(notificationService.success).toHaveBeenCalled();
  });

});
