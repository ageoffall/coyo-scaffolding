import {ChangeDetectionStrategy, Component, Inject, Input, OnInit} from '@angular/core';
import {AuthService} from '@core/auth/auth.service';
import {Share} from '@domain/share/share';
import {User} from '@domain/user/user';
import {environment} from '@root/environments/environment';
import {Ng1NotificationService} from '@root/typings';
import {ShareModalComponent} from '@shared/social/shares/share-modal/share-modal.component';
import {NG1_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {NgxPermissionsService} from 'ngx-permissions';
import {from} from 'rxjs';

interface ShareableSender {
  id: string;
  typeName: string;
  public: boolean;
}

/**
 * Share button for e.g.: pages, workspaces and events.
 */
@Component({
  selector: 'coyo-share-nav-bar-button',
  templateUrl: './share-nav-bar-button.component.html',
  styleUrls: ['./share-nav-bar-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareNavBarButtonComponent implements OnInit {

  /**
   * The share target. Can be a page, workspace or event.
   */
  @Input() shareTarget: ShareableSender;

  /**
   * The translation key.
   */
  @Input() translationKey: string;

  shareModalIsOpen: boolean = false;

  currentUser: User;

  constructor(private authService: AuthService,
              private modalService: BsModalService,
              private permissionService: NgxPermissionsService,
              @Inject(NG1_NOTIFICATION_SERVICE) private notificationService: Ng1NotificationService) {
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(user => this.currentUser = user);
  }

  /**
   * Opens a modal with the share form.
   */
  openShareModal(): void {
    if (this.shareModalIsOpen) {
      return;
    }
    this.shareModalIsOpen = true;

    const shareModal = this.modalService.show(ShareModalComponent, {
      animated: environment.enableAnimations,
      backdrop: 'static',
      initialState: {
        initialSender: this.currentUser,
        parentIsPublic: this.shareTarget.public,
        targetId: this.shareTarget.id,
        typeName: this.shareTarget.typeName,
        canCreateStickyShare: from(this.permissionService.hasPermission('CREATE_STICKY_TIMELINE_ITEM'))
      }
    });

    shareModal.content.onClose.subscribe((createdShare: Share) => {
      if (createdShare) {
        this.notificationService.success('COMMONS.SHARES.SUCCESS');
      }
      this.shareModalIsOpen = false;
    });
  }

}
