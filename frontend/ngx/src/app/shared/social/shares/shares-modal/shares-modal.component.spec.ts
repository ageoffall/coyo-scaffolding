import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Share} from '@domain/share/share';
import {UIRouterModule} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {SharesModalComponent} from './shares-modal.component';

describe('SharesModalComponent', () => {
  let component: SharesModalComponent;
  let fixture: ComponentFixture<SharesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule.forRoot({useHash: true})],
      declarations: [SharesModalComponent],
      providers: [BsModalRef]
    }).overrideTemplate(SharesModalComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharesModalComponent);
    component = fixture.componentInstance;
    component.shares = [
      {id: 'share-1'} as Share,
      {id: 'share-2'} as Share
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return modal result on destroy after updated', () => {
    // given
    const expectedModalChanges = {
      updatedShares: [{id: 'share-1'} as Share] as Share[],
      deletedShares: [] as Share[]
    };
    spyOn(component.onClose, 'next');

    // when
    component.updatedShare({id: 'share-1'} as Share);
    component.ngOnDestroy();

    // then
    expect(component.onClose.next).toHaveBeenCalledWith(expectedModalChanges);
  });

  it('should return modal result on destroy after deleted', () => {
    // given
    const expectedModalChanges = {
      updatedShares: [] as Share[],
      deletedShares: [{id: 'share-2'} as Share] as Share[]
    };
    spyOn(component.onClose, 'next');

    // when
    component.deleteShare({id: 'share-2'} as Share);
    component.ngOnDestroy();

    // then
    expect(component.onClose.next).toHaveBeenCalledWith(expectedModalChanges);
  });
});
