import {Share} from '@domain/share/share';

/**
 * The shares modal result object.
 */
export interface SharesModalUpdateResults {
  updatedShares: Share[];
  deletedShares: Share[];
}
