import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Modal} from '@domain/modal/modal';
import {Share} from '@domain/share/share';
import {TimelineItemType} from '@domain/timeline-item/timeline-item-type';
import {UIRouter} from '@uirouter/angular';
import * as _ from 'lodash';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {SharesModalUpdateResults} from './shares-modal-update-results';

/**
 * Renders the shares modal view.
 */
@Component({
  selector: 'coyo-shares-modal',
  templateUrl: './shares-modal.component.html',
  styleUrls: ['./shares-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SharesModalComponent extends Modal<SharesModalUpdateResults> implements OnInit, OnDestroy {

  /**
   * All shares.
   */
  @Input() shares: Share[];

  /**
   * The item type of the target.
   */
  @Input() itemType: TimelineItemType;

  private modalChanges: SharesModalUpdateResults;

  constructor(modal: BsModalRef,
              uiRouter: UIRouter) {
    super(modal, uiRouter);
  }

  ngOnInit(): void {
    this.modalChanges = {
      updatedShares: [],
      deletedShares: []
    };
  }

  ngOnDestroy(): void {
    this.close(this.modalChanges);
    super.ngOnDestroy();
  }

  /**
   * Update share and emit an update event.
   * @param share The share.
   */
  updatedShare(share: Share): void {
    const idx = _.findIndex(this.shares, {id: share.id});
    if (idx > -1) {
      this.shares[idx] = share;
      this.modalChanges.updatedShares.push(share);
    }
  }

  /**
   * Deletes the share and emit a delete event.
   * @param share The share.
   */
  deleteShare(share: Share): void {
    const idx = _.findIndex(this.shares, {id: share.id});
    if (idx > -1) {
      this.shares.splice(idx, 1);
      this.modalChanges.deletedShares.push(share);
    }
  }
}
