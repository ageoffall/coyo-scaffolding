import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {Shareable} from '@domain/share/shareable';
import {environment} from '@root/environments/environment';
import {SharesModalUpdateResults} from '@shared/social/shares/shares-modal/shares-modal-update-results';
import {SharesModalComponent} from '@shared/social/shares/shares-modal/shares-modal.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

/**
 * Component showing the share count of a target.
 */
@Component({
  selector: 'coyo-share-info',
  templateUrl: './share-info.component.html',
  styleUrls: ['./share-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareInfoComponent {

  /**
   * The count target.
   */
  @Input() target: Shareable;

  /**
   * The number of counts.
   */
  @Input() count: number;

  /**
   * Only show the number.
   */
  @Input() condensed: boolean;

  /**
   * Emits when a share is updated by the shares modal.
   */
  @Output() sharesUpdated: EventEmitter<Share[]> = new EventEmitter<Share[]>();

  /**
   * Emits when a share is deleted by the shares modal.
   */
  @Output() sharesDeleted: EventEmitter<Share[]> = new EventEmitter<Share[]>();

  private sharesModalIsOpen: boolean = false;

  private sharesModal: BsModalRef;

  constructor(private modalService: BsModalService,
              private shareService: ShareService) {
  }

  /**
   * Opens a modal listing all shares.
   */
  openSharesModal(): void {
    if (this.sharesModalIsOpen) {
      return;
    }
    this.sharesModalIsOpen = true;
    this.shareService.getShares(this.target.typeName, this.target.id)
      .subscribe((shares: Share[]) => {
        shares.sort((s1, s2) => s1.created < s2.created ? 1 : -1); // descending
        this.sharesModal = this.modalService.show(SharesModalComponent, {
          animated: environment.enableAnimations,
          initialState: {
            shares: shares,
            itemType: this.target.itemType
          }
        });
        this.sharesModal.content.onClose.subscribe((result: SharesModalUpdateResults) => {
          if (result && result.updatedShares && result.updatedShares.length > 0) {
            this.sharesUpdated.emit(result.updatedShares);
          }
          if (result && result.deletedShares && result.deletedShares.length > 0) {
            this.sharesDeleted.emit(result.deletedShares);
          }
          this.sharesModalIsOpen = false;
        });
      });
  }
}
