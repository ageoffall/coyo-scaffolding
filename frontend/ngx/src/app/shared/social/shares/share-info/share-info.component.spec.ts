import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Share} from '@domain/share/share';
import {ShareService} from '@domain/share/share.service';
import {Shareable} from '@domain/share/shareable';
import {SharesModalUpdateResults} from '@shared/social/shares/shares-modal/shares-modal-update-results';
import {SharesModalComponent} from '@shared/social/shares/shares-modal/shares-modal.component';
import {BsModalService} from 'ngx-bootstrap/modal';
import {of, Subject} from 'rxjs';
import {ShareInfoComponent} from './share-info.component';

describe('ShareInfoComponent', () => {
  let component: ShareInfoComponent;
  let fixture: ComponentFixture<ShareInfoComponent>;
  let modalService: jasmine.SpyObj<BsModalService>;
  let shareService: jasmine.SpyObj<ShareService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareInfoComponent],
      providers: [{
        provide: BsModalService,
        useValue: jasmine.createSpyObj('modalService', ['show'])
      }, {
        provide: ShareService,
        useValue: jasmine.createSpyObj('shareService', ['getShares'])
      }]
    }).overrideTemplate(ShareInfoComponent, '')
      .compileComponents();

    modalService = TestBed.get(BsModalService);
    shareService = TestBed.get(ShareService);
    shareService.getShares.and.returnValue(of([{} as Share]));
    modalService.show.and.returnValue({content: {onClose: of({})}});
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Shares modal', () => {

    it('should open', () => {
      // given
      component.target = {
        typeName: 'timeline-item',
        id: 'id'
      } as Shareable;

      // when
      component.openSharesModal();

      // then
      expect(modalService.show).toHaveBeenCalled();
    });

    it('should open with initial state', () => {
      // given
      component.target = {
        typeName: 'timeline-item',
        id: 'id',
        itemType: 'item-type'
      } as Shareable;

      // when
      component.openSharesModal();

      // then
      expect(shareService.getShares).toHaveBeenCalled();
      expect(modalService.show).toHaveBeenCalledWith(SharesModalComponent, {
        animated: false,
        initialState: {
          shares: [{} as Share],
          itemType: 'item-type'
        }
      });
    });

    it('should open and process result on close', () => {
      // given
      component.target = {
        typeName: 'timeline-item',
        id: 'id'
      } as Shareable;

      spyOn(component.sharesUpdated, 'emit');
      spyOn(component.sharesDeleted, 'emit');
      const modalResult: SharesModalUpdateResults = {
        updatedShares: [{id: 'id-1'}] as Share[],
        deletedShares: [{id: 'id-2'}] as Share[]
      };
      const closeSubject = new Subject<SharesModalUpdateResults>();
      modalService.show.and.returnValue({content: {onClose: closeSubject}});

      // when
      component.openSharesModal();

      closeSubject.next(modalResult);

      // then
      expect(component.sharesUpdated.emit).toHaveBeenCalledWith(modalResult.updatedShares);
      expect(component.sharesDeleted.emit).toHaveBeenCalledWith(modalResult.deletedShares);
    });
  });
});
