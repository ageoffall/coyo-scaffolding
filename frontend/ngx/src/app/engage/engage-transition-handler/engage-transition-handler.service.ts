import {Inject, Injectable} from '@angular/core';
import {Ng1MobileEventService} from '@root/typings';
import {TransitionService} from '@uirouter/core';
import {NG1_MOBILE_EVENTS_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';

/**
 * Handler for the engage app.
 */
@Injectable({
  providedIn: 'root'
})
export class EngageTransitionHandlerService {

  private unregister: Function;

  constructor(private transitionService: TransitionService,
              @Inject(NG1_STATE_SERVICE) private stateService: IStateService,
              @Inject(NG1_MOBILE_EVENTS_SERVICE) private mobileEventService: Ng1MobileEventService) {
  }

  /**
   * Adds a transition listener that cancels all incoming transition except the
   * transitions to the news state and the login state. It also propagates the
   * event 'transition' with the state name and state parameters.
   */
  watchTransitions(): void {
    if (this.unregister) {
      this.unregister();
      this.unregister = null;
    }
    this.unregister = this.transitionService.onStart({}, transition => {
      const targetState = transition.targetState();
      const targetStateName = targetState.name();
      const targetStateParams = targetState.params();
      const targetStateUrl = this.stateService.href(targetStateName, targetStateParams);
      this.mobileEventService.propagate('transition', {
        name: targetStateName,
        params: targetStateParams,
        url: targetStateUrl
      });
      const states = ['news', 'front.login', 'engage'];
      return states.indexOf(targetStateName) !== -1;
    }, {priority: Number.MAX_SAFE_INTEGER});
  }
}
