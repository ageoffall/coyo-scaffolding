import {Injectable} from '@angular/core';
import {NewsViewParams} from '@app/engage/news/news-view-params';
import {StateService} from '@uirouter/core';

/**
 * Service for changing the state to show a specific blog article.
 */
@Injectable({
  providedIn: 'root'
})
export class EngageNewsHandlerService {

  constructor(private stateService: StateService) {
  }

  /**
   * Move to the blog article state.
   *
   * @param params the params defining which blog article to show
   */
  showNews(params: NewsViewParams): void {
    this.stateService.go('news', params);
  }
}
