import {NgModule} from '@angular/core';
import {NewsModule} from '@app/engage/news/news.module';
import './mobile-request-handler-registry/mobile-request-handler-registry.service.downgrade';

/**
 * A module providing all relevant services for the COYO mobile app.
 */
@NgModule({
  imports: [
    NewsModule
  ],
  exports: [
    NewsModule
  ]
})
export class EngageModule {}
