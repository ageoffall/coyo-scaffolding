import {Messages} from '@core/i18n/messages';

/* tslint:disable no-trailing-whitespace */
export const messagesFormEn: Messages = {
  lang: 'en',
  messages: {
    'MODULE.TIMELINE.FORM.HELP.MODAL': `
Depending on your permissions you will find the following options when creating a new timeline post.

### Attach files

Files can be uploaded or linked from the document library. Common image formats and PDFs are displayed as thumbnails 
within the article.

### Make post sticky

If you mark a post as sticky it will be shown to other users at the top of their timeline until the set time has elapsed
 or they mark it as read.

### Lock post

If you lock a post, other users can neither comment on it nor like it.

### Markdown
    
The following guides describe how you can use the **Markdown** syntax to format and structure text content. Markdown is 
allowed in many places in COYO, e.g. on the timeline or inside of comments.

#### Paragraphs and line breaks

You can create a new paragraph by leaving a blank line between lines of text.

#### Styling text

You can indicate emphasis with bold, italic, or strikethrough text.

| Syntax | Example | Output |
| ------------- | ------------- | ------------- |
| \`** **\` or \`__ __\` | \`**This is bold text**\` | **This is bold text** |
| \`* *\` or \`_ _\` | \`*This text is italicized*\` | *This text is italicized* |
| \`~~ ~~\` | \`~~This was mistaken text~~\` | ~~This was mistaken text~~ |
| \`** **\` and \`_ _\` | \`**This text is _extremely_ important**\` | **This text is _extremely_ important**  |

#### Lists

You can make a list by preceding one or more lines of text with \`-\` or \`*\`.

    - George Washington
    - John Adams
    - Thomas Jefferson

- George Washington
- John Adams
- Thomas Jefferson

#### Quoting text

You can quote text with a \`>\`.

    In the words of Abraham Lincoln:

    > Pardon my French

In the words of Abraham Lincoln:

> Pardon my French

#### Ignoring Markdown formatting

You can ignore (or escape) Markdown formatting by using \`\\\` before the Markdown character.

Let's rename \\*our-new-project\\* to \\*our-old-project\\*.

#### Highlighting

You can highlight text within a sentence with single backticks. The text within the backticks will not be formatted.

      This \`word\` should be highlighted.

This \`word\` should be highlighted.

To highlight a code or text block into its own distinct block, use triple backticks.

    \`\`\`
    console.log('Hello World');
    \`\`\`

\`\`\`
console.log('Hello World');
\`\`\`
    `
  }
};
