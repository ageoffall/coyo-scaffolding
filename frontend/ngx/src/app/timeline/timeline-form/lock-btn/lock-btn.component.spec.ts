import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LockBtnComponent} from './lock-btn.component';

describe('LockBtnComponent', () => {
  let component: LockBtnComponent;
  let fixture: ComponentFixture<LockBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LockBtnComponent]
    }).overrideTemplate(LockBtnComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should register on change', () => {
    // given
    const onChange = (_: boolean) => {};

    // when
    component.registerOnChange(onChange);

    // then
    expect((component as any).onChange).toBe(onChange);
  });

  it('should write model value', () => {
    // given
    const value = true;

    // when
    component.writeValue(value);

    // then
    expect(component.model).toBe(value);
  });

  it('should toggle button state', () => {
    // given
    const onChange = jasmine.createSpy();
    component.model = true;
    component.registerOnChange(onChange);
    fixture.detectChanges();

    // when
    const result = component.toggle();

    // then
    expect(result).toBe(false);
    expect(component.model).toBe(false);
    expect(onChange).toHaveBeenCalled();
  });
});
