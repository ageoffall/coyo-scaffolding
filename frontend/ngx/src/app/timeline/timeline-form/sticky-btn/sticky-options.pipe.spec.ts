import {StickyExpiryPeriods} from './sticky-expiry-periods';
import {StickyOptionsPipe} from './sticky-options.pipe';

describe('StickyOptionsPipe', () => {

  it('should create an instance', () => {
    // when
    const pipe = new StickyOptionsPipe();

    // then
    expect(pipe).toBeTruthy();
  });

  it('should filter if model is null ', () => {
    // given
    const pipe = new StickyOptionsPipe();

    // when
    const result = pipe.transform(StickyExpiryPeriods.all, null);

    expect(result).not.toContain(StickyExpiryPeriods.none);
  });

  it('should not filter if model is not null', () => {
    // given
    const pipe = new StickyOptionsPipe();

    // when
    const result = pipe.transform(StickyExpiryPeriods.all, StickyExpiryPeriods.oneDay.expiry);

    expect(result).toContain(StickyExpiryPeriods.none);
  });
});
