/**
 * This interface defines a sticky expiry period.
 */
export interface StickyExpiryPeriod {
  name: string;
  expiry: number | null;
  label: string;
  icon: string;
}
