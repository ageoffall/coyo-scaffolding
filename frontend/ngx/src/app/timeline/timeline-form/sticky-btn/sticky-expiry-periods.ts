import {StickyExpiryPeriod} from './sticky-expiry-period';

/**
 * Predefined sticky expiry periods.
 */
export class StickyExpiryPeriods {

  static oneDay: StickyExpiryPeriod = {
    name: 'oneDay',
    expiry: 1000 * 60 * 60 * 24,
    label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_DAY',
    icon: 'zmdi-timer'
  };

  static oneWeek: StickyExpiryPeriod = {
    name: 'oneWeek',
    expiry: 1000 * 60 * 60 * 24 * 7,
    label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_WEEK',
    icon: 'zmdi-timer'
  };

  static oneMonth: StickyExpiryPeriod = {
    name: 'oneMonth',
    expiry: 1000 * 60 * 60 * 24 * 30,
    label: 'MODULE.TIMELINE.STICKY.EXPIRY.ONE_MONTH',
    icon: 'zmdi-timer'
  };

  static none: StickyExpiryPeriod = {
    name: 'none',
    expiry: null,
    label: 'MODULE.TIMELINE.STICKY.EXPIRY.NONE',
    icon: 'zmdi-timer-off'
  };

  static all: StickyExpiryPeriod[] = [
    StickyExpiryPeriods.oneDay,
    StickyExpiryPeriods.oneWeek,
    StickyExpiryPeriods.oneMonth,
    StickyExpiryPeriods.none
  ];
}
