import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {User} from '@domain/user/user';
import {SublineService} from '@shared/sender-ui/subline/subline.service';
import {of} from 'rxjs';
import {skip} from 'rxjs/operators';
import {FunctionalUserChooserComponent} from './functional-user-chooser.component';

describe('FunctionalUserChooserComponent', () => {
  let component: FunctionalUserChooserComponent;
  let fixture: ComponentFixture<FunctionalUserChooserComponent>;
  let sublineService: jasmine.SpyObj<SublineService>;
  let senderService: jasmine.SpyObj<SenderService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FunctionalUserChooserComponent],
      providers: [{
        provide: SublineService,
        useValue: jasmine.createSpyObj('sublineService', ['getSublineForSender'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('senderService', ['getActableSenders'])
      }]
    }).overrideTemplate(FunctionalUserChooserComponent, '')
      .compileComponents();

    sublineService = TestBed.get(SublineService);
    senderService = TestBed.get(SenderService);
    sublineService.getSublineForSender.and.returnValue(of('subline'));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionalUserChooserComponent);
    component = fixture.componentInstance;
    component.author = {
      id: 'user-id'
    } as User;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should always show the author', fakeAsync(() => {
    // then
    component.senders$.subscribe(senders => {
      expect(senders.length).toBe(1);
      expect(senders[0].id).toBe(component.author.id);
      expect(senders[0].subline).toBe('subline');
    });
    tick();
  }));

  it('should load the first page after opening the dropdown', fakeAsync(() => {
    // given
    const senderId = 'sender-id';
    const page = {
      content: [{id: senderId}]
    } as Page<Sender>;
    senderService.getActableSenders.and.returnValue(of(page));
    component.senders$.pipe(skip(1)).subscribe(senders => {
      expect(senders.length).toBe(2);
      expect(senders[0].id).toBe(component.author.id);
      expect(senders[1].id).toBe(senderId);
    });

    // when
    component.onOpen();
    tick();

    // then
    expect(senderService.getActableSenders).toHaveBeenCalled();
  }));

  it('should load first page only once', fakeAsync(() => {
    // given
    const page = {content: []} as Page<Sender>;
    senderService.getActableSenders.and.returnValue(of(page));

    // when
    component.onOpen();
    component.onOpen();

    // then
    expect(senderService.getActableSenders).toHaveBeenCalledTimes(1);
  }));

  it('should load more when scrolled near the end of the list', fakeAsync(() => {
    // given
    const content = [] as Sender[];
    while (content.length < 20) {
      content.push({} as Sender);
    }
    const page = {
      number: 0,
      size: 20,
      last: false,
      content: content
    } as Page<Sender>;

    const page2 = {
      number: 1,
      size: 20,
      content: content
    } as Page<Sender>;

    senderService.getActableSenders.and.returnValues(of(page), of(page2));
    const spy = jasmine.createSpy();

    component.senders$.subscribe(spy);

    component.onOpen();
    tick();

    senderService.getActableSenders.calls.reset();

    // when
    component.onScroll({end: 11});
    tick();

    // then
    expect(senderService.getActableSenders).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledTimes(41);
  }));

  it('should not load more on last page', fakeAsync(() => {
    // given
    const content = [] as Sender[];
    const page = {
      number: 0,
      size: 20,
      last: true,
      content: content
    } as Page<Sender>;

    senderService.getActableSenders.and.returnValues(of(page));

    component.onOpen();
    tick();

    senderService.getActableSenders.calls.reset();

    // when
    component.onScroll({end: 11});
    tick();

    // then
    expect(senderService.getActableSenders).toHaveBeenCalledTimes(0);
  }));

  it('should not load more if scroll position is not at the end', fakeAsync(() => {
    // given
    const content = [] as Sender[];
    const page = {
      number: 0,
      size: 20,
      last: true,
      content: content
    } as Page<Sender>;

    senderService.getActableSenders.and.returnValues(of(page));

    component.onOpen();
    tick();

    senderService.getActableSenders.calls.reset();

    // when
    component.onScroll({end: 9});
    tick();

    // then
    expect(senderService.getActableSenders).toHaveBeenCalledTimes(0);
  }));
});
