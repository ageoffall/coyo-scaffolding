import {HttpClient} from '@angular/common/http';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {UIRouterModule} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {of, Subject} from 'rxjs';
import {TimelineItemEditModalComponent} from './timeline-item-edit-modal.component';

describe('TimelineItemEditModalComponent', () => {
  let component: TimelineItemEditModalComponent;
  let fixture: ComponentFixture<TimelineItemEditModalComponent>;
  let timelineItemService: jasmine.SpyObj<TimelineItemService>;
  let backendResponse: Subject<object>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule.forRoot({useHash: true})],
      declarations: [TimelineItemEditModalComponent],
      providers: [{
        provide: TimelineItemService,
        useValue: jasmine.createSpyObj('TimelineItemService', ['getItem', 'put'])
      }, {
        provide: HttpClient,
        useValue: jasmine.createSpyObj('HttpClient', ['post'])
      }, BsModalRef, FormBuilder]
    }).overrideTemplate(TimelineItemEditModalComponent, '')
      .compileComponents();

    timelineItemService = TestBed.get(TimelineItemService);
    backendResponse = new Subject<object>();
  }));

  beforeEach(() => {
    timelineItemService.getItem.and.returnValue(backendResponse.asObservable());
    timelineItemService.put.and.returnValue(of({}));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemEditModalComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    backendResponse.complete();
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', fakeAsync(() => {
    // given
    component.itemId = 'item-id';
    const response = {
      id: 'item-id',
      data: {message: 'example message.'},
      _permissions: {
        edit: true
      }
    } as any as TimelineItem;

    // when
    fixture.detectChanges();
    backendResponse.next(response);
    tick();

    // then
    component.showMessageHintSubject
      .subscribe(showHint => expect(showHint).toBeFalsy())
      .unsubscribe();
    expect(timelineItemService.getItem).toHaveBeenCalledWith('item-id', 'personal', '', ['edit']);
    expect(component.formGroup.contains('message')).toBeTruthy();
    expect(component.formGroup.get('message').value).toEqual(response.data['message']);
    expect(component.onClose).toBeDefined();
  }));

  it('should init with liked or commented item', fakeAsync(() => {
    // given
    component.itemId = 'item-id';
    const response = {
      id: 'item-id',
      data: {message: 'example message.'},
      _permissions: {
        edit: false // someone liked or commented on the timeline item
      }
    } as any as TimelineItem;

    // when
    fixture.detectChanges();
    backendResponse.next(response);
    tick();

    // then
    component.showMessageHintSubject
      .subscribe(showHint => expect(showHint).toBeTruthy())
      .unsubscribe();
    expect(timelineItemService.getItem).toHaveBeenCalledWith('item-id', 'personal', '', ['edit']);
    expect(component.formGroup.contains('message')).toBeFalsy(); // yes falsy, because of the disabled state
    expect(component.formGroup.get('message').value).toEqual(response.data['message']);
    expect(component.onClose).toBeDefined();
  }));

  it('should send the right data on submit', fakeAsync(() => {
    // given
    const newMessageText = 'example message. - edited';
    component.itemId = 'item-id';
    const response = {
      id: 'item-id',
      data: {message: 'example message.'},
      _permissions: {
        edit: true
      }
    } as any as TimelineItem;

    // when
    fixture.detectChanges();
    backendResponse.next(response);
    tick();
    component.formGroup.controls['message'].setValue(newMessageText);
    component.onSubmit();

    // then
    const args = timelineItemService.put.calls.mostRecent().args;
    expect(args[0]).toEqual('item-id');
    expect(args[1]).toEqual({
      data: {
        message: newMessageText,
        edited: true
      }
    });
  }));
});
