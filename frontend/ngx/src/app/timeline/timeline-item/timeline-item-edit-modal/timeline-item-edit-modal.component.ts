import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Modal} from '@domain/modal/modal';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemRequest} from '@domain/timeline-item/timeline-item-request';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {CoyoValidators} from '@shared/forms/validators/validators';
import {UIRouter} from '@uirouter/angular';
import * as _ from 'lodash';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {BehaviorSubject} from 'rxjs';

/**
 * The timeline item edit form modal component.
 */
@Component({
  templateUrl: './timeline-item-edit-modal.component.html',
  styleUrls: ['./timeline-item-edit-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemEditModalComponent extends Modal<any> implements OnInit, OnDestroy {

  /**
   * The timeline item id.
   */
  @Input() itemId: string;

  showMessageHintSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  sendFormData: boolean = false;
  formGroup: FormGroup;

  constructor(modal: BsModalRef,
              uiRouter: UIRouter,
              private formBuilder: FormBuilder,
              private timelineItemService: TimelineItemService) {
    super(modal, uiRouter);
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      message: ['', CoyoValidators.notBlank]
    });
    this.timelineItemService.getItem(this.itemId, 'personal', '', ['edit'])
      .subscribe((timelineItem: TimelineItem) => {
        this.formGroup.controls['message'].setValue(_.get(timelineItem, 'data.message', ''));
        this.changeFormEditState(_.get(timelineItem, '_permissions.edit', false));
      });
  }

  ngOnDestroy(): void {
    this.close();
    super.ngOnDestroy();
  }

  /**
   * Sends form data to backend on form submit.
   */
  onSubmit(): void {
    this.sendFormData = true;
    const body = {
      data: {
        message: this.formGroup.get('message').value,
        edited: true
      }
    } as TimelineItemRequest;
    this.timelineItemService.put(this.itemId, body, {handleErrors: false})
      .subscribe(() => {
        this.close(true);
        this.sendFormData = false;
      }, () => {
        this.changeFormEditState(false);
        this.sendFormData = false;
      });
  }

  private changeFormEditState(canEditItem: boolean): void {
    this.showMessageHintSubject.next(!canEditItem);
    if (!canEditItem) {
      this.formGroup.setErrors({itemUsed: true});
      this.formGroup.controls['message'].disable();
    }
  }
}
