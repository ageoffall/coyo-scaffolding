import {HttpClient} from '@angular/common/http';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {UIRouterModule} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {TimelineItemReportModalComponent} from './timeline-item-report-modal.component';

describe('TimelineItemReportModalComponent', () => {
  let component: TimelineItemReportModalComponent;
  let fixture: ComponentFixture<TimelineItemReportModalComponent>;
  let httpClient: jasmine.SpyObj<HttpClient>;
  let backendResponse: Subject<object>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule.forRoot({useHash: true})],
      declarations: [TimelineItemReportModalComponent],
      providers: [{
        provide: HttpClient,
        useValue: jasmine.createSpyObj('HttpClient', ['post'])
      }, BsModalRef, FormBuilder]
    }).overrideTemplate(TimelineItemReportModalComponent, '')
      .compileComponents();

    httpClient = TestBed.get(HttpClient);
    backendResponse = new Subject<object>();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemReportModalComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    httpClient.post.and.returnValue(backendResponse.asObservable());
  });

  afterEach(() => {
    backendResponse.complete();
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given
    component.targetId = 'target-id';
    component.targetType = 'target-type';

    // when
    fixture.detectChanges();

    // then
    expect(component.reportForm.contains('targetId')).toBeTruthy();
    expect(component.reportForm.get('targetId').value).toEqual(component.targetId);
    expect(component.reportForm.contains('targetType')).toBeTruthy();
    expect(component.reportForm.get('targetType').value).toEqual(component.targetType);
    expect(component.reportForm.contains('message')).toBeTruthy();
    expect(component.reportForm.get('message').value).toEqual('');
    expect(component.reportForm.contains('anonymous')).toBeTruthy();
    expect(component.reportForm.get('anonymous').value).toBeFalsy();
    expect(component.onClose).toBeDefined();
  });

  it('should submit form data', fakeAsync(() => {
    // given
    component.targetId = 'target-id';
    component.targetType = 'target-type';
    fixture.detectChanges();
    component.reportForm.get('message').setValue('Report form message');
    component.reportForm.get('anonymous').setValue(true);
    const expectedFormOutput = {
      targetId: 'target-id',
      targetType: 'target-type',
      message: 'Report form message',
      anonymous: true
    };
    spyOn(component, 'close');

    // when
    component.onSubmit();
    backendResponse.next({});
    tick();

    // then
    expect(httpClient.post).toHaveBeenCalledWith('/web/reports', expectedFormOutput);
    expect(component.close).toHaveBeenCalled();
  }));
});
