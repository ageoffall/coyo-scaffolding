import {HttpClient} from '@angular/common/http';
import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Modal} from '@domain/modal/modal';
import {CoyoValidators} from '@shared/forms/validators/validators';
import {UIRouter} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap/modal';

interface ReportData {
  targetId: string;
  targetType: string;
  message: string;
  anonymous: boolean;
}

/**
 * The report modal with the report form.
 */
@Component({
  templateUrl: './timeline-item-report-modal.component.html',
  styleUrls: ['./timeline-item-report-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemReportModalComponent extends Modal<any> implements OnInit, OnDestroy {

  /**
   * The target id.
   */
  @Input() targetId: string;

  /**
   * The entity type name of the target.
   */
  @Input() targetType: string;

  reportForm: FormGroup;

  sendFormData: boolean = false;

  constructor(modal: BsModalRef,
              uiRouter: UIRouter,
              private formBuilder: FormBuilder,
              private http: HttpClient) {
    super(modal, uiRouter);
  }

  ngOnInit(): void {
    this.reportForm = this.formBuilder.group({
      targetId: [this.targetId, Validators.required],
      targetType: [this.targetType, Validators.required],
      message: ['', CoyoValidators.notBlank],
      anonymous: [false]
    });
  }

  ngOnDestroy(): void {
    this.close();
    super.ngOnDestroy();
  }

  /**
   * Returns the form data on form submit.
   */
  onSubmit(): void {
    this.sendFormData = true;
    this.http.post<ReportData>('/web/reports', this.reportForm.getRawValue())
      .subscribe(() => {
        this.close(true);
        this.sendFormData = false;
      }, () => {
        this.sendFormData = false;
      });
  }
}
