import {TimelineItem} from '@domain/timeline-item/timeline-item';

/**
 * Websocket message that is sent when a timeline item has been updated.
 */
export interface TimelineItemUpdatedMessage {
  uid: string;
  route: string;
  event: string;
  content: TimelineItem;
}
