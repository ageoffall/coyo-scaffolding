import {ChangeDetectionStrategy, Component, Inject, Input} from '@angular/core';
import {environment} from '@root/environments/environment';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {TimelineItemReportModalComponent} from '../timeline-item-report-modal/timeline-item-report-modal.component';

/**
 * The report component.
 */
@Component({
  selector: 'coyo-timeline-item-report',
  templateUrl: './timeline-item-report.component.html',
  styleUrls: ['./timeline-item-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemReportComponent {

  /**
   * The target id.
   */
  @Input() targetId: string;

  /**
   * The entity type name of the target.
   */
  @Input() targetType: string;

  constructor(private modalService: BsModalService,
              @Inject(NG1_COYO_NOTIFICATION_SERVICE) private notificationService: Ng1CoyoNotification) {
  }

  /**
   * Opens the report form.
   */
  openReportForm(): void {
    const reportModal = this.modalService.show(TimelineItemReportModalComponent, {
      animated: environment.enableAnimations,
      backdrop: 'static',
      initialState: {
        targetId: this.targetId,
        targetType: this.targetType
      }
    });
    reportModal.content.onClose.subscribe((success: boolean) => {
      if (success) {
        this.notificationService.success('MODULE.REPORT.SUCCESS');
      }
    });
  }
}
