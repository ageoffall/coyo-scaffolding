import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {TimelineItemReportModalComponent} from '../timeline-item-report-modal/timeline-item-report-modal.component';
import {TimelineItemReportComponent} from './timeline-item-report.component';

describe('TimelineItemReportComponent', () => {
  let component: TimelineItemReportComponent;
  let fixture: ComponentFixture<TimelineItemReportComponent>;
  let modalService: jasmine.SpyObj<BsModalService>;
  let notificationService: jasmine.SpyObj<Ng1CoyoNotification>;
  let onCloseSubject: Subject<boolean>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemReportComponent],
      providers: [{
        provide: BsModalService,
        useValue: jasmine.createSpyObj('BsModalService', ['show'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success'])
      }]
    }).overrideTemplate(TimelineItemReportComponent, '')
      .compileComponents();

    modalService = TestBed.get(BsModalService);
    notificationService = TestBed.get(NG1_COYO_NOTIFICATION_SERVICE);
    onCloseSubject = new Subject<boolean>();
  }));

  beforeEach(() => {
    modalService.show.and.returnValue({
      content: {onClose: onCloseSubject.asObservable()},
      hide: () => {}
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    onCloseSubject.complete();
  });

  it('should create', () => {
    // given

    // when

    // then
    expect(component).toBeTruthy();
  });

  it('should open modal', () => {
    // given
    component.targetId = 'target-id';
    component.targetType = 'target-type';

    // when
    component.openReportForm();

    // then
    expect(modalService.show).toHaveBeenCalledWith(TimelineItemReportModalComponent, {
      animated: false,
      backdrop: 'static',
      initialState: {
        targetId: component.targetId,
        targetType: component.targetType
      }
    });
  });

  it('should open modal and process result', () => {
    // given

    // when
    component.openReportForm();
    onCloseSubject.next(true);

    // then
    expect(notificationService.success).toHaveBeenCalled();
  });
});
