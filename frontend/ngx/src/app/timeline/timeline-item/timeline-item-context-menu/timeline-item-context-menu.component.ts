import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {WINDOW} from '@root/injection-tokens';
import {Ng1AuthService, Ng1CoyoNotification} from '@root/typings';
import {NG1_AUTH_SERVICE, NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';

/**
 * Context menu of timeline items.
 */
@Component({
  selector: 'coyo-timeline-item-context-menu',
  templateUrl: './timeline-item-context-menu.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemContextMenuComponent implements OnInit {

  /**
   * The timeline item
   */
  @Input() item: TimelineItem;

  /**
   * Emitted when clicked on show author
   */
  @Output() showAuthor: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Emitted when clicked on delete item
   */
  @Output() delete: EventEmitter<void> = new EventEmitter();

  /**
   * Emitted when clicked on remove sticky
   */
  @Output() unsticky: EventEmitter<void> = new EventEmitter();

  currentUserId: string;

  private showOriginalAuthor: boolean = false;

  constructor(@Inject(NG1_AUTH_SERVICE) private authService: Ng1AuthService,
              @Inject(WINDOW) private windowService: Window,
              @Inject(NG1_COYO_NOTIFICATION_SERVICE) private notificationService: Ng1CoyoNotification) { }

  ngOnInit(): void {
    this.currentUserId = this.authService.getCurrentUserId();
  }

  /**
   * Emits the show original author event and toggles the flag
   */
  onClickShowOriginalAuthors(): void {
    this.showOriginalAuthor = !this.showOriginalAuthor;
    this.showAuthor.emit(this.showOriginalAuthor);
  }

  /**
   * Creates a link for the timeline item
   *
   * @return The deep link of the timeline item
   */
  getTimelineItemLink(): string {
    return this.windowService.location.protocol + '//' + this.windowService.location.host + '/timeline/item/' + this.item.id;
  }

  /**
   * Creates a success toast after the link of the timeline item is added to clipboard
   */
  copyLinkSuccessNotification(): void {
    this.notificationService.success('MODULE.TIMELINE.COPY_LINK.SUCCESS');
  }
}
