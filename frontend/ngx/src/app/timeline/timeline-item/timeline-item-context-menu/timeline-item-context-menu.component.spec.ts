import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {WINDOW} from '@root/injection-tokens';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_AUTH_SERVICE, NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {TimelineItemContextMenuComponent} from './timeline-item-context-menu.component';

describe('TimelineItemContextMenuComponent', () => {
  let component: TimelineItemContextMenuComponent;
  let fixture: ComponentFixture<TimelineItemContextMenuComponent>;
  let notificationService: jasmine.SpyObj<Ng1CoyoNotification>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemContextMenuComponent],
      providers: [{
        provide: WINDOW,
        useValue: {
          location: {
            protocol: 'http:',
            host: 'test.host'
          }
        }
      }, {
        provide: NG1_AUTH_SERVICE,
        useValue: jasmine.createSpyObj('authService', ['getCurrentUserId'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success'])
      }]
    }).overrideTemplate(TimelineItemContextMenuComponent, '')
      .compileComponents();

    notificationService = TestBed.get(NG1_COYO_NOTIFICATION_SERVICE);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the show original author event', fakeAsync(() => {
    // when
    component.showAuthor.subscribe((showAuthor: boolean) => {
      expect(showAuthor).toBeTruthy();
    });

    component.onClickShowOriginalAuthors();

    // then --> observable
    tick();
  }));

  it('should generate the deep link', () => {
    // given
    component.item = {id: 'testId'} as TimelineItem;

    // when
    const result = component.getTimelineItemLink();

    // then
    expect(result).toBe('http://test.host/timeline/item/testId');
  });

  it('should notify the user about clipboard changes', () => {
    // when
    component.copyLinkSuccessNotification();

    // then
    expect(notificationService.success).toHaveBeenCalledWith('MODULE.TIMELINE.COPY_LINK.SUCCESS');
  });
});
