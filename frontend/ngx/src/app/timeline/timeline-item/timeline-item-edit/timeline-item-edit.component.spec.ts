import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {TimelineItemEditModalComponent} from '../timeline-item-edit-modal/timeline-item-edit-modal.component';
import {TimelineItemEditComponent} from './timeline-item-edit.component';

describe('TimelineItemEditComponent', () => {
  let component: TimelineItemEditComponent;
  let fixture: ComponentFixture<TimelineItemEditComponent>;
  let modalService: jasmine.SpyObj<BsModalService>;
  let notificationService: jasmine.SpyObj<Ng1CoyoNotification>;
  let onCloseSubject: Subject<boolean>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimelineItemEditComponent],
      providers: [{
        provide: BsModalService,
        useValue: jasmine.createSpyObj('BsModalService', ['show'])
      }, {
        provide: NG1_COYO_NOTIFICATION_SERVICE,
        useValue: jasmine.createSpyObj('notificationService', ['success'])
      }]
    }).overrideTemplate(TimelineItemEditComponent, '')
      .compileComponents();

    modalService = TestBed.get(BsModalService);
    notificationService = TestBed.get(NG1_COYO_NOTIFICATION_SERVICE);
    onCloseSubject = new Subject<boolean>();
  }));

  beforeEach(() => {
    modalService.show.and.returnValue({
      content: {
        onClose: onCloseSubject.asObservable()
      },
      hide: () => {
      }
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    onCloseSubject.complete();
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should open modal', () => {
    // given
    component.targetId = 'target-id';
    const expectedState = {
      itemId: component.targetId
    };

    // when
    component.openEditModal();

    // then
    expect(modalService.show).toHaveBeenCalledWith(TimelineItemEditModalComponent, {
      animated: false,
      backdrop: 'static',
      initialState: expectedState
    });
  });

  it('should open modal and process result', () => {
    // given

    // when
    component.openEditModal();
    onCloseSubject.next(true);

    // then
    expect(notificationService.success).toHaveBeenCalled();
  });
});
