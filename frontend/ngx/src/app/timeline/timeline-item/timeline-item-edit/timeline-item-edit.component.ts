import {ChangeDetectionStrategy, Component, Inject, Input} from '@angular/core';
import {environment} from '@root/environments/environment';
import {Ng1CoyoNotification} from '@root/typings';
import {NG1_COYO_NOTIFICATION_SERVICE} from '@upgrade/upgrade.module';
import {BsModalService} from 'ngx-bootstrap/modal';
import {TimelineItemEditModalComponent} from '../timeline-item-edit-modal/timeline-item-edit-modal.component';

/**
 * The edit timeline item component.
 */
@Component({
  selector: '<coyo-timeline-item-edit',
  templateUrl: './timeline-item-edit.component.html',
  styleUrls: ['./timeline-item-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineItemEditComponent {

  /**
   * The timeline item id.
   */
  @Input() targetId: string;

  constructor(private modalService: BsModalService,
              @Inject(NG1_COYO_NOTIFICATION_SERVICE) private notificationService: Ng1CoyoNotification) {
  }

  /**
   * Opens the timeline item edit modal.
   */
  openEditModal(): void {
    const modal = this.modalService.show(TimelineItemEditModalComponent, {
      animated: environment.enableAnimations,
      backdrop: 'static',
      initialState: {
        itemId: this.targetId
      }
    });
    modal.content.onClose.subscribe((success: boolean) => {
      if (success) {
        this.notificationService.success('MODULE.TIMELINE.EDIT.MODAL.SUCCESS');
      }
    });
  }
}
