import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SearchResultsExternalPanelComponent} from './search-results-external-panel.component';

describe('SearchResultsExternalPanelComponent', () => {
  let component: SearchResultsExternalPanelComponent;
  let fixture: ComponentFixture<SearchResultsExternalPanelComponent>;
  const searchQuery = 'some-search-query';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchResultsExternalPanelComponent]
    })
      .overrideTemplate(SearchResultsExternalPanelComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultsExternalPanelComponent);
    component = fixture.componentInstance;
    component.searchQuery = searchQuery;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct Google Drive search url', () => {
    // when
    const result = component.getExternalSearchURL('GOOGLE');

    // then
    expect(result).toBe('https://drive.google.com/drive/search?q=' + searchQuery);
  });

  it('should return empty external search url for unknown search providers', () => {
    // when
    const result = component.getExternalSearchURL('UNKNOWN_SEARCH_PROVIDER');

    // then
    expect(result).toBe('');
  });
});
