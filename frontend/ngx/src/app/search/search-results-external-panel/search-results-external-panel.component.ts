import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {SearchResultsList} from './search-results-list';

/**
 * Displays a list of external search results in a separate panel/card.
 */
@Component({
  selector: 'coyo-search-results-external-panel',
  templateUrl: './search-results-external-panel.component.html',
  styleUrls: ['./search-results-external-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsExternalPanelComponent {

  /**
   * List of search results and the name of the search provider.
   */
  @Input() externalSearchResults: SearchResultsList[];

  /**
   * The search term, the user has entered.
   */
  @Input() searchQuery: string;

  constructor() {
  }

  /**
   * Returns the url which is used for the link to the external search provider.
   *
   * @param searchProvider name of the search provider
   * @return the url of the external search provider
   */
  getExternalSearchURL(searchProvider: string): string {
    switch (searchProvider) {
      case 'GOOGLE': {
        return 'https://drive.google.com/drive/search?q=' + this.searchQuery;
      }
      default:
        return '';
    }
  }
}
