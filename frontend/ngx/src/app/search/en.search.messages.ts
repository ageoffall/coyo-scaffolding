import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'MODULE.SEARCH.EXTERNAL.HEADLINE.RESULTS_ON': ' {count, plural, =1{hit} other{hits}} on ',
    'MODULE.SEARCH.EXTERNAL.HEADLINE.VIEW_ALL_ON.GOOGLE': 'View all in Google Drive',
    'MODULE.SEARCH.EXTERNAL.HEADLINE.VIEW_ALL_ON.GOOGLE.ARIA': 'View all search results in Google Drive',
    'MODULE.SEARCH.EXTERNAL.HEADLINE.WE_ALSO_FOUND': 'We also found',
    'MODULE.SEARCH.EXTERNAL.MODIFIED_DATE.ARIA': 'Last modified ',
    'MODULE.SEARCH.EXTERNAL.PROVIDER.GOOGLE': 'Google Drive'
  }
};
