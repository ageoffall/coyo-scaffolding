import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SearchResultExternalComponent} from '@app/search/search-result-external/search-result-external.component';

getAngularJSGlobal()
  .module('coyo.search')
  .directive('coyoSearchResultExternal', downgradeComponent({
    component: SearchResultExternalComponent
  }));
