import {Messages} from '@core/i18n/messages';
/* tslint:disable no-trailing-whitespace */
export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'MODULE.APPS.HELP.MODAL': `
**You have all the different apps to get your work done in the easiest and most efficient way. For every kind of work or
 project there is the right app.**

## Permissions
To create new apps you first have to ensure that you have the permission to do so. Otherwise an admin has to give you a 
new role in the page or workspace of your choice which entitles you to create a new App.

## Overview
Apps are tools to make your work in a page or workspace as easy as possible. It is so easy it is even fun to add them. 
Simply add the apps you need for a certain project or group work to keep your page and workspace as clear as possible. 
With the apps you are able to share, edit and update documents, integrate wikis and blogs and communicate on the 
progress using a timeline.

## Visibility
All apps you can see, can be edited within your permissions. Additionally, you can deactivate apps as an admin.

*Tip:* If you are starting in COYO, it is possible that some apps may be deactivated. You can check it out in the 
admin's administration. See for more information the 
[Administration](/docs/guide/user/administration_apps_widgets?lang=en).

## Delete App
If you do not longer need an app in a page or workspace and want to delete it, simply choose the app and click edit app.

Now you can delete it by using the button at the bottom of your settings.
    `
  }
};
