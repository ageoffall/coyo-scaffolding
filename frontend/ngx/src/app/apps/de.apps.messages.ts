import {Messages} from '@core/i18n/messages';

/* tslint:disable no-trailing-whitespace */
export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'MODULE.APPS.HELP.MODAL': `
**Es stehen dir eine Reihe von Apps zur Verfügung, die dir deine Arbeit so leicht und effizient wie möglich macht. 
Für jede Art von Arbeit oder Projekt gibt es die passende App.**

### Welche Berechtigungen benötige ich?
Um eine neue App zu erstellen, musst du zunächst sicherstellen, dass du die dafür benötigten Rechte besitzt.
Sollte dies nicht der Fall sein, müsste dir ein Admin zunächst die entsprechende Rolle zuweisen, die es dir ermöglicht 
eine App hinzuzufügen.

### Wozu benötige ich Apps?
Mit Apps hast du die Möglichkeit deine Arbeit zu vereinfachen und Aufgaben schnell und übersichtlich zu verteilen. 
Desweiteren kannst du externe Inhalte integrieren, Dokumente bearbeiten und teilen und einen RSS-Feed integrieren. 
Eine Übersicht aller Apps, inklusive Beschreibung, findest du auf den folgenden Seiten.

### Wie funktioniert das Bearbeiten von Apps?
Alle Apps die du sehen kannst, kannst du auch im Rahmen deiner Berechtigungen bearbeiten. Darüber hinaus kannst du als 
Administrator auch Apps ausblenden.

Tipp: wen du neu mit COYO startest, kann es sein, dass einige Apps noch deaktiviert sind. Über die 
[Administration](/docs/guide/user/administration_apps_widgets?lang=de) kannst du dies ändern!

### Wie kann ich Apps löschen?
Wenn du die App nicht länger für deine Seite oder deinen Workspace benötigst und diese löschen möchtest, wähle die App 
links unter Navigation aus und klicke den \`Einstellung\`-Button neben dem Titel der App. Nun kannst du die App löschen.
    `
  }
};
