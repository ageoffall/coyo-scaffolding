import {Inject, Injectable} from '@angular/core';
import {TranslateLoader} from '@ngx-translate/core';
import {Ng1CoyoTranslationLoader, Ng1TranslationRegistry} from '@root/typings';
import {NG1_COYO_TRANSLATION_LOADER, NG1_TRANSLATION_REGISTRY} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Messages} from '../messages';

/**
 * Service for merging the AngularJS and Angular translations and prepare them for ng-translate use.
 *
 * Attention: Message keys that are used in both frameworks will be overriden by the messages of the AngularJs part.
 */
@Injectable({
  providedIn: 'root'
})
export class TranslationLoaderService implements TranslateLoader {

  private messages: { [key: string]: {} } = {};

  /**
   * Constructs the service
   *
   * Merges all messages defined in the application
   *
   * @param coyoTranslationLoader The AngularJs translation message loader
   * @param messages The messages defined in the Angular part of the application
   * @param translationRegistry The AngularJs translation registry. Used for making the message keys accessible in the
   * AngularJs Context.
   */
  constructor(@Inject(NG1_COYO_TRANSLATION_LOADER) private coyoTranslationLoader: Ng1CoyoTranslationLoader,
              @Inject('messages') messages: Messages[],
              @Inject(NG1_TRANSLATION_REGISTRY) translationRegistry: Ng1TranslationRegistry) {

    messages.forEach(elem => {
      const langMessages = this.messages[elem.lang];
      this.messages[elem.lang] = _.assign(langMessages ? langMessages : {}, elem.messages);

      // register the translations to the translation registry to make them overrideable in admin section
      translationRegistry.addTranslations(elem.lang, elem.messages);
    });
  }

  /**
   * Returns the translations for the given language.
   *
   * @param lang the language key
   * @return the messages
   */
  getTranslation(lang: string): Observable<any> {
    return from(this.coyoTranslationLoader({key: lang})).pipe(map(elem =>
      _.assign({}, this.messages[lang], elem as object)));
  }
}
