/**
 * Entity with id and typename
 */
export interface EntityId {
  id: string;
  typeName: string;
}
