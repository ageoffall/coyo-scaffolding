import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Share} from '@domain/share/share';
import {TimelineItemService} from '@domain/timeline-item/timeline-item.service';
import {User} from '@domain/user/user';
import {NG1_COYO_CONFIG} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {ShareService} from './share.service';

describe('ShareService', () => {
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ShareService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['getBackendUrl', 'join'])
      },
      {
        provide: TimelineItemService,
        useValue: jasmine.createSpyObj('TimelineItemService', ['getShareCount'])
      },
      {provide: NG1_COYO_CONFIG, useValue: {entityTypes: {user: {icon: 'account', color: '#444', label: 'ENTITY_TYPE.USER', plural: 'ENTITY_TYPE.USERS'}}}}]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.getBackendUrl.and.returnValue('backendUrl');
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should use correct url', inject([ShareService], (service: ShareService) => {
    expect(service.getUrl()).toEqual('/web/shares');
  }));

  it('should get shares', inject([ShareService], (service: ShareService) => {
    // given
    const typeName = 'type-name';
    const targetId = 'target-id';
    const expectedShares = [{}, {}] as Share[];

    // when
    const result$ = service.getShares(typeName, targetId);

    // then
    result$.subscribe((shares: Share[]) => expect(shares).toEqual(expectedShares));
    const req = httpTestingController.expectOne(request => request.urlWithParams === '/web/shares/' + typeName + '/' + targetId + '?_permissions=*');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(expectedShares);
  }));

  it('should get original author', inject([ShareService], (service: ShareService) => {
    // given
    const userId = 'user-id';
    const expectedUser = {} as User;

    // when
    const result$ = service.getOriginalAuthor(userId);

    // then
    result$.subscribe((user: User) => expect(user).toEqual(expectedUser));
    const req = httpTestingController.expectOne(request => request.url === '/web/shares/' + userId + '/originalauthor');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(expectedUser);
  }));
});
