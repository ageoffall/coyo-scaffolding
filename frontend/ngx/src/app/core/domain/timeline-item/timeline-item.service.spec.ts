import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {async, discardPeriodicTasks, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {Share} from '@domain/share/share';
import {NewTimelineItemsResponse} from '@domain/timeline-item/new-timeline-items-response';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {TimelineItemTarget} from '@domain/timeline-item/timeline-item-target';
import {NG1_COYO_CONFIG} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {TimelineItemService} from './timeline-item.service';

describe('TimelineItemService', () => {
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;
  let senderService: jasmine.SpyObj<SenderService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TimelineItemService,
        {provide: UrlService, useValue: jasmine.createSpyObj('UrlService', ['join'])},
        {provide: SenderService, useValue: jasmine.createSpyObj('SenderService', ['get'])},
        {provide: NG1_COYO_CONFIG,
          useValue: {
            entityTypes: {
              user: {
                icon: 'account',
                color: '#444',
                label: 'ENTITY_TYPE.USER',
                plural: 'ENTITY_TYPE.USERS'
              }
            }
          }
        }]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
    senderService = TestBed.get(SenderService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: TimelineItemService = TestBed.get(TimelineItemService);
    expect(service).toBeTruthy();
  });

  it('should return the correct URL', inject([TimelineItemService], (service: TimelineItemService) => {
    // when
    const url = service.getUrl();

    // then
    expect(url).toEqual('/web/timeline-items');
  }));

  it('should get a share count', fakeAsync(inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const timelineId = 'timeline-id';
    const expectedCount = 1;

    // when
    const result$ = service.getShareCount(timelineId);

    // then
    result$.subscribe((count: number) => expect(count).toEqual(expectedCount));
    tick(TimelineItemService.THROTTLE);

    const req = httpTestingController
      .expectOne(request => request.urlWithParams === `/web/timeline-items/shares/count?ids=${timelineId}`);
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush({'timeline-id': expectedCount});
    discardPeriodicTasks();
  })));

  it('should get share count for multiple requests', fakeAsync(inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const timelineId1 = 'timeline-id-1';
    const timelineId2 = 'timeline-id-2';
    const expectedCount1 = 1;
    const expectedCount2 = 2;

    // when
    const result1$ = service.getShareCount(timelineId1);
    const result2$ = service.getShareCount(timelineId2);

    // then
    result1$.subscribe((count: number) => expect(count).toEqual(expectedCount1));
    result2$.subscribe((count: number) => expect(count).toEqual(expectedCount2));
    tick(TimelineItemService.THROTTLE);

    const req = httpTestingController
      .expectOne(request => request.urlWithParams === `/web/timeline-items/shares/count?ids=${timelineId1}&ids=${timelineId2}`);
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush({'timeline-id-1': expectedCount1, 'timeline-id-2': expectedCount2});
    discardPeriodicTasks();
  })));

  it('should get Author Icon', inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const sender = {typeName: 'user'} as Sender;
    // when
    const result = service.getAuthorIcon(sender);
    // then
    expect(result).toBe('account');
  }));

  it('should get a timeline item', async(inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const itemId = 'item-id';
    const timelineType = 'personal';
    const senderId = 'sender-id';
    const permissions = ['*'];
    const timelineItem: TimelineItem = {
      id: itemId
    } as TimelineItem;

    // when
    const result$ = service.getItem(itemId, timelineType, senderId, permissions);

    // then
    result$.subscribe(item => expect(item).toEqual(timelineItem));
    const req = httpTestingController
      .expectOne(request =>
        request.urlWithParams === `/web/timeline-items/${itemId}?timelineType=${timelineType.toUpperCase()}&senderId=${senderId}&_permissions=*`);
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(timelineItem);
  })));

  it('should get a relevant share', async(inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const itemId = 'item-id';
    const timelineType = 'PERSONAL';
    const senderId = 'sender-id';
    const relevantShare = {} as Share;
    const itemTarget = {
      id: itemId
    } as TimelineItemTarget;

    // when
    const result$ = service.getRelevantShare(itemTarget, senderId, timelineType);

    // then
    result$.subscribe(item => expect(item).toEqual(relevantShare));
    const req = httpTestingController
      .expectOne(request => request.urlWithParams === `/web/timeline-items/${itemId}/relevant-share?senderId=${senderId}&timelineType=${timelineType}`);
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(relevantShare);
  })));

  it('should mark an item as read', async(inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const itemId = 'item-id';
    const timelineItem = {
      id: itemId
    } as TimelineItem;

    // when
    const result$ = service.markAsRead(timelineItem);

    // then
    result$.subscribe(item => expect(item).toEqual(timelineItem));
    const req = httpTestingController
      .expectOne(request => request.urlWithParams === `/web/timeline-items/${itemId}/read?_permissions=*`);
    expect(req.request.method).toEqual('POST');

    // finally
    req.flush(timelineItem);
  })));

  it('should unsticky an item', async(inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const itemId = 'item-id';
    const timelineItem = {
      id: itemId
    } as TimelineItem;

    // when
    const result$ = service.unsticky(timelineItem);

    // then
    result$.subscribe(item => expect(item).toEqual(timelineItem));
    const req = httpTestingController
      .expectOne(request => request.urlWithParams === `/web/timeline-items/${itemId}/unsticky?_permissions=*`);
    expect(req.request.method).toEqual('POST');

    // finally
    req.flush(timelineItem);
  })));

  it('should get the original author', inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const originalAuthorId = 'original-id';
    const item = {
      originalAuthorId: originalAuthorId
    } as TimelineItem;

    // when
    service.getOriginalAuthor(item);

    // then
    expect(senderService.get).toHaveBeenCalledWith(originalAuthorId);
  }));

  it('should get new items', inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const permissions = ['permission-a', 'permission-b'];
    const senderId = 'sender-id';
    const type = 'personal';
    const resultPage = {page: {}, data: {}} as NewTimelineItemsResponse;

    // when
    const $result = service.getNewItems(senderId, type, permissions);

    // then
    $result.subscribe(result => {
      expect(result).toBe(resultPage);
    });
    const req = httpTestingController
      .expectOne(request =>
        request.urlWithParams === '/web/timeline-items/new?senderId=sender-id&type=personal' +
          '&_page=0&_pageSize=8&_permissions=permission-a,permission-b');
    expect(req.request.method).toEqual('GET');
    req.flush(resultPage);
  }));

  it('should get new item count', inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const type = 'personal';

    // when
    const $result = service.getNewItemCount(type);

    // then
    $result.subscribe(result => {
      expect(result).toBe(3);
    });
    const req = httpTestingController
      .expectOne(request => request.urlWithParams === '/web/timeline-items/new/count?type=personal');
    expect(req.request.method).toEqual('GET');
    req.flush(3);
  }));

  it('should get items by id', inject([TimelineItemService], (service: TimelineItemService) => {
    // given
    const permissions = ['permission-a', 'permission-b'];
    const senderId = 'sender-id';
    const type = 'personal';
    const resultMap = {a: {id: 'a'} as TimelineItem, b: {id: 'b'} as TimelineItem} as {[key: string]: TimelineItem};

    // when
    const $result = service.getItems(['a', 'b'], senderId, type, permissions);

    // then
    $result.subscribe(result => {
      expect(result).toBe(resultMap);
    });
    const req = httpTestingController
      .expectOne(request => request.urlWithParams === '/web/timeline-items?timelineType=PERSONAL' +
        '&senderId=sender-id&ids=a&ids=b&_permissions=permission-a,permission-b');
    expect(req.request.method).toEqual('GET');
    req.flush(resultMap);
  }));
});
