import {async, fakeAsync, TestBed} from '@angular/core/testing';
import {App} from '@domain/apps/app';
import {AppService} from '@domain/apps/app.service';
import {BlogArticleService} from '@domain/blog-article/blog-article.service';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {
  ARTICLE_NOT_TARGET_NOT_FOUND,
  TimelineItemTargetService
} from '@domain/timeline-item/timeline-item-target.service';
import {WikiArticleService} from '@domain/wiki-article/wiki-article.service';
import {of} from 'rxjs';

describe('TimelineItemTargetService', () => {
  let service: TimelineItemTargetService;
  let appService: jasmine.SpyObj<AppService>;
  let blogService: jasmine.SpyObj<BlogArticleService>;
  let wikiService: jasmine.SpyObj<WikiArticleService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimelineItemTargetService, {
        provide: AppService,
        useValue: jasmine.createSpyObj('AppService', ['get'])
      }, {
        provide: BlogArticleService,
        useValue: jasmine.createSpyObj('BlogArticleService', ['get'])
      }, {
        provide: WikiArticleService,
        useValue: jasmine.createSpyObj('WikiArticleService', ['get'])
      }]
    });

    appService = TestBed.get(AppService);
    blogService = TestBed.get(BlogArticleService);
    wikiService = TestBed.get(WikiArticleService);

    service = TestBed.get(TimelineItemTargetService);
  });

  it('should determine default article target with full permissions',
    async(() => {
        // given
        const item: TimelineItem = {
          itemType: 'timeline-item',
          restricted: false,
          _permissions: {
            comment: true,
            like: true
          }
        } as any;

        // when
        const result$ = service.determineTarget(item);

        // then
        result$.subscribe(result => {
          expect(result.socialPermissions).toEqual({
            commentsShown: true,
            commentsAndLikesNotAllowed: false,
            likesShown: true
          });
        });
      }));

  it('should determine default article target with no like permission',
    async(() => {
        // given
        const item: TimelineItem = {
          itemType: 'timeline-item',
          restricted: false,
          _permissions: {
            comment: true,
            like: false
          }
        } as any;

        // when
        const result$ = service.determineTarget(item);

        // then
        result$.subscribe(result => {
          expect(result.socialPermissions).toEqual({
            commentsShown: true,
            commentsAndLikesNotAllowed: false,
            likesShown: false
          });
        });
      }));

  it('should determine default article target with no comment permission',
    async(() => {
        // given
        const item: TimelineItem = {
          itemType: 'timeline-item',
          restricted: false,
          _permissions: {
            comment: false,
            like: true
          }
        } as any;

        // when
        const result$ = service.determineTarget(item);

        // then
        result$.subscribe(result => {
          expect(result.socialPermissions).toEqual({
            commentsShown: false,
            commentsAndLikesNotAllowed: false,
            likesShown: true
          });
        });
      }));

  it('should determine blog article target with full permissions',
    async(() => {
        // given
        const app = {
          id: 'app-id',
          senderId: 'sender-id',
          settings: {
            commentsAllowed: true
          }
        } as App;
        const article = {
          id: 'article-id',
          typeName: 'blog-article',
          app: app
        };
        const item: TimelineItem = {
          itemType: 'blog',
          data: {
            article: article
          }
        } as any;
        appService.get.and.returnValue(of(app));
        blogService.get.and.returnValue(of(article));

        // when
        const result$ = service.determineTarget(item);

        // then
        result$.subscribe(result => {
          expect(result.id).toBe('article-id');
          expect(result.typeName).toBe('blog-article');
          expect(result.senderId).toBe(app.senderId);
          expect(result.socialPermissions).toEqual({
            commentsShown: true,
            commentsAndLikesNotAllowed: false,
            likesShown: true
          });
        });
      }));

  it('should determine wiki article target with full permissions', async(() => {
        // given
        const app = {
          id: 'app-id',
          senderId: 'sender-id',
          settings: {
            commentsAllowed: true
          }
        } as App;
        const article = {
          id: 'article-id',
          typeName: 'wiki-article',
          app: app,
          _permissions: {}
        };
        const item: TimelineItem = {
          itemType: 'wiki',
          data: {
            article: article
          }
        } as any;
        appService.get.and.returnValue(of(app));
        wikiService.get.and.returnValue(of(article));

        // when
        const result$ = service.determineTarget(item);

        // then
        result$.subscribe(result => {
          expect(result.id).toBe('article-id');
          expect(result.typeName).toBe('wiki-article');
          expect(result.socialPermissions).toEqual({
            commentsShown: true,
            commentsAndLikesNotAllowed: false,
            likesShown: true
          });
        });
      }));

  it('should result throw error when blog article app is not given', fakeAsync(() => {
    // when
    const result = service.determineTarget({itemType: 'blog', data: {article: {}}} as TimelineItem);

    // then
    result.subscribe(() => {}, error => {
      expect(error).toBe(ARTICLE_NOT_TARGET_NOT_FOUND);
    });
  }));

  it('should result throw error when wiki article app is not given', fakeAsync(() => {
    // when
    const result = service.determineTarget({itemType: 'wiki', data: {article: {}}} as TimelineItem);

    // then
    result.subscribe(() => {}, error => {
      expect(error).toBe(ARTICLE_NOT_TARGET_NOT_FOUND);
    });
  }));
});
