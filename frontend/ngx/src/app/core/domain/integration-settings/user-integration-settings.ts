/**
 * Class which represents the user integration settings.
 * These settings are represented and managed by the account module -> integration settings.
 */
export interface UserIntegrationSettings {
  syncEvents: boolean;
}
