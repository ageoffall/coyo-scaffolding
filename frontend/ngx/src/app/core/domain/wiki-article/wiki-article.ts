import {App} from '@domain/apps/app';
import {BaseModel} from '@domain/base-model/base-model';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';

/**
 * Interface for a blog article
 */
export interface WikiArticle extends BaseModel {
  author: Sender;
  senderId: string;
  appId: string;
  title: string;
  usedLanguage: string;
  app?: App;
  articleTarget: Target;

  _permissions?: {
    like?: boolean;
    comment?: boolean;
  };

  buildLayoutName(appId: string, language: string): string;
}
