import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {WikiArticleService} from './wiki-article.service';

describe('WikiArticleService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [WikiArticleService, {
      provide: UrlService,
      useValue: jasmine.createSpyObj('urlService', [''])
    }]
  }));

  it('should be created', inject([WikiArticleService], (service: WikiArticleService) => {
    expect(service).toBeTruthy();
  }));
});
