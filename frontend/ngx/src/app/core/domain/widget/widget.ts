import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * Base interface for all widgets.
 */
export interface Widget<Settings extends WidgetSettings> {
  id: string;
  settings: Settings;
}
