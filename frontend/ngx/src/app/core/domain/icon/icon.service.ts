import {Injectable} from '@angular/core';
import {GSUITE, storageType} from '@domain/attachment/storage';
import {File} from '@domain/file/file.ts';
import * as _ from 'lodash';

/**
 * This service provides the classes of supported file icons
 */
@Injectable({
  providedIn: 'root'
})
export class IconService {

  constructor() {
  }

  gSuiteIconMapping: { [key: string]: string } = {
    'application/vnd.google-apps.document': 'zmdi-gsuite zmdi-gsuite-document',
    'application/vnd.google-apps.drawing': 'zmdi-gsuite zmdi-gsuite-drawing',
    'application/vnd.google-apps.folder': 'zmdi-gsuite zmdi-gsuite-folder',
    'application/vnd.google-apps.form': 'zmdi-gsuite zmdi-gsuite-form',
    'application/vnd.google-apps.fusiontable': 'zmdi-gsuite zmdi-gsuite-fusiontable',
    'application/vnd.google-apps.map': 'zmdi-gsuite zmdi-gsuite-map',
    'application/vnd.google-apps.presentation': 'zmdi-gsuite zmdi-gsuite-presentation',
    'application/vnd.google-apps.script': 'zmdi-gsuite zmdi-gsuite-script',
    'application/vnd.google-apps.site': 'zmdi-gsuite zmdi-gsuite-site',
    'application/vnd.google-apps.spreadsheet': 'zmdi-gsuite zmdi-gsuite-spreadsheet',
    'application/vnd.google-apps.video': 'zmdi-gsuite zmdi-gsuite-video',
    'application/pdf': 'zmdi-gsuite zmdi-gsuite-pdf',
    'image/.+': 'zmdi-gsuite zmdi-gsuite-image',
    'video/.+': 'zmdi-gsuite zmdi-gsuite-video'
  };

  msOfficeIconMapping: { [key: string]: string } = {
    'application/msword': 'zmdi-coyo zmdi-coyo-word',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'zmdi-coyo zmdi-coyo-word',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template': 'zmdi-coyo zmdi-coyo-word',
    'application/vnd.ms-word.document.macroEnabled.12': 'zmdi-coyo zmdi-coyo-word',
    'application/vnd.ms-word.template.macroEnabled.12': 'zmdi-coyo zmdi-coyo-word',
    'application/vnd.ms-excel': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.template': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.ms-excel.sheet.macroEnabled.12': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.ms-excel.template.macroEnabled.12': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.ms-excel.addin.macroEnabled.12': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.ms-excel.sheet.binary.macroEnabled.12': 'zmdi-coyo zmdi-coyo-excel',
    'application/vnd.ms-powerpoint': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.template': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.ms-powerpoint.addin.macroEnabled.12': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.ms-powerpoint.template.macroEnabled.12': 'zmdi-coyo zmdi-coyo-powerpoint',
    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12': 'zmdi-coyo zmdi-coyo-powerpoint'
  };

  /**
   * Get icons css classes key for the attached file
   *
   * @param file Attached file
   *
   * @returns a string 'Array' with the icons css classes keys.
   * An Array is being returned here because for files in AppRoot two css classes are required
   */
  getFileIcons(file: File): string[] {
    if (!file) {
      return ['zmdi-coyo zmdi-coyo-file'];
    }

    if (file.folder) {
      return (file.appRoot) ? ['zmdi-folder-outline', 'zmdi-lock'] : ['zmdi-folder-outline'];
    } else if (file.uploadFailed) {
      return ['zmdi-alert-circle-o upload-failed'];
    } else if (!file.contentType) {
      return ['zmdi-coyo zmdi-coyo-file'];
    }

    return this.determineIcon(file.contentType, file.storage);
  }

  /**
   * Get icons css classes key for the mime type
   *
   * @param mimeType mime type
   * @param storage the storage type of the file
   *
   * @returns a string with the icons css classes key.
   */
  getFileIconByMimeType(mimeType: string, storage: storageType): string {
    return this.determineIcon(mimeType, storage)[0];
  }

  private determineExternalIcon(contentType: string, storage: storageType): string {
    if (this.isGsuiteFile(storage)) {
      return this.determineGsuiteIconByMimeType(contentType);
    } else {
      return _.get(this.msOfficeIconMapping, contentType, '');
    }
  }

  private isGsuiteFile(storage: storageType): boolean {
    return storage === GSUITE;
  }

  private determineGsuiteIconByMimeType(mimeType: string): string {
    const determineIcon = _.find(
      this.gSuiteIconMapping, (iconClasses, mimeTypeRegEx) => !!mimeType.match(new RegExp(mimeTypeRegEx)));
    return determineIcon ? determineIcon : 'zmdi-coyo zmdi-coyo-file';
  }

  private determineIcon(contentType: string, storage: storageType): string[] {
    // G Suite or microsoft office files
    const icon = this.determineExternalIcon(contentType, storage);
    if (icon) {
      return [icon];
    }

    // Images
    if (contentType.indexOf('image') >= 0) {
      return ['zmdi-coyo zmdi-coyo-image'];
    }

    // Videos
    if (contentType.indexOf('video') >= 0) {
      return ['zmdi-coyo zmdi-coyo-video'];
    }

    // PDF
    if (contentType.indexOf('pdf') >= 0) {
      return ['zmdi-coyo zmdi-coyo-pdf'];
    }

    // Plain text
    if (contentType.indexOf('plain') >= 0) {
      return ['zmdi-coyo zmdi-coyo-text'];
    }

    // Zip
    if (contentType.indexOf('application') >= 0) {
      if (contentType.indexOf('zip') >= 0) {
        return ['zmdi-coyo zmdi-coyo-zip'];
      }
    }

    // Default
    return ['zmdi-coyo zmdi-coyo-file'];
  }
}
