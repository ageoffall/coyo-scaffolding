import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {WebPreview} from '@domain/preview/web-preview';
import {WebPreviewService} from './web-preview.service';

describe('WebPreviewService', () => {
  let service: WebPreviewService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [WebPreviewService]
  }));

  beforeEach(() => {
    service = TestBed.get(WebPreviewService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should extract urls', () => {
    // when
    const result = service.extractUrls('http://1.de http://2.de. http://3.de/test http:// 4.de');

    // then
    expect(result.length).toBe(3);
    expect(result[0]).toBe('http://1.de');
    expect(result[1]).toBe('http://2.de');
    expect(result[2]).toBe('http://3.de/test');
  });

  it('should get web previews', () => {
    // given
    const urls = ['http://test.de', 'http://test2.de'];
    const result = [{url: 'http://test.de'}, {url: 'http://test2.de'}] as WebPreview[];

    // when
    service.generateWebPreviews(urls).subscribe(webPreviews => {
      expect(webPreviews).toBe(result);
    });

    // then
    const request = httpMock.expectOne(req => req.url === '/web/web-previews' && req.body.urls === urls);
    request.flush(result);
    httpMock.verify();
  });

  it('should get a web preview', () => {
    // given
    const url = 'http://test.de';
    const result = {url: 'http://test.de'} as WebPreview;

    // when then
    service.generateWebPreview(url).subscribe(webPreview => {
      expect(webPreview).toBe(result);
    });
  });
});
