import {Attachment} from '@domain/attachment/attachment';
import {BaseModel} from '@domain/base-model/base-model';
import {Likeable} from '@domain/like/likeable';
import {Sender} from '@domain/sender/sender';

/**
 * Model for comments
 */
export interface Comment extends BaseModel, Likeable {
  _permissions: any;
  author: Sender;
  message: string;
  edited: boolean;
  attachments: Attachment[];
  partiallyLoaded?: boolean;
}
