import {User} from '@domain/user/user';

/**
 * A mentionable item.
 */
export interface MentionItem {
  user: User;
  subline: string;
}
