import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {Sender} from '@domain/sender/sender';
import {NG1_APP_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {SenderService} from './sender.service';

describe('SenderService', () => {
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SenderService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }, {
        provide: NG1_STATE_SERVICE,
        useValue: {}
      }, {
        provide: NG1_APP_SERVICE,
        useValue: jasmine.createSpyObj('appService', ['getCurrentAppIdOrSlug'])
      }]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', inject([SenderService], (service: SenderService) => {
    expect(service).toBeTruthy();
  }));

  it('should return the correct URL', inject([SenderService], (service: SenderService) => {
    // when
    const url = service.getUrl();

    // then
    expect(url).toEqual('/web/senders');
  }));

  it('should get actable senders', inject([SenderService], (service: SenderService) => {
    // given
    const pageable = new Pageable(0, 2);
    const id = 'context-id';
    const type = 'sender';
    const resultPage = {} as Page<Sender>;

    // when
    const observable = service.getActableSenders(pageable, id, type);

    // given
    observable.subscribe(page => {
      expect(page).toBe(resultPage);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/senders/search/actable-senders' &&
      req.params.get('id') === id &&
      req.params.get('type') === type &&
      req.params.get('filters') === 'type=page&type=workspace&type=event'
    );

    request.flush(resultPage);
  }));
});
