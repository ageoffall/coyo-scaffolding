import {Sender} from '@domain/sender/sender';

/**
 * A coyo page entity model.
 */
export interface ShareableSender extends Sender {
  slug: string;
  active: boolean;
  public: boolean;
  title: string;
  description: string;
  subscriptions: number;
}
