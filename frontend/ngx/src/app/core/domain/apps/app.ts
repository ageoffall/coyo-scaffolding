import {Permissions} from '@domain/permissions/permissions';
import {BaseModel} from '../base-model/base-model';

/**
 * Model for an App
 */
export interface App extends BaseModel {
  rootFolderId: string;
  settings: any;
  slug: string;
  key: string;
  senderType: string;
  senderId: string;
  name: string;
  _permissions?: Permissions;
}
