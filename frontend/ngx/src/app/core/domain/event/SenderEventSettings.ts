/**
 * Domain modal for Event settings.
 */
export interface SenderEventSettings {
  participantsLimit: number;
}
