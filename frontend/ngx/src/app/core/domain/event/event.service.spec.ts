import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {SenderEvent} from '@domain/event/SenderEvent';
import * as _ from 'lodash';
import {EventService} from './event.service';

describe('EventService', () => {
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;
  let urlService: jasmine.SpyObj<UrlService>;
  let eventService: jasmine.SpyObj<EventService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EventService, {
        provide: UrlService, useValue: jasmine.createSpyObj('urlService', ['join'])
      }]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
    urlService = TestBed.get(UrlService);
    eventService = TestBed.get(EventService);

    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(eventService).toBeTruthy();
  });

  it('should call get event', () => {
    // given
    const eventId = 'event-id';
    const event = {
      id: eventId
    } as SenderEvent;

    // when
    const result$ = eventService.getEvent(eventId);

    // then
    result$.subscribe(result => {
      expect(result).toBe(event);
    });
    const request = httpTestingController.expectOne(req => req.url === `/web/events/${eventId}`);
    expect(request.request.method).toBe('GET');

    // finally
    request.flush(event);
  });

  it('should call update event', () => {
    // given
    const eventId = 'event-id';
    const event = {
      id: eventId,
      description: 'event-description'
    } as SenderEvent;

    // when
    const result$ = eventService.updateEvent(event);

    // then
    result$.subscribe(result => {
      expect(result).toBe(event);
    });
    const request = httpTestingController.expectOne(req => req.url === `/web/events/${eventId}`);
    expect(request.request.method).toBe('PUT');

    // finally
    request.flush(event);
  });

  it('should call delete event', () => {
    // given
    const eventId = 'event-id';

    // when
    const result$ = eventService.deleteEvent(eventId);

    // then
    result$.subscribe(result => {
      expect(result).toBe(null);
    });
    const request = httpTestingController.expectOne(req => req.url === `/web/events/${eventId}`);
    expect(request.request.method).toBe('DELETE');

    // finally
    request.flush(null);
  });

});
