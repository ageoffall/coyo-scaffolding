import {Sender} from '../sender/sender';

/**
 * User entity model
 */
export interface User extends Sender {
  moderatorMode: boolean;
  language: string;
  globalPermissions?: [string];
}
