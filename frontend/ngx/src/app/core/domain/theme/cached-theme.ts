/**
 * This theme representation is stored in the localStorage
 */
export interface CachedTheme {
  version: string;
  modified: Date;
  css: string;
  defaultVariables: { [key: string]: string };
  defaultVariablesOnly: boolean;
  variables: { [key: string]: string };
}
