import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {SubscriptionService} from './subscription.service';

getAngularJSGlobal()
  .module('commons.subscriptions')
  .factory('ngxSubscriptionService', downgradeInjectable(SubscriptionService) as any);
