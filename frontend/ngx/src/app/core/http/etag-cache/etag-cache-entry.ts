/**
 * A single entry in an ETag cache.
 */
export interface EtagCacheEntry<T> {
  etag: string;
  status: number;
  body: T | null;
  access: number;
}
