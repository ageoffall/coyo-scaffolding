import {Injectable} from '@angular/core';
import {environment} from '@root/environments/environment';

/**
 * Service for recognizing urls that will be handled by a micro service.
 */
@Injectable({
  providedIn: 'root'
})
export class ServiceRecognitionService {

  /**
   * List of paths that should be excluded from the default service unavailable handling of the error interceptor.
   */
  static readonly services: {[key: string]: string} = {
    translation: '/web/translation/'
  };

  constructor() {
  }

  /**
   * Checks if the given url will be handled by a micro service.
   *
   * @param url the url to be checked
   *
   * @return the target service name of the given url
   */
  getTargetService(url: string): string {
    let targetService: string = null;
    Object.keys(ServiceRecognitionService.services).forEach(key => {
      const value = ServiceRecognitionService.services[key];
      if (!targetService && url.includes(value)) {
        targetService = key;
      }
    });
    return targetService;
  }

  isServiceDisabled(targetService: string): boolean {
    return environment.disabledServices.includes(targetService);
  }
}
