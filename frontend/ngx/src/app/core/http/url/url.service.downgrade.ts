import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {UrlService} from './url.service';

getAngularJSGlobal()
  .module('commons.path')
  .factory('ngxUrlService', downgradeInjectable(UrlService));
