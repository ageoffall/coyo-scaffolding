/**
 * Contains session id and tenant information
 */
export interface SessionInfo {
  sessionId: string;
  tenantId: string;
  tenantUrl: string;
}
