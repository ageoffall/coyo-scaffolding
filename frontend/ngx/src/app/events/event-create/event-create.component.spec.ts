import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UrlService} from '@core/http/url/url.service';
import {Ng1EventDateSyncService} from '@root/typings';
import {NG1_EVENT_DATE_SYNC_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import {EventCreateComponent} from './event-create.component';

describe('EventCreateComponent', () => {
  let component: EventCreateComponent;
  let fixture: ComponentFixture<EventCreateComponent>;
  let urlService: jasmine.SpyObj<UrlService>;
  let stateService: jasmine.SpyObj<IStateService>;
  let eventDateSyncService: jasmine.SpyObj<Ng1EventDateSyncService>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [EventCreateComponent],
        providers: [{
          provide: UrlService,
          useValue: jasmine.createSpyObj('urlService', ['toUrlParamString'])
        }, {
          provide: NG1_STATE_SERVICE,
          useValue: jasmine.createSpyObj('stateService', ['go'])
        }, {
          provide: NG1_EVENT_DATE_SYNC_SERVICE,
          useValue: jasmine.createSpyObj('eventDateSyncService', [
            'updateStartDate', 'updateEndDate', 'updateStartTime', 'updateEndTime'
          ])
        }, FormBuilder, ChangeDetectorRef]
      })
      .overrideTemplate(EventCreateComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCreateComponent);
    component = fixture.componentInstance;
    urlService = TestBed.get(UrlService);
    stateService = TestBed.get(NG1_STATE_SERVICE);
    eventDateSyncService = TestBed.get(NG1_EVENT_DATE_SYNC_SERVICE);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm instanceof FormGroup).toBeTruthy();
    expect(component.eventCreateForm.get('name').value).toEqual('');
    expect(component.eventCreateForm.get('host').value).toEqual(null);
    expect(component.eventCreateForm.get('location').value).toEqual('');
    expect(component.eventCreateForm.get('description').value).toEqual('');
    expect(component.eventCreateForm.get('fullDay').value).toEqual(false);
    expect(component.eventCreateForm.get('eventStart').value).toBeDefined();
    expect(component.eventCreateForm.get('eventEnd').value).toBeDefined();
    expect(component.eventCreateForm.get('showParticipants').value).toEqual(false);
    expect(component.eventCreateForm.get('requestDefiniteAnswer').value).toEqual(false);
    expect(component.eventCreateForm.get('limitedParticipants').value).toEqual(false);
    expect(component.eventCreateForm.get('participantsLimit').value).toEqual(0);
    expect(component.eventDates.startDate instanceof Date).toBeTruthy();
    expect(component.eventDates.startTime instanceof Date).toBeTruthy();
    expect(component.eventDates.endDate instanceof Date).toBeTruthy();
    expect(component.eventDates.endDate instanceof Date).toBeTruthy();
    expect(component.senderSelectOptions).toEqual({
      filters: urlService.toUrlParamString('type', ['user', 'page', 'workspace']),
      findOnlyManagedSenders: true
    });
  });

  it('should set form to invalid if limitedParticipants is true but limit is zero', () => {
    // given
    fixture.detectChanges();
    component.eventCreateForm.patchValue({
      name: 'Test event',
      host: {host: 'host'},
      eventStart: new Date(),
      eventEnd: new Date(),
      limitedParticipants: true,
      participantsLimit: 0
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm.valid).toBeFalsy();
  });

  it('should set form to valid if limitedParticipants is true and limit is greater than zero', () => {
    // given
    fixture.detectChanges();
    component.eventCreateForm.patchValue({
      name: 'Test event',
      host: {host: 'host'},
      eventStart: new Date(),
      eventEnd: new Date(),
      limitedParticipants: true,
      participantsLimit: 1
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm.valid).toBeTruthy();
  });

  it('should set form to valid if limitedParticipants is false and limit is zero', () => {
    // given
    fixture.detectChanges();
    component.eventCreateForm.patchValue({
      name: 'Test event',
      host: {host: 'host'},
      eventStart: new Date(),
      eventEnd: new Date(),
      limitedParticipants: false,
      participantsLimit: 0
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm.valid).toBeTruthy();
  });

  it('should call updateStartDate', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateStartDate();

    // then
    expect(eventDateSyncService.updateStartDate).toHaveBeenCalled();
  });

  it('should call updateEndDate', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateEndDate();

    // then
    expect(eventDateSyncService.updateEndDate).toHaveBeenCalled();
  });

  it('should call updateStartTime', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateStartTime();

    // then
    expect(eventDateSyncService.updateStartTime).toHaveBeenCalled();
  });

  it('should call updateEndTime', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateEndTime();

    // then
    expect(eventDateSyncService.updateEndTime).toHaveBeenCalled();
  });

  it('should submit the form', () => {
    // given
    const date = new Date();
    const eventData = {
      name: 'name',
      host: 'host',
      location: 'location',
      description: 'description',
      fullDay: 'full-day',
      eventStart: date,
      eventEnd: date,
      showParticipants: true,
      requestDefiniteAnswer: true,
      limitedParticipants: true,
      participantsLimit: 24
    };
    fixture.detectChanges();
    component.eventCreateForm.patchValue(eventData);
    spyOn(component.formResult, 'emit');

    // when
    component.submitForm();

    // then
    expect(component.formResult.emit).toHaveBeenCalledWith(eventData);
  });

  it('should cancel the form with default settings', () => {
    // given
    fixture.detectChanges();

    // when
    component.cancelForm();

    // then
    expect(stateService.go).toHaveBeenCalledWith('main.event', {});
  });

  it('should cancel the form with a previous state', () => {
    // given
    stateService['previous'] = {
      name: 'state-name',
      params: {a: 1}
    };
    fixture.detectChanges();

    // when
    component.cancelForm();

    // then
    expect(stateService.go).toHaveBeenCalledWith('state-name', {a: 1});
  });

  it('should cleanup on destroy', () => {
    // given
    fixture.detectChanges();
    spyOn(component.formResult, 'complete');

    // when
    component.ngOnDestroy();

    // then
    expect(component.formResult.complete).toHaveBeenCalled();
  });

  it('should update event form if description changes', () => {
    // given
    fixture.detectChanges();
    spyOn(component.eventCreateForm, 'patchValue');

    // when
    component.descriptionChanges('new content');

    // then
    expect(component.eventCreateForm.patchValue).toHaveBeenCalledWith({description: 'new content'});
  });

});
