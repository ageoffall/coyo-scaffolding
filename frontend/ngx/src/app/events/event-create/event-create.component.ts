import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {rteOptions} from '@app/events/rte-options';
import {UrlService} from '@core/http/url/url.service';
import {Sender} from '@domain/sender/sender';
import {EventDates, Ng1EventDateSyncService} from '@root/typings';
import {CoyoValidators} from '@shared/forms/validators/validators';
import {SenderParameter} from '@shared/sender-ui/select-sender/sender-parameter';
import {NG1_EVENT_DATE_SYNC_SERVICE, NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';

@Component({
  selector: 'coyo-event-create',
  templateUrl: './event-create.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventCreateComponent implements OnInit, OnDestroy {

  /**
   * A given sender used as initial sender for the form.
   */
  @Input() initialSender: Sender;

  /**
   * The form result of the general settings.
   */
  @Output() formResult: EventEmitter<object> = new EventEmitter<object>();

  eventCreateForm: FormGroup;
  eventDates: EventDates = {
    startDate: null,
    startTime: null,
    endDate: null,
    endTime: null,
  };
  name: FormControl = new FormControl('', [CoyoValidators.notBlank, Validators.maxLength(255)]);
  host: FormControl = new FormControl(null, [Validators.required]);
  eventStart: FormControl = new FormControl(null, [Validators.required]);
  eventEnd: FormControl = new FormControl(null, [Validators.required]);

  readonly senderSelectOptions: SenderParameter = {
    filters: this.urlService.toUrlParamString('type', ['user', 'page', 'workspace']),
    findOnlyManagedSenders: true
  };

  readonly eventDescriptionRteOptions: object = rteOptions;

  private eventEndTouched: boolean = false;

  constructor(@Inject(NG1_STATE_SERVICE) private stateService: IStateService,
              @Inject(NG1_EVENT_DATE_SYNC_SERVICE) private eventDateSyncService: Ng1EventDateSyncService,
              private urlService: UrlService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.initDates();
  }

  ngOnDestroy(): void {
    this.formResult.complete();
  }

  /**
   * Called after rte content changes.
   *
   * @param content The new content of the rte.
   */
  descriptionChanges(content: any): void {
    this.eventCreateForm.patchValue({description: content});
  }

  /**
   * Cancel form and return to origin state.
   */
  cancelForm(): void {
    let state = 'main.event';
    let params = {};
    if (!!this.stateService['previous']) {
      const prevState = this.stateService['previous'];
      state = prevState.name;
      params = prevState.params;
    }
    this.stateService.go(state, params);
  }

  /**
   * Submit form emits the form values.
   */
  submitForm(): void {
    if (this.eventCreateForm.valid) {
      this.formResult.emit(this.eventCreateForm.value);
    }
  }

  /**
   * Sync start and end date.
   */
  updateStartDate(): void {
    this.eventDateSyncService.updateStartDate(this.eventDates, !this.eventEndTouched);
    this.updateEventDates();
  }

  /**
   * Sync start and end date.
   */
  updateEndDate(): void {
    this.eventEndTouched = true;
    this.eventDateSyncService.updateEndDate(this.eventDates);
    this.updateEventDates();
  }

  /**
   * Sync start and end time.
   */
  updateStartTime(): void {
    this.eventDateSyncService.updateStartTime(this.eventDates, !this.eventEndTouched);
    this.updateEventDates();
  }

  /**
   * Sync start and end time.
   */
  updateEndTime(): void {
    this.eventEndTouched = true;
    this.eventDateSyncService.updateEndTime(this.eventDates);
    this.updateEventDates();
  }

  private initDates(): void {
    const currentDate = new Date();

    this.eventDates.startDate = currentDate;
    const startTime = new Date(currentDate);
    startTime.setHours(currentDate.getHours() + 1, 0, 0, 0);
    this.eventDates.startTime = startTime;

    this.eventDates.endDate = currentDate;
    const endTime = new Date(currentDate);
    endTime.setHours(startTime.getHours() + 1, 0, 0, 0);
    this.eventDates.endTime = endTime;

    this.updateEventDates();
  }

  private initForm(): void {
    this.eventCreateForm = this.formBuilder.group({
      name: this.name,
      host: this.host,
      location: [''],
      description: [''],
      fullDay: [false],
      eventStart: this.eventStart,
      eventEnd: this.eventEnd,
      showParticipants: [false],
      requestDefiniteAnswer: [false],
      limitedParticipants: [false],
      participantsLimit: [0]
    });
    this.eventCreateForm.get('limitedParticipants').valueChanges.subscribe(limitedParticipants => {
      if (limitedParticipants) {
        this.eventCreateForm.get('participantsLimit').setValidators([Validators.required, Validators.min(1)]);
      } else {
        this.eventCreateForm.get('participantsLimit').clearValidators();
      }
      this.eventCreateForm.get('participantsLimit').updateValueAndValidity();
    });
  }

  private updateEventDates(): void {
    this.eventCreateForm
      .patchValue({eventStart: this.setTimeForDate(this.eventDates.startDate, this.eventDates.startTime)});
    this.eventCreateForm
      .patchValue({eventEnd: this.setTimeForDate(this.eventDates.endDate, this.eventDates.endTime)});
  }

  private setTimeForDate(dateToUpdate: Date, time: Date): Date {
    const date = new Date(dateToUpdate);
    date.setHours(time.getHours());
    date.setMinutes(time.getMinutes());
    date.setSeconds(0, 0);
    return date;
  }
}
