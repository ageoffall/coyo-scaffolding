import {Directive, ElementRef, Injector, Input} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';

/**
 * This directive adds a tour step to the desktop tour. It is not visible when the user starts the application in a
 * mobile resolution (xs or sm). The tour step is automatically registered and bound to the html element containing
 * the component.
 *
 * IMPORTANT NOTE: Make sure that the parent element of this directive has "position: relative" or add the class
 * "tour-placeholder-container" to it.
 */
@Directive({
  selector: 'coyo-tour-step' // tslint:disable-line directive-selector
})
export class TourStepDirective extends UpgradeComponent {

  /**
   * Every tour step needs to be assigned to a topic, which determines what the tour step is describing. E.g. all tour
   * steps explaining menu items of the main navigation should have the topic "navigation". If one step of the a topic
   * was loaded and the user ends the tour, all steps of that topic are marked as seen.
   */
  @Input() topic: string;

  /**
   * Sets the order in which the step should be shown within the tour. For example a step with the order "1" is
   * displayed before a step with the order "5".
   */
  @Input() order: number;

  /**
   * Identifies a tour step.
   */
  @Input() stepId: string;

  /**
   * A placement for the tour popover can be provided. By default the placement is determined automatically. See the
   * bootstrap-ui documentation for possible values.
   */
  @Input() placement: 'top' | 'bottom' | 'left' | 'right';

  /**
   * The title of the tour step.
   */
  @Input() title: string;

  /**
   * The content of the tour step. This supports displaying HTML.
   */
  @Input() content: string;

  constructor(elementRef: ElementRef, injector: Injector) {
    super('coyoTourStep', elementRef, injector);
  }
}
