import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerSelection} from '@app/filepicker/filepicker-modal/filepicker-selection/filepicker-selection';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {UIRouterModule} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap';
import {of} from 'rxjs';
import {root} from 'rxjs/internal-compatibility';
import {FilepickerModalComponent} from './filepicker-modal.component';
import Spy = jasmine.Spy;

describe('FilepickerModalComponent', () => {
  let component: FilepickerModalComponent;
  let fixture: ComponentFixture<FilepickerModalComponent>;

  function createFolder(name: string, children: FilepickerItem[]): FilepickerItem & Spy {
    const folder = jasmine.createSpyObj(name, ['getChildren']);
    folder.isFolder = true;
    folder.getChildren.and.returnValue(of(children));
    return folder;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule.forRoot({useHash: true})],
      declarations: [FilepickerModalComponent],
      providers: [
        BsModalRef,
        {provide: WindowSizeService, useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'isSm'])}
      ]
    }).overrideTemplate(FilepickerModalComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepickerModalComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch the children of the root folder on init', () => {
    // given
    const rootFolder = createFolder('root', []);
    component.rootFolder = rootFolder;

    // when
    fixture.detectChanges();

    // then
    expect(rootFolder.getChildren).toHaveBeenCalled();
  });

  it('should add root folder to breadcrumb on init', () => {
    // given
    const rootFolder = createFolder('root', []);
    component.rootFolder = rootFolder;

    // when
    fixture.detectChanges();

    // then
    expect(component.breadcrumbItems.length).toBe(1);
    expect(component.breadcrumbItems).toContain(rootFolder);
  });

  it('should add the folder to the breadcrumb while opening a folder', () => {
    // given
    const subFolder = createFolder('folder', []);
    const rootFolder = createFolder('root', [subFolder]);
    component.rootFolder = rootFolder;
    fixture.detectChanges();
    component.breadcrumbItems = [rootFolder];

    // when
    component.openFolder(subFolder);

    // then
    expect(component.breadcrumbItems.length).toBe(2);
    expect(component.breadcrumbItems).toContain(rootFolder);
    expect(component.breadcrumbItems).toContain(subFolder);
  });

  it('should remove subfolders of the breadcrumb while opening a folder', () => {
    // given
    const folder2 = createFolder('folder2', []);
    const folder1 = createFolder('folder1', [folder2]);
    const rootFolder = createFolder('root', [folder1]);
    component.rootFolder = rootFolder;
    fixture.detectChanges();
    component.breadcrumbItems = [rootFolder, folder1, folder2];

    // when
    component.openFolder(folder1);

    // then
    expect(component.breadcrumbItems.length).toBe(2);
    expect(component.breadcrumbItems).toContain(rootFolder);
    expect(component.breadcrumbItems).toContain(folder1);
    expect(component.breadcrumbItems).not.toContain(folder2);
  });

  it('should return selected files on click', () => {
    // given
    const selectedFiles = [{id: 'ITEM-1'}, {id: 'ITEM-2'}] as FilepickerItem[];
    component.fileSelection = {selectedFiles} as FilepickerSelection;

    component.onClose.subscribe(items => {
      expect(items).toBe(selectedFiles);
    });

    // when
    component.onSubmitSelectedFiles();
  });

  it('should close modal with empty state', () => {
    // given
    component.onClose.subscribe(items => {
      expect(items).toBeDefined();
      expect(items.length).toBe(0);
    });

    // when
    component.close();
  });
});
