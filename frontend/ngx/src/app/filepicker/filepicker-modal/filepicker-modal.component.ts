import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {Modal} from '@domain/modal/modal';
import {UIRouter} from '@uirouter/angular';
import {BsModalRef} from 'ngx-bootstrap';
import {Subject} from 'rxjs';
import {FilepickerSelection} from './filepicker-selection/filepicker-selection';

/**
 * This component contains the filepicker inside of a modal
 */
@Component({
  selector: 'coyo-filepicker-modal',
  templateUrl: './filepicker-modal.component.html',
  styleUrls: ['./filepicker-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilepickerModalComponent extends Modal<any> implements OnInit {
  /**
   * The folder that is the entrypoint of the filepicker
   */
  @Input() rootFolder: FilepickerItem;

  breadcrumbItems: FilepickerItem[] = [];
  loading: boolean = true;
  items$: Subject<FilepickerItem[]>;
  fileSelection: FilepickerSelection;

  constructor(private windowSizeService: WindowSizeService,
              modal: BsModalRef,
              uiRouter: UIRouter) {
    super(modal, uiRouter);
  }

  ngOnInit(): void {
    this.items$ = new Subject();
    this.openFolder(this.rootFolder);
    this.fileSelection = new FilepickerSelection(this.items$);
  }

  filePickerItemClicked(item: FilepickerItem, $event?: Event): void {
    item.isFolder ? this.openFolder(item) : this.toggleFileSelection(item, $event);
  }

  /**
   * Load the contents of the given folder item
   * @param item a folder item
   * @param $event row click event
   */
  openFolder(item: FilepickerItem, $event?: Event): void {
    this.loading = true;
    this.items$.next([]);
    this.setCurrentBreadcrumbFolder(item);
    item.getChildren().subscribe({
      next: items => this.items$.next(items),
      complete: () => this.loading = false
    });
  }

  /**
   * Checks for mobile view.
   *
   * @returns true if the window size is typical for mobile.
   */
  isMobile(): boolean {
    return this.windowSizeService.isXs() || this.windowSizeService.isSm();
  }

  private toggleFileSelection(item: FilepickerItem, $event?: Event): void {
    $event.preventDefault();
    this.fileSelection.toggleFileSelection(item);
  }

  private setCurrentBreadcrumbFolder(item: FilepickerItem): void {
    const index = this.breadcrumbItems.indexOf(item);
    if (index >= 0) {
      this.breadcrumbItems.length = index;
    }

    this.breadcrumbItems = [...this.breadcrumbItems, item];
  }

  /**
   * Submits the selected files
   */
  onSubmitSelectedFiles(): void {
    this.close(this.fileSelection.selectedFiles);
  }

  close(closeValue?: any): void {
    super.close(closeValue ? closeValue : []);
  }

  /**
   * Goes back to previous folder.
   */
  goBackToPreviousFolder(): void {
    const latestPath = this.breadcrumbItems[this.breadcrumbItems.length - 2];
    this.openFolder(latestPath);
  }
}
