import {TestBed} from '@angular/core/testing';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerModalComponent} from '@app/filepicker/filepicker-modal/filepicker-modal.component';
import {BsModalService} from 'ngx-bootstrap';
import {Observable, of} from 'rxjs';
import {FilepickerService} from './filepicker.service';

describe('FilepickerService', () => {
  let service: FilepickerService;
  let modalService: jasmine.SpyObj<BsModalService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: BsModalService,
        useValue: jasmine.createSpyObj('BsModalService', ['show'])
      }]
    });

    service = TestBed.get(FilepickerService);
    modalService = TestBed.get(BsModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open modal', () => {
    // given
    const onClose: Observable<FilepickerItem[]> = of();
    const rootFolder = {id: 'ITEM-ID'} as FilepickerItem;
    modalService.show.and.returnValue({content: {onClose}});

    // when
    const returnValue = service.openFilepicker(rootFolder);

    // then
    expect(returnValue).toBe(onClose);
    expect(modalService.show).toHaveBeenCalledWith(FilepickerModalComponent, {initialState: {rootFolder}});
  });
});
