import {Injectable} from '@angular/core';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerModalComponent} from '@app/filepicker/filepicker-modal/filepicker-modal.component';
import {BsModalService} from 'ngx-bootstrap';
import {Observable} from 'rxjs';

/**
 * This service handles the filepicker
 */
@Injectable({
  providedIn: 'root'
})
export class FilepickerService {
  constructor(private modalService: BsModalService) {
  }

  /**
   * Opens a filepicker
   * @param rootFolder The folder that is the entrypoint of the filepicker
   * @return Observable of selected items
   */
  openFilepicker(rootFolder: FilepickerItem): Observable<FilepickerItem[]> {
    return this.modalService.show(FilepickerModalComponent, {
      initialState: {rootFolder},
    }).content.onClose;
  }
}
