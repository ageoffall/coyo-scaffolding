import {Component, ComponentFactoryResolver, ComponentRef, Input, OnChanges, OnInit, SimpleChanges, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetComponent} from '../widget-component';
import {WidgetConfig} from '../widget-config';

/**
 * A component to dynamically render a {@link WidgetComponent}.
 */
@Component({
  selector: 'coyo-widget-container',
  template: '',
  encapsulation: ViewEncapsulation.None
})
export class WidgetContainerComponent implements OnInit, OnChanges {

  private componentRef: ComponentRef<WidgetComponent<Widget<WidgetSettings>>>;

  /**
   * The underlying widget config definition.
   */
  @Input() config: WidgetConfig<Widget<WidgetSettings>>;

  /**
   * The actual widget data.
   */
  @Input() widget: any;

  /**
   * The state of the widget edit mode.
   */
  @Input() editMode: boolean;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.config.component);
    this.componentRef = this.viewContainerRef.createComponent(componentFactory);
    this.updateComponentRefValues();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.componentRef) {
      this.updateComponentRefValues();
      this.componentRef.instance.ngOnChanges(changes);
      this.componentRef.instance.detectChanges();
    }
  }

  private updateComponentRefValues(): void {
    this.componentRef.instance.config = this.config;
    this.componentRef.instance.widget = this.widget;
    this.componentRef.instance.editMode = this.editMode;
  }
}
