import {InjectionToken, Type} from '@angular/core';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetComponent} from './widget-component';
import {WidgetInlineSettingsComponent} from './widget-inline-settings-component';
import {WidgetSettingsComponent} from './widget-settings-component';

/**
 * The Widget configuration injection token.
 */
export const WIDGET_CONFIGS: InjectionToken<WidgetConfig<Widget<WidgetSettings>>[]> =
  new InjectionToken<WidgetConfig<Widget<WidgetSettings>>[]>('WIDGET_CONFIGS');

/**
 * The configuration of a widget
 */
export class WidgetConfig<WidgetType extends Widget<WidgetSettings>> {

  /**
   * The registration key of the widget.
   */
  key: string;

  /**
   * The component definition of the widget.
   */
  component: Type<WidgetComponent<WidgetType>>;

  /**
   * The Material Design Iconic Font class (zmdi) for the widget.
   */
  icon: string;

  /**
   * The i18n name key of the widget.
   */
  name: string;

  /**
   * The i18n description key of the widget.
   */
  description: string;

  /**
   * The category of the widget (static, dynamic or personal)
   */
  categories: WidgetCategory;

  /**
   * Options for the inline settings.
   */
  inlineOptions?: {

    /**
     * The component to be rendered as inline settings.
     */
    component: Type<WidgetInlineSettingsComponent<WidgetType>>;
  };

  /**
   * Options for the settings.
   */
  settings?: {

    /**
     * Flag indicating if the settings dialog should be shown on widget creation.
     */
    skipOnCreate: boolean;

    /**
     * The component to be rendered as settings.
     */
    component: Type<WidgetSettingsComponent<WidgetType>>;
  };

  /**
   * A list of titles that can be changed inline.
   */
  titles?: string[];

  /**
   * The AngularJS legacy flag. This is set by the AngularJS widget registration.
   */
  legacy?: boolean = false;

  /**
   * Additional rendering options.
   */
  renderOptions?: {

    /**
     * Flag indicating if this widget is printable.
     */
    printable: boolean;

    /**
     * Render options for widgets in "panels" mode.
     */
    panels?: {

      /**
       * Flag indicating if this widget is rendered within a panel.
       */
      noPanel: boolean;
    },

    /**
     * Render options for widgets in "panel" mode.
     */
    panel?: {

      /**
       * Flag indicating if this widget is rendered within a panel.
       */
      noPanel: boolean;
    }
  };

  /**
   * Flag indicating whether the widget can be seen by an external workspace member
   */
  whitelistExternal?: boolean = false;
}

/**
 * The set of widget categories.
 */
export enum WidgetCategory {
  STATIC = 'static',
  DYNAMIC = 'dynamic',
  PERSONAL = 'personal'
}
