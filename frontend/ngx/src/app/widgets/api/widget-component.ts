import {ChangeDetectorRef, Inject, Input, OnChanges, SimpleChanges, ViewRef} from '@angular/core';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetConfig} from './widget-config';

/**
 * A component that renders a specific widget.
 */
export abstract class WidgetComponent<WidgetType extends Widget<WidgetSettings>> implements OnChanges {

  /**
   * The underlying widget config definition.
   */
  @Input() config: WidgetConfig<WidgetType>;

  /**
   * The actual widget data.
   */
  @Input() widget: WidgetType;

  /**
   * The state of the widget edit mode.
   */
  @Input() editMode: boolean;

  constructor(@Inject(ChangeDetectorRef) private cd: ChangeDetectorRef) {
  }

  /**
   * Call the change detector to detect changes.
   */
  detectChanges(): void {
    if (this.cd && !(this.cd as ViewRef).destroyed) {
      this.cd.detectChanges();
    }
  }

  // Despite of "doing nothing" this function is necessary to make change detection work on the widgets that extend WidgetComponent.
  // With this we should be able to use OnPush change detection on widgets.
  ngOnChanges(changes: SimpleChanges): void {
    // default that does nothing
  }
}
