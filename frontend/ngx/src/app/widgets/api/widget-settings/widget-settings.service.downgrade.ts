import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetSettingsService} from './widget-settings.service';

getAngularJSGlobal()
  .module('coyo.widgets.api')
  .factory('ngxWidgetSettingsService', downgradeInjectable(WidgetSettingsService) as any);
