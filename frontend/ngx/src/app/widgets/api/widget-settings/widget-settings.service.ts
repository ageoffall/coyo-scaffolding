import {Injectable} from '@angular/core';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetSettingsUpdatedEvent} from '@widgets/api/widget-settings/widget-settings-updated-event';
import {Observable, Subject} from 'rxjs';
import {filter, map} from 'rxjs/operators';

/**
 * Service that has the responsibility to propagate changes of widget settings.
 */
@Injectable({
  providedIn: 'root'
})
export class WidgetSettingsService {

  private changedSubject: Subject<WidgetSettingsUpdatedEvent> = new Subject();

  constructor() {
  }

  /**
   * Gets an observable that emit when the settings of a widget are updated
   * @returns an observable with all changes of widget settings
   */
  getAllChanges$(): Observable<WidgetSettingsUpdatedEvent> {
    return this.changedSubject.asObservable();
  }

  /**
   * Gets an observable that emits when the settings of the widget are updated.
   * @param widgetId the widget widgetId or tempId if not already saved.
   * @returns an observable that emits when the settings of the widget are updated.
   */
  getChanges$(widgetId: string): Observable<WidgetSettings> {
    return this.changedSubject.asObservable().pipe(
        filter(widget => widget.widgetId === widgetId || widget.tempId === widgetId),
        map(value => value.settings));
  }

  /**
   * Propagate changes of the widget settings to interested observers.
   * @param widget the widget
   * @param settings the widget settings
   */
  propagateChanges(widget: any, settings: WidgetSettings): void {
    this.changedSubject.next({
      widgetId: widget.id,
      tempId: widget.tempId,
      settings: settings
    });
  }
}
