import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {Widget} from '@domain/widget/widget';
import {NG1_WIDGET_REGISTRY} from '@upgrade/upgrade.module';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {Ng1WidgetRegistry} from 'typings';
import {WidgetComponent} from '../widget-component';
import {WIDGET_CONFIGS, WidgetCategory, WidgetConfig} from '../widget-config';
import {WidgetRegistryService} from './widget-registry.service';

class DummyWidgetComponent extends WidgetComponent<Widget<WidgetSettings>> {
}

describe('WidgetRegistryService', () => {
  const ng1Configs: any[] = [{
    key: 'dummy-ng1',
    name: 'AngularJS Widget',
    description: 'An AngularJS widget.',
    icon: 'zmdi-globe',
    categories: 'dynamic',
    directive: 'coyo-dummy-widget'
  }];
  const ngxConfigs: WidgetConfig<Widget<WidgetSettings>>[] = [{
    key: 'dummy-ngx',
    name: 'Angular Widget',
    description: 'An Angular widget.',
    icon: 'zmdi-globe',
    component: DummyWidgetComponent,
    categories: WidgetCategory.STATIC
  }];

  let httpTestingController: HttpTestingController;
  let widgetRegistry: jasmine.SpyObj<Ng1WidgetRegistry>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WidgetRegistryService, {
        provide: NG1_WIDGET_REGISTRY,
        useValue: jasmine.createSpyObj('widgetRegistry', ['getAll'])
      }, {
        provide: WIDGET_CONFIGS,
        useValue: ngxConfigs
      }]
    });

    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    widgetRegistry = TestBed.get(NG1_WIDGET_REGISTRY);
  });

  beforeEach(() => {
    widgetRegistry.getAll.and.returnValue(ng1Configs);
  });

  it('should get all widget configs', inject([WidgetRegistryService], (service: WidgetRegistryService) => {
    // when
    const result = service.getAll();

    // then
    expect(result.length).toEqual(2);
    expect(result).toContain(jasmine.objectContaining({key: 'dummy-ng1'}));
    expect(result).toContain(jasmine.objectContaining({key: 'dummy-ngx'}));
  }));

  it('should get widget config by key', inject([WidgetRegistryService], (service: WidgetRegistryService) => {
    // when
    const result1 = service.get('dummy');
    const result2 = service.get('dummy-ng1');

    // then
    expect(result1).toBeUndefined();
    expect(result2).toEqual(jasmine.objectContaining({key: 'dummy-ng1'}));
  }));

  it('should only get enabled configs', inject([WidgetRegistryService], (service: WidgetRegistryService) => {
    // when
    const response = [{
      key: 'dummy-ng1',
      enabled: true,
      moderatorsOnly: true
    }, {
      key: 'dummy-ngx',
      enabled: true,
      moderatorsOnly: false
    }];
    const result$ = service.getEnabled();

    // then
    result$.subscribe(result => {
      expect(result.length).toEqual(1);
      expect(result).toContain(jasmine.objectContaining({key: 'dummy-ngx'}));
    });

    const req = httpTestingController.expectOne(request => request.url === '/web/widget-configurations');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  }));

  it('should include restricted configs', inject([WidgetRegistryService], (service: WidgetRegistryService) => {
    // when
    const response = [{
      key: 'dummy-ng1',
      enabled: true,
      moderatorsOnly: true
    }, {
      key: 'dummy-ngx',
      enabled: true,
      moderatorsOnly: false
    }];
    const result$ = service.getEnabled(true);

    // then
    result$.subscribe(result => {
      expect(result.length).toEqual(2);
      expect(result).toContain(jasmine.objectContaining({key: 'dummy-ng1'}));
      expect(result).toContain(jasmine.objectContaining({key: 'dummy-ngx'}));
    });
    const req = httpTestingController.expectOne(request => request.url === '/web/widget-configurations');
    expect(req.request.method).toEqual('GET');

    // finally
    req.flush(response);
  }));
});
