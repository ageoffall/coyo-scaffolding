import {ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {Subject} from 'rxjs';
import {WidgetSettingsComponent} from '../widget-settings-component';
import {WidgetSettingsContainerComponent} from './widget-settings-container.component';

class DummyWidgetSettingsComponent extends WidgetSettingsComponent<Widget<WidgetSettings>> {
}

describe('WidgetSettingsContainerComponent', () => {
  let component: WidgetSettingsContainerComponent;
  let fixture: ComponentFixture<WidgetSettingsContainerComponent>;
  let componentFactoryResolver: jasmine.SpyObj<ComponentFactoryResolver>;
  let viewContainerRef: jasmine.SpyObj<ViewContainerRef>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetSettingsContainerComponent],
      providers: [{
        provide: ComponentFactoryResolver,
        useValue: jasmine.createSpyObj('ComponentFactoryResolver', ['resolveComponentFactory'])
      }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSettingsContainerComponent);
    component = fixture.componentInstance;
    componentFactoryResolver = fixture.debugElement.injector.get(ComponentFactoryResolver); // tslint:disable-line:deprecation
    viewContainerRef = fixture.debugElement.injector.get(ViewContainerRef); // tslint:disable-line:deprecation
  });

  it('should dynamically create a component', () => {
    spyOn(viewContainerRef, 'createComponent');

    // given
    const componentFactory = {};
    componentFactoryResolver.resolveComponentFactory.and.returnValue(componentFactory);
    const componentRef = {instance: {}};
    viewContainerRef.createComponent.and.returnValue(componentRef);

    component.component = DummyWidgetSettingsComponent;
    component.widget = {};
    component.parentForm = new FormGroup({});
    component.onSubmit = new Subject<any>();
    fixture.detectChanges();

    // when
    component.ngOnInit();

    // then
    expect(componentFactoryResolver.resolveComponentFactory).toHaveBeenCalledWith(component.component);
    expect(viewContainerRef.createComponent).toHaveBeenCalledWith(componentFactory);
    expect(component['componentRef'].instance.widget).toEqual(component.widget);
    expect(component['componentRef'].instance.parentForm).toEqual(component.parentForm);
    expect(component['componentRef'].instance.onSubmit).toEqual(component.onSubmit);
  });
});
