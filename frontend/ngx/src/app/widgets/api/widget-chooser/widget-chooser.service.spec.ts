import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {BsModalService} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {WidgetChooserComponent} from './widget-chooser.component';
import {WidgetChooserService} from './widget-chooser.service';

describe('WidgetChooserService', () => {
  let modalService: jasmine.SpyObj<BsModalService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetChooserService, {
        provide: BsModalService,
        useValue: jasmine.createSpyObj('BsModalService', ['show'])
      }]
    });

    modalService = TestBed.get(BsModalService);
  });

  it('should open a modal', fakeAsync(inject([WidgetChooserService], (service: WidgetChooserService) => {
    // given
    const subject = new Subject<any>();
    modalService.show.and.returnValue({
      content: {onClose: subject}
    });

    // when
    let args: any;
    service.open().then(result => {
      args = result;
    });
    subject.next('result');
    tick();

    // then
    expect(args).toEqual('result');
    expect(modalService.show).toHaveBeenCalledWith(WidgetChooserComponent, {
      animated: false,
      class: 'modal-lg',
      initialState: {widget: {isNew: jasmine.any(Function), settings: {}}}
    });
    expect(modalService.show.calls.mostRecent().args[1].initialState.widget.isNew()).toBeTruthy();
  })));
});
