import {Component, Inject, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Widget} from '@domain/widget/widget';
import {Ng1ScrollBehaviourService} from '@root/typings';
import {UIRouter} from '@uirouter/angular';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {Subject} from 'rxjs';
import {WidgetConfig} from '../widget-config';
import {WidgetModal} from '../widget-modal';

/**
 * A modal dialog to configure widgets.
 *
 * This component should not be used directly to show a widget configuration dialog. The
 * {@link WidgetSettingsModalService#open} provides the canonical way to configure widgets.
 */
@Component({
  selector: 'coyo-widget-settings-modal',
  templateUrl: './widget-settings-modal.component.html',
  styleUrls: ['./widget-settings-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WidgetSettingsModalComponent extends WidgetModal implements OnInit, OnDestroy {

  /**
   * The actual widget being configured.
   */
  @Input() widget: any;

  /**
   * The onClose callback.
   */
  onClose: Subject<{ config: WidgetConfig<Widget<WidgetSettings>>; settings: any; }>;

  constructor(modal: BsModalRef, uiRouter: UIRouter,
    @Inject(NG1_SCROLL_BEHAVIOR_SERVICE) private scrollBehaviourService: Ng1ScrollBehaviourService) {
    super(modal, uiRouter);
  }

  ngOnInit(): void {
    this.scrollBehaviourService.disableBodyScrolling();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.scrollBehaviourService.enableBodyScrolling();
  }
}
