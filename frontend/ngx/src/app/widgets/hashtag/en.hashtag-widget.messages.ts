import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.HASHTAG.DESCRIPTION': 'Shows a list of the most popular hashtags of the last days, weeks or months.',
    'WIDGET.HASHTAG.EMPTY': 'There are no hashtags, yet.',
    'WIDGET.HASHTAG.NAME': 'Trending hashtags',
    'WIDGET.HASHTAG.TITLE': 'Trending hashtags',
    'WIDGET.HASHTAG.SETTINGS.PERIOD': 'Time Period',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.ONE_DAY': '1 day',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.ONE_MONTH': '1 month',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.ONE_WEEK': '1 week',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.ONE_YEAR': '12 months',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.SIX_MONTHS': '6 months',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.THREE_MONTHS': '3 months',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.TWO_MONTHS': '2 months',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.TWO_WEEKS': '2 weeks',
    'WIDGET.HASHTAG.SETTINGS.PERIOD.UNLIMITED': 'Ever'
  }
};
