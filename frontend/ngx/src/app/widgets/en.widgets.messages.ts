import {Messages} from '@core/i18n/messages';

/* tslint:disable no-trailing-whitespace */
export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGETS.WIDGET_CHOOSER.EMPTY': 'There are no widgets available.',
    'WIDGETS.WIDGET_CHOOSER.LOADING': 'Widgets are currently being loaded...',
    'WIDGETS.WIDGET_CHOOSER.TITLE': 'Choose Widget',
    'WIDGETS.WIDGET_CHOOSER.SEARCH': 'Search…',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.ALL': 'All',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.DYNAMIC': 'Dynamic',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.STATIC': 'Static',
    'WIDGETS.WIDGET_CHOOSER.CATEGORY.PERSONAL': 'Personal',
    'WIDGETS.HELP.MODAL': `
**Widgets are the most important content management function in COYO. You use them in order to manage the Apps you use 
in a wider mode.**

### Apps AND Widgets?
The difference between an App and a Widget is that Widgets are an extension of the App. By using a Widget you can add 
additional functions within Apps and in doing so bring more flexibility and creativity to your COYO.

### Permissions
In order to add widgets, you have to make sure that you have the required permissions. Should this not be the case, you 
have to ask an admin to assign you the respective role that will allow you to add Widgets to an app first.

**Tip**: When getting started with COYO, some Widgets might be deactivated. You can change this via the 
[Administration](/docs/guide/user/administration_apps_widgets?lang=en)!

### Types of Widgets
Widgets generally distinct between static, dynamic and personal content.
You can use them in different areas of COYO, for example on homepages, pages, or in workspaces.

#### Static Content
Using static content Widgets you can create customized fixed content.

#### Dynamic Content
Additionally, you can create widgets which gather information from different sources. Because of them being able to 
change and/or coming from different sources, they are dynamic.

#### Personal Content
Additionally, you can add personalized content for users.

### Widget API
COYO has a simple Front-End Widget API, which allows you and your partners to create new Widgets for special 
requirements easily.

For more information, please check our [Developer Guide](/docs/guide/dev/widget-architecture).
    `
  }
};
