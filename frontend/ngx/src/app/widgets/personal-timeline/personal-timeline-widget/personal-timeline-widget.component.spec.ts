import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {User} from '@domain/user/user';
import {of} from 'rxjs';
import {PersonalTimelineWidgetComponent} from './personal-timeline-widget.component';

describe('PersonalTimelineWidgetComponent', () => {
  let component: PersonalTimelineWidgetComponent;
  let fixture: ComponentFixture<PersonalTimelineWidgetComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  const user = {
    id: 'user-id'
  } as User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonalTimelineWidgetComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getUser'])
      }]
    }).overrideTemplate(PersonalTimelineWidgetComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    authService.getUser.and.returnValue(of(user));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalTimelineWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set current user on init', () => {
    expect(component).toBeTruthy();
    component.currentUser$.subscribe(u => expect(u).toBe(user));
  });
});
