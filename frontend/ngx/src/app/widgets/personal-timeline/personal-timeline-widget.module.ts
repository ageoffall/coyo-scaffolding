import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TimelineModule} from '@app/timeline/timeline.module';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {messagesDe} from './de.personal-timeline-widget.messages';
import {messagesEn} from './en.personal-timeline-widget.messages';
import {PERSONAL_TIMELINE_WIDGET} from './personal-timeline-widget-config';
import {PersonalTimelineWidgetComponent} from './personal-timeline-widget/personal-timeline-widget.component';

/**
 * Personal timeline widget module
 */
@NgModule({
  imports: [
    CommonModule,
    CoyoCommonsModule,
    TimelineModule
  ],
  declarations: [
    PersonalTimelineWidgetComponent
  ],
  entryComponents: [
    PersonalTimelineWidgetComponent
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: PERSONAL_TIMELINE_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ]
})
export class PersonalTimelineWidgetModule {}
