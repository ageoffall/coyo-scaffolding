import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.DIVIDER.DESCRIPTION': 'Displays a simple divider line.',
    'WIDGET.DIVIDER.HELP.TEXT': 'Please enter the divider\'s title or leave blank to omit the title.',
    'WIDGET.DIVIDER.NAME': 'Divider',
    'WIDGET.DIVIDER.SETTINGS.TEXT': 'Title'
  }
};
