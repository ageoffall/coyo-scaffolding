import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.SUBSCRIPTIONS.DESCRIPTION': 'Zeigt die persönlichen Abonnements des aktuellen Benutzers an.',
    'WIDGET.SUBSCRIPTIONS.EMPTY.PAGE': 'Du hast keine Seiten abonniert.',
    'WIDGET.SUBSCRIPTIONS.EMPTY.WORKSPACE': 'Du hast keine Workspaces abonniert.',
    'WIDGET.SUBSCRIPTIONS.FAVORITE.SET': 'Zu Favoriten hinzufügen',
    'WIDGET.SUBSCRIPTIONS.FAVORITE.UNSET': 'Aus Favoriten entfernen',
    'WIDGET.SUBSCRIPTIONS.MORE.PAGE': 'Mehr anzeigen',
    'WIDGET.SUBSCRIPTIONS.MORE.PAGE.ARIA': 'Mehr Seiten anzeigen',
    'WIDGET.SUBSCRIPTIONS.MORE.WORKSPACE': 'Mehr anzeigen',
    'WIDGET.SUBSCRIPTIONS.MORE.WORKSPACE.ARIA': 'Mehr Workspaces anzeigen',
    'WIDGET.SUBSCRIPTIONS.NAME': 'Abonnements',
    'WIDGET.SUBSCRIPTIONS.PAGE': 'Seiten',
    'WIDGET.SUBSCRIPTIONS.PAGE.ARIA': 'Abonnierte Seiten',
    'WIDGET.SUBSCRIPTIONS.WORKSPACE': 'Workspaces',
    'WIDGET.SUBSCRIPTIONS.WORKSPACE.ARIA': 'Abonnierte Workspaces'
  }
};
