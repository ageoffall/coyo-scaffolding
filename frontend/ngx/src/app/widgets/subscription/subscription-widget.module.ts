import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {SUBSCRIPTION_WIDGET} from '@widgets/subscription/subscription-widget-config';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {messagesDe} from './de.subscription-widget.messages';
import {messagesEn} from './en.subscription-widget.messages';
import {SubscriptionWidgetSkeletonComponent} from './subscription-widget-skeleton/subscription-widget-skeleton.component';
import {SubscriptionWidgetComponent} from './subscription-widget/subscription-widget.component';

/**
 * Module providing the subscription widget.
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    TooltipModule
  ],
  declarations: [
    SubscriptionWidgetComponent,
    SubscriptionWidgetSkeletonComponent
  ],
  entryComponents: [
    SubscriptionWidgetComponent
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: SUBSCRIPTION_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ],
})
export class SubscriptionWidgetModule {}
