import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {
  BlogArticleWidget,
  BlogArticleWidgetSettings,
  SingleBlogArticle
} from '@widgets/blog-article/blog-article-widget';
import {BlogArticleWidgetService} from '@widgets/blog-article/blog-article-widget.service';
import {of} from 'rxjs';

import {BlogArticleWidgetComponent} from './blog-article-widget.component';

class TestTarget implements Target {
  name: string;
  params: { [p: string]: string };
}

describe('BlogArticleWidgetComponent', () => {
  let component: BlogArticleWidgetComponent;
  let fixture: ComponentFixture<BlogArticleWidgetComponent>;
  const blogArticleWidgetService = jasmine.createSpyObj('BlogArticleWidgetService', ['getArticleById']);
  const widgetSettingsService = jasmine.createSpyObj('WidgetSettingsService', ['getChanges$']);
  const cd = jasmine.createSpyObj('ChangeDetectorRef', ['detectChanges']);
  const targetService = jasmine.createSpyObj('TargetService', ['getLinkTo']);
  const singleBlogArticle: SingleBlogArticle = new SingleBlogArticle();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: BlogArticleWidgetService,
        useValue: blogArticleWidgetService
      }, {
        provide: WidgetSettingsService,
        useValue: widgetSettingsService
      }, {
        provide: TargetService,
        useValue: targetService
      }, {
        provide: ChangeDetectorRef,
        useValue: cd
      }],
      declarations: [BlogArticleWidgetComponent]
    }).overrideTemplate(BlogArticleWidgetComponent, '')
        .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogArticleWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _articleId: 'any-article-id'
      },
      article: undefined
    } as BlogArticleWidget;
    singleBlogArticle.id = 'any-article-id';
    singleBlogArticle.title = 'any-article-title';
    singleBlogArticle.teaserText = 'any-article-teaser-text';
    component.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve article when setting _articleId is given', fakeAsync(() => {
    // given
    blogArticleWidgetService.getArticleById.and.returnValue(of(singleBlogArticle));

    // when
    component.ngOnInit();
    tick();

    // then
    expect(blogArticleWidgetService.getArticleById).toHaveBeenCalledWith(singleBlogArticle.id);
    expect(component.widget.article).toEqual(singleBlogArticle);
    expect(component.isLoading()).toBeFalsy();
  }));

  it('should update article data when a already saved widget were edited', fakeAsync(() => {
    // given
    const newSingleBlogArticle = new SingleBlogArticle();
    newSingleBlogArticle.id = 'any-other-article-id';
    newSingleBlogArticle.title = 'any-other-article-title';
    newSingleBlogArticle.teaserText = 'any-other-article-teaser-text';
    blogArticleWidgetService.getArticleById.and.returnValue(of(newSingleBlogArticle));
    component.widget = {
      id: 'widget-id',
      settings: {
        _articleId: 'any-other-article-id'
      },
      article: singleBlogArticle
    } as BlogArticleWidget;
    const widgetSettings: BlogArticleWidgetSettings = {
      _articleId: 'any-other-article-id'
    };
    widgetSettingsService.getChanges$.and.returnValue(of(widgetSettings));

    // when
    component.ngAfterContentInit(); // as replacement of a real change action of the widget settings
    tick();

    // then
    expect(widgetSettingsService.getChanges$).toHaveBeenCalledWith('widget-id');
    expect(blogArticleWidgetService.getArticleById).toHaveBeenCalledWith(newSingleBlogArticle.id);
    expect(component.widget.article).toEqual(newSingleBlogArticle);
    expect(component.isLoading()).toBeFalsy();
  }));

  it('should update article data when a currently not saved widget (just ' +
      'created before) were edited again (by using the widget tempId)', fakeAsync(() => {
    // given
    const newSingleBlogArticle = new SingleBlogArticle();
    newSingleBlogArticle.id = 'any-other-article-id';
    newSingleBlogArticle.title = 'any-other-article-title';
    newSingleBlogArticle.teaserText = 'any-other-article-teaser-text';
    blogArticleWidgetService.getArticleById.and.returnValue(of(newSingleBlogArticle));
    component.widget = {
      id: undefined,
      tempId: 'widget-tempId',
      settings: {
        _articleId: 'any-other-article-id'
      },
      article: singleBlogArticle
    } as BlogArticleWidget;
    const widgetSettings: BlogArticleWidgetSettings = {
      _articleId: 'any-other-article-id'
    };
    widgetSettingsService.getChanges$.and.returnValue(of(widgetSettings));

    // when
    component.ngAfterContentInit(); // as replacement of a real change action of the widget system
    tick();

    // then
    expect(widgetSettingsService.getChanges$).toHaveBeenCalledWith('widget-tempId');
    expect(blogArticleWidgetService.getArticleById).toHaveBeenCalledWith(newSingleBlogArticle.id);
    expect(component.widget.article).toEqual(newSingleBlogArticle);
    expect(component.isLoading()).toBeFalsy();
  }));

  it('should retrieve link article by calling getLinktTo with target object', () => {
    // given
    blogArticleWidgetService.getArticleById.and.returnValue(of(singleBlogArticle));
    const target: Target = new TestTarget();
    targetService.getLinkTo.and.returnValue('targetLink');

    // when
    component.getLink(target);

    // then
    expect(targetService.getLinkTo).toHaveBeenCalledWith(target);
  });
});
