import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGETS.BLOGARTICLE.DESCRIPTION': 'Zeigt einen einzelnen Blogbeitrag an.',
    'WIDGETS.BLOGARTICLE.IN': 'in',
    'WIDGETS.BLOGARTICLE.NAME': 'Einzelner Blogbeitrag',
    'WIDGETS.BLOGARTICLE.NO_ARTICLES': 'Keine Beiträge gefunden.',
    'WIDGETS.BLOGARTICLE.NOTHING.SELECTED': 'Bitte Blogbeitrag auswählen.',
    'WIDGETS.BLOGARTICLE.READMORE': 'Weiterlesen',
    'WIDGETS.BLOGARTICLE.SETTINGS.BLOG_ARTICLE': 'Blogbeitrag',
    'WIDGETS.BLOGARTICLE.SETTINGS.BLOG_ARTICLE.HELP': 'Blogbeitrag auswählen.',
    'WIDGETS.BLOGARTICLE.SETTINGS.PERMISSION_ERROR': 'Du verfügst nicht über ausreichende Berechtigungen.'
  }
};
