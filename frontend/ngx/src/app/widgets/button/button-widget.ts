import {Widget} from '@domain/widget/widget';
import {ButtonWidgetSettings} from '@widgets/button/button-widget-settings.model';

export interface ButtonWidget extends Widget<ButtonWidgetSettings> {
}
