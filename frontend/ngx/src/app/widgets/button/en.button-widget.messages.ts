import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.BUTTON.NAME': 'Link Button',
    'WIDGET.BUTTON.DESCRIPTION': 'Displays a single Button which redirects to an url with the styling of your choice.',
    'WIDGET.BUTTON.SETTINGS.TEXT': 'Title',
    'WIDGET.BUTTON.SETTINGS.TARGET': 'Open the link in a new browser tab',
    'WIDGET.BUTTON.SETTINGS.STYLE': 'Style',
    'WIDGET.BUTTON.SETTINGS.STYLE.DEFAULT': 'Default',
    'WIDGET.BUTTON.SETTINGS.STYLE.PRIMARY': 'Primary',
    'WIDGET.BUTTON.SETTINGS.STYLE.SUCCESS': 'Success',
    'WIDGET.BUTTON.SETTINGS.STYLE.INFO': 'Info',
    'WIDGET.BUTTON.SETTINGS.STYLE.WARNING': 'Warning',
    'WIDGET.BUTTON.SETTINGS.STYLE.DANGER': 'Danger',
    'WIDGET.BUTTON.SETTINGS.URL': 'URL',
    'WIDGET.BUTTON.SETTINGS.URL.PLACEHOLDER': 'https://'
  }
};
