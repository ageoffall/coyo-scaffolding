import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {WIDGET_CONFIGS} from '../api/widget-config';
import {BUTTON_WIDGET} from './button-widget-config';
import {ButtonWidgetSettingsComponent} from './button-widget-settings/button-widget-settings.component';
import {ButtonWidgetComponent} from './button-widget/button-widget.component';
import {messagesDe} from './de.button-widget.messages';
import {messagesEn} from './en.button-widget.messages';

/**
 * Module providing the button widget
 */
@NgModule({
  imports: [
    ButtonsModule,
    CoyoCommonsModule,
    CoyoFormsModule
  ],
  declarations: [
    ButtonWidgetComponent,
    ButtonWidgetSettingsComponent
  ],
  entryComponents: [
    ButtonWidgetComponent,
    ButtonWidgetSettingsComponent
  ],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: BUTTON_WIDGET, multi: true},
    {provide: 'messages', useValue: messagesDe, multi: true},
    {provide: 'messages', useValue: messagesEn, multi: true}
  ],
})
export class ButtonWidgetModule {}
