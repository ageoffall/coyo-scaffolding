import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.IMAGE.DESCRIPTION': 'Displays a single image.',
    'WIDGET.IMAGE.NAME': 'Image'
  }
};
