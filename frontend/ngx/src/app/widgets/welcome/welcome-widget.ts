import {Widget} from '@domain/widget/widget';
import {WelcomeWidgetSettings} from '@widgets/welcome/welcome-widget-settings.model';

export interface WelcomeWidget extends Widget<WelcomeWidgetSettings> {
}
