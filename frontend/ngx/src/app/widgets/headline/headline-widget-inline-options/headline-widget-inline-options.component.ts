import {ChangeDetectionStrategy, Component} from '@angular/core';
import {WidgetInlineSettingsComponent} from '@widgets/api/widget-inline-settings-component';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {HeadlineWidget} from '@widgets/headline/headline-widget';
import * as _ from 'lodash';
import {SizeOption} from '../size-option';
import sizeOptions from '../size-options';

/**
 * Inline settings component for the headline widget
 */
@Component({
  selector: 'coyo-headline-widget-inline-options',
  templateUrl: './headline-widget-inline-options.component.html',
  styleUrls: ['./headline-widget-inline-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeadlineWidgetInlineOptionsComponent extends WidgetInlineSettingsComponent<HeadlineWidget> {

  options: SizeOption[] = sizeOptions;

  constructor(private widgetSettingsService: WidgetSettingsService) {
    super();
  }

  /**
   * Select a size option
   * @param option size option
   */
  select(option: SizeOption): void {
    _.set(this.widget, 'settings._headline', option);
    this.widgetSettingsService.propagateChanges(this.widget, {_headline: option});
  }
}
