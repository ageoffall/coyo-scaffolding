import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {HeadlineWidget} from '@widgets/headline/headline-widget';
import sizeOptions from '@widgets/headline/size-options';
import {HeadlineWidgetInlineOptionsComponent} from './headline-widget-inline-options.component';

describe('HeadlineWidgetInlineOptionsComponent', () => {
  let component: HeadlineWidgetInlineOptionsComponent;
  let fixture: ComponentFixture<HeadlineWidgetInlineOptionsComponent>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeadlineWidgetInlineOptionsComponent],
      providers: [{
        provide: WidgetSettingsService,
        useValue: jasmine.createSpyObj('widgetSettingsService', ['propagateChanges'])
      }]
    }).compileComponents();

    widgetSettingsService = TestBed.get(WidgetSettingsService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadlineWidgetInlineOptionsComponent);
    component = fixture.componentInstance;
    component.widget = {id: 'widget-id'} as HeadlineWidget;
    fixture.detectChanges();
  });

  it('should select a size option', () => {
    // when
    component.select(sizeOptions[0]);

    // then
    expect(widgetSettingsService.propagateChanges).toHaveBeenCalledWith(component.widget, {_headline: sizeOptions[0]});
  });
});
