import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.HEADLINE.DESCRIPTION': 'Zeigt eine Überschrift in anpassbarer Größe an.',
    'WIDGET.HEADLINE.NAME': 'Überschrift',
    'WIDGET.HEADLINE.PLACEHOLDER': 'Überschrift...'
  }
};
