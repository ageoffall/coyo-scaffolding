import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'WIDGET.CALLOUT.DESCRIPTION': 'Displays a callout box.',
    'WIDGET.CALLOUT.NAME': 'Callout',
    'WIDGET.CALLOUT.SETTINGS.TEXT': 'Text',
    'WIDGET.CALLOUT.SETTINGS.STYLE': 'Style',
    'WIDGET.CALLOUT.SETTINGS.STYLE.SUCCESS': 'Success',
    'WIDGET.CALLOUT.SETTINGS.STYLE.INFO': 'Info',
    'WIDGET.CALLOUT.SETTINGS.STYLE.WARNING': 'Warning',
    'WIDGET.CALLOUT.SETTINGS.STYLE.DANGER': 'Danger'
  }
};
