import {Widget} from '@domain/widget/widget';
import {BookmarkingWidgetSettings} from '@widgets/bookmarking/bookmarking-widget-settings.model';

export interface BookmarkingWidget extends Widget<BookmarkingWidgetSettings> {
}
