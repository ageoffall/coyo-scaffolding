import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {TextWidgetComponent} from './text-widget.component';

describe('TextWidgetComponent', () => {
  let component: TextWidgetComponent;
  let fixture: ComponentFixture<TextWidgetComponent>;
  let textarea: jasmine.SpyObj<HTMLTextAreaElement>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextWidgetComponent]
    }).overrideTemplate(TextWidgetComponent, '')
      .compileComponents();

    textarea = jasmine.createSpyObj('textarea', ['focus']);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextWidgetComponent);
    component = fixture.componentInstance;
    component.editContainer = {
      nativeElement: {
        firstElementChild: textarea
      } as any as HTMLDivElement
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be show the text on blur', () => {
    // when
    component.onBlur();

    // then
    expect(component.showInput).toBeFalsy();
  });

  it('should be show the text on click', fakeAsync(() => {
    // when
    component.onClick();

    // then
    expect(component.showInput).toBeTruthy();
    tick();
    expect(textarea.focus).toHaveBeenCalled();
  }));
});
