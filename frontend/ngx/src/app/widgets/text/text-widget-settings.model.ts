/**
 * The entity model for the settings of a text widget
 */
export interface TextWidgetSettings {
  md_text: string;
}
