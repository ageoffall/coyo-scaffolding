import {Messages} from '@core/i18n/messages';

export const messagesDe: Messages = {
  lang: 'de',
  messages: {
    'WIDGET.USER_ONLINE.DESCRIPTION': 'Zeigt die Anzahl aller Benutzer an, die gerade online sind.',
    'WIDGET.USER_ONLINE.NAME': 'Benutzer Online',
    'WIDGET.USER_ONLINE.USERS': 'Benutzer',
    'WIDGET.USER_ONLINE.ONLINE': 'Online'
  }
};
