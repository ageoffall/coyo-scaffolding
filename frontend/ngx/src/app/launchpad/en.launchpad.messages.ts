import {Messages} from '@core/i18n/messages';

export const messagesEn: Messages = {
  lang: 'en',
  messages: {
    'LAUNCHPAD.HEADLINE': 'Launchpad',
    'LAUNCHPAD.CATEGORY.ADD_LINK': 'Add link',
    'LAUNCHPAD.CATEGORY.EMPTY.HINT': 'Hint:',
    'LAUNCHPAD.CATEGORY.EMPTY.MESSAGE': 'Assigned users will not see this category until links have been added.',
    'LAUNCHPAD.CATEGORY.PERSONAL': 'Your links',
    'LAUNCHPAD.MANAGE.CATEGORY': 'Category',
    'LAUNCHPAD.MANAGE.CATEGORY.ARIA': 'Category for the new link',
    'LAUNCHPAD.MANAGE.CATEGORY.PLACEHOLDER': 'Choose a category',
    'LAUNCHPAD.MANAGE.DISCARD': 'Discard',
    'LAUNCHPAD.MANAGE.HEADLINE': 'Link details',
    'LAUNCHPAD.MANAGE.ICON.UPLOAD': 'Upload',
    'LAUNCHPAD.MANAGE.ICON.REMOVE': 'Remove',
    'LAUNCHPAD.MANAGE.SAVE': 'Save link',
    'LAUNCHPAD.MANAGE.NAME': 'Title',
    'LAUNCHPAD.MANAGE.NAME.ARIA': 'Title for the new link',
    'LAUNCHPAD.MANAGE.NAME.PLACEHOLDER': '',
    'LAUNCHPAD.MANAGE.URL': 'URL',
    'LAUNCHPAD.MANAGE.URL.ARIA': 'URL for the new link',
    'LAUNCHPAD.MANAGE.AUTOSUGGEST': 'Fetch icon and title from URL',
    'LAUNCHPAD.MANAGE.SHOW_ALL': 'Show all',
    'LAUNCHPAD.MANAGE.SHOW_LESS': 'Show less'
  }
};
