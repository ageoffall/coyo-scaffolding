import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LaunchpadHeaderComponent} from './launchpad-header.component';

describe('LaunchpadHeaderComponent', () => {
  let component: LaunchpadHeaderComponent;
  let fixture: ComponentFixture<LaunchpadHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchpadHeaderComponent]
    }).overrideTemplate(LaunchpadHeaderComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchpadHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
