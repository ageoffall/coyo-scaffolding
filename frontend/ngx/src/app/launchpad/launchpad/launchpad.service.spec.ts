import {inject, TestBed} from '@angular/core/testing';
import {OverlayService} from '@shared/overlay/overlay.service';
import {LaunchpadComponent} from './launchpad.component';
import {LaunchpadService} from './launchpad.service';

describe('LaunchpadService', () => {
  let overlayService: jasmine.SpyObj<OverlayService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LaunchpadService, {
        provide: OverlayService,
        useValue: jasmine.createSpyObj('OverlayService', ['open'])
      }]
    });

    overlayService = TestBed.get(OverlayService);
    (overlayService as any).scrollStrategies = {block: () => 'BLOCK'};
  });

  it('should be created', () => {
    const service: LaunchpadService = TestBed.get(LaunchpadService);
    expect(service).toBeTruthy();
  });

  it('should open a modal', inject([LaunchpadService], (service: LaunchpadService) => {
    // when
    service.open();

    // then
    expect(overlayService.open).toHaveBeenCalledWith(LaunchpadComponent, {
      scrollStrategy: 'BLOCK',
      panelClass: 'launchpad',
      hasBackdrop: false,
      height: '100%',
      width: '100%'
    });
  }));
});
