import {HttpClient} from '@angular/common/http';
import {fakeAsync, TestBed} from '@angular/core/testing';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {of} from 'rxjs';
import {GraphApiService} from './graph-api.service';

describe('GraphApiService', () => {
  let service: GraphApiService;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  let httpClient: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('IntegrationApiService', ['updateAndGetActiveState', 'getToken'])
      }, {
        provide: HttpClient,
        useValue: jasmine.createSpyObj('HttpClient', ['get'])
      }]
    });
    integrationApiService = TestBed.get(IntegrationApiService);
    httpClient = TestBed.get(HttpClient);

    service = TestBed.get(GraphApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add Authorization header in GET request', fakeAsync(() => {
    // given
    const expectedToken = 'TOKEN';
    integrationApiService.getToken.and.returnValue(of({token: expectedToken}));
    let actualToken = null;
    httpClient.get.and.callFake((url: string, opts: { headers?: { 'Authorization': string } }) => {
      actualToken = opts.headers.Authorization;
      return of({value: []});
    });

    // when
    service.get<any>('/endpoint').subscribe();

    // then
    expect(integrationApiService.getToken).toHaveBeenCalled();
    expect(actualToken).toEqual('Bearer ' + expectedToken);
  }));
});
