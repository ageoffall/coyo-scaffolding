import {NgModule} from '@angular/core';
import {FilepickerModule} from '@app/filepicker/filepicker.module';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import './o365-api/o365-api.service.downgrade';
import './share-point-filepicker/share-point-filepicker.service.downgrade';

/**
 * Module for providing the functionality around the O365 integration.
 */
@NgModule({
  imports: [
    CoyoCommonsModule,
    FilepickerModule
  ],
})
export class O365Module {
}
