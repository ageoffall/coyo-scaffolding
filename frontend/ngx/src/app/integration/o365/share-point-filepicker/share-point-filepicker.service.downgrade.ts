import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {SharePointFilepickerService} from '@app/integration/o365/share-point-filepicker/share-point-filepicker.service';

getAngularJSGlobal()
  .module('coyo.domain')
  .factory('ngxSharePointFilepickerService', downgradeInjectable(SharePointFilepickerService));
