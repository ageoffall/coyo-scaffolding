import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {Site} from '@app/integration/o365/o365-api/domain/site';
import {DriveFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-filepicker-item';
import {FetchDrives} from '@app/integration/o365/share-point-filepicker/share-point-filepicker.service';
import {OFFICE365, storageType} from '@domain/attachment/storage';
import {Observable} from 'rxjs';

/**
 * This class creates a FilepickerItem based on a SharePoint Site entity
 */
export class SiteFilepickerItem implements FilepickerItem {
  name: string;
  isFolder: boolean;
  sizeInBytes: number | null;
  lastModified: Date;
  mimeType: string | null;
  storageType: storageType;

  constructor(private site: Site, private fetchDrives: FetchDrives) {
    this.name = site.displayName;
    this.isFolder = true;
    this.sizeInBytes = null;
    this.lastModified = new Date(this.site.lastModifiedDateTime);
    this.mimeType = null;
    this.storageType = OFFICE365;
  }

  static fromArray(sites: Site[], fetchDrives: FetchDrives): SiteFilepickerItem[] {
    return sites.map(site => new SiteFilepickerItem(site, fetchDrives));
  }

  getChildren(): Observable<DriveFilepickerItem[]> {
    return this.fetchDrives(this.site.id);
  }
}
