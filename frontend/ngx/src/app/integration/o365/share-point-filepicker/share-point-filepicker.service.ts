import {Injectable} from '@angular/core';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerService} from '@app/filepicker/filepicker.service';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {DriveFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-filepicker-item';
import {DriveItemFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-item-filepicker-item';
import {SiteFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/site-filepicker-item';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export type FetchSites = () => Observable<SiteFilepickerItem[]>;
export type FetchDrives = (siteId: string) => Observable<DriveFilepickerItem[]>;
export type FetchDriveItems = (driveId: string, driveItemId?: string) => Observable<DriveItemFilepickerItem[]>;

/**
 * Service that handles the SharePoint Filepicker.
 */
@Injectable({
  providedIn: 'root'
})
export class SharePointFilepickerService {
  constructor(private filepickerService: FilepickerService, private o365ApiService: O365ApiService) {
  }

  /**
   * Opens the SharePoint filepicker
   * @return Observable of selected items
   */
  openFilepicker(): Observable<FilepickerItem[]> {
    return this.filepickerService.openFilepicker({
      lastModified: null,
      mimeType: null,
      sizeInBytes: null,
      storageType: 'OFFICE_365',
      name: 'SharePoint',
      isFolder: true,
      getChildren: this.fetchSites
    });
  }

  private fetchSites: FetchSites = () =>
    this.o365ApiService
      .getSites()
      .pipe(map(sites => SiteFilepickerItem.fromArray(sites, this.fetchDrives)));

  private fetchDrives: FetchDrives = siteId =>
    this.o365ApiService
      .getDrivesBySiteId(siteId)
      .pipe(map(drives => DriveFilepickerItem.fromArray(drives, this.fetchDriveItems)));

  private fetchDriveItems: FetchDriveItems = (driveId, driveItemId = 'root') =>
    this.o365ApiService
      .getDriveItems(driveId, driveItemId)
      .pipe(map(driveItems => DriveItemFilepickerItem.fromArray(driveItems, this.fetchDriveItems)))
}
