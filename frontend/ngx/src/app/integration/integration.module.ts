import {NgModule} from '@angular/core';
import './integration-api/integration-api.service.downgrade';

/**
 * Module for providing the functionality around the integration systems.
 */
@NgModule({})
export class IntegrationModule {}
