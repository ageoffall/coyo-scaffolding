import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReportListState} from '@app/reports/report-list/report-list-state';
import {Report} from '@domain/reports/report';
import {ReportsService} from '@domain/reports/reports.service';
import {of} from 'rxjs';
import {ReportListComponent} from './report-list.component';

describe('ReportListComponent', () => {
  let component: ReportListComponent;
  let fixture: ComponentFixture<ReportListComponent>;
  let reportService: jasmine.SpyObj<ReportsService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportListComponent],
      providers: [{
        provide: ReportsService,
        useValue: jasmine.createSpyObj('reportsService', ['getPage', 'resolve'])
      }]
    }).overrideTemplate(ReportListComponent, '')
      .compileComponents();

    reportService = TestBed.get(ReportsService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportListComponent);
    component = fixture.componentInstance;
  });

  it('should init', async(() => {
    // given
    reportService.getPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10
    }));

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe((state: ReportListState) => {
      expect(state.isLoading).toEqual(false);
      expect(state.reports[0].message).toEqual('Hello');
      expect(state.reports[1].message).toEqual('World');
      expect(state.count).toEqual(2);
      expect(state.total).toEqual(10);
      component.state$.unsubscribe();
    });
  }));

  it('should resolve reports', async(() => {
    // given
    reportService.resolve.and.returnValue(of({}));
    reportService.getPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10
    }));
    fixture.detectChanges();

    // when
    component.resolve({id: '2'} as Report);
    fixture.detectChanges();

    // then
    component.state$.subscribe(state => {
      expect(state.reports[0].message).toEqual('Hello');
      expect(state.reports.length).toEqual(1);
      expect(state.count).toEqual(1);
      expect(state.total).toEqual(9);
      component.state$.unsubscribe();
    });
  }));

  it('should load more reports', async(() => {
    // given
    reportService.getPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10
    }));
    fixture.detectChanges();

    reportService.getPage.and.returnValue(of({
      content: [{id: '3', message: 'Foo'}, {id: '4', message: 'Bar'}],
      numberOfElements: 2,
      totalElements: 5
    }));

    // when
    component.loadMore();

    // then
    component.state$.subscribe(state => {
      expect(state.reports[0].message).toEqual('Hello');
      expect(state.reports[1].message).toEqual('World');
      expect(state.reports[2].message).toEqual('Foo');
      expect(state.reports[3].message).toEqual('Bar');
      expect(state.count).toEqual(4);
      expect(state.total).toEqual(5);
      component.state$.unsubscribe();
    });
  }));
});
