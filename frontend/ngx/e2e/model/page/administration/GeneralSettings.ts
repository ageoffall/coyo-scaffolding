import {by, element, ElementFinder} from 'protractor';
import {TestHelper} from '../../../utils/Util';
import {Button, Checkbox} from '../components/Elements';
import {CoyoPage} from '../CoyoPage';

export class GeneralSettings extends CoyoPage {

  private multiLanguageSupportId: ElementFinder = element(by.id('multiLanguageActive'));
  private multiLanguageSupportClickable: ElementFinder = this.multiLanguageSupportId;

  private multiLanguageSupportCheckBox: Checkbox = new Checkbox(
    'multiLanguageSupportCheckBox',
    this.multiLanguageSupportId,
    this.multiLanguageSupportClickable,
    () => TestHelper.hasClass(this.multiLanguageSupportId.$('label'), 'checked'));

  private saveButton: Button = new Button('saveButton', element(by.css('button[type="submit"]')));

  activateMultiLanguage(): void {
    this.open();
    this.multiLanguageSupportCheckBox.isChecked().then(isChecked => {
      if (!isChecked) {
        this.multiLanguageSupportCheckBox.toggle();
        expect(this.multiLanguageSupportCheckBox.isChecked()).toBeTruthy();
        this.saveButton.click();
      }
    });
  }

  deactivateMultiLanguage(): void {
    this.open();
    this.multiLanguageSupportCheckBox.isChecked().then(isChecked => {
      if (isChecked) {
        this.multiLanguageSupportCheckBox.toggle();
        expect(this.multiLanguageSupportCheckBox.isChecked()).toBeFalsy();
        this.saveButton.click();
      }
    });
  }

  constructor() {
    super('/admin/settings/general');
  }
}
