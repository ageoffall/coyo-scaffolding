/**
 * Checks whether template URLs start with a leading '/'
 */
'use strict';

module.exports = function(context) {
    function templateUrlValid(url) {
        if (url.startsWith('/')) {
            return false;
        }
        return true;
    }

    function isTemplateUrlIdentifier(property) {
        return property.key && property.key.type === 'Identifier' && property.key.name === 'templateUrl';
    }

    function hasStringValue(property) {
        return property.value && property.value.value;
    }

    function checkProperty(node, property) {
        if (isTemplateUrlIdentifier(property)
            && hasStringValue(property)
            && !templateUrlValid(property.value.value)) {
            context.report(node, 'Template URL declared with a leading "/"');
        }
    }

    function checkProperties(node) {
        if (node && node.properties) {
            for (var i = 0; i < node.properties.length; ++i) {
                checkProperty(node, node.properties[i]);
            }
        }
    }

    return {
        ObjectExpression: checkProperties
    };
};

module.exports.schema = [
    // JSON Schema for rule options goes here
];
