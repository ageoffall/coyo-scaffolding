/// <reference types="Cypress" />

describe('Timeline item footer', () => {

  let timelinePostMessage = '';
  const otherSender = 'Company News';

  beforeEach(() => {
    timelinePostMessage = Cypress.faker.random.word();
    cy.server();
    cy.route('GET', '/web/senders/search/actable-senders?**').as('getActableSenders');
    cy.route('GET', '/web/timeline-items?**_page=0**').as('getInitialTimelineItems');
    cy.route('POST', '/web/like-targets/timeline-item/*/likes/*').as('postLikeTimelineItem');
    cy.login('rl', 'demo').dismissTour();
  });

  afterEach(() => {
    cy.logout();
  });

  it('should like a timeline post', () => {
    const likeButton = '[data-test="like-button"]';
    cy.createTimelinePost(timelinePostMessage, false)
      .showNewTimelinePosts()
      .visit('/home/timeline')
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get(likeButton).click().blur()
          .get(likeButton).should('have.class', 'active')
          .get('[data-test="info-container"]').should('be.visible')
          .get('[data-test="like-info-button"]').should('be.visible');
      });
  });

  it('should share a timeline post', () => {
    cy.createTimelinePost(timelinePostMessage, false)
      .showNewTimelinePosts()
      .visit('/home/timeline')
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="share-button"]').click();
      })
      .get('.modal-dialog').within(() => {
        cy.get('[data-test="share-with-sender-select"] input').type(otherSender);
      })
      .get('.ng-dropdown-panel').within(() => {
        cy.contains('.ng-option', otherSender).click();
      })
      .get('.modal-dialog').within(() => {
        cy.get('[data-test="share-message"]').type(Cypress.faker.random.word());
      })
      .get('[data-test="share-submit"]').click()
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="info-container"]').should('be.visible');
        cy.get('[data-test="shares-info-button"]').should('be.visible');
      });
  });

  it('should unsubscribe timeline post', () => {
    cy.createTimelinePost(timelinePostMessage, false)
      .showNewTimelinePosts()
      .visit('/home/timeline')
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="subscribe-button"]').should('have.class', 'active')
          .get('[data-test="subscribe-button"]').click()
          .get('[data-test="subscribe-button"]').should('not.have.class', 'active');
      });
  });

  it('should change author for timeline post comment', () => {
    const timelinePostCommentMessage = Cypress.faker.random.word();
    cy.createTimelinePost(timelinePostMessage, false)
      .showNewTimelinePosts()
      .visit('/home/timeline')
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="functional-user-chooser"]').click()
          .get('.ng-dropdown-panel-items').within(() => {
            cy.contains('.ng-option', otherSender).click();
          })
          .get('[data-test="timeline-comment-message"]').type(timelinePostCommentMessage)
          .get('[data-test="comment-form-submit"]').click()
          .get('[data-test="comment-author"]').should('contain', otherSender);
      });
  });

  it('should not have social features on a restricted timeline post', () => {
    cy.createTimelinePost(timelinePostMessage, true)
      .showNewTimelinePosts()
      .visit('/home/timeline')
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="like-button"]').should('not.be.visible');
        cy.get('[data-test="share-button"]').should('not.be.visible');
        cy.get('[data-test="functional-user-chooser"]').should('not.be.visible');
        cy.get('[tip="MODULE.TIMELINE.ITEM.HELP"]').should('be.visible');
      });
  });

  it('should like as another sender', () => {
    const likeButton = '[data-test="like-button"]';
    cy.createTimelinePost(timelinePostMessage, false)
      .showNewTimelinePosts()
      .visit('/home/timeline')
      .get('[data-test="timeline-stream"]')
      .contains('[data-test="timeline-item"]', timelinePostMessage).within(() => {
        cy.get('[data-test="functional-user-chooser"]').click()
          .get('.ng-dropdown-panel-items').within(() => {
            cy.contains('.ng-option', otherSender).click();
          })
          .get(likeButton).click()
          .get('.active[data-test="like-button"]')
          .get('[data-test="info-container"]').should('be.visible')
          .get('[data-test="like-info-button"]').should('be.visible')
          .get('[data-test="functional-user-chooser"]').click()
          .get('.ng-dropdown-panel-items').within(() => {
            cy.contains('.ng-option', 'Robert Lang').click();
          })
          .get(likeButton).should('not.have.class', 'active')
          .get('[data-test="like-info-button"]').should('contain', otherSender);
      });
  });

});
