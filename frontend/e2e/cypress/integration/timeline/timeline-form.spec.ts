/// <reference types="Cypress" />

describe('Timeline form', () => {

  let timelinePostMessage = '';

  beforeEach(() => {
    timelinePostMessage = Cypress.faker.random.word();
    cy.server();
    cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
    cy.route('GET', '/web/timeline-items?**_page=0**').as('getInitialTimelineItems');
    cy.clearLocalStorage().login('ib', 'demo').dismissTour();
  });

  afterEach(() => {
    cy.logout();
  });

  it('should create a simple timeline post with text and an emoji', () => {
    cy.visit('/home/timeline')
      .get('coyo-timeline-form').within(() => {
        cy.get('textarea').click().type(timelinePostMessage + ' ');
        cy.get('coyo-emoji-picker').click();
        cy.get('emoji-mart .emoji-mart').within(() => {
          cy.get('.emoji-mart-anchor:nth-child(2)').click();
          cy.get('.emoji-mart-emoji span').contains('😀').click();
        })
        .get('textarea').click()
        .get('[type=submit]').click();
      })
      .get('[data-test="timeline-stream"]')
      .contains('.timeline-item-message p', timelinePostMessage + ' 😀');
  });

  it('should create a timeline post with another sender', () => {
    cy.visit('/home/timeline')
      .get('coyo-timeline-form').within(() => {
        cy.get('textarea').click().type(timelinePostMessage);
        cy.get('coyo-functional-user-chooser').click();
        cy.get('coyo-functional-user-chooser').contains('About Coyo').click();
        cy.get('[type=submit]').click();
      })
      .get('[data-test="show-new-timeline-items"]').click()
      .wait('@getNewTimelineItems')
      .get('[data-test="timeline-stream"]')
      .contains('.timeline-item-message p', timelinePostMessage);
  });

  it('should create a post with a file and a link', () => {
    const url = Cypress.config().baseUrl + '/pages/company-news/apps/timeline/timeline';
    cy.visit('/home/timeline')
      .get('coyo-timeline-form').within(() => {
        cy.get('textarea').click().type(url).type(' ').clear().type(timelinePostMessage)
        cy.get('coyo-attachment-btn button').click();
        cy.get('coyo-context-menu').within(() => {
          cy.get('li[role="menuitem"]').contains('file library').click();
        });
      })
      .get('.modal-dialog').within(() => {
        cy.get('.fl-table-row').first().click();
        cy.get('button.btn-primary').click();
      })
      .get('coyo-timeline-form').within(() => {
        cy.get('coyo-link-preview').should('be.visible');
        cy.get('coyo-attachment').should('be.visible');
        cy.get('[type=submit]').click();
      })
      .wait('@getNewTimelineItems')
      .get('.timeline-stream').contains('coyo-timeline-item', timelinePostMessage).within(() => {
        cy.get('coyo-link-preview').should('be.visible');
        cy.get('.pi-preview-image-container').should('be.visible');
      });
  });

  it('should create a simple locked timeline post', () => {
    cy.visit('/home/timeline')
      .get('coyo-timeline-form').within(() => {
        cy.get('textarea').click().type(timelinePostMessage);
        cy.get('coyo-lock-btn button').click();
        cy.get('[type=submit]').click();
      })
      .logout()
      .clearLocalStorage()
      .login('rl', 'demo')
      .dismissTour()
      .visit('/home/timeline')
      .wait('@getInitialTimelineItems')
      .get('[data-test="show-new-timeline-items"]').click()
      .wait('@getNewTimelineItems')
      .get('.timeline-stream').contains('coyo-timeline-item', timelinePostMessage).within(() => {
        cy.get('[data-test=timeline-item-help]').should('be.visible');
        cy.get('textarea').should('not.be.visible');
        cy.get('[type=submit]').should('not.be.visible');
      });
  });

  it('should create a sticky timeline post', () => {
    cy.visit('/home/timeline')
      .get('coyo-timeline-form').within(() => {
        cy.get('textarea').click().type(timelinePostMessage);
        cy.get('coyo-sticky-btn button').click();
        cy.get('.context-menu-list.dropdown-menu').within(() => {
          cy.get('li[role="menuitem"]').first().click();
        });
        cy.get('[type=submit]').click();
      })
      .wait('@getNewTimelineItems')
      .get('.timeline-stream').contains('coyo-timeline-item', timelinePostMessage).within(() => {
        cy.get('coyo-ribbon .ribbon-sticky').should('be.visible');
        cy.get('coyo-ribbon .ribbon-sticky-btn').should('be.visible');
      });
  });

  it('should show timeline post help', () => {
    cy.visit('/home/timeline')
      .get('coyo-timeline-form').within(() => {
        cy.get('textarea').click()
          .get('.modal-dialog').should('not.be.visible')
        cy.get('coyo-help[modaltext="MODULE.TIMELINE.FORM.HELP.MODAL"] button').click();
      })
      .get('.modal-dialog').within(() => {
        cy.get('.modal-title').contains('Help').should('be.visible');
      });
  });

});
