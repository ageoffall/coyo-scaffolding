/// <reference types="Cypress" />

describe('Notifications', function () {

  describe('Form app', function () {

    const textFieldName = Cypress.faker.random.word();
    const textFieldValue = Cypress.faker.random.word();

    before(function () {
      cy.clearLocalStorage().login('rl', 'demo').dismissTour();
      cy.createPage(Cypress.faker.random.word()).as('page')
        .then(page => cy.createFormApp(Cypress.faker.random.word(), page.id, true).as('formApp')
        .then(app => cy.addFieldToFormApp(textFieldName, page.id, app.id, 'text').as('formAppField')));
      cy.logout();
    });

    it('should show notification and open details view when clicking on notification', function () {
      cy.clearLocalStorage().login('nf', 'demo').dismissTour();
      cy.createFormAppEntry(textFieldValue, this.formAppField.id, this.formApp.senderId, this.formApp.id);
      cy.logout().clearLocalStorage();
      cy.login('rl', 'demo').then(function () {
        cy.visit('/home');
        cy.get('[data-test=notification-toggle]').click();
        cy.get(':nth-child(2) > [data-test=notification-tabs]').click();
        cy.get(':nth-child(1) > [data-test=notification-item]').contains(this.formApp.name);
        cy.get(':nth-child(1) > [data-test=notification-item]').contains(this.page.name);
        cy.get('#notification-panel-activity > ul > :nth-child(1)').click();
        cy.get('[data-test=text-value]').contains(textFieldValue);
        cy.get(`[data-test="${textFieldName}"]`).contains(textFieldName);
      });
    });
  });
});
