// ***********************************************************
// Custom commands related to users
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Dismisses the COYO tour for the given topics.
     *
     * @param {string[]} [topics] The list of tour topics to be dismissed.
     * @example
     *    cy.dismissTour();
     *    cy.dismissTour(['timeline', 'pages', 'workspaces']);
     */
    dismissTour(topics?: string[]): Chainable<Response>;
  }
}
