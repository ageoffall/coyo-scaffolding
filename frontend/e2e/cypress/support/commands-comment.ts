// ***********************************************************
// Custom commands related to coyo timeline comments
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

Cypress.Commands.add('createTimelineComment', function (message: string, targetId: string, targetType: string) {
  return cy.requestAuth('POST', '/web/comments?_permissions=*', {
    authorId: this.user.id,
    message: message,
    targetId: targetId,
    targetType: targetType,
    attachments: [],
    fileLibraryAttachments: []
  }).its('body');
});
