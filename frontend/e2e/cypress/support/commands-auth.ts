// ***********************************************************
// Custom commands related to authentication
//
// Commands defined in this file must not utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// ***********************************************************

Cypress.Commands.add('csrf', function () {
  return cy.request({
    method: 'GET',
    url: Cypress.env('backendUrl') + '/web/csrf',
  }).then((response) => cy.wrap(response.body.token).as('csrf'));
});

Cypress.Commands.add('login', function (username, password) {
  cy.visit('/f/login');
  cy.wait(1000); // FIXME temporary workaround to avoid race condition triggering invalid CSRF error
  return cy.csrf().then((csrf) => cy.request({
    method: 'POST',
    url: Cypress.env('backendUrl') + '/web/auth/login',
    form: true,
    headers: {
      'X-CSRF-TOKEN': csrf
    },
    body: {
      username: username || Cypress.env('username'),
      password: password || Cypress.env('password')
    }
  }).then((response) => {
    const user = JSON.parse(response.body);
    localStorage.setItem('ngStorage-backendUrl', '"' + Cypress.env('backendUrl') + '"');
    localStorage.setItem('ngStorage-isAuthenticated', 'true');
    localStorage.setItem('ngStorage-userId', '"' + user.id + '"');
    return cy.wrap(user).as('user').then(() =>
      cy.csrf().then(() => user)
    );
  }));
});

Cypress.Commands.add('logout', function () {
  return cy.csrf().then((csrf) => cy.request({
    method: 'POST',
    url: Cypress.env('backendUrl') + '/web/auth/logout',
    headers: {
      'X-CSRF-TOKEN': csrf
    }
  }));
});
