// ***********************************************************
// Custom commands related to apps
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

Cypress.Commands.add('createFormApp', function (name: string, senderId: string, enableNotifications: boolean) {
  const notificationLevel = enableNotifications ? 'ADMIN' : 'NONE';
  return cy.requestAuth('POST', '/web/senders/' + senderId + '/apps?_permissions=manage', {
    active: true,
    defaultLanguage: null,
    key: 'form',
    name: name,
    senderId: senderId,
    settings: {notification: notificationLevel, title: name},
    translations: {}
  }).its('body');
});

Cypress.Commands.add('createFormAppEntry', function (value: string, fieldId: string, senderId: string, appId: string) {
  return cy.requestAuth('POST', '/web/senders/' + senderId + '/apps/' + appId + '/form/entries', {
    appId: appId,
    appKey: 'form',
    senderId: senderId,
    values: [{fieldId: fieldId, value: value}]
  }).its('body');
});

Cypress.Commands.add('addFieldToFormApp', function (name: string, senderId: string, appId: string, type: string) {
  return cy.requestAuth('POST', '/web/senders/' + senderId + '/apps/' + appId + '/form/fields', {
    appId: appId,
    appKey: 'form',
    key: type,
    name: name,
    senderId: senderId,
    settings: {}
  }).its('body');
});
