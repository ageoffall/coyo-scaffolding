// ***********************************************************
// Custom commands related to coyo timeline
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

Cypress.Commands.add('createTimelinePost', function (message: string, isRestricted: boolean) {
  return cy.requestAuth('POST', '/web/timeline-items', {
    attachments: [],
    authorId: this.user.id,
    data: {message: message},
    fileLibraryAttachments: [],
    recipientIds: [this.user.id],
    restricted: isRestricted,
    stickyExpiry: null,
    type: 'post',
    webPreviews: {}
  }).its('body');
});

Cypress.Commands.add('showNewTimelinePosts', function () {
  return cy.requestAuth('GET', '/web/timeline-items/new?contextId=' + this.user.id + '&type=personal');
});
