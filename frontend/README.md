# README for coyo-frontend #

Copyright (c) 2015-2019 COYO GmbH


## Prerequisites ##

- [Node.js](https://nodejs.org) - JavaScript runtime built on Chrome's V8 JavaScript engine. 
    - Check .nvmrc file for supported version
    - Installation via [nvm](https://github.com/creationix/nvm#installation) is recommended
- [Yarn](https://yarnpkg.com) - fast, reliable, and secure dependency management. 
    - Check .yvmrc file for the currently supported version.
    - To install any version differing from the latest via homebrew, refer to:  https://github.com/yarnpkg/yarn/issues/1882#issuecomment-421372892

## Project Structure ##

The COYO frontend project is essentially made up of three sub-projects, all of
which are located in separate directories:

- **AngularJS application**  
The legacy AngularJS application is located in `./src`. It is in maintenance mode and not actively developed. All future
development should happen in the new Angular application (see below). Thus, all script commands are not provided by the
`./package.json` but by the `./ngx/package.json`.
- **Angular application**  
All code related to the new Angular application is located in `./ngx`. It is bootstrapped together with the legacy
AngularJS application. Active development should happen within the context of this sub-project.


## Setup and Development ##

Yarn provides the following useful (script) commands:

- `yarn install` - Installs all dependencies for the project. This will also install all dependencies of the legacy
AngularJS application in a `postinstall` step. A prune command isn’t necessary as this command will prune extraneous
packages.
- `yarn outdated` - Checks for outdated package dependencies.
- `yarn licenses list` - Lists licenses for installed packages.
- `yarn add` - Installs a new dependency and automatically adds it to the `package.json`. Use `--dev` to install a
dependency for development. 
- `yarn start` - Starts the frontend development mode, watches the source files and serves the application at 
[http://localhost:3000](http://localhost:3000).
- `yarn build` - Builds the frontend app in production mode.
- `yarn lint` - Lints all JavaScript files using [ESLint](https://eslint.org/) and
[TSLint](https://palantir.github.io/tslint/).
- `yarn test` - Starts the unit tests.
- `yarn clean` - Deletes all auto-generated directories.

On top of the Yarn script, the [Angular CLI](https://cli.angular.io/) provides more handy commands to create new
classes, components or directives:

- **[Component](https://github.com/angular/angular-cli/wiki/generate-component):** `ng g component my-new-component`
- **[Directive](https://github.com/angular/angular-cli/wiki/generate-directive):** `ng g directive my-new-directive`
- **[Pipe](https://github.com/angular/angular-cli/wiki/generate-pipe):** `ng g pipe my-new-pipe`
- **[Service](https://github.com/angular/angular-cli/wiki/generate-service):** `ng g service my-new-service`
- **[Class](https://github.com/angular/angular-cli/wiki/generate-class):** `ng g class my-new-class`
- **[Guard](https://github.com/angular/angular-cli/wiki/generate-guard):** `ng g guard my-new-guard`
- **[Interface](https://github.com/angular/angular-cli/wiki/generate-interface):** `ng g interface my-new-interface`
- **[Enum](https://github.com/angular/angular-cli/wiki/generate-enum):** `ng g enum my-new-enum`
- **[Module](https://github.com/angular/angular-cli/wiki/generate-module):** `ng g module my-module`


## Run as Docker Container ##

Coyo's frontend module can be run inside a single docker container. The container starts an Apache _httpd_ server with
the configuration located in the `assembly` subfolder.

In order to build and start the coyo-frontend docker container locally run through the following steps:
1. Open a terminal and change to the _coyo-frontend_ directory. Run `yarn run build`
2. Move to the _ngx/dist_ subfolder and edit the _config.js_. Set `backendUrlStrategy='configurable'`
3. Move back to the _coyo-frontend_ folder and build the Docker container by running `docker build -t coyo-frontend`.
4. Start the coyo-frontend container by running `docker run -p 0.0.0.0:9000:80 -t coyo-frontend`
5. The frontend should now be available at http://localhost:9000

**Note:** You still need to manually start the docker dev compose script and the backend as usual. In the login dialog
supply the backend url of your local machine.


## Documentation ##

All documentation is included with the **coyo-docs** project.
