// ***********************************************************
// Custom commands related to events
// ***********************************************************

/**
 * Command for creating a event
 * @param withHost boolean if true the event will be created with a host
 */

Cypress.Commands.add('createEvent', function (withHost) {
  // Create an event
  cy.get('[data-test=navigation-events]')
      .click();
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/events');
  cy.get('[data-test=button-event-create]').eq(0)
      .click();
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/events/create');
  const eventName = 'qa-event-' + Date.now();
  cy.get('[data-test=input-event-name]')
      .click()
      .type(eventName);
  cy.get('[data-test="button-event-create-name submit"]')
      .click();
  cy.get('[data-test=checkbox-full-day-event-create] > [data-test=checkbox]')
      .click();
  cy.get('[data-test="button-event-create-date submit"]')
      .click();
  if (withHost === true) {
    cy.get('[data-test="dropdown-event-create-host"]')
        .click();
    cy.get('[data-test="dropdown-sender-select"]')
        .click();
  }
  cy.get('[data-test="button-event-create-host submit"]')
      .click();
  cy.get('[data-test=events-show-participants-checkbox]')
      .click();
  cy.get('[data-test=events-definite-answer-checkbox] > .coyo-checkbox')
      .click();
  cy.get('[data-test="button-event-create-event submit"]')
      .click();
  cy.get('[data-test="headline-event-title"]')
      .should('have.text', eventName);
});
