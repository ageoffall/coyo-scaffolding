// ***********************************************************
// Custom commands related to the login
// ***********************************************************

// Successful login
Cypress.Commands.add('login', function (user, password) {
  cy.server();
  cy.route('GET', '/web/setup/check').as('getSetupCheck');
  cy.wait('@getSetupCheck');
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/login');
  // Login with given user
  cy.get('[data-test=input-username]')
      .type(user);
  cy.get('[data-test=input-password]')
      .type(password);
  cy.get('[data-test=button-submit]')
      .click();
});

// Reset the password
Cypress.Commands.add('resetPassword', function (user) {
  cy.server();
  cy.route('GET', '/web/setup/check').as('getSetupCheck');
  cy.wait('@getSetupCheck');
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/login');

  // Reset forgotten password
  cy.get('[data-test="button-password-reset"]')
      .click();
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/reset');
  cy.get('[data-test=input-username]')
      .type(user);
  cy.get('[data-test=button-submit]')
      .click();
  cy.get('[data-test=text-mail-sent]')
      .should('contain', 'A link to reset your password');
  
  // Go back to login afterwards
  cy.get('[data-test=button-back-to-login]')
      .click();
  cy.url()
      .should('eq', Cypress.config().baseUrl + '/f/login');
});

// Login with SAML

// Login with Office 365

// Login with G Suite
