// ***********************************************************
// Custom commands done via API
// ***********************************************************

// Get CSRF token via API
Cypress.Commands.add('csrf', function () {
  return cy.request({
    method: 'GET',
    url: Cypress.env('backendUrl') + '/web/csrf'
  }).then((response) => cy.wrap(response.body.token).as('csrf'));
});

// Request authentication via API
Cypress.Commands.add('requestAuth', function (method, path, body) {
  return cy.request({
    method: method,
    url: Cypress.env('backendUrl') + path,
    headers: method === 'GET' ? {
      'Accept': 'application/json',
      'X-Coyo-Current-User': this.user.id
    } : {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRF-TOKEN': this.csrf,
      'X-Coyo-Current-User': this.user.id
    },
    body: body
  });
});

// Get client secret via API
Cypress.Commands.add('getClientsecret', function () {
  cy.request({
    method: 'GET',
    url: Cypress.env('backendUrl') + '/web/api-clients'
  }).then((response) => {
    cy.wrap({
      clientId: response.body.content[0].clientId,
      clientSecret: response.body.content[0].clientSecret
    });
  });
});

// Get bearer token via API
Cypress.Commands.add('apiBearerToken', function (username, password) {
  cy.getClientsecret().then((credentials) => cy.request({
    auth: {
      user: credentials.clientId,
      pass: credentials.clientSecret
    },
    method: 'POST',
    url: Cypress.env('backendUrl') + '/api/oauth/token?grant_type=password&username=' + username + '&password='
        + password
  })).then((response) => cy.wrap(response.body.access_token).as('bearerToken'));
});

/**
 * Command for logging in via API
 * @param {string} username can be any available username
 * @param {string} password is the corresponding password
 * If left empty the given user in the cypress.json will taken, usually Robert Lang
 *
 */

// Login via API
Cypress.Commands.add('apiLogin', function (username, password) {
  return cy.csrf().then((csrf) => cy.request({
    method: 'POST',
    url: Cypress.env('backendUrl') + '/web/auth/login',
    form: true,
    headers: {
      'X-CSRF-TOKEN': csrf
    },
    body: {
      username: username || Cypress.env('username'),
      password: password || Cypress.env('password')
    }
  }).then((response) => {
    const user = JSON.parse(response.body);
    localStorage.setItem('ngStorage-backendUrl', '"' + Cypress.env('backendUrl') + '"');
    localStorage.setItem('ngStorage-isAuthenticated', 'true');
    localStorage.setItem('ngStorage-userId', '"' + user.id + '"');

    return cy.wrap(user).as('user').then(() =>
        cy.csrf().then(() => user)
    );
  }));
});

/**
 * Command for setting a users language via API
 * @param {string} should be "DE" or "EN" or any language available for the instance
 *
 */

// Set language via API
Cypress.Commands.add('apiLanguage', function (language) {
  return cy.requestAuth('PUT', '/web/users/' + this.user.id + '/language', {
    language: language
  });
});

// Logout via API
Cypress.Commands.add('apiLogout', function () {
  return cy.csrf().then((csrf) => cy.request({
    method: 'POST',
    url: Cypress.env('backendUrl') + '/web/auth/logout',
    headers: {
      'X-CSRF-TOKEN': csrf
    }
  }));
});

/**
 * Command for dismissing the tour via API
 * @param {array} topics can be any tour that should be dismissed
 * If left empty all possible tours will be dismissed
 *
 */

Cypress.Commands.add('apiDismissTour', function (topics) {
  return cy.requestAuth('PUT', '/web/users/' + this.user.id + '/tour', {
    visited: topics || [
      'navigation',
      'messaging',
      'timeline',
      'pages',
      'workspaces',
      'events',
      'profile',
      'colleagues',
      'search'
    ]
  });
});

/**
 * Command for creating a user via API
 * @param {boolean} superadmin defines if the user is a superadmin or not
 *
 */

Cypress.Commands.add('apiCreateUser', function (superadmin) {
  const firstName = Cypress.faker.name.firstName();
  const lastName = Cypress.faker.name.lastName();
  const email = firstName + '.' + lastName + '@coyoapp.com';
  return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
    auth: {
      bearer: bearerToken
    },
    method: 'POST',
    url: Cypress.env('backendUrl') + '/api/users',
    body: {
      firstname: firstName,
      lastname: lastName,
      email: email,
      active: true,
      superadmin: superadmin,
      password: 'Test1234'
    }
  }));
});

/**
 * Command for activating and deactivating apps for pages and workspaces
 * @param {string} app can be 'blog', 'content', 'events', 'file-library', 'form', 'forum', 'list', 'task', 'timeline', 'wiki', or 'championship'
 * @param {string} senderType should be 'page' or 'workspace'
 * @param {boolean} enabled describes if the app is enabled or disabled
 *
 */

Cypress.Commands.add('apiAppConfiguration', function (app, senderType, enabled) {
  return cy.apiBearerToken('robert.lang@coyo4.com', 'demo').then((bearerToken) => cy.request({
    url: Cypress.env('backendUrl') + '/api/apps/configurations/' + app,
    method: 'PUT',
    auth: {
      bearer: bearerToken
    },
    body: {
      senderType : senderType,
      enabled : enabled
    }
  }));
});