// ***********************************************************
// Custom commands related to workspaces
// ***********************************************************

/**
 * Command for creating a workspace
 * @param {string} category can be 'Ideas', 'Projects', 'Products', or 'Partners'
 * @param {string} visibility can be 'public', 'protected' or 'private'
 *
 */

Cypress.Commands.add('createWorkspace', function (category, visibility) {
  cy.server();
  cy.route('GET', '/web/workspaces/check-name**').as('getNameCheck');

  // Visit workspace overview and start creation
  cy.get('[data-test=navigation-workspaces]')
      .click();
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/workspaces');
  cy.get('[data-test=button-create-workspace]').eq(0)
      .click();
  cy.url()
      .should('contain', Cypress.config().baseUrl + '/workspaces/create');

  // Setup the new workspace
  const workspaceName = `qa-ws-${visibility}-${category}-${Date.now()}`;
  cy.get('[data-test=input-workspace-name]')
      .type(workspaceName);
  cy.wait('@getNameCheck');
  cy.get('[data-test=input-workspace-description]')
      .type(`This is an automated ${visibility} workspace for ${category}`);

  // Set category
  cy.get('[data-test=input-workspace-category]')
      .click();
  cy.contains(category)
      .click();
  cy.get('[data-test="button-workspace-continue submit"]')
      .click();

  // Set admin and members
  cy.get('[data-test=button-choose-admin]')
      .click();
  cy.get('[data-test=user-chooser-element]').eq(0)
      .click();
  cy.get('[data-test=button-user-chooser-submit]')
      .click();
  cy.get('[data-test=button-choose-member]')
      .click();
  cy.get('[data-test=user-chooser-element]').eq(1)
      .click();
  cy.get('[data-test=button-user-chooser-submit]')
      .click();

  // Set visibility
  cy.get(`[data-test=radio-workspace-visibility-${visibility}]`)
      .check();
  cy.get('[data-test="button-workspace-create submit"]')
      .click();

  // Assert that the workspace was created
  cy.get('[data-test=text-workspace-title]')
      .should('contain', workspaceName)
      .should('contain', visibility)
      .should('contain', category);
});
