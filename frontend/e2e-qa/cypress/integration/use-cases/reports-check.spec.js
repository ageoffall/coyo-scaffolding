describe('Reports check', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should search for a single word and check if it appears in the reports', function () {
    // Set prerequisites
    cy.server();
    cy.route('GET', '/web/statistics/SEARCH?dataSets=TERMS**').as('getReportsSearchTerms');
    const firstWord = Cypress.faker.lorem.word();
    cy.search(firstWord);
    cy.adminAreaNavigation();
    cy.get('[data-test=button-admin-reports]')
        .click();
    cy.get('[data-test=button-reports-popular-search-terms]')
        .click();

    // Check if visible with correct data
    cy.get('[data-test=canvas-reports]')
        .should('be.visible');
    cy.get('@getReportsSearchTerms')
        .then((reportsSearchTerms) => {
          cy.expect(reportsSearchTerms.response.body.dataSets[0].labels).to.contain(firstWord);
        });
  });

  it('should search for two words and check if they appear in the reports', function () {
    //Set prerequisites
    cy.server();
    cy.route('GET', '/web/statistics/SEARCH?dataSets=TERMS**').as('getReportsSearchTerms');
    cy.route('GET', '/web/statistics/SEARCH?dataSets=PHRASES**').as('getReportsSearchPhrases');
    const firstWord = Cypress.faker.lorem.word();
    const secondWord = Cypress.faker.lorem.word();
    cy.search(firstWord, secondWord);
    cy.adminAreaNavigation();
    cy.get('[data-test=button-admin-reports]')
        .click();
    cy.get('[data-test=button-reports-popular-search-terms]')
        .click();

    // Check if single terms are visible with correct data
    cy.get('[data-test=canvas-reports]')
        .should('be.visible');
    cy.wait('@getReportsSearchTerms');
    cy.get('@getReportsSearchTerms')
        .then((reportsSearchTerms) => {
          cy.expect(reportsSearchTerms.response.body.dataSets[0].labels).to.contain(firstWord);
          cy.expect(reportsSearchTerms.response.body.dataSets[0].labels).to.contain(secondWord);
        });

    // Check if phrase is visible with correct data
    cy.get('span[role="button"]').contains('Search phrases')
        .click();
    cy.wait('@getReportsSearchPhrases');
    cy.get('@getReportsSearchPhrases')
        .then((reportsSearchPhrases) => {
          cy.expect(reportsSearchPhrases.response.body.dataSets[0].labels).to.contain(firstWord + ' ' + secondWord);
        });
  });
});
