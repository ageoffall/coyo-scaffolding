describe('Workspace creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should create a new public workspace', function () {
    cy.createWorkspace('Ideas', 'public');
  });

  it('should create a new private workspace', function () {
    cy.createWorkspace('Projects', 'private');
  });

  it('should create a new protected workspace', function () {
    cy.createWorkspace('Products', 'protected');
  });
});
