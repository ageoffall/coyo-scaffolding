/// <reference types="Cypress" />
/// <reference types="../support" />

describe('Timeline entry creation', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.logout();
    });

    it('should create a timeline entry', () => {
        const timeLineEntry = 'qa timeline-post for create test' + Date.now();

        cy.server();
        cy.route('GET', '/web/timeline-items/new?**').as('getNewTimelineItems');
        cy.route('GET', '/web/users/*/subscriptions**').as('getSubscriptions');
        cy.route('POST', '/web/timeline-items**').as('postTimelineItems');

        cy.get('[data-test=textarea-timeline-post]')
            .type(timeLineEntry);
        cy.get('[data-test=button-timeline-submit]').wait(1000)
            .click();
        cy.wait('@postTimelineItems').its('status').should('eq', 201);
        cy.wait('@getNewTimelineItems').its('status').should('eq', 200);
        cy.wait('@getSubscriptions').its('status').should('eq', 200);
        cy.get('[data-test="timeline-stream"]').get('[data-test=timeline-item]').eq(0)
            .should('contain', timeLineEntry);
    });

    it('should be possible to edit an own timeline entry', () => {
        const newPost = 'qa timeline-post for edit test' + Date.now();
        const editPost = ' edited';
        cy.createTimelinePostAPI(newPost, false).showNewTimelinePosts();

        cy.server();
        cy.route('PUT', '/web/timeline-items/**').as('putEditTimelineItem');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').as('post').then(($post) => {
            cy.wrap($post).find('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
                .get('[data-test="button-timeline-post-edit"]')
                .click()
                .get('[data-test=input-timeline-edit]')
                .type(editPost);
            cy.get('[data-test=button-timeline-edit-save]')
                .click();
            cy.wait('@putEditTimelineItem').its('status').should('eq', 200);
            cy.get('@post')
                .should('contain', newPost + editPost);
        })
    });

    it('should be possible to comment on a timeline entry of non-restricted post', () => {
        const newPost = 'qa timeline-post for comment test ' + Date.now();
        const comment = 'little comment for timeline comment test';
        cy.createTimelinePostAPI(newPost, false).showNewTimelinePosts();

        cy.server();
        cy.route('POST', '/web/comments**').as('postTimelineComment');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').as('post').within(() => {
            cy.get('[data-test=timeline-comment-message]')
                .type(comment + '{enter}');
            cy.wait('@postTimelineComment').its('status').should('eq', 201);
            cy.get('@post')
                .should('contain', comment);
        });
    });

    it('should be possible to like a non-restricted timeline entry', () => {
        const newPost = 'qa timeline-post for like post test ' + Date.now();
        cy.createTimelinePostAPI(newPost, false).showNewTimelinePosts();

        cy.server();
        cy.route('GET', '/web/like-targets/timeline-item**').as('getLikeTimelineItem');
        cy.route('POST', '/web/like-targets/timeline-item/**').as('postLikeTimelineItem');

        cy.get('[data-test="timeline-stream"]')
            .contains('[data-test="timeline-item"]', newPost).as('post').within(() => {
            cy.wait('@getLikeTimelineItem').its('status').should('eq', 200);
            cy.get('[data-test=like-button]').should('be.visible')
                .click();
            cy.wait('@postLikeTimelineItem').its('status').should('eq', 201);
            cy.get('@post').find('[data-test=like-button]')
                .should('have.attr', 'aria-checked', 'true');
        });

    });

    it('should be possible to unlike a timeline entry', () => {
        const newPost = 'qa timeline-post for unlike post test ' + Date.now();
        cy.createTimelinePostAPI(newPost, false).showNewTimelinePosts();

        cy.server();
        cy.route('GET', '/web/like-targets/timeline-item**').as('getLikeTimelineItem');
        cy.route('POST', '/web/like-targets/timeline-item/**').as('postLikeTimelineItem');
        cy.route('DELETE', '/web/like-targets/timeline-item/**').as('deleteLikeTimelineItem');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').as('post').within(() => {
            cy.wait('@getLikeTimelineItem').its('status').should('eq', 200);
            cy.get('[data-test=like-button]').should('be.visible')
                .click();
            cy.wait('@postLikeTimelineItem').its('status').should('eq', 201);
            cy.get('[data-test=like-button]').click();
            cy.wait('@deleteLikeTimelineItem').its('status').should('eq', 200);
            cy.get('@post').find('[data-test=like-button]')
                .should('have.attr', 'aria-checked', 'false');
        });
    });

    it('should be possible to delete a timeline entry', () => {
        const newPost = 'qa timeline-post for delete test ' + Date.now();
        cy.createTimelinePostAPI(newPost, false).showNewTimelinePosts();

        cy.server();
        cy.route('DELETE', '/web/timeline-items/**').as('deleteTimelineItem');

        cy.get('[data-test=timeline-item]').contains(newPost).parents('[data-test=timeline-item]').within(() => {
            cy.get('[data-test=button-timeline-entry-edit-dropdown]')
                .click()
                .get('[data-test=delete-post-button]')
                .click();
        });
        cy.wait('@deleteTimelineItem').its('status').should('eq', 204);
        cy.get('[data-test=timeline-item]').should('not.contain', newPost);
    });
});
