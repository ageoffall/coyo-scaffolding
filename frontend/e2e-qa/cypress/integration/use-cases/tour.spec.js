describe('Tour check', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should check and click through the tour for the timeline', function () {
    cy.tour('timeline');
  });

  it('should check and click through the tour for pages', function () {
    cy.tour('pages');
  });

  it('should check and click through the tour for workspaces', function () {
    cy.tour('workspaces');
  });

  it('should check and click through the tour for events', function () {
    cy.tour('events');
  });

  it('should check and click through the tour for the search', function () {
    cy.tour('search');
  });

  it('should check and click through the tour for colleagues', function () {
    cy.tour('colleagues');
  });

  it('should check and click through the tour for the profile', function () {
    cy.tour('profile');
  });
});
