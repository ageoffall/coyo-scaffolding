describe('User creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour()
        .then(() => {
          cy.visit('/');
        });
  });

  afterEach(function () {
    cy.logout();
  });

  it('should create a new admin', function () {
    cy.adminAreaNavigation();
    cy.createUser('Admins', 'Admin');
  });

  it('should create a new user', function () {
    cy.adminAreaNavigation();
    cy.createUser('Users', 'User');
  });

  it('should create a new external workspace member', function () {
    cy.adminAreaNavigation();
    cy.createUser('Users', 'External Workspace Member');
  });
});
